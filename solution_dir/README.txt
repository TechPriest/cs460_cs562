project: cs562
author: Beñat Morisset de Pérdigo (b.morisset)
date: 2020/12/15

CONTROLS:
 WASDQE: movement (+SHIFT for x3 speed)
 arrows: for pitch nd yaw
 right-click + drag mouse: for pitch nd yaw
 V:reset cam
 SPACE: cycle through display modes
 K: toggle anti-aliasing
 L: toggle emmisive glow
 O: toggle wireframe
 F5: reload shaders
 CTRL+R: reload scene

WARNINGS:
  + influence volume light optimization is intended to work with lights that have a finite, small radius. So having null attenuation coefficients wil break the effect.

RELEVANT CODES:
 + The relevant scene are final_project.h/cpp and final_project_sponza.h/cpp
 + Each version of the effect are done in final_project.frag, final_project_opti.frag and final_project_opti_2.frag 
 + All opengl calls are in render_actions.cpp

FEATURES:
  + support for many lights
    + point_light optimization (drawing influence spheres)
	  + also use depth from gbuffer and pass when depth is greater
  + performance info
  + render thread
  + differed shading
    + LDR position reconstructing g_buffer (14bytes/pixel + depth)
    + final project g_buffer of 6 bytes/pixel (normals) and depth
  + nice free-cam
  + uniform caching
  + blinn-phong light model
  + anti-aliasing: post processing effect using Sobel edge detection and Gaussian blur
  + Sobel edge detection
  + Gaussian blur
  + Bloom (lerped)
  + hot reload for shaders
  + multiple scenes
  + decals
  + SSAA: just make the g_buffer resolution bigger than the screen's (x2 recommended)
  + Screen Space Horizon Based Ambient Occlusion
  + Screen Space Lines For Conveying Shape