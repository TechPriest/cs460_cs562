/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    application.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:58:20 2020

\brief  Responsible for the renderer_thread and the window.

***************************************************************************/


#include "application.h"

#include <array>
#include <thread>
#include <iostream>
#include <memory>
#include "external/glfw/glfw_api_tokens.h" // to be able to use the defines GL_DEPTH_TEST, GL_CULL_FACE
#include "graphics/window/window.h"
#include "threading/work_thread.h"
#include "actions/render_actions.h"
#include "actions/gui_actions.h"
#include "scene_manager/scene_manager.h"
#include "maths/rng_utils.h"



void application::run()
{
  work_thread rend_thr;
  std::thread renderer(&work_thread::work, &rend_thr);

  //init glfw and hint version and profile
  {
    rend_thr.begin_pushing();
    rend_thr.push_action(std::make_shared<init>());
    rend_thr.push_action(std::make_shared<hint_version>(4, 6)); //3,3 //4,2
    rend_thr.push_action(std::make_shared<hint_profile>(GLFW_OPENGL_CORE_PROFILE));
    rend_thr.end_pushing();
  }

  window the_window("cs562_b.morisset_finalproject", 1600, 900);

  //make window and set swap interval and blending
  {
    rend_thr.begin_pushing();
    ///* only needed if multiple windows */rend_thr.push_action(the_window.get_bind_action());
    rend_thr.push_action(the_window.get_create_action());
    rend_thr.push_action(std::make_shared<set_swap_interval>(0)); //1 = Vsync
    //m_rend_thr.push_action(std::make_shared<enable>(GL_BLEND));
    //rend_thr.push_action(std::make_shared<set_blending>(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));
    rend_thr.push_action(std::make_shared<enable>(GL_DEPTH_TEST));
    rend_thr.push_action(std::make_shared<enable>(GL_CULL_FACE));
    rend_thr.push_action(std::make_shared<print_version>());
    rend_thr.push_action(std::make_shared<gui_init>(the_window.get_glfw_win()));
    rend_thr.end_pushing();
  }

  random_seed();

  scene_manager scene_mngr;
  scene_mngr.initialize(scene_manager::scene_enum::parenting_demo, the_window, rend_thr);
  scene_mngr.run(the_window, rend_thr);
  scene_mngr.shutdown();

  //destroy window
  {
    rend_thr.begin_pushing();
    rend_thr.push_action(std::make_shared<gui_shutdown>());
    rend_thr.push_action(the_window.get_delete_action());
    rend_thr.end_pushing();
  }

  rend_thr.close();

  renderer.join();
}
