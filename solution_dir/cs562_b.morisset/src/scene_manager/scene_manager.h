/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    scene_manager.h

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu

\par    DigiPen login: b.morisset

\par    Course: CS562

\par    Assignment #3

\date    Tue Nov 10 2020

\brief  Responsible for correctly switching between scenes

***************************************************************************/

#pragma once

#include <array>
#include <memory> //std::shared_ptr, std::unique_ptr

class scene;
class window;
class work_thread;

class scene_manager
{
public:
  enum class scene_enum : unsigned char
    {simple, sponza, phong_tess, decal, simple_decal, simple_occlusion, occlusion,
      final_project, final_project_sponza, parenting_demo, SIZE};

  void initialize(scene_enum start_scene, window& win, work_thread& render_thread);
  void run(window& win, work_thread& render_thread);
  void shutdown();

  void change_scene(scene_enum new_scene);
  void close();
  void restart_scene();

  inline scene_enum get_current_scene_id() const
  {
    return m_current_scene;
  }

private:
  std::array<std::shared_ptr<scene>, static_cast<size_t>(scene_enum::SIZE)> m_scenes;
  scene_enum m_current_scene;
  scene_enum m_next_scene;

  inline std::weak_ptr<scene> get_scene(scene_enum scene_id)
  {
    return m_scenes[static_cast<size_t>(scene_id)];
  }
};