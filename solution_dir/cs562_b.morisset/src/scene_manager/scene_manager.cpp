/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    scene_manager.cpp

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu

\par    DigiPen login: b.morisset

\par    Course: CS562

\par    Assignment #1

\date    Tue Nov 10 2020

\brief  Responsible for correctly switching between scenes

***************************************************************************/

#include "scene_manager.h"

#include "scenes/simple_scene/simple_scene.h"
#include "scenes/sponza_scene/sponza_scene.h"
#include "scenes/phong_tess_scene/phong_tess_scene.h"
#include "scenes/decal_scene/decal_scene.h"
#include "scenes/simple_decal_scene/simple_decal_scene.h"
#include "scenes/simple_occlusion_scene/simple_occlusion_scene.h"
#include "scenes/occlusion_scene/occlusion_scene.h"
#include "scenes/final_project/final_project.h"
#include "scenes/final_project_sponza/final_project_sponza.h"
#include "scenes/parenting_scene/parenting_scene.h"
#include "other/splash_screen.h"


void scene_manager::initialize(scene_enum start_scene, window& win, work_thread& render_thread)
{
  m_scenes[static_cast<size_t>(scene_enum::simple)] =
    std::make_shared<simple_scene>(win, render_thread, *this);
  m_scenes[static_cast<size_t>(scene_enum::sponza)] =
    std::make_shared <sponza_scene>(win, render_thread, *this);
  m_scenes[static_cast<size_t>(scene_enum::phong_tess)] =
    std::make_shared <phong_tess_scene>(win, render_thread, *this);
  m_scenes[static_cast<size_t>(scene_enum::decal)] =
    std::make_shared <decal_scene>(win, render_thread, *this);
  m_scenes[static_cast<size_t>(scene_enum::simple_decal)] =
    std::make_shared <simple_decal_scene>(win, render_thread, *this);
  m_scenes[static_cast<size_t>(scene_enum::simple_occlusion)] =
    std::make_shared <simple_occlusion_scene>(win, render_thread, *this);
  m_scenes[static_cast<size_t>(scene_enum::occlusion)] =
    std::make_shared <occlusion_scene>(win, render_thread, *this);
  m_scenes[static_cast<size_t>(scene_enum::final_project)] =
    std::make_shared <final_project>(win, render_thread, *this);
  m_scenes[static_cast<size_t>(scene_enum::final_project_sponza)] =
    std::make_shared <final_project_sponza>(win, render_thread, *this);
  m_scenes[static_cast<size_t>(scene_enum::final_project_sponza)] =
    std::make_shared <final_project_sponza>(win, render_thread, *this);
  m_scenes[static_cast<size_t>(scene_enum::parenting_demo)] =
    std::make_shared <parenting_scene>(win, render_thread, *this);

  m_current_scene = scene_enum::SIZE;
  m_next_scene = start_scene;
}

void scene_manager::run(window& win, work_thread& render_thread)
{
  while (m_next_scene != scene_enum::SIZE)
  {
    m_current_scene = m_next_scene;
    m_next_scene = scene_enum::SIZE; //this way is change_scene is not called, we will shutdown upon scene end

    splash_screen::draw(&render_thread, win.get_glfw_win());

    win.m_lvl = get_scene(m_current_scene);
    get_scene(m_current_scene).lock()->load();
    get_scene(m_current_scene).lock()->init();
    get_scene(m_current_scene).lock()->run();
    get_scene(m_current_scene).lock()->unload();
  }
}

void scene_manager::shutdown()
{
}

void scene_manager::change_scene(scene_enum new_scene)
{
  m_next_scene = new_scene;
}

void scene_manager::close()
{
  change_scene(scene_enum::SIZE);
}

void scene_manager::restart_scene()
{
  change_scene(m_current_scene);
}
