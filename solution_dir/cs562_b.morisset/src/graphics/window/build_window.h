/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    build_window.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:57:20 2020

\brief  Creates a window using glfw



***************************************************************************/


#pragma once

#include <vector>
#include <string>

//forward declaration:
struct GLFWwindow; 
class window;

class build_window
{
public:
  static bool build(GLFWwindow** glfw_win_ptr, window* win,
    const std::string& name, int width, int height);
   
};


