/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    build_window.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:57:13 2020

\brief  Creates a window using glfw



***************************************************************************/



#include "build_window.h"

#include <iostream> //std::cerr on error callback
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "graphics/debug/GL_CHECK.h"
#include "window.h"
#include "input/input.h"
#include "nodes/camera_node.h"
#include "scenes/scene.h"

bool build_window::build(GLFWwindow ** glfw_win_ptr, window * win,
  const std::string & name, int width, int height)
{
  //window not resizable
  glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

  //to make it borderless
  //glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);

  //set the error callback function
  {
    auto error_callback = [](int error, const char* description)
    {
      std::cerr << "WINDOW ERROR: " <<error<<"("<< description<<")" << std::endl;
    };
    glfwSetErrorCallback(error_callback);
  }

  //create window
  *glfw_win_ptr = glfwCreateWindow(width, height, name.c_str(), NULL, NULL);
  if (!*glfw_win_ptr)
    return false;

  //send object as user pointer to be able to handle it in callbacks
  glfwSetWindowUserPointer(*glfw_win_ptr, win);

  //bind window
  glfwMakeContextCurrent(*glfw_win_ptr);

  //
  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    return false;

  //may be placed inside the loop if we want multiple viewports
  GL_CHECK(glViewport(0, 0, width, height));

  //set resize callback fn
  {
    auto resize_callback = [](GLFWwindow* w, int width, int height)
    {
      window* win_obj = static_cast<window*>(glfwGetWindowUserPointer(w));

      //bind
      glfwMakeContextCurrent(w);

      win_obj->m_width = width;
      win_obj->m_height = height;
      GL_CHECK(glViewport(0, 0, width, height));

      for (unsigned cam_idx : win_obj->m_cam_indices)
      {
        std::shared_ptr<camera_node> cam =
          std::static_pointer_cast<camera_node>((*(win_obj->m_lvl.lock()->get_nodes_ptr()))[cam_idx]);

        cam->set_viewport(width, height);
      }
    };

    glfwSetFramebufferSizeCallback(*glfw_win_ptr, resize_callback);
  }

//input initialization
  {
    auto call_back_fn_keys =
      [](GLFWwindow* win, int key, int, int action, int)
    {
      window* win_obj = static_cast<window*>(glfwGetWindowUserPointer(win));

      input::get_instance().Key_callback(*(win_obj->get_glfw_win()), key, 0, action, 0);
    };

    glfwSetKeyCallback(*glfw_win_ptr, call_back_fn_keys);

    auto call_back_fn_mouse =
      [](GLFWwindow* win, double xpos, double ypos)
    {input::get_instance().Cursor_position_callback(win, xpos, ypos); };

    glfwSetCursorPosCallback(*glfw_win_ptr, call_back_fn_mouse);

    auto call_back_fn_mouse_buttons =
      [](GLFWwindow* win, int button, int action, int mods)
    {input::get_instance().Mouse_button_callback(win, button, action, mods); };

    glfwSetMouseButtonCallback(*glfw_win_ptr, call_back_fn_mouse_buttons);
  }

  return true;
}

