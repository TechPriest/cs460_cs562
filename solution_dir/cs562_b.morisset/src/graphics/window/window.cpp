/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    window.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:57:25 2020

\brief  window object.

possible improvements:
  -multiple windows working
    -have the option of sharing openGL context: https://www.glfw.org/docs/latest/context_guide.html)
  -multiple viewports in a window (abstract glViewport away)
  -the window should not know about the level

***************************************************************************/

#include <iostream>

#include <cassert>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <functional>
#include <chrono>
#include <thread>

#include "window.h"


window::window(const std::string & name, int width, int height)
  : m_name(name), m_width(width), m_height(height)
{}

/**
* destructor, prints a warning if shader_program hasn't been deleted
*/
window::~window()
{
  if (m_open)
    std::cerr << "WARNING: window \"" << m_name << "\" hasn't been closed" << std::endl;
}

bool window::should_close() const
{
  return glfwWindowShouldClose(m_window);
}

void window::wait_for_window_creation() const
{
  while (m_window == nullptr)
  {
    using namespace std::chrono_literals;
    std::this_thread::sleep_for(5ms);
  }
}

