/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    window.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:57:30 2020

\brief  window object.

possible improvements:
  -multiple windows working
    -have the option of sharing openGL context: https://www.glfw.org/docs/latest/context_guide.html)
  -multiple viewports in a window (abstract glViewport away)
  -the window should not know about the level


***************************************************************************/

#pragma once

#include <vector>
#include <string>
#include <memory> //std::make_shared
#include "actions/render_actions.h"

struct GLFWwindow; //forward declaration
class scene;

class window
{
  ///DATA
public:
  std::string m_name;
  int m_width = 800;
  int m_height = 600;
  std::vector<unsigned> m_cam_indices; //indices in the nodes of the level
  std::weak_ptr<scene> m_lvl;

private:
  GLFWwindow* m_window = nullptr;			//The window we'll be rendering to
  bool m_open = false; //in truth this variable tells us if the get_create_action was called, not if create_window.do_action() was done.
  bool m_vsync = false; //no real reason why it is in window. It is just convenient for this project

  ///FUNCTIONS
public:
  window(const std::string& name, int width, int height); //thread safe
  ~window();

  inline std::shared_ptr<create_window> get_create_action()
  {
    m_open = true;
    return std::make_shared<create_window>(&m_window, this, m_name, m_width, m_height);
  }

  inline std::shared_ptr<delete_window> get_delete_action()
  {
    m_open = false;
    return std::make_shared<delete_window>(&m_window);
  }

  inline std::shared_ptr<bind_window> get_bind_action()
  {
    return std::make_shared<bind_window>(&m_window);
  }

  inline std::shared_ptr<set_window_resolution> get_resize_action(int width, int height)
  {
    return std::make_shared<set_window_resolution>(this, width, height);
  }

  inline std::shared_ptr<set_viewport> get_set_viewport_action()
  {
    return std::make_shared<set_viewport>(m_width, m_height);
  }

  inline std::shared_ptr<set_swap_interval> get_set_vsync_action(bool vsync)
  {
    assert(m_vsync != vsync);
    m_vsync = vsync;

    return std::make_shared<set_swap_interval>(vsync ? 1 : 0);
  }

  inline bool get_vsync() const
  {
    return m_vsync;
  }


  inline GLFWwindow** get_glfw_win() { return &m_window; }
  bool should_close() const; //thread safe
  void wait_for_window_creation() const; //blocking
};

