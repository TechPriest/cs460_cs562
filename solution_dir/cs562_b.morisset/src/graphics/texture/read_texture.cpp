/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    read_texture.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:55:36 2020

\brief  Loads a texture into the CPU. Uses stb_image



***************************************************************************/


#include "read_texture.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

unsigned char * read_texture::read(const std::string & path, int * width, int * height, int * BPP)
{
  /* this is needed because PNGs have the origin at the top-left
  and openGL et the bot-left */
  stbi_set_flip_vertically_on_load(1);

  return stbi_load(path.c_str(), width, height, BPP, 4);
}

