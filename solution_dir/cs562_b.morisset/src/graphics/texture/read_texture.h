/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    read_texture.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:55:44 2020

\brief  Loads a texture into the CPU. Uses stb_image



***************************************************************************/


#pragma once

#include <string>

class read_texture
{
public:
  static unsigned char* read(const std::string& path, int* width, int* height, int* BPP);
};


