/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    texture.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:55:51 2020

\brief  Texture object that manages correct freing



***************************************************************************/

#include "texture.h"

#include <iostream> //std::cerr in destructor


bool texture::operator==(const std::string & str) const
{
  return m_path == str;
}

texture::texture(const std::string & path, int wrap_mode /*= GL_REPEAT*/)
  : m_path(path), m_wrap_mode(wrap_mode)
{}

texture::~texture()
{
  if (m_in_GPU)
    std::cerr << "WARNING: texture " << m_path << " hasn't been deleted from the GPU" << std::endl;
}

std::shared_ptr<set_texture_slot> texture::get_set_active_slot_action(unsigned slot)
{
  return std::make_shared<set_texture_slot>(slot);
}

