/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    texture3D.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:55:56 2020

\brief  class for 3D textures. It derives from texture class. Used in LUT.

***************************************************************************/

#pragma once

#include <string>
#include <memory>
#include "texture.h"

class texture3D : public texture
{
private: //data
  int m_depth = 0;

public: //public interface

  inline texture3D(const std::string& path, int wrap_mode = GL_CLAMP_TO_EDGE )
    : texture::texture(path, wrap_mode)
  {}

  inline std::shared_ptr<action> get_load_action() override
  {
    return std::make_shared<load_texture3D>(m_path, &m_width, &m_height, &m_depth, &m_BPP, &m_data);
  }

  inline std::shared_ptr<action> get_create_action() override
  {
    m_in_GPU = true;
    return std::make_shared<create_texture_3D>(&m_handle, m_data, m_width, m_height, m_depth, m_wrap_mode);
  }

  inline std::shared_ptr<bind_texture> get_bind_action() const override
  {
    return std::make_shared<bind_texture>(&m_handle, GL_TEXTURE_3D);
  }
};

