/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    texture.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:55:56 2020

\brief  Texture object that manages correct freing



***************************************************************************/

#pragma once

#include <string>
#include <memory>
#include "actions/action.h"
#include "actions/render_actions.h"
#include "actions/load_actions.h"

class texture
{
protected: //data

  unsigned m_handle = 0u;//openGL handle for the texture

  const std::string m_path = "";
  int m_wrap_mode = 0;
  int m_width = 0;
  int m_height = 0;
  int m_BPP = 0; //bytes per pixel
  unsigned char* m_data = nullptr;
  bool m_in_GPU = false; //in truth this variable tells us if the get_create_action was called, not if create_texture.do_action() was done.

public: //public interface


  bool operator== (const std::string& str) const;

  texture(const std::string& path, int wrap_mode = GL_REPEAT );
  virtual ~texture();

  virtual inline std::shared_ptr<action> get_load_action()
  {
    return std::make_shared<load_texture>(m_path, &m_width, &m_height, &m_BPP, &m_data);
  }

  virtual inline std::shared_ptr<action> get_create_action()
  {
    m_in_GPU = true;
    return std::make_shared<create_texture_2D>(&m_handle, m_data, m_width, m_height, m_wrap_mode, GL_LINEAR_MIPMAP_LINEAR);
  }

  virtual inline std::shared_ptr<delete_texture> get_delete_action()
  {
    m_in_GPU = false;
    return std::make_shared<delete_texture>(&m_handle);
  }

  virtual inline std::shared_ptr<bind_texture> get_bind_action() const
  {
    return std::make_shared<bind_texture>(&m_handle, GL_TEXTURE_2D);
  }

  virtual inline unsigned get_handle() const { return m_handle; }
  virtual inline const unsigned* get_handle_ptr() const { return &m_handle; }


  static std::shared_ptr<set_texture_slot> get_set_active_slot_action(unsigned slot);
};
