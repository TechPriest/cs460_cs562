/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    build_shader.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:54:11 2020

\brief  Functions to create the shader program in the GPU.



***************************************************************************/


#pragma once

#include <string>

class build_shader
{
public:
  static unsigned build(const std::string& vert, const std::string& frag,
    const std::string& geom = "", const std::string& tesc = "", const std::string& tese = "");
};


