/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    build_shader.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:54:04 2020

\brief  Functions to create the shader program in the GPU.



***************************************************************************/


#include "build_shader.h"

#include <iostream>
#include <glad/glad.h>
#include "graphics/debug/GL_CHECK.h"

#ifdef _DEBUG
#define SHADER_CHECK(handle, shdrname) CheckShaderError(handle, /*__FILE__, __LINE__,*/ shdrname)
#else
#define SHADER_CHECK(handle, shdrname) 
#endif

#ifdef _DEBUG
#define SHADERPROG_CHECK(handle) CheckShaderProgError(handle/*, __FILE__, __LINE__*/)
#else
#define SHADERPROG_CHECK(handle) 
#endif

namespace
{
  /**
  * Checks for shader compiling errors
  * @param ShdrHandle, handle of the shader
  * @param shdrname, name of the shader
  */
  void CheckShaderError(unsigned ShdrHandle, const char* shdrname)
  {
    int success;
    GL_CHECK(glGetShaderiv(ShdrHandle, GL_COMPILE_STATUS, &success));
    if (success)
      return;

    int len;
    GL_CHECK(glGetShaderiv(ShdrHandle, GL_INFO_LOG_LENGTH, &len));
    char* infoLog = (char*)alloca(len * sizeof(char));
    GL_CHECK(glGetShaderInfoLog(ShdrHandle, 512, NULL, infoLog));
    std::cout << "\nERROR::SHADER::COMPILATION_FAILED " << shdrname << " \n" << infoLog << std::endl;

    GL_CHECK(glDeleteShader(ShdrHandle));
  }

  /**
  * Checks for shader linking errors
  * @param shdrProgHandle, handle of the program shader
  */
  void CheckShaderProgError(unsigned shdrProgHandle)
  {
    int success;
    GL_CHECK(glGetProgramiv(shdrProgHandle, GL_LINK_STATUS, &success));
    if (success)
      return;

    //print shader error
    int len;
    GL_CHECK(glGetProgramiv(shdrProgHandle, GL_INFO_LOG_LENGTH, &len));
    char* infoLog = (char*)alloca(len * sizeof(char));
    GL_CHECK(glGetProgramInfoLog(shdrProgHandle, 512, NULL, infoLog));
    std::cout << "ERROR::SHADER_PROGRAM:LINKING_FAILED\n" << infoLog << std::endl;

    GL_CHECK(glDeleteProgram(shdrProgHandle));
  }
}

unsigned build_shader::build(const std::string & vert, const std::string & frag,
  const std::string & geom /*= ""*/, const std::string& tesc /*= ""*/, const std::string& tese /*= ""*/)
{
  unsigned output = glCreateProgram();

  //Vertex shader
  unsigned vtxShdrHandle = glCreateShader(GL_VERTEX_SHADER);
  const char* vertCstr = vert.c_str();
  GL_CHECK(glShaderSource(vtxShdrHandle, 1, &vertCstr, NULL)); //only takes one argument: positions
  glCompileShader(vtxShdrHandle);
  SHADER_CHECK(vtxShdrHandle, "Vert"); //CheckCompilation

  //Fragment shader
  unsigned fragShdrHandle = glCreateShader(GL_FRAGMENT_SHADER);
  const char* fragCstr = frag.c_str();
  GL_CHECK(glShaderSource(fragShdrHandle, 1, &fragCstr, NULL));
  glCompileShader(fragShdrHandle);
  SHADER_CHECK(fragShdrHandle, "Frag"); //CheckCompilation

  //Geometry shader
  unsigned geoShdrHandle = 0u;
  if (geom != "")
  {
    geoShdrHandle = glCreateShader(GL_GEOMETRY_SHADER);
    const char* geoCstr = geom.c_str();
    GL_CHECK(glShaderSource(geoShdrHandle, 1, &geoCstr, NULL));
    glCompileShader(geoShdrHandle);
    SHADER_CHECK(geoShdrHandle, "Geo"); //CheckCompilation
  }

  //Tesselation Control Shader
  unsigned tescShdrHandle = 0u;
  if (tesc != "")
  {
    tescShdrHandle = glCreateShader(GL_TESS_CONTROL_SHADER);
    const char* tescCstr = tesc.c_str();
    GL_CHECK(glShaderSource(tescShdrHandle, 1, &tescCstr, NULL));
    glCompileShader(tescShdrHandle);
    SHADER_CHECK(tescShdrHandle, "Tesc"); //CheckCompilation
  }

  //Tesselation Evaluation Shader
  unsigned teseShdrHandle = 0u;
  if (tese != "")
  {
    teseShdrHandle = glCreateShader(GL_TESS_EVALUATION_SHADER);
    const char* teseCstr = tese.c_str();
    GL_CHECK(glShaderSource(teseShdrHandle, 1, &teseCstr, NULL));
    glCompileShader(teseShdrHandle);
    SHADER_CHECK(teseShdrHandle, "Tese"); //CheckCompilation
  }

  //shader Program
  GL_CHECK(glAttachShader(output, vtxShdrHandle));
  GL_CHECK(glAttachShader(output, fragShdrHandle));
  if (geom != "")
    GL_CHECK(glAttachShader(output, geoShdrHandle));
  if (tesc != "")
    GL_CHECK(glAttachShader(output, tescShdrHandle));
  if (tese != "")
    GL_CHECK(glAttachShader(output, teseShdrHandle));
  glLinkProgram(output);
  SHADERPROG_CHECK(output); //check linking

  //Delete Frag and Vert (also geo, tesc and tese if they exists)
  GL_CHECK(glDeleteShader(vtxShdrHandle));
  GL_CHECK(glDeleteShader(fragShdrHandle));
  if (geom != "")
    GL_CHECK(glDeleteShader(geoShdrHandle));
  if (tesc != "")
    GL_CHECK(glDeleteShader(tescShdrHandle));
  if (tese != "")
    GL_CHECK(glDeleteShader(teseShdrHandle));

  return output;
}

