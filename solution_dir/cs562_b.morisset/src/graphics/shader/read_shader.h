/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    read_shader.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:54:28 2020

\brief  reads the shader file and stores the code in memory



***************************************************************************/


#pragma once

#include <string>

class read_shader
{
public:
  static void read(const std::string& path, std::string* vert, std::string* frag,
    std::string* geo, std::string* tesc, std::string* tese);
};


