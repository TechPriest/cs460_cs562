/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    shader.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:54:42 2020

\brief  shader object that manages correct freing and uniform setting



***************************************************************************/


#pragma once

/*
  improvements: when reading shader files remember locations. Store them in a hash_map. Use enums as a key

*/



#include <string>
#include <glm/glm.hpp> //for uniforms
#include <memory> //std::make_shared
#include <unordered_map>
#include <queue> //for get_actions (plural)
#include "actions/action.h"
#include "actions/render_actions.h"
#include "actions/load_actions.h"


class material;
class point_light_node;

class shader
{
private:
  unsigned m_handle = 0u; //openGL handle of the shader program
  const std::string m_path = "";
  std::string m_vert_code = "";
  std::string m_frag_code = "";
  std::string m_geom_code = "";
  std::string m_tesc_code = "";
  std::string m_tese_code = "";
  int m_uni_loc = -2; //will store the uniform location for the currently executed set uniform action
  std::unordered_map<std::string, int> m_location_cache;
  bool m_in_GPU = false; //in truth this variable tells us if the get_create_action was called, not if create_shader.do_action() was done.

public:
  shader(const std::string& path);
  ~shader();

  inline std::shared_ptr<load_shader> get_load_action()
  {
    return std::make_shared<load_shader>(
      m_path, &m_vert_code, &m_frag_code, &m_geom_code, &m_tesc_code, &m_tese_code);
  }

  inline std::shared_ptr<create_shader> get_create_action()
  {
    m_in_GPU = true;
    return std::make_shared<create_shader>(
      &m_handle, m_vert_code, m_frag_code, m_geom_code, m_tesc_code, m_tese_code);
  }

  inline std::shared_ptr<delete_shader> get_delete_action()
  {
    m_in_GPU = false;
    return std::make_shared<delete_shader>(&m_handle);
  }

  inline std::shared_ptr<clear_shader_loc_cache> get_clear_cache_action()
  {
    return std::make_shared<clear_shader_loc_cache>(&m_location_cache);
  }

  inline std::shared_ptr<bind_shader> get_bind_action()
  {
    return std::make_shared<bind_shader>(&m_handle);
  }

  //uniforms:
  //    no_caching
  //void SetBool(const char* varName, bool value) const;
  //void SetInt(const char* varName, int value) const;
  //void SetUint(const char * varName, unsigned value) const;
  //void SetFloat(const char* varName, float value) const;
  //void SetVec2(const char* varName, const glm::vec2& value) const;
  //void SetVec3(const char* varName, const glm::vec3& value) const;
  std::queue<std::shared_ptr<action>> get_set_uniform_vec4_actions(const std::string& uni_name, glm::vec4 value);
  std::queue<std::shared_ptr<action>> get_set_uniform_mat4_actions(const std::string& uni_name, glm::mat4 value);
  std::queue<std::shared_ptr<action>> get_set_uniform_texture_actions(const std::string& uni_name, int texture_slot);
  std::queue<std::shared_ptr<action>> get_set_material_uniforms_actions(const material& mat,const std::vector<texture>& textures);
  std::queue<std::shared_ptr<action>> get_set_material_for_decal_uniforms_actions(const material& mat,
    unsigned diff_tex_idx, unsigned norm_tex_idx, std::vector<texture>& textures);
  std::queue<std::shared_ptr<action>> get_set_light_uniforms_actions(std::shared_ptr<point_light_node> li);


  //    caching
  //void SetBool(const char* varName, bool value) const;
  inline std::shared_ptr<set_uniform_int_caching> get_set_uniform_bool_caching(const std::string& uni_name, int value)
  {
    return std::make_shared<set_uniform_int_caching>(&m_location_cache, &m_handle, uni_name, (value ? 1 : 0) );
  }
  inline std::shared_ptr<set_uniform_int_caching> get_set_uniform_int_caching(const std::string& uni_name, int value)
  {
    return std::make_shared<set_uniform_int_caching>(&m_location_cache, &m_handle, uni_name, value);
  }
  //void SetUint(const char * varName, unsigned value) const;
  inline std::shared_ptr<set_uniform_float_caching> get_set_uniform_float_caching(const std::string& uni_name, float value)
  {
    return std::make_shared<set_uniform_float_caching>(&m_location_cache, &m_handle, uni_name, value);
  }
  inline std::shared_ptr<set_uniform_vec2_caching> get_set_uniform_vec2_caching(const std::string& uni_name, glm::vec2 value)
  {
    return std::make_shared<set_uniform_vec2_caching>(&m_location_cache, &m_handle, uni_name, value);
  }
  inline std::shared_ptr<set_uniform_vec3_caching> get_set_uniform_vec3_caching(const std::string& uni_name, glm::vec3 value)
  {
    return std::make_shared<set_uniform_vec3_caching>(&m_location_cache, &m_handle, uni_name, value);
  }
  inline std::shared_ptr<set_uniform_vec4_caching> get_set_uniform_vec4_caching(const std::string& uni_name, glm::vec4 value)
  {
    return std::make_shared<set_uniform_vec4_caching>(&m_location_cache, &m_handle, uni_name, value);
  }
  inline std::shared_ptr<set_uniform_mat4_caching> get_set_uniform_mat4_caching(const std::string& uni_name, glm::mat4 value)
  {
    return std::make_shared<set_uniform_mat4_caching>(&m_location_cache, &m_handle, uni_name, value);
  }
  inline std::shared_ptr<set_uniform_texture_caching> get_set_uniform_texture_caching(const std::string& uni_name, unsigned slot)
  {
    return std::make_shared<set_uniform_texture_caching>(&m_location_cache, &m_handle, uni_name, slot);
  }
  inline std::shared_ptr<set_uniform_int_array_caching> get_set_uniform_int_array_caching(const std::string& uni_name, const int* array, unsigned count)
  {
    return std::make_shared<set_uniform_int_array_caching>(&m_location_cache, &m_handle, uni_name, array, count);
  }
  inline std::shared_ptr<set_uniform_float_array_caching> get_set_uniform_float_array_caching(const std::string& uni_name, const float* array, unsigned count)
  {
    return std::make_shared<set_uniform_float_array_caching>(&m_location_cache, &m_handle, uni_name, array, count);
  }
  inline std::shared_ptr<set_uniform_vec2_array_caching> get_set_uniform_vec_array_caching(const std::string& uni_name, const glm::vec2* array, unsigned count)
  {
    return std::make_shared<set_uniform_vec2_array_caching>(&m_location_cache, &m_handle, uni_name, array, count);
  }
};


