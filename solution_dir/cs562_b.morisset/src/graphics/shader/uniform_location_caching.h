/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    uniform_location_caching.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:55:07 2020

\brief  Contains functions that use the passed cache to retrieve uniform locations.
The idea is that finding a uniform location on the cache is much faster that quering
it from the gpu.



***************************************************************************/


#pragma once

#include <unordered_map> //uniform caching

class uniform_location_caching
{
public:
  static int get_uni_location(std::unordered_map<std::string, int>* loc_cache,
    const unsigned* shdr_handle, const std::string& uni_name);
};


