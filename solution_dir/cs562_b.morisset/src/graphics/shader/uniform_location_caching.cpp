/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    uniform_location_caching.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:54:56 2020

\brief  Contains functions that use the passed cache to retrieve uniform locations.
The idea is that finding a uniform location on the cache is much faster that quering 
it from the gpu.



***************************************************************************/


#include "uniform_location_caching.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "graphics/debug/GL_CHECK.h"

/* tries to find the uniform location from the cahe(CPU),
if it cant, it finds it from the shader(GPU) */
int uniform_location_caching::get_uni_location(std::unordered_map<std::string, int>* loc_cache, const unsigned * shdr_handle, const std::string & uni_name)
{
  int output = -1;

  std::unordered_map<std::string, int>::const_iterator found =
    loc_cache->find(uni_name);

  if (found == loc_cache->cend())
  {
    GL_CHECK(output = glGetUniformLocation(*shdr_handle, uni_name.c_str()));
    (*loc_cache)[uni_name] = output;
  }
  else
    output = found->second;

  //printing done in calling function
  //if (output == -1)
  //  std::cout << "WARNING: uniform " << uni_name <<
  //  " was not found in shader " << *shdr_handle << std::endl;

  return output;
}
