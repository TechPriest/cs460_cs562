/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    shader.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:54:33 2020

\brief  shader object that manages correct freing and uniform setting



***************************************************************************/


#include "shader.h"

#include <iostream> //std::cerr
#include "graphics/material/material.h"
#include "graphics/texture/texture.h"
#include "nodes/point_light_node.h"

/**
* constructor. Stores the .shdr path
* @param path
*/
shader::shader(const std::string & path)
  : m_path(path)
{}


/**
* destructor, prints a warning if shader_program hasn't been deleted
*/
shader::~shader()
{
  if (m_in_GPU)
    std::cerr << "WARNING: shader program " << m_path << " hasn't been deleted from the GPU" << std::endl;
}

std::queue<std::shared_ptr<action>> shader::get_set_uniform_vec4_actions(const std::string& uni_name, glm::vec4 value)
{
  std::queue<std::shared_ptr<action>> output;
  
  output.push(std::make_shared<get_uniform_location>(&m_uni_loc, &m_handle, uni_name));
  output.push(std::make_shared<set_uniform_vec4>(&m_uni_loc, value));

  return output;
}

std::queue<std::shared_ptr<action>> shader::get_set_uniform_mat4_actions(const std::string & uni_name, glm::mat4 value)
{
  std::queue<std::shared_ptr<action>> output;

  output.push(std::make_shared<get_uniform_location>(&m_uni_loc, &m_handle, uni_name));
  output.push(std::make_shared<set_uniform_mat4>(&m_uni_loc, value));

  return output;
}

std::queue<std::shared_ptr<action>> shader::get_set_uniform_texture_actions(const std::string & uni_name, int texture_slot)
{
  std::queue<std::shared_ptr<action>> output;

  output.push(std::make_shared<get_uniform_location>(&m_uni_loc, &m_handle, uni_name));
  output.push(std::make_shared<set_uniform_texture>(&m_uni_loc, texture_slot));

  return output;
}

std::queue<std::shared_ptr<action>> shader::get_set_material_uniforms_actions(const material & mat,
  const std::vector<texture>& textures)
{
  std::queue<std::shared_ptr<action>> output;

  output.push(std::make_shared<set_uniform_vec3_caching>(
    &m_location_cache, &m_handle, "u_Kd", mat.m_color_d));
  output.push(std::make_shared<set_uniform_vec3_caching>(
    &m_location_cache, &m_handle, "u_Ks", mat.m_color_s));
  output.push(std::make_shared<set_uniform_vec3_caching>(
    &m_location_cache, &m_handle, "u_Ke", mat.m_color_e));
  output.push(std::make_shared<set_uniform_float_caching>(
    &m_location_cache, &m_handle, "u_ns", mat.m_shininess));

  //the following lines could be made in a loop
  const texture& tex_d = 
    textures[mat.m_texture_ids[e_tex_types::diffuse]];
  const texture& tex_s = 
    textures[mat.m_texture_ids[e_tex_types::specular]];
  const texture& tex_e =
    textures[mat.m_texture_ids[e_tex_types::emissive]];
  const texture& tex_n = 
    textures[mat.m_texture_ids[e_tex_types::normal]];

  output.push(texture::get_set_active_slot_action(0));
  output.push(tex_d.get_bind_action());
  output.push(texture::get_set_active_slot_action(1));
  output.push(tex_s.get_bind_action());
  output.push(texture::get_set_active_slot_action(2));
  output.push(tex_e.get_bind_action());
  output.push(texture::get_set_active_slot_action(3));
  output.push(tex_n.get_bind_action());

  output.push(std::make_shared<set_uniform_texture_caching>(
    &m_location_cache, &m_handle, "u_texture_d", 0));
  output.push(std::make_shared<set_uniform_texture_caching>(
    &m_location_cache, &m_handle, "u_texture_s", 1));
  output.push(std::make_shared<set_uniform_texture_caching>(
    &m_location_cache, &m_handle, "u_texture_e", 2));
  output.push(std::make_shared<set_uniform_texture_caching>(
    &m_location_cache, &m_handle, "u_texture_n", 3));

  return output;
}

std::queue<std::shared_ptr<action>> shader::get_set_material_for_decal_uniforms_actions(
  const material& mat, unsigned diff_tex_idx, unsigned norm_tex_idx, std::vector<texture>& textures)
{
  std::queue<std::shared_ptr<action>> output;

  //the following lines could be made in a loop
  const texture& tex_d =
    textures[diff_tex_idx];
  const texture& tex_n =
    textures[norm_tex_idx];

  output.push(texture::get_set_active_slot_action(1));
  output.push(tex_d.get_bind_action());
  output.push(texture::get_set_active_slot_action(2));
  output.push(tex_n.get_bind_action());

  output.push(std::make_shared<set_uniform_texture_caching>(
    &m_location_cache, &m_handle, "u_texture_d", 1));
  output.push(std::make_shared<set_uniform_texture_caching>(
    &m_location_cache, &m_handle, "u_texture_n", 2));

  return output;
}

std::queue<std::shared_ptr<action>> shader::get_set_light_uniforms_actions(std::shared_ptr<point_light_node> li)
{
    std::queue<std::shared_ptr<action>> output;

    output.push(get_set_uniform_vec3_caching("u_LightPos", glm::vec3(li->get_world_transform().m_pos)));
    output.push(get_set_uniform_vec3_caching("u_Ia", li->m_ia));
    output.push(get_set_uniform_vec3_caching("u_Id", li->m_id));
    output.push(get_set_uniform_vec3_caching("u_Is", li->m_is));
    output.push(get_set_uniform_vec3_caching("u_attenuation", li->m_attenuation));

    return output;
}

