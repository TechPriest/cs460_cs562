/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    read_shader.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:54:20 2020

\brief  reads the shader file and stores the code in memory



***************************************************************************/


#include "read_shader.h"

#include <fstream>
#include <sstream>
#include <cassert>

namespace
{
  /**
  * Loads shader code into memory
  * @param path,
  */
  std::string LoadFile(const std::string& path)
  {
    std::ifstream File;

    // open files
    File.open(path);
    assert(File);

    // read file's buffer contents into streams
    std::stringstream sstream;
    sstream << File.rdbuf();
    // close file handlers
    File.close();
    // convert stream into c_string
    return sstream.str();
  }

  bool get_optional_shader_path(std::ifstream& ifs, std::string* geo_path,
    std::string* tesc_path, std::string* tese_path)
  {
    std::string path;

    ifs >> path;

    switch (path.back())
    {
    case 'o': //.geo
      *geo_path = path;
      break;
    case 'c': //.tesc
      *tesc_path = path;
      break;
    case 'e': //.tese
      *tese_path = path;
      break;
    default:
      return false;
    }

    return true;
  }
}

void read_shader::read(const std::string& path, std::string* vert, std::string* frag,
  std::string* geo, std::string* tesc, std::string* tese)
{
  std::string vert_path = "", frag_path = "", geo_path = "", tesc_path = "", tese_path = "";

  //read paths
  {
    std::ifstream file;
  
    // open file
    file.open(path);
    assert(file);
  
    assert(!file.eof());
    file >> vert_path;
    assert(!file.eof());
    file >> frag_path;

    while (!file.eof())
    {
      const bool success = get_optional_shader_path(file, &geo_path, &tesc_path, &tese_path);
      assert(success);
    }
  
    // close file handler
    file.close();
  }
  

  //fill output strings with the code from each shader
  *vert = LoadFile(vert_path);
  *frag = LoadFile(frag_path);
  if(geo_path != "")
    *geo = LoadFile(geo_path);
  if (tesc_path != "")
    *tesc = LoadFile(tesc_path);
  if (tese_path != "")
    *tese = LoadFile(tese_path);
}

