/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    g_buffer.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:46:28 2020

\brief  Contains the class g_buffer, derived from framebuffer

  get_size_per_pixel() should be updated manually

***************************************************************************/


#include "g_buffer.h"

#include <iostream> //std::cerr
#include "graphics/model/vertex_buffer_layout.h"
#include "graphics/texture/texture.h"
#include "other/append_queue.h"
#include "nodes/point_light_node.h"


/// BASE CLASS

/// <summary>
/// Constructor
/// </summary>
/// <param name="minification"></param>
/// <param name="magnification"></param>
g_buffer::g_buffer(int minification, int magnification)
  : frame_buffer::frame_buffer(minification, magnification)
{
}


/// LDR-OPTI G-BUFFER

/// <summary>
/// Contructor
/// </summary>
/// <param name="minification"></param>
/// <param name="magnification"></param>
g_buffer_v4::g_buffer_v4(int minification, int magnification)
  : g_buffer::g_buffer(minification, magnification)
{
}

/// <summary>
/// Size per pixel in bytes. Not counting depth
/// </summary>
int g_buffer_v4::get_size_per_pixel() const
{
  //normals buffer:   6 bytes/pixel
  //diffuse buffer:   4 bytes/pixel
  //emissive buffer:  4 bytes/pixel
  //depth buffer:     ? bytes/pixel

  return 14;
}

/// <summary>
/// Gets the action to create this buffer
/// </summary>
/// <returns> a shared pointer to the creation action</returns>
std::shared_ptr<action> g_buffer_v4::get_create_buffer_action()
{
  return std::make_shared<create_g_buffer_v4>(&m_handle,
    m_tex_handles.data(), static_cast<unsigned>(e_gbuff_v4_textures::SIZE)
    , m_width, m_height, m_minif, m_magnif);
}

/// <summary>
/// Gets the action to delete this buffer
/// </summary>
/// <returns>a shared pointer to the deletion action</returns>
std::shared_ptr<action> g_buffer_v4::get_delete_buffer_action()
{
  return std::make_shared<delete_g_buffer>(&m_handle,
    m_tex_handles.data(), static_cast<unsigned>(e_gbuff_v4_textures::SIZE));
}

/// <summary>
/// Gets the action sequence needed to pass the core
/// textures of this buffer to the shader.
/// The core textures are all the textures in this case
/// 
/// The uniforms must be named:
/// u_norms, u_diffuse, u_emissive, u_depth
/// </summary>
/// <param name="shdr">
/// Shader to wich to pass the textures
/// </param>
/// <returns>the queue of actions</returns>
std::queue<std::shared_ptr<action>>
  g_buffer_v4::get_bind_core_textures_actions(shader& shdr)
{
  std::queue<std::shared_ptr<action>> output;

  output.push(texture::get_set_active_slot_action(0));
  output.push(std::make_shared<bind_texture>(
    get_texture_handle_ptr(e_gbuff_v4_textures::normals), GL_TEXTURE_2D));
  output.push(shdr.get_set_uniform_texture_caching("u_norms", 0));

  output.push(texture::get_set_active_slot_action(1));
  output.push(std::make_shared<bind_texture>(
    get_texture_handle_ptr(e_gbuff_v4_textures::diff), GL_TEXTURE_2D));
  output.push(shdr.get_set_uniform_texture_caching("u_diffuse", 1));

  output.push(texture::get_set_active_slot_action(2));
  output.push(std::make_shared<bind_texture>(
    get_texture_handle_ptr(e_gbuff_v4_textures::emi), GL_TEXTURE_2D));
  output.push(shdr.get_set_uniform_texture_caching("u_emissive", 2));

  output.push(texture::get_set_active_slot_action(3));
  output.push(std::make_shared<bind_texture>(
    get_texture_handle_ptr(e_gbuff_v4_textures::depth), GL_TEXTURE_2D));
  output.push(shdr.get_set_uniform_texture_caching("u_depth", 3));

  return output;
}

/// <summary>
/// Gets the action sequence needed to bind the
/// emissive textures of this buffer.
///
/// The uniform must be named: u_emissive
/// </summary>
/// <param name="shdr">
/// Shader to wich to pass the texture
/// </param>
/// <returns>the queue of actions</returns>
std::queue<std::shared_ptr<action>>
  g_buffer_v4::get_bind_emissive_texture_actions(shader& shdr)
{
  std::queue<std::shared_ptr<action>> output;

  output.push(texture::get_set_active_slot_action(0));
  output.push(std::make_shared<bind_texture>(
    get_texture_handle_ptr(e_gbuff_v4_textures::emi), GL_TEXTURE_2D));
  output.push(shdr.get_set_uniform_texture_caching("u_emissive", 0));

  return output;
}

/// <summary>
/// Gets the action sequence needed to bind the
/// depth textures of this buffer.
///
/// The uniform must be named: u_depth
/// </summary>
/// <param name="shdr">
/// Shader to wich to pass the texture
/// </param>
/// <returns>the queue of actions</returns>
std::queue<std::shared_ptr<action>>
  g_buffer_v4::get_bind_depth_texture_actions(shader& shdr)
{
  std::queue<std::shared_ptr<action>> output;

  output.push(texture::get_set_active_slot_action(0));
  output.push(std::make_shared<bind_texture>(
    get_texture_handle_ptr(e_gbuff_v4_textures::depth), GL_TEXTURE_2D));
  output.push(shdr.get_set_uniform_texture_caching("u_depth", 0));

  return output;
}

std::queue<std::shared_ptr<action>> g_buffer_v4::fill(
  shader& shdr, std::vector<std::shared_ptr<node>>* nodes,
  std::vector<model>* models, std::vector<texture>* textures,
  const glm::mat4& projection, const glm::mat4& world_to_cam,
  bool wireframe)
{
  std::queue<std::shared_ptr<action>> output;

  {
    std::queue<std::shared_ptr<action>> q =
      setup_filling(shdr, projection, world_to_cam);

    append_queue(&output, &q);
  }

  if (wireframe)
    output.push(std::make_shared<set_polygon_mode>(GL_FRONT_AND_BACK, GL_LINE));

  //draw all objects
  for (std::shared_ptr<node> node_ptr : *nodes)
  {
    std::queue<std::shared_ptr<action>> q =
      node_ptr->get_draw_actions(shdr, *models, *textures);
    append_queue(&output, &q);
  }

  if (wireframe)
    output.push(std::make_shared<set_polygon_mode>(GL_FRONT_AND_BACK, GL_FILL));

  //disable depth test
  output.push(std::make_shared<disable>(GL_DEPTH_TEST));

  return output;
}

std::queue<std::shared_ptr<action>> g_buffer_v4::fill_debug_lights(
  shader& shdr, std::vector<std::shared_ptr<node>>* nodes,
  std::vector<model>* models, std::vector<texture>* textures,
  const glm::mat4& projection, const glm::mat4& world_to_cam,
  bool wireframe, bool draw_bulbs, bool draw_infl_spheres,
  unsigned first_light_idx)
{
    std::queue<std::shared_ptr<action>> output;

    {
      std::queue<std::shared_ptr<action>> q =
        setup_filling(shdr, projection, world_to_cam);

      append_queue(&output, &q);
    }

    if (wireframe)
      output.push(std::make_shared<set_polygon_mode>(GL_FRONT_AND_BACK, GL_LINE));

    //draw all objects
    for (std::shared_ptr<node> node_ptr : *nodes)
    {
      std::queue<std::shared_ptr<action>> q =
        node_ptr->get_draw_actions(shdr, *models, *textures);
      append_queue(&output, &q);
    }

    if (wireframe)
      output.push(std::make_shared<set_polygon_mode>(GL_FRONT_AND_BACK, GL_FILL));

    //debug pointlight stuff
    {
      if (draw_bulbs)
      {
        auto draw_bulb = [&](std::shared_ptr<node> n)
        {
          std::shared_ptr<point_light_node> l =
            std::static_pointer_cast<point_light_node>(n);

          std::queue<std::shared_ptr<action>> q =
            l->get_draw_bulb_actions(shdr, (*models)[0u], *textures);
          append_queue(&output, &q);
        };

        std::for_each(nodes->begin() + first_light_idx, nodes->end(), draw_bulb);
      }

      if (draw_infl_spheres)
      {
        output.push(std::make_shared<disable>(GL_CULL_FACE));

        auto draw_bulb = [&](std::shared_ptr<node> n)
        {
          std::shared_ptr<point_light_node> l =
            std::static_pointer_cast<point_light_node>(n);

          std::queue<std::shared_ptr<action>> q =
            l->get_draw_influence_actions(shdr, (*models)[0u], *textures);
          append_queue(&output, &q);
        };

        output.push(std::make_shared<set_polygon_mode>(GL_FRONT_AND_BACK, GL_LINE));
        std::for_each(nodes->begin() + first_light_idx, nodes->end(), draw_bulb);
        output.push(std::make_shared<set_polygon_mode>(GL_FRONT_AND_BACK, GL_FILL));

        output.push(std::make_shared<enable>(GL_CULL_FACE));
      }
    }

    //disable depth test
    output.push(std::make_shared<disable>(GL_DEPTH_TEST));

    return output;
}

std::queue<std::shared_ptr<action>> g_buffer_v4::setup_filling(
  shader& shdr, const glm::mat4& projection, const glm::mat4& world_to_cam)
{
  std::queue<std::shared_ptr<action>> output;

  //enable depth test and backface culling
  output.push(std::make_shared<enable>(GL_DEPTH_TEST));
  output.push(std::make_shared<set_culling>(GL_BACK));

  //bind g_buffer
  output.push(get_bind_action());
  output.push(get_set_viewport_action());
  output.push(std::make_shared<render_clear>(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

  //bind shader
  output.push(shdr.get_bind_action());

  //pass common matrices
  output.push(shdr.get_set_uniform_mat4_caching("u_worldToCam", world_to_cam));
  output.push(shdr.get_set_uniform_mat4_caching("u_camToVP", projection));

  return output;
}



/// FINAL PROJECT G-BUFFER

gbuffer_final_project::gbuffer_final_project(int minification, int magnification)
  : g_buffer::g_buffer(minification, magnification)
{}

int gbuffer_final_project::get_size_per_pixel() const
{
  //normals buffer:   6 bytes/pixel
  //depth buffer:     ? bytes/pixel

  return 6;
}

std::shared_ptr<action> gbuffer_final_project::get_create_buffer_action()
{
  return std::make_shared<create_final_g_buffer>(&m_handle,
    m_tex_handles.data(), static_cast<unsigned>(e_gbuff_v4_textures::SIZE)
    , m_width, m_height, m_minif, m_magnif);
}

std::shared_ptr<action> gbuffer_final_project::get_delete_buffer_action()
{
  return std::make_shared<delete_g_buffer>(&m_handle,
    m_tex_handles.data(), static_cast<unsigned>(e_gbuff_v4_textures::SIZE));
}

std::queue<std::shared_ptr<action>> gbuffer_final_project::get_bind_core_textures_actions(
  shader& shdr)
{
  std::queue<std::shared_ptr<action>> output;

  output.push(texture::get_set_active_slot_action(0));
  output.push(std::make_shared<bind_texture>(
    get_texture_handle_ptr(e_gbuff_final_project::normals), GL_TEXTURE_2D));
  output.push(shdr.get_set_uniform_texture_caching("u_norms", 0));

  output.push(texture::get_set_active_slot_action(1));
  output.push(std::make_shared<bind_texture>(
    get_texture_handle_ptr(e_gbuff_final_project::depth), GL_TEXTURE_2D));
  output.push(shdr.get_set_uniform_texture_caching("u_depth", 1));

  return output;
}

std::queue<std::shared_ptr<action>> gbuffer_final_project::get_bind_emissive_texture_actions(shader& shdr)
{
  assert(false);
  return std::queue<std::shared_ptr<action>>();
}

std::queue<std::shared_ptr<action>> gbuffer_final_project::get_bind_depth_texture_actions(
  shader& shdr)
{
  std::queue<std::shared_ptr<action>> output;

  output.push(texture::get_set_active_slot_action(0));
  output.push(std::make_shared<bind_texture>(
    get_texture_handle_ptr(e_gbuff_final_project::depth), GL_TEXTURE_2D));
  output.push(shdr.get_set_uniform_texture_caching("u_depth", 0));

  return output;
}

std::queue<std::shared_ptr<action>> gbuffer_final_project::fill(
  shader& shdr, std::vector<std::shared_ptr<node>>* nodes,
  std::vector<model>* models, std::vector<texture>* textures,
  const glm::mat4& projection, const glm::mat4& world_to_cam,
  bool wireframe)
{
  std::queue<std::shared_ptr<action>> output;

  {
    std::queue<std::shared_ptr<action>> q =
      setup_filling(shdr, projection, world_to_cam);

    append_queue(&output, &q);
  }

  if (wireframe)
    output.push(std::make_shared<set_polygon_mode>(GL_FRONT_AND_BACK, GL_LINE));

  //draw all objects
  for (std::shared_ptr<node> node_ptr : *nodes)
  {
    std::queue<std::shared_ptr<action>> q =
      node_ptr->get_draw_actions(shdr, *models, *textures);
    append_queue(&output, &q);
  }

  if (wireframe)
    output.push(std::make_shared<set_polygon_mode>(GL_FRONT_AND_BACK, GL_FILL));

  //disable depth test
  output.push(std::make_shared<disable>(GL_DEPTH_TEST));

  return output;
}

std::queue<std::shared_ptr<action>> gbuffer_final_project::fill_debug_lights(shader& shdr, std::vector<std::shared_ptr<node>>* nodes, std::vector<model>* models, std::vector<texture>* textures, const glm::mat4& projection, const glm::mat4& world_to_cam, bool wireframe, bool draw_bulbs, bool draw_infl_spheres, unsigned first_light_idx)
{
  assert(false);
  return std::queue<std::shared_ptr<action>>();
}

std::queue<std::shared_ptr<action>> gbuffer_final_project::setup_filling(
  shader& shdr, const glm::mat4& projection, const glm::mat4& world_to_cam)
{
  std::queue<std::shared_ptr<action>> output;

  //enable depth test and backface culling
  output.push(std::make_shared<enable>(GL_DEPTH_TEST));
  output.push(std::make_shared<set_culling>(GL_BACK));

  //bind g_buffer
  output.push(get_bind_action());
  output.push(get_set_viewport_action());
  output.push(std::make_shared<render_clear>(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

  //bind shader
  output.push(shdr.get_bind_action());

  //pass common matrices
  output.push(shdr.get_set_uniform_mat4_caching("u_worldToCam", world_to_cam));
  output.push(shdr.get_set_uniform_mat4_caching("u_camToVP", projection));

  return output;
}
