/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    g_buffer.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:46:39 2020

\brief  Contains the class g_buffer, derived from framebuffer

  get_size_per_pixel() should be updated manually


***************************************************************************/
#pragma once

#include <array>
#include "framebuffer.h"
#include "nodes/node.h"
#include "graphics/model/model.h"
#include "graphics/texture/texture.h"


/// G-BUFFER BASE CLASS

class g_buffer : public frame_buffer
{
public:
  g_buffer(int minification, int magnification);
  virtual ~g_buffer() {}

  virtual int get_size_per_pixel() const = 0;

  virtual std::queue<std::shared_ptr<action>> get_bind_textures_actions(
    shader& shdr) override
  {
    return get_bind_core_textures_actions(shdr);
  }

  virtual std::queue<std::shared_ptr<action>>
    get_bind_core_textures_actions(shader& shdr) = 0;
  virtual std::queue<std::shared_ptr<action>>
    get_bind_emissive_texture_actions(shader& shdr) = 0;
  virtual std::queue<std::shared_ptr<action>>
    get_bind_depth_texture_actions(shader& shdr) = 0;

  virtual std::queue<std::shared_ptr<action>>
    fill(shader& shdr, std::vector<std::shared_ptr<node>>* nodes,
      std::vector<model>* models, std::vector<texture>* textures,
      const glm::mat4& projection, const glm::mat4& world_to_cam,
      bool wireframe) = 0;

  virtual std::queue<std::shared_ptr<action>>
    fill_debug_lights(shader& shdr, std::vector<std::shared_ptr<node>>* nodes,
      std::vector<model>* models, std::vector<texture>* textures,
      const glm::mat4& projection, const glm::mat4& world_to_cam,
      bool draw_bulbs, bool draw_infl_spheres, bool wireframe,
      unsigned first_light_idx) = 0;
  
};


// ldr_simple: only works with greyscale ambient and specular. 
//            1st buffer is for position and ambient(1 channel)
//            2nd buffer is for normal and shininess
//            3rd buffer is for diffuse and specular(1 channel)
//


/// G-BUFFER V4
/// 
///   considers ambient to be the same as diffuse.
///   1st buffer is for normals
///   2nd buffer is for diffuse and specular grayscale
///   3rd buffer is for emissive and shininess
///   4th buffer is the depth buffer

enum class e_gbuff_v4_textures { normals, diff, emi, depth, SIZE };

class g_buffer_v4 : public g_buffer
{
public:
  using tex_array = std::array<unsigned, static_cast<size_t>(e_gbuff_v4_textures::SIZE)>;

private:
  tex_array m_tex_handles = {};

public:
  g_buffer_v4(int minification, int magnification);

  inline unsigned get_texture_handle(e_gbuff_v4_textures tex_enum)
  {
    return m_tex_handles[static_cast<size_t>(tex_enum)];
  }

  inline unsigned* get_texture_handle_ptr(e_gbuff_v4_textures tex_enum)
  {
    return &m_tex_handles[static_cast<size_t>(tex_enum)];
  }

  inline tex_array* get_textures_arr_ptr()
  {
    return &m_tex_handles;
  }
 
  int get_size_per_pixel() const;
  std::shared_ptr<action> get_create_buffer_action() override;
  std::shared_ptr<action> get_delete_buffer_action() override;
  std::queue<std::shared_ptr<action>> get_bind_core_textures_actions(shader& shdr) override;
  std::queue<std::shared_ptr<action>> get_bind_emissive_texture_actions(shader& shdr) override;
  std::queue<std::shared_ptr<action>> get_bind_depth_texture_actions(shader& shdr) override;

  std::queue<std::shared_ptr<action>>
    fill(shader& shdr, std::vector<std::shared_ptr<node>>* nodes,
      std::vector<model>* models, std::vector<texture>* textures,
      const glm::mat4& projection, const glm::mat4& world_to_cam,
      bool wireframe) override;

  std::queue<std::shared_ptr<action>>
    fill_debug_lights(shader& shdr, std::vector<std::shared_ptr<node>>* nodes,
      std::vector<model>* models, std::vector<texture>* textures,
      const glm::mat4& projection, const glm::mat4& world_to_cam,
      bool wireframe, bool draw_bulbs, bool draw_infl_spheres,
      unsigned first_light_idx) override;

private:
  std::queue<std::shared_ptr<action>> setup_filling(shader& shdr,
    const glm::mat4& projection, const glm::mat4& world_to_cam);
};


/// FINAL PROJECT G-BUFFER
/// 
///   1st buffer is for normals
///   2nd buffer is the depth buffer

enum class e_gbuff_final_project { normals, depth, SIZE };

class gbuffer_final_project : public g_buffer
{
public:
  using tex_array = std::array<unsigned, static_cast<size_t>(e_gbuff_final_project::SIZE)>;

private:
  tex_array m_tex_handles = {};

public:
  gbuffer_final_project(int minification, int magnification);

  inline unsigned get_texture_handle(e_gbuff_final_project tex_enum)
  {
    return m_tex_handles[static_cast<size_t>(tex_enum)];
  }

  inline unsigned* get_texture_handle_ptr(e_gbuff_final_project tex_enum)
  {
    return &m_tex_handles[static_cast<size_t>(tex_enum)];
  }

  inline tex_array* get_textures_arr_ptr()
  {
    return &m_tex_handles;
  }

  int get_size_per_pixel() const;
  std::shared_ptr<action> get_create_buffer_action() override;
  std::shared_ptr<action> get_delete_buffer_action() override;
  std::queue<std::shared_ptr<action>> get_bind_core_textures_actions(shader& shdr) override;
  std::queue<std::shared_ptr<action>> get_bind_emissive_texture_actions(shader& shdr) override;
  std::queue<std::shared_ptr<action>> get_bind_depth_texture_actions(shader& shdr) override;

  std::queue<std::shared_ptr<action>>
    fill(shader& shdr, std::vector<std::shared_ptr<node>>* nodes,
      std::vector<model>* models, std::vector<texture>* textures,
      const glm::mat4& projection, const glm::mat4& world_to_cam,
      bool wireframe) override;

  std::queue<std::shared_ptr<action>>
    fill_debug_lights(shader& shdr, std::vector<std::shared_ptr<node>>* nodes,
      std::vector<model>* models, std::vector<texture>* textures,
      const glm::mat4& projection, const glm::mat4& world_to_cam,
      bool wireframe, bool draw_bulbs, bool draw_infl_spheres,
      unsigned first_light_idx) override;

private:
  std::queue<std::shared_ptr<action>> setup_filling(shader& shdr,
    const glm::mat4& projection, const glm::mat4& world_to_cam);
};
