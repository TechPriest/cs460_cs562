/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    framebuffer.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:46:15 2020

\brief  Contains the abstract class framebuffer as well as the derived class
rend_buffer.

possible improvements:
  - have the quad (vao, vbo, ibo) be shared among all


***************************************************************************/


/*
instead of each framebuffer having its quad vao, etc. it could be shared among all

*/

#pragma once

#include <queue>
#include <memory> //std::shared_ptr
#include "graphics/shader/shader.h"

class action;
class bind_frame_buffer;
class bind_vertex_array;
class unbind_frame_buffer;
class set_viewport;

class frame_buffer
{
protected:
  unsigned m_handle = -1;

  int m_width = -1;
  int m_height = -1;

  unsigned m_quad_vao = 0;
  unsigned m_quad_vbo = 0;
  unsigned m_quad_ibo = 0;

  int m_minif = -1;
  int m_magnif = -1;

private:
  bool m_in_GPU = false; //in truth this variable tells us if the get_create_action was called, not if create_shader.do_action() was done.

public:
  frame_buffer(int minification, int magnification);
  virtual ~frame_buffer();

  inline int get_width() const { return m_width; }
  inline int get_height() const { return m_height; }

  std::queue<std::shared_ptr<action>> get_set_resolution_actions(
    int width, int height);

  inline std::shared_ptr<bind_frame_buffer> get_bind_action()
  {
    return std::make_shared<bind_frame_buffer>(&m_handle);
  }
  inline std::shared_ptr<bind_vertex_array> get_bind_quad_action()
  {
    return std::make_shared<bind_vertex_array>(&m_quad_vao);
  }
  static inline std::shared_ptr<unbind_frame_buffer> get_unbind_action()
  {
    return std::make_shared<unbind_frame_buffer>();
  }
  inline std::shared_ptr<set_viewport> get_set_viewport_action()
  {
    return std::make_shared<set_viewport>(m_width, m_height);
  }

  std::queue<std::shared_ptr<action>> get_create_actions(
    float* quad_vertices, unsigned* quad_indices); //verts_size_must be 4, ind_size must be 6
  std::queue<std::shared_ptr<action>> get_delete_actions();
  virtual std::queue<std::shared_ptr<action>> get_bind_textures_actions(
    shader& shdr) = 0;

protected:

  std::queue<std::shared_ptr<action>> get_create_quad_actions(
    float* vertices, unsigned* indices);
  std::queue<std::shared_ptr<action>> get_delete_quad_actions();
  virtual std::shared_ptr<action> get_create_buffer_action() = 0;
  virtual std::shared_ptr<action> get_delete_buffer_action() = 0;
};

class rend_buffer : public frame_buffer
{
protected:
  unsigned m_texture_handle = 0u;

public:

  rend_buffer(int minification, int magnification);
  virtual ~rend_buffer() {}

  inline unsigned* get_texture_handle_ptr() { return &m_texture_handle; }
  inline const unsigned* get_texture_handle_ptr() const { return &m_texture_handle; }
  std::queue<std::shared_ptr<action>> get_bind_textures_actions(
    shader& shdr) override; //unused
  virtual std::shared_ptr<action> get_create_buffer_action() override;
  std::shared_ptr<action> get_delete_buffer_action() override;
};

class rend_buffer_with_unowned_depth : public rend_buffer
{
protected:
  unsigned* m_shared_depth_tex_handle = nullptr;

public:
  rend_buffer_with_unowned_depth(int minification, int magnification);
  virtual ~rend_buffer_with_unowned_depth() {}

  void set_shared_depth_texture(unsigned* texture_handle_ptr);
  std::shared_ptr<action> get_create_buffer_action() override;
};

class rend_buffer_with_unowned_depth_diff_norm : public frame_buffer
{
protected:
  unsigned* m_shared_depth_tex_handle = nullptr;
  unsigned* m_shared_diff_tex_handle = nullptr;
  unsigned* m_shared_norm_tex_handle = nullptr;

public:
  rend_buffer_with_unowned_depth_diff_norm(int minification, int magnification);
  virtual ~rend_buffer_with_unowned_depth_diff_norm() {}

  void set_shared_depth_texture(unsigned* texture_handle_ptr);
  void set_shared_diff_texture(unsigned* texture_handle_ptr);
  void set_shared_norm_texture(unsigned* texture_handle_ptr);

  std::shared_ptr<action> get_create_buffer_action() override;
  std::shared_ptr<action> get_delete_buffer_action() override;

  std::queue<std::shared_ptr<action>> get_bind_textures_actions(
    shader& shdr) override;
};
