/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    framebuffer.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:46:07 2020

\brief  Contains the abstract class framebuffer as well as the derived class
rend_buffer.

possible improvements:
  - have the quad (vao, vbo, ibo) be shared among all

***************************************************************************/


#include "framebuffer.h"

#include <iostream> //std::cerr
#include "graphics/model/vertex_buffer_layout.h"
#include "graphics/texture/texture.h"
#include "actions/render_actions.h"


/// G-BUFFER BASE CLASS

frame_buffer::frame_buffer(int minification, int magnification)
  : m_minif(minification), m_magnif(magnification)
{}

frame_buffer::~frame_buffer()
{
  if (m_in_GPU)
    std::cerr << "WARNING: shader g_buffer hasn't been deleted from the GPU" << std::endl;
}

std::queue<std::shared_ptr<action>> frame_buffer::get_set_resolution_actions(int width, int height)
{
  std::queue<std::shared_ptr<action>> output;

  m_width = width;
  m_height = height;

  if (m_in_GPU)
  {
    output.push(get_delete_buffer_action());
    output.push(get_create_buffer_action());
  }

  return output;
}

std::queue<std::shared_ptr<action>> frame_buffer::get_create_actions(
  float* quad_vertices, unsigned* quad_indices)
{
  std::queue<std::shared_ptr<action>> output =
    get_create_quad_actions(quad_vertices, quad_indices);

  output.push(get_create_buffer_action());

  return output;
}

std::queue<std::shared_ptr<action>> frame_buffer::get_delete_actions()
{
  std::queue<std::shared_ptr<action>> output =
    get_delete_quad_actions();

  output.push(get_delete_buffer_action());

  return output;
}

std::queue<std::shared_ptr<action>> frame_buffer::get_create_quad_actions(float* vertices, unsigned* indices)
{
  m_in_GPU = true;

  std::queue<std::shared_ptr<action>> output;

  vertex_buffer_layout layout;
  layout.push_interleaved<float>(3u); //pos
  layout.push_interleaved<float>(2u); //uv

  output.push(std::make_shared<create_vertex_array>(&m_quad_vao)); //does not bind
  output.push(std::make_shared<bind_vertex_array>(&m_quad_vao));
  output.push(std::make_shared<create_vertex_buffer>(&m_quad_vbo,
    vertices, static_cast<unsigned>(4u * 5u * sizeof(float)), GL_STATIC_DRAW)); //binds
  output.push(std::make_shared<set_vertex_buffer_layout>(layout));
  output.push(std::make_shared<create_index_buffer>(&m_quad_ibo, indices,
    6u * static_cast<unsigned>(sizeof(unsigned)), GL_STATIC_DRAW)); //binds

  return output;
}

std::queue<std::shared_ptr<action>> frame_buffer::get_delete_quad_actions()
{
  m_in_GPU = false;

  std::queue<std::shared_ptr<action>> output;

  output.push(std::make_shared<delete_index_buffer>(&m_quad_ibo));
  output.push(std::make_shared<delete_vertex_buffer>(&m_quad_vbo));
  output.push(std::make_shared<delete_vertex_array>(&m_quad_vao));

  return output;
}


/// REND-BUFFER

rend_buffer::rend_buffer(int minification, int magnification)
  : frame_buffer::frame_buffer(minification, magnification)
{
}

std::queue<std::shared_ptr<action>>
  rend_buffer::get_bind_textures_actions(shader& shdr)
{
  std::queue<std::shared_ptr<action>> output;

  output.push(texture::get_set_active_slot_action(0));
  output.push(std::make_shared<bind_texture>(
    &m_texture_handle, GL_TEXTURE_2D));
  output.push(shdr.get_set_uniform_texture_caching("u_frame", 0));

  return output;
}

std::shared_ptr<action> rend_buffer::get_create_buffer_action()
{
  return std::make_shared<create_rend_buffer>(&m_handle,
    &m_texture_handle, m_width, m_height, m_minif, m_magnif);
}

std::shared_ptr<action> rend_buffer::get_delete_buffer_action()
{
  return std::make_shared<delete_rend_buffer>(&m_handle,
    &m_texture_handle);
}







rend_buffer_with_unowned_depth::rend_buffer_with_unowned_depth(int minification, int magnification)
  : rend_buffer::rend_buffer(minification, magnification)
{
}

void rend_buffer_with_unowned_depth::set_shared_depth_texture(unsigned* texture_handle_ptr)
{
  m_shared_depth_tex_handle = texture_handle_ptr;
}

std::shared_ptr<action> rend_buffer_with_unowned_depth::get_create_buffer_action()
{
  assert(m_shared_depth_tex_handle); //call set_shared_depth_texture before this

  return std::make_shared<create_rend_buffer_with_shared_depth>(&m_handle, &m_texture_handle,
    const_cast<const unsigned*>(m_shared_depth_tex_handle),
    m_width, m_height, m_minif, m_magnif);
}


rend_buffer_with_unowned_depth_diff_norm::rend_buffer_with_unowned_depth_diff_norm
  (int minification, int magnification)
    : frame_buffer::frame_buffer(minification, magnification)
{}

void rend_buffer_with_unowned_depth_diff_norm::set_shared_depth_texture(unsigned* texture_handle_ptr)
{
  m_shared_depth_tex_handle = texture_handle_ptr;
}

void rend_buffer_with_unowned_depth_diff_norm::set_shared_diff_texture(unsigned* texture_handle_ptr)
{
  m_shared_diff_tex_handle = texture_handle_ptr;
}

void rend_buffer_with_unowned_depth_diff_norm::set_shared_norm_texture(unsigned* texture_handle_ptr)
{
  m_shared_norm_tex_handle = texture_handle_ptr;
}

std::shared_ptr<action> rend_buffer_with_unowned_depth_diff_norm::get_create_buffer_action()
{
  //call all testure setters before this
  assert(m_shared_depth_tex_handle);
  assert(m_shared_diff_tex_handle);
  assert(m_shared_norm_tex_handle);


  return std::make_shared<create_rend_buffer_with_shared_depth_diff_norm>(
    &m_handle,
    const_cast<const unsigned*>(m_shared_depth_tex_handle),
    const_cast<const unsigned*>(m_shared_diff_tex_handle),
    const_cast<const unsigned*>(m_shared_norm_tex_handle),
    m_width, m_height,
    m_minif, m_magnif);
}

std::shared_ptr<action> rend_buffer_with_unowned_depth_diff_norm::get_delete_buffer_action()
{
  return std::make_shared<delete_frame_buffer>(&m_handle);
}

std::queue<std::shared_ptr<action>> rend_buffer_with_unowned_depth_diff_norm::get_bind_textures_actions(shader& shdr)
{
  assert(false);

  return {};
}
