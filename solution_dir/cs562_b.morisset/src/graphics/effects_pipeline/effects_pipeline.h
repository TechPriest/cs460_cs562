/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    effets_pipeline.h

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu

\par    DigiPen login: b.morisset

\par    Course: CS562

\par    Assignment #2

\date   Thur Oct 29 02:00:10 2020

\brief  A class that takes the result of the lightin pass and applies
the post processing effects specified 


***************************************************************************/

#pragma once

#include <queue>
#include <memory> //shared_ptr
#include <bitset>
#include "graphics/framebuffers/g_buffer.h"
#include "graphics/shader/shader.h"
#include "graphics/texture/texture3D.h"

class action;
class window;

class effects_pipeline
{
public:
  enum e_effects {bloom, emis_glow, anti_aliasing, outlines, outlines_depth, blur, LUT, invert};
  using active_effects = std::bitset<8u>;

  active_effects m_act_effects = {};
  float m_aa_factor = 50.0f;
  float m_edge_detect_depth_factor = 500.0f;
  float m_edge_detect_factor = 0.01f;

  using weight_buffer = std::array<float, 33u>; //32 on each side and 1 in the middle (dimension must match blur_lighted shader's max)
  weight_buffer m_blur_weights;
  size_t m_blur_weights_size = 0u;

  // lim = 0 and lerp = 1 to get full lerped bloom
  float m_lumin_lim = 0.0f;
  float m_lumi_lerp_margin = 1.0f;

private:
  std::queue<std::shared_ptr<action>> m_action_q; //reset on each pipeline call

  g_buffer_v4::tex_array* m_gbuff_textures = nullptr;
  unsigned* m_light_result_tex = nullptr;

  rend_buffer m_work_fb_0;
  rend_buffer m_work_fb_1; 
  rend_buffer m_work_fb_2;

  rend_buffer m_small_work_fb_0; //used for blur_lighted intermediate (between x and y)
  rend_buffer m_small_work_fb_1; //used for blur_lighted final

  window* m_window = nullptr;
  std::vector<shader>* m_shaders;
  std::vector<texture3D>* m_LUTs;


public:

  inline effects_pipeline(
    g_buffer_v4::tex_array* g_buff_textures,
    unsigned* light_result_tex,
    std::vector<shader>* shaders,
    std::vector<texture3D>* LUTs
  )
    : m_gbuff_textures(g_buff_textures),
      m_light_result_tex(light_result_tex),
      m_work_fb_0(GL_LINEAR, GL_LINEAR), //GL_NEAREST
      m_work_fb_1(GL_LINEAR, GL_LINEAR),
      m_work_fb_2(GL_LINEAR, GL_LINEAR),
      m_small_work_fb_0(GL_LINEAR, GL_LINEAR),
      m_small_work_fb_1(GL_LINEAR, GL_LINEAR),
      m_shaders(shaders),
      m_LUTs(LUTs),
      m_work_buffers({ &m_work_fb_1, &m_work_fb_2 })
  {
    //{ 0.382928f, 0.241732f, 0.060598f, 0.005977f, 0.000229f };
    m_blur_weights[0u] = 0.382928f;
    m_blur_weights[1u] = 0.241732f;
    m_blur_weights[2u] = 0.060598f;
    m_blur_weights[3u] = 0.005977f;
    m_blur_weights[4u] = 0.000229f;
    m_blur_weights_size = 5u;
  }

  std::queue<std::shared_ptr<action>> get_create_actions(
    float* quad_vertices, unsigned* quad_indices);
  std::queue<std::shared_ptr<action>> get_delete_actions();

  std::queue<std::shared_ptr<action>> get_set_resolution_actions(int width, int height);

  std::queue<std::shared_ptr<action>> get_pipeline_actions(glm::mat4 inv_projection);

  inline unsigned* get_final_texture() const
  {
    if (m_first_done)
      return (*(m_work_buffers[last_used_fb_i()])).get_texture_handle_ptr();

    return m_light_result_tex;
  }

private:

  unsigned m_free_fb_i = 0u;
  std::array<rend_buffer*, 2u> m_work_buffers;
  mutable bool m_first_done = false;

  inline unsigned last_used_fb_i() const
  {
    return (m_free_fb_i - 1u) % m_work_buffers.size();
  };
  inline unsigned post_incr_fb_i()
  {
    unsigned temp = m_free_fb_i;
    m_free_fb_i = (m_free_fb_i +1u) %
      static_cast<unsigned>(m_work_buffers.size());
    return temp;
  };


  inline unsigned* get_last_fb_tex() const
  {
    unsigned* output = get_final_texture();

    m_first_done = true;

    return output;
  };

  inline unsigned* get_gbuffer_tex_ptr(e_gbuff_v4_textures tex_type)
  {
    return &( (*m_gbuff_textures)[static_cast<size_t>(tex_type)] );
  }

  void masked_blur_x(frame_buffer& out_fb, unsigned* in_tex, unsigned* mask_tex);
  void masked_blur_y(frame_buffer& out_fb, unsigned* in_tex, unsigned* mask_tex);
  void emissive_glow(frame_buffer& out_fb, unsigned* in_tex, unsigned* mask_tex);
  void fill_edge_buffer(float edge_detection_factor, glm::mat4 inv_projection);
  void fill_edge_buffer_depth(float edge_detection_factor, glm::mat4 inv_projection);
  void tex_quad_draw(frame_buffer& out_fb, unsigned* in_tex, bool clear);
  void blur_passes(frame_buffer& out_fb, rend_buffer& internal_fb, unsigned* in_tex,
    float* weights, size_t weights_count);
  void draw_luminance(frame_buffer& out_fb, unsigned* in_tex, float luminance_limit, float lumi_lerp_margin);
  void bloom_passes(frame_buffer& out_fb, unsigned* in_tex);
  void do_LUT(frame_buffer& out_fb, unsigned* in_tex, const unsigned* in_LUT);
  void do_inversion(frame_buffer& out_fb, unsigned* in_tex);
};