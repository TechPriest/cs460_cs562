/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    effets_pipeline.cpp

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu

\par    DigiPen login: b.morisset

\par    Course: CS562

\par    Assignment #2

\date   Thur Oct 29 02:00:10 2020

\brief  A class that takes the result of the lightin pass and applies
the post processing effects specified

***************************************************************************/

#include "effects_pipeline.h"

#include "graphics/texture/texture.h"
#include "graphics/window/window.h"
#include "graphics/shader/shader.h"
#include "actions/gui_actions.h"
#include "other/append_queue.h"
#include "scenes/lvl_shader_types.h"


std::queue<std::shared_ptr<action>> effects_pipeline::get_create_actions(
  float* quad_vertices, unsigned* quad_indices)
{
  std::queue<std::shared_ptr<action>> output;
  std::queue<std::shared_ptr<action>> q;

  q = m_work_fb_0.get_create_actions(quad_vertices, quad_indices);
  append_queue(&output, &q);
  q = m_work_fb_1.get_create_actions(quad_vertices, quad_indices);
  append_queue(&output, &q);
  q = m_work_fb_2.get_create_actions(quad_vertices, quad_indices);
  append_queue(&output, &q);
  q = m_small_work_fb_0.get_create_actions(quad_vertices, quad_indices);
  append_queue(&output, &q);
  q = m_small_work_fb_1.get_create_actions(quad_vertices, quad_indices);
  append_queue(&output, &q);

  return output;
}

std::queue<std::shared_ptr<action>> effects_pipeline::get_delete_actions()
{
  std::queue<std::shared_ptr<action>> output;
  std::queue<std::shared_ptr<action>> q;

  q = m_work_fb_0.get_delete_actions();
  append_queue(&output, &q);
  q = m_work_fb_1.get_delete_actions();
  append_queue(&output, &q);
  q = m_work_fb_2.get_delete_actions();
  append_queue(&output, &q);
  q = m_small_work_fb_0.get_delete_actions();
  append_queue(&output, &q);
  q = m_small_work_fb_1.get_delete_actions();
  append_queue(&output, &q);

  return output;
}

std::queue<std::shared_ptr<action>>
  effects_pipeline::get_set_resolution_actions(int width, int height)
{
  std::queue<std::shared_ptr<action>> output;
  std::queue<std::shared_ptr<action>> q;

  q = m_work_fb_0.get_set_resolution_actions(width, height);
  append_queue(&output, &q);
  q = m_work_fb_1.get_set_resolution_actions(width, height);
  append_queue(&output, &q);
  q = m_work_fb_2.get_set_resolution_actions(width, height);
  append_queue(&output, &q);
  q = m_small_work_fb_0.get_set_resolution_actions(width/4, height/4);
  append_queue(&output, &q);
  q = m_small_work_fb_1.get_set_resolution_actions(width/4, height/4);
  append_queue(&output, &q);

  return output;
}

std::queue<std::shared_ptr<action>> effects_pipeline::get_pipeline_actions(
  glm::mat4 inv_projection)
{
  clear_queue(&m_action_q);

  m_first_done = false;

  //bloom
  if (m_act_effects[e_effects::bloom])
  {
    unsigned* last_output_tex = get_last_fb_tex();
    rend_buffer& free_buffer = *(m_work_buffers[post_incr_fb_i()]);

    bloom_passes(free_buffer, last_output_tex);
  }

  //emissive_glow
  if (m_act_effects[e_effects::emis_glow])
  {
    unsigned* last_output_tex = get_last_fb_tex();
    rend_buffer& free_buffer = *(m_work_buffers[post_incr_fb_i()]);
    emissive_glow(
      free_buffer, last_output_tex,
      get_gbuffer_tex_ptr(e_gbuff_v4_textures::emi));
  }

  //Anti-Aliasing
  if (m_act_effects[e_effects::anti_aliasing])
  {
    fill_edge_buffer_depth(m_aa_factor, inv_projection); //fills m_edge_buffer

    {
      unsigned* last_output_tex = get_last_fb_tex();
      rend_buffer& free_buffer = *(m_work_buffers[post_incr_fb_i()]);
      masked_blur_x(
        free_buffer, last_output_tex,
        m_work_fb_0.get_texture_handle_ptr());
    }

    {
      unsigned* last_output_tex = get_last_fb_tex();
      rend_buffer& free_buffer = *(m_work_buffers[post_incr_fb_i()]);
      masked_blur_y(
        free_buffer, last_output_tex,
        m_work_fb_0.get_texture_handle_ptr());
    }
  }

  //outlines
  if (m_act_effects[e_effects::outlines])
  {
    fill_edge_buffer(m_edge_detect_factor, inv_projection);

    unsigned* last_output_tex = get_last_fb_tex();
    rend_buffer& free_buffer = *(m_work_buffers[post_incr_fb_i()]);

    tex_quad_draw(free_buffer, last_output_tex, true);

    m_action_q.push(std::make_shared<enable>(GL_BLEND));
    m_action_q.push(std::make_shared<set_blending>(GL_ONE, GL_ONE, GL_FUNC_ADD));

    tex_quad_draw(free_buffer, m_work_fb_0.get_texture_handle_ptr(), false);

    m_action_q.push(std::make_shared<disable>(GL_BLEND));
  }

  //outlines depth
  if (m_act_effects[e_effects::outlines_depth])
  {
    fill_edge_buffer_depth(m_edge_detect_depth_factor, inv_projection);

    unsigned* last_output_tex = get_last_fb_tex();
    rend_buffer& free_buffer = *(m_work_buffers[post_incr_fb_i()]);

    tex_quad_draw(free_buffer, last_output_tex, true);

    m_action_q.push(std::make_shared<enable>(GL_BLEND));
    m_action_q.push(std::make_shared<set_blending>(GL_ONE, GL_ONE, GL_FUNC_ADD));

    tex_quad_draw(free_buffer, m_work_fb_0.get_texture_handle_ptr(), false);

    m_action_q.push(std::make_shared<disable>(GL_BLEND));
  }

  //blur
  if (m_act_effects[e_effects::blur])
  {
    unsigned* last_output_tex = get_last_fb_tex();
    rend_buffer& free_buffer = *(m_work_buffers[post_incr_fb_i()]);

    blur_passes(free_buffer, m_work_fb_0, last_output_tex,
      m_blur_weights.data(), m_blur_weights_size);
  }

  //LUT
  if(m_act_effects[e_effects::LUT])
  {
    unsigned* last_output_tex = get_last_fb_tex();
    rend_buffer& free_buffer = *(m_work_buffers[post_incr_fb_i()]);

    do_LUT(free_buffer, last_output_tex, (*m_LUTs)[0].get_handle_ptr());
  }

  //invert colors
  if (m_act_effects[e_effects::invert])
  {
    unsigned* last_output_tex = get_last_fb_tex();
    rend_buffer& free_buffer = *(m_work_buffers[post_incr_fb_i()]);

    do_inversion(free_buffer, last_output_tex);
  }
  
  return m_action_q;
}


void effects_pipeline::masked_blur_x(frame_buffer& out_fb, unsigned* in_tex, unsigned* mask_tex)
{
  shader& shdr = (*m_shaders)[e_shader_type::AA_blur_x];

  //setup
  m_action_q.push(out_fb.get_bind_quad_action());
  m_action_q.push(out_fb.get_bind_action());
  m_action_q.push(out_fb.get_set_viewport_action());
  m_action_q.push(std::make_shared<render_clear>(GL_COLOR_BUFFER_BIT));
  m_action_q.push(shdr.get_bind_action());

  //uniforms
  {
    m_action_q.push(texture::get_set_active_slot_action(0));
    m_action_q.push(std::make_shared<bind_texture>(in_tex, GL_TEXTURE_2D));
    m_action_q.push(shdr.get_set_uniform_texture_caching(
      "u_texture", 0));
    m_action_q.push(texture::get_set_active_slot_action(1));
    m_action_q.push(std::make_shared<bind_texture>(mask_tex, GL_TEXTURE_2D));
    m_action_q.push(shdr.get_set_uniform_texture_caching(
      "u_edge_texture", 1));
    m_action_q.push(shdr.get_set_uniform_vec2_caching("u_tex_resolution",
      glm::vec2(out_fb.get_width(), out_fb.get_height())));
  }

  //draw
  m_action_q.push(std::make_shared<draw_elements>(6u));
}

void effects_pipeline::masked_blur_y(frame_buffer& out_fb, unsigned* in_tex, unsigned* mask_tex)
{
  shader& shdr = (*m_shaders)[e_shader_type::AA_blur_y];

  //setup
  m_action_q.push(out_fb.get_bind_quad_action());
  m_action_q.push(out_fb.get_bind_action());
  m_action_q.push(out_fb.get_set_viewport_action());
  m_action_q.push(std::make_shared<render_clear>(GL_COLOR_BUFFER_BIT));
  m_action_q.push(shdr.get_bind_action());

  //uniforms
  {
    m_action_q.push(texture::get_set_active_slot_action(0));
    m_action_q.push(std::make_shared<bind_texture>(in_tex, GL_TEXTURE_2D));
    m_action_q.push(shdr.get_set_uniform_texture_caching(
      "u_texture", 0));
    m_action_q.push(texture::get_set_active_slot_action(1));
    m_action_q.push(std::make_shared<bind_texture>(mask_tex, GL_TEXTURE_2D));
    m_action_q.push(shdr.get_set_uniform_texture_caching(
      "u_edge_texture", 1));
    m_action_q.push(shdr.get_set_uniform_vec2_caching("u_tex_resolution",
      glm::vec2(out_fb.get_width(), out_fb.get_height())));
  }

  //draw
  m_action_q.push(std::make_shared<draw_elements>(6u));
}

void effects_pipeline::emissive_glow(frame_buffer& out_fb, unsigned* in_tex, unsigned* mask_tex)
{
  //copy in texture to output
  tex_quad_draw(out_fb, in_tex, true);

  //add result of blur on top 
  m_action_q.push(std::make_shared<enable>(GL_BLEND));
  m_action_q.push(std::make_shared<set_blending>(GL_ONE, GL_ONE, GL_FUNC_ADD));

  {
    /*std::array<float, 5u> weights_sig1 =
    {
      0.382928f, 0.241732f, 0.060598f, 0.005977f, 0.000229f
    };*/

    std::array<float, 5u> weights_sig3 =
    {
      0.152781f, 0.144599f, 0.122589f, 0.093095f, 0.063327f
    };

    tex_quad_draw(m_small_work_fb_1, mask_tex, true);
    blur_passes(m_small_work_fb_1, m_small_work_fb_0,
      m_small_work_fb_1.get_texture_handle_ptr(), weights_sig3.data(), weights_sig3.size());
    tex_quad_draw(out_fb, m_small_work_fb_1.get_texture_handle_ptr(), false);
  }

  m_action_q.push(std::make_shared<disable>(GL_BLEND));
}

void effects_pipeline::bloom_passes(frame_buffer& out_fb, unsigned* in_tex)
{
  //copy in texture to output
  tex_quad_draw(out_fb, in_tex, true);

  //add result of blur on top 
  m_action_q.push(std::make_shared<enable>(GL_BLEND));
  m_action_q.push(std::make_shared<set_blending>(GL_ONE, GL_ONE, GL_FUNC_ADD));

  {
    /*std::array<float, 5u> weights_sig1 =
    {
      0.382928f, 0.241732f, 0.060598f, 0.005977f, 0.000229f
    };*/

    std::array<float, 5u> weights_sig3 =
    {
      0.152781f, 0.144599f, 0.122589f, 0.093095f, 0.063327f
    };


    draw_luminance(m_work_fb_0, in_tex, m_lumin_lim, m_lumi_lerp_margin);

    tex_quad_draw(m_small_work_fb_1, m_work_fb_0.get_texture_handle_ptr(), true);
    blur_passes(m_small_work_fb_1, m_small_work_fb_0,
      m_small_work_fb_1.get_texture_handle_ptr(), weights_sig3.data(), weights_sig3.size());
    tex_quad_draw(out_fb, m_small_work_fb_1.get_texture_handle_ptr(), false);
  }

  m_action_q.push(std::make_shared<disable>(GL_BLEND));
}

void effects_pipeline::fill_edge_buffer(float edge_detection_factor, glm::mat4 inv_projection)
{
  shader& shdr = (*m_shaders)[e_shader_type::edge_detection];

  //already disabled
  //m_action_q.push(std::make_shared<disable>(GL_DEPTH_TEST));

  m_action_q.push(m_work_fb_0.get_bind_quad_action());
  m_action_q.push(m_work_fb_0.get_bind_action());
  m_action_q.push(m_work_fb_0.get_set_viewport_action());
  m_action_q.push(std::make_shared<render_clear>(GL_COLOR_BUFFER_BIT));

  m_action_q.push(shdr.get_bind_action());
  m_action_q.push(texture::get_set_active_slot_action(0));
  m_action_q.push(std::make_shared<bind_texture>(
    get_gbuffer_tex_ptr(e_gbuff_v4_textures::depth),
    GL_TEXTURE_2D));
  m_action_q.push(shdr.get_set_uniform_texture_caching(
    "u_depth_texture", 0));

  m_action_q.push(shdr.get_set_uniform_mat4_caching(
    "u_invCamToVP", inv_projection));
  m_action_q.push(shdr.get_set_uniform_float_caching(
    "u_factor", edge_detection_factor));
  m_action_q.push(shdr.get_set_uniform_vec2_caching("u_tex_resolution",
    glm::vec2(m_work_fb_0.get_width(), m_work_fb_0.get_height())));

  m_action_q.push(std::make_shared<draw_elements>(6u));

  //m_action_q.push(std::make_shared<enable>(GL_DEPTH_TEST));
}

void effects_pipeline::fill_edge_buffer_depth(float edge_detection_factor, glm::mat4 inv_projection)
{
  shader& shdr = (*m_shaders)[e_shader_type::edge_detection_depth];

  //already disabled
  //m_action_q.push(std::make_shared<disable>(GL_DEPTH_TEST));

  m_action_q.push(m_work_fb_0.get_bind_quad_action());
  m_action_q.push(m_work_fb_0.get_bind_action());
  m_action_q.push(m_work_fb_0.get_set_viewport_action());
  m_action_q.push(std::make_shared<render_clear>(GL_COLOR_BUFFER_BIT));

  m_action_q.push(shdr.get_bind_action());
  m_action_q.push(texture::get_set_active_slot_action(0));
  m_action_q.push(std::make_shared<bind_texture>(
    get_gbuffer_tex_ptr(e_gbuff_v4_textures::depth),
    GL_TEXTURE_2D));
  m_action_q.push(shdr.get_set_uniform_texture_caching(
    "u_depth_texture", 0));

  m_action_q.push(shdr.get_set_uniform_float_caching(
    "u_factor", edge_detection_factor));
  m_action_q.push(shdr.get_set_uniform_vec2_caching("u_tex_resolution",
    glm::vec2(m_work_fb_0.get_width(), m_work_fb_0.get_height())));

  m_action_q.push(std::make_shared<draw_elements>(6u));

  //m_action_q.push(std::make_shared<enable>(GL_DEPTH_TEST));
}

void effects_pipeline::tex_quad_draw(frame_buffer& out_fb, unsigned* in_tex, bool clear)
{
  shader& shdr = (*m_shaders)[e_shader_type::texture_quad];

  m_action_q.push(out_fb.get_bind_action());
  m_action_q.push(out_fb.get_set_viewport_action());
  m_action_q.push(std::make_shared<disable>(GL_DEPTH_TEST));

  if (clear)
    m_action_q.push(std::make_shared<render_clear>(GL_COLOR_BUFFER_BIT));

  m_action_q.push(shdr.get_bind_action());
  m_action_q.push(texture::get_set_active_slot_action(0));
  m_action_q.push(
    std::make_shared<bind_texture>(in_tex, GL_TEXTURE_2D));
  m_action_q.push(
    shdr.get_set_uniform_texture_caching("u_texture", 0));

  m_action_q.push(std::make_shared<draw_elements>(6u));

  m_action_q.push(std::make_shared<enable>(GL_DEPTH_TEST));
}

void effects_pipeline::blur_passes(frame_buffer& out_fb, rend_buffer& internal_fb,
  unsigned* in_tex, float* weights, size_t weights_count)
{
  weight_buffer w_buff;

  assert(weights_count <= w_buff.size());

  for (size_t i = 0u; i < weights_count; i++)
    w_buff[i] = weights[i];


  //already disabled
  //m_action_q.push(std::make_shared<disable>(GL_DEPTH_TEST));

  //HORIZONTAL
  {
    shader& shdr = (*m_shaders)[e_shader_type::blur_x];

    m_action_q.push(internal_fb.get_bind_quad_action());
    m_action_q.push(internal_fb.get_bind_action());
    m_action_q.push(internal_fb.get_set_viewport_action());
    m_action_q.push(std::make_shared<render_clear>(GL_COLOR_BUFFER_BIT));

    m_action_q.push(shdr.get_bind_action());
    m_action_q.push(texture::get_set_active_slot_action(0));
    m_action_q.push(std::make_shared<bind_texture>(in_tex, GL_TEXTURE_2D));
    m_action_q.push(shdr.get_set_uniform_texture_caching(
      "u_texture", 0));
    m_action_q.push(shdr.get_set_uniform_vec2_caching("u_tex_resolution",
      glm::vec2(internal_fb.get_width(), internal_fb.get_height())));
    m_action_q.push(shdr.get_set_uniform_int_caching("u_dim", static_cast<int>(weights_count - 1u)));

    m_action_q.push(shdr.get_set_uniform_float_array_caching(
      "u_weights", w_buff.data(),
      static_cast<unsigned>(w_buff.size())));

    m_action_q.push(std::make_shared<draw_elements>(6u));
  }

  //VERTICAL
  {
    shader& shdr = (*m_shaders)[e_shader_type::blur_y];

    m_action_q.push(out_fb.get_bind_quad_action());
    m_action_q.push(out_fb.get_bind_action());
    m_action_q.push(out_fb.get_set_viewport_action());
    m_action_q.push(std::make_shared<render_clear>(GL_COLOR_BUFFER_BIT));

    m_action_q.push(shdr.get_bind_action());
    m_action_q.push(texture::get_set_active_slot_action(0));
    m_action_q.push(std::make_shared<bind_texture>(internal_fb.get_texture_handle_ptr(), GL_TEXTURE_2D));
    m_action_q.push(shdr.get_set_uniform_texture_caching(
      "u_texture", 0));
    m_action_q.push(shdr.get_set_uniform_vec2_caching("u_tex_resolution",
      glm::vec2(out_fb.get_width(), out_fb.get_height())));
    m_action_q.push(shdr.get_set_uniform_int_caching("u_dim", static_cast<int>(weights_count - 1u)));

    m_action_q.push(shdr.get_set_uniform_float_array_caching(
      "u_weights", w_buff.data(),
      static_cast<unsigned>(w_buff.size())));

    m_action_q.push(std::make_shared<draw_elements>(6u));
  }

  //m_action_q.push(std::make_shared<enable>(GL_DEPTH_TEST));
}

void effects_pipeline::draw_luminance(frame_buffer& out_fb,
  unsigned* in_tex, float luminance_limit, float lumi_lerp_margin)
{
  shader& shdr = (*m_shaders)[e_shader_type::luminance_filter];

  m_action_q.push(out_fb.get_bind_action());
  m_action_q.push(out_fb.get_set_viewport_action());
  m_action_q.push(std::make_shared<disable>(GL_DEPTH_TEST));
  m_action_q.push(std::make_shared<render_clear>(GL_COLOR_BUFFER_BIT));

  m_action_q.push(shdr.get_bind_action());
  m_action_q.push(texture::get_set_active_slot_action(0));
  m_action_q.push(
    std::make_shared<bind_texture>(in_tex, GL_TEXTURE_2D));
  m_action_q.push(
    shdr.get_set_uniform_texture_caching("u_texture", 0));
  m_action_q.push(
    shdr.get_set_uniform_float_caching("u_luminance_lim", luminance_limit));
  m_action_q.push(
    shdr.get_set_uniform_float_caching("u_lumi_lerp_margin", lumi_lerp_margin));

  m_action_q.push(std::make_shared<draw_elements>(6u));

  m_action_q.push(std::make_shared<enable>(GL_DEPTH_TEST));
}

void effects_pipeline::do_LUT(frame_buffer& out_fb,
  unsigned* in_tex, const unsigned* in_LUT)
{
  //set shader
  shader& shdr = (*m_shaders)[e_shader_type::LUT];
  m_action_q.push(shdr.get_bind_action());

  //set frambe buffer
  m_action_q.push(out_fb.get_bind_action());
  m_action_q.push(out_fb.get_set_viewport_action());

  //set non-default options
  m_action_q.push(std::make_shared<disable>(GL_DEPTH_TEST));
  m_action_q.push(std::make_shared<render_clear>(GL_COLOR_BUFFER_BIT));

  //pass LUT 3D texture
  m_action_q.push(texture::get_set_active_slot_action(0));
  m_action_q.push(
    std::make_shared<bind_texture>(in_LUT, GL_TEXTURE_3D));
  m_action_q.push(
    shdr.get_set_uniform_texture_caching("u_LUT", 0));

  //pass in_tex
  m_action_q.push(texture::get_set_active_slot_action(1));
  m_action_q.push(
    std::make_shared<bind_texture>(in_tex, GL_TEXTURE_2D));
  m_action_q.push(
    shdr.get_set_uniform_texture_caching("u_texture", 1));

  //draw
  m_action_q.push(std::make_shared<draw_elements>(6u));

  //restore to defaults
  m_action_q.push(std::make_shared<enable>(GL_DEPTH_TEST));
}

void effects_pipeline::do_inversion(frame_buffer& out_fb, unsigned* in_tex)
{
  shader& shdr = (*m_shaders)[e_shader_type::invert];

  m_action_q.push(out_fb.get_bind_action());
  m_action_q.push(out_fb.get_set_viewport_action());
  m_action_q.push(std::make_shared<disable>(GL_DEPTH_TEST));
  m_action_q.push(std::make_shared<render_clear>(GL_COLOR_BUFFER_BIT));

  m_action_q.push(shdr.get_bind_action());
  m_action_q.push(texture::get_set_active_slot_action(0));
  m_action_q.push(
    std::make_shared<bind_texture>(in_tex, GL_TEXTURE_2D));
  m_action_q.push(
    shdr.get_set_uniform_texture_caching("u_texture", 0));

  m_action_q.push(std::make_shared<draw_elements>(6u));

  m_action_q.push(std::make_shared<enable>(GL_DEPTH_TEST));
}
