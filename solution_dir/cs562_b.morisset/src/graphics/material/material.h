/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    material.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:47:16 2020

\brief  Simple class containing all the data for a material



***************************************************************************/


#pragma once

#include <array>
#include <glm/glm.hpp>

class shader;

enum e_tex_types { ambient, diffuse, specular, emissive, normal, SIZE }; //bump == normal in this case

class material
{
public:

  std::array<int, e_tex_types::SIZE> m_texture_ids = {-1, -1, -1, -1, -1}; //indices of model::texture

  glm::vec3 m_color_a = { 1.0f, 1.0f, 1.0f }; //ambient
  glm::vec3 m_color_d = { 1.0f, 1.0f, 1.0f }; //difuse
  glm::vec3 m_color_s = { 1.0f, 1.0f, 1.0f }; //specular
  glm::vec3 m_color_e = glm::vec3(0);         //emissive

  float m_shininess = 1.0f; //36.0f is a good value
};




