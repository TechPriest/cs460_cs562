/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    GL_CHECK.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:58:56 2020

\brief  Contains 2 macros and the declaration of the function to check
 for OpenGL errors
 go to http://docs.gl to look for the specific function that failed
 to find what the error code means



***************************************************************************/

#include "GL_CHECK.h"

#include <iostream>
#include <glad/glad.h>

namespace
{
  /**
  * converts the openGL error code into its error name (name in the enum of that code)
  * 
  * Note, on http://docs.gl/, you can see what the error means for a particular function
  * @param err_code, the code returned by glGetError()
  * @param the name of the error, or UKNOWN if the code is not on the list
  */
  std::string err_code_to_name(GLenum err_code)
  {
    switch (err_code)
    {
    case 0x0500:
      return "GL_INVALID_ENUM";
    case 0x0501:
      return "GL_INVALID_VALUE";
    case 0x0502:
      return "GL_INVALID_OPERATION";
    case 0x0503:
      return "GL_STACK_OVERFLOW";
    case 0x0504:
      return "GL_STACK_UNDERFLOW";
    case 0x0505:
      return "GL_OUT_OF_MEMORY";
    case 0x0506:
      return "GL_INVALID_FRAMEBUFFER_OPERATION";
    case 0x0507:
      return "GL_CONTEXT_LOST";
    case 0x8031:
      return "GL_TABLE_TOO_LARGE1";
    default:
        return "UNKNOWN";
    }

  }
}

/**
* calls glGetError. Clears errors, then executes stmt then checks for errors.
* Returns true and prints info for each error found error is found.
* Call this function just after a OpenGL function.
* @param stmt
* @param fname, filename
* @param line
* @param return true if error
*/
bool CheckOpenGLError(const char* stmt, const char* fname, int line)
{
  bool output = false;

  GLenum err = glGetError();
  while (err != GL_NO_ERROR)
  {
    printf("\nOpenGL error: %s(0x%04x), at %s:%i - for %s\n",
      err_code_to_name(err).c_str(), err, fname, line, stmt);
    output = true;
    err = glGetError();
  }
  return output;
}

