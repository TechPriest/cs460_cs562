/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    GL_CHECK.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:59:05 2020

\brief  Contains 2 macros and the declaration of the function to check
 for OpenGL errors
 go to http://docs.gl to look for the specific function that failed
 to find what the error code means 



***************************************************************************/


#pragma once


#include <cstdlib> //std::abort

#define GL_CLEAR_ERRORS while(glGetError() != GL_NO_ERROR);

/// USAGE EXAMPLE:
/// GL_CHECK(glGenVertexArrays(1, &VAO));

#define GL_CHECK(stmt) { \
GL_CLEAR_ERRORS \
stmt; \
if(CheckOpenGLError(#stmt, __FILE__, __LINE__)) \
  std::abort();\
}

bool CheckOpenGLError(const char* stmt, const char* fname, int line);

