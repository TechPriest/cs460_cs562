/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    model.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:49:36 2020

\brief  A model contains multiple meshes and a material for each.
Drawing it calls the draw of each its meshes with its apropriate
material.

WARNING: 2 models cannot be loaded at the same time as the texture
vector is shared for both. Solution: mutexes or duplicate textures



***************************************************************************/

#pragma once

#include <vector>
#include <string>
#include <queue>
#include <glm/glm.hpp>
#include "mesh.h"
#include "graphics/material/material.h"
#include "actions/render_actions.h"
#include "actions/load_actions.h"

class shader;
class texture;


class mesh_node
{
public:

  glm::mat4 m_local_TRS = glm::mat4(1);

  std::vector<unsigned> m_mesh_ids;
  std::vector<mesh_node> m_children;
};

class model
{
public:
  std::string m_path = "";

public: //private:

  std::vector<mesh> m_meshes;
  std::vector<material> m_materials;
  std::vector<texture>* m_textures = nullptr;
  bool m_in_GPU = false; //in truth this variable tells us if the get_create_action was called, not if create_shader.do_action() was done.

  mesh_node m_root_node;

public:
  model(const std::string& path, std::vector<texture>* textures);
  ~model();

  inline std::shared_ptr<load_model_assimp> get_load_action()
  {
    return std::make_shared<load_model_assimp>(this);
  }

  inline std::shared_ptr<load_model_assimp> get_assimp_load_action()
  {
    return std::make_shared<load_model_assimp>(this);
  }

  std::queue<std::shared_ptr<action>> get_create_actions();
  std::queue<std::shared_ptr<action>> get_draw_actions(shader& shdr, std::vector<texture>& textures);
  std::queue<std::shared_ptr<action>> get_draw_decal_actions(shader& shdr,
    unsigned diff_tex_idx, unsigned norm_tex_idx, std::vector<texture>& textures);
  std::queue<std::shared_ptr<action>> get_draw_actions_patches(shader& shdr, std::vector<texture>& textures);
  std::queue<std::shared_ptr<action>> get_delete_actions();


/// HELPERS:
private:
  
};


