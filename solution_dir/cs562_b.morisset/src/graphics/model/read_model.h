/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    read_model.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:49:52 2020

\brief  reads the model file and fills the model data with each
of its meshes and materials



***************************************************************************/


#pragma once

#include <array>
#include <vector>
#include <string>
#include "graphics/material/material.h" //can be forward decl

struct aiScene;
struct aiMesh;
class vertex_buffer_layout;
class texture;
class mesh;
class model;


class read_model
{
public:
  static bool read(const std::string& obj_path, model* modl);

private:
  static void make_model(const aiScene* scene, model* modl);
  static void make_mesh(const aiMesh* ai_m, mesh* m);
  static unsigned find_or_add_texture(const std::string& path, std::vector<texture>* textures,
    const std::string& default_tex_path);
};


