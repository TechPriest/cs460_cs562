/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    vertex_buffer_layout.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:50:04 2020

\brief  Usefull tool to simplify passing the correct vertex layout to the vbo



***************************************************************************/


#pragma once

#include <vector>
#include <cassert>

#include <glad/glad.h> //to get opengl enum types like GL_FLOAT

struct layout_element
{
  unsigned start = 0;
  unsigned type; //a opengl enum
  unsigned count;
  unsigned char normalize; //GL_TRUE or GL_FALSE

  static unsigned get_size_of_GLtype(unsigned type)
  {
    switch (type)
    {
    case GL_FLOAT: //intended fallthrough
    case GL_UNSIGNED_INT:
      return 4;
    case GL_UNSIGNED_BYTE:
      return 1;
    }

    assert(false);
    return 0u;
  }
};

class vertex_buffer_layout
{
private:
  std::vector<layout_element> m_elements;
  unsigned m_stride = 0u; //leave at 0 for continuous

public:

  template<typename T>
  void push_interleaved(unsigned count)
  {
    assert(false);
  }

  template<typename T>
  void push_continuous(unsigned start, unsigned count)
  {
    assert(false);
  }

  /*
  * NOT normalized on shader
  */
  template<>
  void push_interleaved<float>(unsigned count)
  {
    m_elements.push_back({ get_start_interleaved(), GL_FLOAT, count, GL_FALSE });
    m_stride += count * layout_element::get_size_of_GLtype(GL_FLOAT);
  }

  /*
  * NOT normalized on shader
  */
  template<>
  void push_continuous<float>(unsigned start, unsigned count)
  {
    m_elements.push_back({ start, GL_FLOAT, count, GL_FALSE });
  }


  /*
  * NOT normalized on shader
  */
  template<>
  void push_interleaved<unsigned>(unsigned count)
  {
    m_elements.push_back({ get_start_interleaved(),GL_UNSIGNED_INT, count, GL_FALSE });
    m_stride += count * layout_element::get_size_of_GLtype(GL_UNSIGNED_INT);
  }

  /*
  * NOT normalized on shader
  */
  template<>
  void push_continuous<unsigned>(unsigned start, unsigned count)
  {
    m_elements.push_back({ start, GL_UNSIGNED_INT, count, GL_FALSE });
  }

  /*
  * normalized on shader
  */
  template<>
  void push_interleaved<unsigned char>(unsigned count)
  {
    m_elements.push_back({ get_start_interleaved(),GL_UNSIGNED_BYTE, count, GL_TRUE });
    m_stride += count * layout_element::get_size_of_GLtype(GL_UNSIGNED_BYTE);
  }

  /*
  * normalized on shader
  */
  template<>
  void push_continuous<unsigned char>(unsigned start, unsigned count)
  {
    m_elements.push_back({ start, GL_UNSIGNED_BYTE, count, GL_TRUE });
  }

  inline const std::vector<layout_element>& get_elements() const { return m_elements; };
  inline unsigned get_stride() const { return m_stride; }

private:
  inline unsigned get_start_interleaved() const
  {
    unsigned output = 0u;

    for (unsigned i = 0u; i < m_elements.size(); i++)
    {
      const layout_element& el = m_elements[i];
      output += el.count * layout_element::get_size_of_GLtype(el.type);
    }

    return output;
  }
};


