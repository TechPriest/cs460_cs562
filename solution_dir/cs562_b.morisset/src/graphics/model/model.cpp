/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    model.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:49:31 2020

\brief  A model contains multiple meshes and a material for each.
Drawing it calls the draw of each its meshes with its apropriate
material.

WARNING: 2 models cannot be loaded at the same time as the texture
vector is shared for both. Solution: mutexes or duplicate textures

***************************************************************************/

#include "model.h"

#include <iostream> //std::cerr

#include "graphics/shader/shader.h"
#include "graphics/texture/texture.h"
#include "other/append_queue.h"


model::model(const std::string & path, std::vector<texture>* textures)
  : m_path(path), m_textures(textures)
{}

model::~model()
{
  if (m_in_GPU)
    std::cerr << "WARNING: model " << m_path << " hasn't been deleted from the GPU" << std::endl;
}

std::queue<std::shared_ptr<action>> model::get_create_actions()
{
  std::queue<std::shared_ptr<action>> output;

  m_in_GPU = true;

  for(mesh& m : m_meshes)
  {
    std::queue<std::shared_ptr<action>> queue =
      m.get_create_actions();

    append_queue(&output, &queue);
  }

  return output;
}

std::queue<std::shared_ptr<action>> model::get_draw_actions(shader& shdr, std::vector<texture>& textures)
{
  std::queue<std::shared_ptr<action>> output;

  for (mesh& m : m_meshes)
  {
    std::queue<std::shared_ptr<action>> queue =
      m.get_draw_actions(shdr, m_materials, textures);

    append_queue(&output, &queue);
  }

  return output;
}

std::queue<std::shared_ptr<action>> model::get_draw_decal_actions(shader& shdr,
  unsigned diff_tex_idx, unsigned norm_tex_idx, std::vector<texture>& textures)
{
  std::queue<std::shared_ptr<action>> output;

  for (mesh& m : m_meshes)
  {
    std::queue<std::shared_ptr<action>> queue = m.get_draw_decal_actions(
      shdr, m_materials, diff_tex_idx, norm_tex_idx, textures);

    append_queue(&output, &queue);
  }

  return output;
}

std::queue<std::shared_ptr<action>> model::get_draw_actions_patches(shader& shdr, std::vector<texture>& textures)
{
  std::queue<std::shared_ptr<action>> output;

  for (mesh& m : m_meshes)
  {
    std::queue<std::shared_ptr<action>> queue = m.get_draw_actions_patches(shdr, m_materials, textures);

    append_queue(&output, &queue);
  }

  return output;
}


std::queue<std::shared_ptr<action>> model::get_delete_actions()
{
  std::queue<std::shared_ptr<action>> output;

  for (mesh& m : m_meshes)
  {
    std::queue<std::shared_ptr<action>> queue = m.get_delete_actions();

    append_queue(&output, &queue);
  }

  m_in_GPU = false;

  return output;
}

