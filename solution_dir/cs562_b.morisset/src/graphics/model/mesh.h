/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    mesh.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:49:25 2020

\brief  A mesh is a part of a model that has only one material. It contains functions
to draw it




***************************************************************************/

#pragma once

#include <array>
#include <vector>
#include <queue>
#include <memory>

#include "vertex_buffer_layout.h"

class material;
class shader;
class texture;
class action;

class mesh
{
public:
  std::vector<float> m_data; //TODO: should be a byte buffer
  std::vector<unsigned> m_indices; //TODO: maybe this one too
  vertex_buffer_layout m_layout;

  unsigned m_vao = 0;
  unsigned m_vbo = 0; // only needed to delete
  unsigned m_ibo = 0; // only needed to delete

  unsigned m_mode = 0; //GL_TRIANGLES
  unsigned m_matterial_idx = 0; //index in model::m_materials

  std::queue<std::shared_ptr<action>> get_create_actions();
  std::queue<std::shared_ptr<action>> get_draw_actions(shader& shdr,
    std::vector<material>& materials, const std::vector<texture>& textures);
  std::queue<std::shared_ptr<action>> get_draw_decal_actions(shader& shdr,
    std::vector<material>& materials, unsigned diff_tex_idx,
    unsigned norm_tex_idx, std::vector<texture>& textures);
  std::queue<std::shared_ptr<action>> get_draw_actions_patches(shader& shdr,
    std::vector<material>& materials, const std::vector<texture>& textures);
  std::queue<std::shared_ptr<action>> get_delete_actions();

  std::queue<std::shared_ptr<action>> get_draw_with_custom_material_actions(shader& shdr,
    const material& material, const std::vector<texture>& textures);
  std::queue<std::shared_ptr<action>> get_draw_with_custom_material_actions_patches(shader& shdr,
    const material& material, const std::vector<texture>& textures);
  std::queue<std::shared_ptr<action>> get_draw_no_material_actions(shader& shdr);
  //std::queue<std::shared_ptr<action>> get_draw_no_material_actions_patches(shader& shdr);
};

