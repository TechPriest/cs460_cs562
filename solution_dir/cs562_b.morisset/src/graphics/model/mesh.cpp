/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    mesh.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:49:17 2020

\brief  A mesh is a part of a model that has only one material. It contains functions
to draw it



***************************************************************************/


#include "mesh.h"

#include "graphics/material/material.h"
#include "graphics/shader/shader.h"
#include "graphics/texture/texture.h"
#include "actions/render_actions.h"

std::queue<std::shared_ptr<action>> mesh::get_create_actions()
{
  std::queue<std::shared_ptr<action>> output;

  output.push(std::make_shared<create_vertex_array>(&m_vao)); //does not bind
  output.push(std::make_shared<bind_vertex_array>(&m_vao));
  output.push(std::make_shared<create_vertex_buffer>(&m_vbo, m_data.data(),
    static_cast<unsigned>(m_data.size() * sizeof(float)), GL_STATIC_DRAW)); //binds
  output.push(std::make_shared<set_vertex_buffer_layout>(m_layout));
  output.push(std::make_shared<create_index_buffer>(&m_ibo, m_indices.data(),
    static_cast<unsigned>(m_indices.size() * sizeof(unsigned)), GL_STATIC_DRAW)); //binds
  
  return output;
}

std::queue<std::shared_ptr<action>> mesh::get_draw_actions(shader& shdr,
  std::vector<material>& materials, const std::vector<texture>& textures)
{
  std::queue<std::shared_ptr<action>> output;

  output = shdr.get_set_material_uniforms_actions(materials[m_matterial_idx], textures);

  //actual draw call:
  output.push(std::make_shared<bind_vertex_array>(&m_vao));

  output.push(std::make_shared<draw_elements>(static_cast<unsigned>(m_indices.size())));

  return output;
}

std::queue<std::shared_ptr<action>> mesh::get_draw_decal_actions(shader& shdr,
  std::vector<material>& materials, unsigned diff_tex_idx, unsigned norm_tex_idx, std::vector<texture>& textures)
{
  std::queue<std::shared_ptr<action>> output;

  output = shdr.get_set_material_for_decal_uniforms_actions(
    materials[m_matterial_idx], diff_tex_idx, norm_tex_idx, textures);

  //actual draw call:
  output.push(std::make_shared<bind_vertex_array>(&m_vao));

  output.push(std::make_shared<draw_elements>(static_cast<unsigned>(m_indices.size())));

  return output;
}

std::queue<std::shared_ptr<action>> mesh::get_draw_actions_patches(shader& shdr,
  std::vector<material>& materials, const std::vector<texture>& textures)
{
  std::queue<std::shared_ptr<action>> output;

  output = shdr.get_set_material_uniforms_actions(materials[m_matterial_idx], textures);

  //actual draw call:
  output.push(std::make_shared<bind_vertex_array>(&m_vao));

  output.push(std::make_shared<draw_patches>(static_cast<unsigned>(m_indices.size()), 3));

  return output;
}

std::queue<std::shared_ptr<action>> mesh::get_delete_actions()
{
  std::queue<std::shared_ptr<action>> output;

  output.push(std::make_shared<delete_index_buffer>(&m_ibo));
  output.push(std::make_shared<delete_vertex_buffer>(&m_vbo));
  output.push(std::make_shared<delete_vertex_array>(&m_vao));

  return output;
}

std::queue<std::shared_ptr<action>> mesh::get_draw_with_custom_material_actions(
  shader& shdr, const material& material, const std::vector<texture>& textures)
{
  std::queue<std::shared_ptr<action>> output;

  output = shdr.get_set_material_uniforms_actions(material, textures);

  //actual draw call:
  output.push(std::make_shared<bind_vertex_array>(&m_vao));
  output.push(std::make_shared<draw_elements>(static_cast<unsigned>(m_indices.size())));

  return output;
}

std::queue<std::shared_ptr<action>> mesh::get_draw_with_custom_material_actions_patches(
  shader& shdr, const material& material, const std::vector<texture>& textures)
{
  std::queue<std::shared_ptr<action>> output;

  output = shdr.get_set_material_uniforms_actions(material, textures);

  //actual draw call:
  output.push(std::make_shared<bind_vertex_array>(&m_vao));
  output.push(std::make_shared<draw_patches>(static_cast<unsigned>(m_indices.size()), 3));

  return output;
}

std::queue<std::shared_ptr<action>> mesh::get_draw_no_material_actions(shader& shdr)
{
  std::queue<std::shared_ptr<action>> output;

  output.push(std::make_shared<bind_vertex_array>(&m_vao));
  output.push(std::make_shared<draw_elements>(static_cast<unsigned>(m_indices.size())));

  return output;
}

