/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    read_model.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:49:45 2020

\brief  reads the model file and fills the model data with each
of its meshes and materials


***************************************************************************/

//examples: http://assimp.sourceforge.net/lib_html/usage.html

#include "read_model.h"

#include <iostream> //std::cerr
#include <sstream> //std::cerr
#include <fstream> //std::cerr
#include <algorithm> //std::find
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags
#include "vertex_buffer_layout.h"
#include "graphics/material/material.h"
#include "graphics/texture/texture.h"
#include "graphics/model/model.h"


/*namespace testing
{
  std::stringstream objStream(
    "mtllib cube.mtl\n"
    "\n"
    "v 0.000000 2.000000 2.000000\n"
    "v 0.000000 0.000000 2.000000\n"
    "v 2.000000 0.000000 2.000000\n"
    "v 2.000000 2.000000 2.000000\n"
    "v 0.000000 2.000000 0.000000\n"
    "v 0.000000 0.000000 0.000000\n"
    "v 2.000000 0.000000 0.000000\n"
    "v 2.000000 2.000000 0.000000\n"
    "# 8 vertices\n"
    "\n"
    "g front cube\n"
    "usemtl white\n"
    "f 1 2 3 4\n"
    "g back cube\n"
    "# expects white material\n"
    "f 8 7 6 5\n"
    "g right cube\n"
    "usemtl red\n"
    "f 4 3 7 8\n"
    "g top cube\n"
    "usemtl white\n"
    "f 5 1 4 8\n"
    "g left cube\n"
    "usemtl green\n"
    "f 5 6 2 1\n"
    "g bottom cube\n"
    "usemtl white\n"
    "f 2 6 7 3\n"
    "# 6 elements");

  std::string matStream(
    "newmtl white\n"
    "Ka 0 0 0\n"
    "Kd 1 1 1\n"
    "Ks 0 0 0\n"
    "\n"
    "newmtl red\n"
    "Ka 0 0 0\n"
    "Kd 1 0 0\n"
    "Ks 0 0 0\n"
    "\n"
    "newmtl green\n"
    "Ka 0 0 0\n"
    "Kd 0 1 0\n"
    "Ks 0 0 0\n"
    "\n"
    "newmtl blue\n"
    "Ka 0 0 0\n"
    "Kd 0 0 1\n"
    "Ks 0 0 0\n"
    "\n"
    "newmtl light\n"
    "Ka 20 20 20\n"
    "Kd 1 1 1\n"
    "Ks 0 0 0");
}*/

namespace
{
  /**
  * Loads shader code into memory
  * @param path,
  * @return file content stream
  */
  std::stringstream LoadFile(const std::string& path)
  {
    std::ifstream File;

    // open files
    File.open(path);
    assert(File);

    // read file's buffer contents into streams
    std::stringstream sstream;
    sstream << File.rdbuf();
    // close file handlers
    File.close();
    // convert stream into c_string
    return sstream;
  }
}



bool read_model::read(const std::string & obj_path, model* modl)
{
  // Create an instance of the Importer class
  Assimp::Importer importer;
  // And have it read the given file with some example postprocessing
  // Usually - if speed is not the most important aspect for you - you'll 
  // propably to request more postprocessing than we do in this example.
  const aiScene* scene = importer.ReadFile(obj_path,
    aiProcess_CalcTangentSpace |
    aiProcess_Triangulate |

    aiProcess_JoinIdenticalVertices |
    aiProcess_RemoveRedundantMaterials |
    aiProcess_ImproveCacheLocality | //reorders triangles, specially usefull for large meshes
    aiProcess_OptimizeMeshes | // this joins meshes into one reducing the number of draw calls
    aiProcess_OptimizeGraph  //| // effective when using with the above

    //aiProcess_SortByPType | //This step splits meshes with more than one primitive type (triangles, points, lines) in homogeneous sub-meshes
    );

  // If the import failed, report it
  if (!scene)
  {
    std::cerr << "ASSIMP LOADING ERROR: " << importer.GetErrorString() << std::endl;
    return false;
  }
  // Now we can access the file's contents. 
  make_model(scene, modl);
  // We're done. Everything will be cleaned up by the importer destructor
  return true;
}


namespace
{
  inline glm::mat4 to_mat4(const aiMatrix4x4& m)
  {
    return
    {
      m.a1, m.a2, m.a3, m.a4,
      m.b1, m.b2, m.b3, m.b4,
      m.c1, m.c2, m.c3, m.c4,
      m.d1, m.d2, m.d3, m.d4
    };
  }


  mesh_node build_node_rec(const aiNode* ai_n)
  {
    mesh_node n; //output

    n.m_local_TRS = to_mat4(ai_n->mTransformation);

    n.m_children.resize(ai_n->mNumChildren);
    for (unsigned i = 0u; i < ai_n->mNumChildren; i++)
      n.m_children[i] = build_node_rec(ai_n->mChildren[i]);

    return n;
  }

}

void read_model::make_model(const aiScene * scene, model * modl)
{

  modl->m_meshes.resize(scene->mNumMeshes);

  //create meshes
  for (unsigned i = 0u; i < scene->mNumMeshes; i++)
  {
    const aiMesh* ai_m = scene->mMeshes[i];
    mesh& m = modl->m_meshes[i];

    make_mesh(ai_m, &m);
  }
  
  //create materials
  for (unsigned i = 0u; i < scene->mNumMaterials; i++)
  {
    const aiMaterial& ai_mat = *(scene->mMaterials[i]);
    modl->m_materials.emplace_back();
    material& mat = modl->m_materials.back();

    {
      {
        aiColor3D ambient_k(0.1f);
        aiColor3D diffuse_k(0.8f);
        aiColor3D specular_k(1.0f);
        aiColor3D emissive_k(0.0f);

        ai_mat.Get(AI_MATKEY_COLOR_AMBIENT, ambient_k);
        ai_mat.Get(AI_MATKEY_COLOR_DIFFUSE, diffuse_k);
        ai_mat.Get(AI_MATKEY_COLOR_SPECULAR, specular_k);
        ai_mat.Get(AI_MATKEY_COLOR_EMISSIVE, emissive_k);
        ai_mat.Get(AI_MATKEY_SHININESS, mat.m_shininess);

        mat.m_color_a = { ambient_k.r, ambient_k.g, ambient_k.b };
        mat.m_color_d = { diffuse_k.r, diffuse_k.g, diffuse_k.b };
        mat.m_color_s = { specular_k.r, specular_k.g, specular_k.b };
        mat.m_color_e = { emissive_k.r, emissive_k.g, emissive_k.b };
      }

      //textures
      {
      aiString tex_path_a, tex_path_d, tex_path_s, tex_path_e, tex_path_n;
      ai_mat.GetTexture(aiTextureType_AMBIENT, 0, &tex_path_a);
      ai_mat.GetTexture(aiTextureType_DIFFUSE, 0, &tex_path_d);
      ai_mat.GetTexture(aiTextureType_SPECULAR, 0, &tex_path_s);
      ai_mat.GetTexture(aiTextureType_EMISSIVE, 0, &tex_path_e);
      ai_mat.GetTexture(aiTextureType_HEIGHT, 0, &tex_path_n);

      mat.m_texture_ids[e_tex_types::ambient] =
        find_or_add_texture(tex_path_a.C_Str(), modl->m_textures, "./data/textures/default.png");
      mat.m_texture_ids[e_tex_types::diffuse] =
        find_or_add_texture(tex_path_d.C_Str(), modl->m_textures, "./data/textures/default.png");
      mat.m_texture_ids[e_tex_types::specular] =
        find_or_add_texture(tex_path_s.C_Str(), modl->m_textures, "./data/textures/default.png");
      mat.m_texture_ids[e_tex_types::emissive] =
        find_or_add_texture(tex_path_e.C_Str(), modl->m_textures, "./data/textures/default.png");
      mat.m_texture_ids[e_tex_types::normal] =
        find_or_add_texture(tex_path_n.C_Str(), modl->m_textures, "./data/textures/default_ddn.png");
      }
    }
  }

  //nodes
  modl->m_root_node = build_node_rec(scene->mRootNode);
}

void read_model::make_mesh(const aiMesh * ai_m, mesh * m)
{
  //attributes (data)
  {
    const unsigned num_verts = ai_m->mNumVertices;

    //resize data
    {
      unsigned size = num_verts * 3u; //pos
      size += ai_m->HasNormals() ? (num_verts * 3u) : 0u;
      size += ai_m->HasTangentsAndBitangents() ? (num_verts * 6u) : 0u;
      size += ai_m->HasTextureCoords(0) ? (num_verts * 2u) : 0u;
      size += ai_m->HasVertexColors(0) ? (num_verts * 4u) : 0u;

      m->m_data.resize(size);
    }

    size_t prev_count = 0u;

    //positions
    {
      m->m_layout.push_continuous<float>(
        static_cast<unsigned>(prev_count * sizeof(float)), 3u); 
      for (unsigned i = 0; i < num_verts; i++)
      {
        const aiVector3D& pos = ai_m->mVertices[i];

        m->m_data[prev_count + 3u * i] = pos.x;
        m->m_data[prev_count + 3u * i + 1] = pos.y;
        m->m_data[prev_count + 3u * i + 2] = pos.z;
      }
      prev_count += num_verts * 3u;
    }

    //normals
    if (ai_m->HasNormals())
    {
      m->m_layout.push_continuous<float>(
        static_cast<unsigned>(prev_count * sizeof(float)), 3u);
      for (unsigned i = 0; i < num_verts; i++)
      {
        const aiVector3D& norm = ai_m->mNormals[i];

        m->m_data[prev_count + 3u * i] = norm.x;
        m->m_data[prev_count + 3u * i + 1] = norm.y;
        m->m_data[prev_count + 3u * i + 2] = norm.z;
      }
      prev_count += num_verts * 3u;
    }

    //tangent & bitangent
    if (ai_m->HasTangentsAndBitangents())
    {
      m->m_layout.push_continuous<float>(
        static_cast<unsigned>(prev_count * sizeof(float)), 3u);
      for (unsigned i = 0; i < num_verts; i++)
      {
        const aiVector3D& tan = ai_m->mTangents[i];

        m->m_data[prev_count + 3u * i] = tan.x;
        m->m_data[prev_count + 3u * i + 1u] = tan.y;
        m->m_data[prev_count + 3u * i + 2u] = tan.z;
      }
      prev_count += num_verts * 3u;

      m->m_layout.push_continuous<float>(
        static_cast<unsigned>(prev_count * sizeof(float)), 3u);
      for (unsigned i = 0; i < num_verts; i++)
      {
        const aiVector3D& bitan = ai_m->mBitangents[i];

        m->m_data[prev_count + 3u * i] = bitan.x;
        m->m_data[prev_count + 3u * i + 1u] = bitan.y;
        m->m_data[prev_count + 3u * i + 2u] = bitan.z;
      }
      prev_count += num_verts * 3u;
    }

    //uvs I HARDCODE IT TO UV (not UVW or anything else)
    if (ai_m->HasTextureCoords(0))
    {
      m->m_layout.push_continuous<float>(
        static_cast<unsigned>(prev_count * sizeof(float)), 2u);
      for (unsigned i = 0; i < num_verts; i++)
      {
        const aiVector3D& uv = ai_m->mTextureCoords[0][i];

        m->m_data[prev_count + 2u * i] = uv.x;
        m->m_data[prev_count + 2u * i + 1] = uv.y;
      }
      prev_count += num_verts * 2u;
    }

    //colors
    if (ai_m->HasVertexColors(0))
    {
      m->m_layout.push_continuous<float>(
        static_cast<unsigned>(prev_count * sizeof(float)), 4u);
      for (unsigned i = 0; i < num_verts; i++)
      {
        const aiColor4D& col = ai_m->mColors[i][0];

        m->m_data[prev_count + 4u * i] = col.r;
        m->m_data[prev_count + 4u * i + 1u] = col.g;
        m->m_data[prev_count + 4u * i + 2u] = col.b;
        m->m_data[prev_count + 4u * i + 3u] = col.a;
      }
      prev_count += num_verts * 4u;
    }
  }

  //indices
  for (unsigned i = 0; i<ai_m->mNumFaces; i++)
  {
    const aiFace& face = ai_m->mFaces[i];

    assert(face.mNumIndices == 3u); //triangulation should take care of this

    m->m_indices.push_back(face.mIndices[0u]);
    m->m_indices.push_back(face.mIndices[1u]);
    m->m_indices.push_back(face.mIndices[2u]);
  }

  //material idx
  m->m_matterial_idx = ai_m->mMaterialIndex;
}

unsigned read_model::find_or_add_texture(const std::string& path, std::vector<texture>* textures,
  const std::string& default_tex_path)
{
  std::string pth;

  if (path == "")
    pth = default_tex_path;
  else
    pth = path;

  unsigned output = -1;

  std::vector<texture>::const_iterator found =
    std::find(textures->cbegin(), textures->cend(), pth);

  if (found != textures->cend())
  {
    output = static_cast<unsigned>(found - textures->cbegin());
  }
  else
  {
    output = static_cast<unsigned>(textures->size());
    textures->emplace_back(pth);
  }

  return output;
}



