/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file   occlusion_settings.h

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu

\par    DigiPen login: b.morisset

\par    Course: CS562

\par    Assignment #3

\date   Wed Nov 18 2020

\brief  Simple class containing all occlusion settings passed to gui

***************************************************************************/

#pragma once

#include <vector>
#include "glm/glm.hpp"

class occlusion_settings
{
public:
  enum occ_type {paper, mine, mine_opti};

  std::vector<glm::vec2> dirs; //in screen space
  float att = 1.0f;
  float scale = 1.0f;
  float radius = 1.0f; //in cam space
  int step_count = 11;
  float min_angle = 2.0f; //[rad] angle from normal
  occ_type type = occ_type::mine_opti;

  //bilateral filter
  float dist_sigma = 1.5f;
  float depth_sigma = 0.001f;
  int dist_kernel_size = 5; //no-duplicates. so 5 corresponds to 1+2*(5-1)= 9

  void set_dirs(size_t count);
};
