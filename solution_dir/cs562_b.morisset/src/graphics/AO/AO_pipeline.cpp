/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    AO_pipeline.cpp

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu

\par    DigiPen login: b.morisset

\par    Course: CS562

\par    Assignment #4

\date    Mon Nov 23 2020

\brief  A class that takes the depth result of the geometry pass and
computes with it the Screen Space Horizon Based Ambient Occlusion

***************************************************************************/

#include "AO_pipeline.h"

#include "graphics/texture/texture.h"
#include "graphics/window/window.h"
#include "graphics/shader/shader.h"
#include "actions/gui_actions.h"
#include "other/append_queue.h"
#include "scenes/lvl_shader_types.h"
#include "occlusion_settings.h"


std::queue<std::shared_ptr<action>> AO_pipeline::get_create_actions(
  float* quad_vertices, unsigned* quad_indices)
{
  std::queue<std::shared_ptr<action>> output;
  std::queue<std::shared_ptr<action>> q;

  q = m_work_fb_0.get_create_actions(quad_vertices, quad_indices);
  append_queue(&output, &q);
  q = m_work_fb_1.get_create_actions(quad_vertices, quad_indices);
  append_queue(&output, &q);
  q = m_work_fb_2.get_create_actions(quad_vertices, quad_indices);
  append_queue(&output, &q);

  return output;
}

std::queue<std::shared_ptr<action>> AO_pipeline::get_delete_actions()
{
  std::queue<std::shared_ptr<action>> output;
  std::queue<std::shared_ptr<action>> q;

  q = m_work_fb_0.get_delete_actions();
  append_queue(&output, &q);
  q = m_work_fb_1.get_delete_actions();
  append_queue(&output, &q);
  q = m_work_fb_2.get_delete_actions();
  append_queue(&output, &q);

  return output;
}

std::queue<std::shared_ptr<action>>
  AO_pipeline::get_set_resolution_actions(int width, int height)
{
  std::queue<std::shared_ptr<action>> output;
  std::queue<std::shared_ptr<action>> q;

  q = m_work_fb_0.get_set_resolution_actions(width, height);
  append_queue(&output, &q);
  q = m_work_fb_1.get_set_resolution_actions(width, height);
  append_queue(&output, &q);
  q = m_work_fb_2.get_set_resolution_actions(width, height);
  append_queue(&output, &q);

  return output;
}

std::queue<std::shared_ptr<action>> AO_pipeline::get_AO_actions(
  glm::mat4 projection, glm::mat4 inv_projection,
  const occlusion_settings& occl_setts)
{
  std::queue<std::shared_ptr<action>> output =
    draw_occlusion(m_work_fb_0, projection, inv_projection,
      m_depth_tex, occl_setts);

  //x (0)
  std::queue<std::shared_ptr<action>> q =
    bilateral_filter(
      m_work_fb_1, m_work_fb_0.get_texture_handle_ptr(),
      m_depth_tex, occl_setts, 0);
  append_queue(&output, &q);

  //y (1)
  q =
    bilateral_filter(
      m_work_fb_2, m_work_fb_1.get_texture_handle_ptr(),
      m_depth_tex, occl_setts, 1);
  append_queue(&output, &q);

  output.push(std::make_shared<disable>(GL_DEPTH_TEST));
  
  return output;
}

std::queue<std::shared_ptr<action>> AO_pipeline::get_unfiltered_AO_actions(
  glm::mat4 projection, glm::mat4 inv_projection,
  const occlusion_settings& occl_setts)
{
  return draw_occlusion(m_work_fb_0, projection, inv_projection,
    m_depth_tex, occl_setts);
}

std::queue<std::shared_ptr<action>> AO_pipeline::draw_occlusion(
  frame_buffer& out_fb, glm::mat4 projection, glm::mat4 inv_projection,
  unsigned* in_tex, const occlusion_settings& occl_setts)
{
  std::queue<std::shared_ptr<action>> output;


  shader& shdr = (*m_shaders)[e_shader_type::AO + occl_setts.type];
  output.push(shdr.get_bind_action());

  output.push(out_fb.get_bind_quad_action());

  output.push(out_fb.get_bind_action());
  output.push(out_fb.get_set_viewport_action());
  output.push(std::make_shared<disable>(GL_DEPTH_TEST));
  output.push(std::make_shared<render_clear>(GL_COLOR_BUFFER_BIT));

  output.push(shdr.get_bind_action());
  output.push(texture::get_set_active_slot_action(0));
  output.push(
    std::make_shared<bind_texture>(in_tex, GL_TEXTURE_2D));
  output.push(
    shdr.get_set_uniform_texture_caching("u_texture", 0));
  output.push(texture::get_set_active_slot_action(1));
  output.push(
    std::make_shared<bind_texture>((*m_textures)[m_noise_tex_idx].get_handle_ptr(), GL_TEXTURE_2D));
  output.push(
    shdr.get_set_uniform_texture_caching("u_noise_tex", 1));
  output.push(
    shdr.get_set_uniform_mat4_caching("u_CamToVP", projection));
  output.push(
    shdr.get_set_uniform_mat4_caching("u_invCamToVP", inv_projection));

  const unsigned dirs_size = static_cast<unsigned>(occl_setts.dirs.size());

  output.push(
    shdr.get_set_uniform_vec_array_caching("u_dirs", occl_setts.dirs.data(), dirs_size));
  output.push(
    shdr.get_set_uniform_int_caching("u_dir_count", dirs_size));
  output.push(
    shdr.get_set_uniform_float_caching("u_att", occl_setts.att));
  output.push(
    shdr.get_set_uniform_float_caching("u_scale", occl_setts.scale));
  output.push(
    shdr.get_set_uniform_float_caching("u_radius", occl_setts.radius));
  output.push(
    shdr.get_set_uniform_int_caching("u_step_count", occl_setts.step_count));
  output.push(
    shdr.get_set_uniform_float_caching("u_angle_lim", occl_setts.min_angle));

  output.push(std::make_shared<draw_elements>(6u));

  output.push(std::make_shared<enable>(GL_DEPTH_TEST));


  return output;
}

std::queue<std::shared_ptr<action>> AO_pipeline::bilateral_filter(
  frame_buffer& out_fb, unsigned* in_tex, unsigned* intensity_tex,
  const occlusion_settings& occl_setts, size_t dir)
{
  std::queue<std::shared_ptr<action>> output;


  shader& shdr = (*m_shaders)[e_shader_type::bilateral_filter_x + dir];
  output.push(shdr.get_bind_action());

  output.push(out_fb.get_bind_quad_action());

  output.push(out_fb.get_bind_action());
  output.push(out_fb.get_set_viewport_action());
  output.push(std::make_shared<disable>(GL_DEPTH_TEST));
  output.push(std::make_shared<render_clear>(GL_COLOR_BUFFER_BIT));

  output.push(texture::get_set_active_slot_action(0));
  output.push(
    std::make_shared<bind_texture>(in_tex, GL_TEXTURE_2D));
  output.push(
    shdr.get_set_uniform_texture_caching("u_texture", 0));

  output.push(texture::get_set_active_slot_action(1));
  output.push(
    std::make_shared<bind_texture>(intensity_tex, GL_TEXTURE_2D));
  output.push(
    shdr.get_set_uniform_texture_caching("u_intensity_tex", 1));

  output.push(
    shdr.get_set_uniform_int_caching("u_N", occl_setts.dist_kernel_size));

  output.push(
    shdr.get_set_uniform_float_caching("u_dist_sigma", occl_setts.dist_sigma));

  output.push(
    shdr.get_set_uniform_float_caching("u_intensity_sigma", occl_setts.depth_sigma));


  output.push(std::make_shared<draw_elements>(6u));

  output.push(std::make_shared<enable>(GL_DEPTH_TEST));


  return output;
}

/*std::queue<std::shared_ptr<action>> AO_pipeline::bilateral_filter_old(
  frame_buffer& out_fb, unsigned* in_tex, unsigned* intensity_tex,
  const occlusion_settings& occl_setts)
{
  std::queue<std::shared_ptr<action>> output;


  shader& shdr = (*m_shaders)[e_shader_type::bilateral_filter];
  output.push(shdr.get_bind_action());

  output.push(out_fb.get_bind_quad_action());

  output.push(out_fb.get_bind_action());
  output.push(out_fb.get_set_viewport_action());
  output.push(std::make_shared<disable>(GL_DEPTH_TEST));
  output.push(std::make_shared<render_clear>(GL_COLOR_BUFFER_BIT));

  output.push(texture::get_set_active_slot_action(0));
  output.push(
    std::make_shared<bind_texture>(in_tex, GL_TEXTURE_2D));
  output.push(
    shdr.get_set_uniform_texture_caching("u_texture", 0));

  output.push(texture::get_set_active_slot_action(1));
  output.push(
    std::make_shared<bind_texture>(intensity_tex, GL_TEXTURE_2D));
  output.push(
    shdr.get_set_uniform_texture_caching("u_intensity_tex", 1));

  output.push(
    shdr.get_set_uniform_int_caching("u_N", occl_setts.dist_kernel_size));

  output.push(
    shdr.get_set_uniform_float_caching("u_dist_sigma", occl_setts.dist_sigma));

  output.push(
    shdr.get_set_uniform_float_caching("u_intensity_sigma", occl_setts.depth_sigma));


  output.push(std::make_shared<draw_elements>(6u));

  output.push(std::make_shared<enable>(GL_DEPTH_TEST));


  return output;
}*/
