/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    AO_pipeline.h

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu

\par    DigiPen login: b.morisset

\par    Course: CS562

\par    Assignment #4

\date    Mon Nov 23 2020

\brief  A class that takes the depth result of the geometry pass and
computes with it the Screen Space Horizon Based Ambient Occlusion

***************************************************************************/

#pragma once

#include <queue>
#include <memory> //shared_ptr
#include <bitset>
#include "graphics/framebuffers/g_buffer.h"
#include "graphics/shader/shader.h"
#include "graphics/texture/texture.h"

class action;
class window;
class occlusion_settings;

class AO_pipeline
{
private:
  unsigned* m_depth_tex = nullptr; //handle
  unsigned m_noise_tex_idx = 2u; //index of scene's textures vector

  rend_buffer m_work_fb_0;
  rend_buffer m_work_fb_1;
  rend_buffer m_work_fb_2;

  window* m_window = nullptr;
  std::vector<shader>* m_shaders;
  std::vector<texture>* m_textures;

public:

  inline AO_pipeline(
    unsigned* depth_tex,
    unsigned noise_tex_idx,
    std::vector<shader>* shaders,
    std::vector<texture>* textures
  )
    : m_depth_tex(depth_tex),
      m_noise_tex_idx(noise_tex_idx),
      m_work_fb_0(GL_LINEAR, GL_LINEAR), //GL_NEAREST
      m_work_fb_1(GL_LINEAR, GL_LINEAR),
      m_work_fb_2(GL_LINEAR, GL_LINEAR),
      m_shaders(shaders),
      m_textures(textures)
  {}

  std::queue<std::shared_ptr<action>> get_create_actions(
    float* quad_vertices, unsigned* quad_indices);
  std::queue<std::shared_ptr<action>> get_delete_actions();

  std::queue<std::shared_ptr<action>> get_set_resolution_actions(int width, int height);

  std::queue<std::shared_ptr<action>> get_AO_actions(
    glm::mat4 projection, glm::mat4 inv_projection,
    const occlusion_settings& occl_setts);

  std::queue<std::shared_ptr<action>> get_unfiltered_AO_actions(
    glm::mat4 projection, glm::mat4 inv_projection,
    const occlusion_settings& occl_setts);

  inline const unsigned* get_final_texture() const
  {
    return m_work_fb_2.get_texture_handle_ptr();
  }
  inline const unsigned* get_unfiltered_texture() const
  {
    return m_work_fb_0.get_texture_handle_ptr();
  }

private:

  std::queue<std::shared_ptr<action>> draw_occlusion(frame_buffer& out_fb,
    glm::mat4 projection, glm::mat4 inv_projection,
    unsigned* in_tex, const occlusion_settings& occl_setts);

  std::queue<std::shared_ptr<action>> bilateral_filter(frame_buffer& out_fb,
    unsigned* in_tex, unsigned* intensity_tex,
    const occlusion_settings& occl_setts, size_t dir);

  /*std::queue<std::shared_ptr<action>> bilateral_filter_old(frame_buffer& out_fb,
    unsigned* in_tex, unsigned* intensity_tex,
    const occlusion_settings& occl_setts);*/

};