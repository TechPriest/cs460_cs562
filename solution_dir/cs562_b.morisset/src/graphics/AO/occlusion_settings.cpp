/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file   occlusion_settings.h

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu

\par    DigiPen login: b.morisset

\par    Course: CS562

\par    Assignment #3

\date   Wed Nov 18 2020

\brief  Simple class containing all occlusion settings passed to gui

***************************************************************************/

#include "occlusion_settings.h"

#include <glm/gtc/constants.hpp>

void occlusion_settings::set_dirs(size_t count)
{
  dirs.clear();
  dirs.resize(count);

  const float angle_incr = 2.0f*glm::pi<float>() / count;

  dirs[0] = glm::vec2(1.0f,0.0f);

  for (size_t i = 1u; i < count -0u; i++)
  {
    const float angle = i * angle_incr;
    dirs[i] = glm::mat2(cosf(angle), -sinf(angle), sinf(angle), cosf(angle)) * dirs[0];
  }
}
