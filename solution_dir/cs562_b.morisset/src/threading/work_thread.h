/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    work_thread.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:56:28 2020

\brief  object used as a way to pass actions safelly to a thread. Uses an action queue.



***************************************************************************/


#pragma once

#include <queue>
#include <memory>

#include "actions/action.h"

//for starts
#include "time/chronometer.h"
#include "other/history.h"

class work_thread
{
private:
  std::queue<std::shared_ptr<action>> m_action_queue_0; //filled by main thread
  std::queue<std::shared_ptr<action>> m_action_queue_1; //read by renderer thread

  bool m_q1_for_renderer = false; //set to true by main thread, read and set to false by renderer thread
  bool m_should_close = false; //set to true by main thread, read by renderer thread

  history<unsigned, 120u> m_q_time_his; //it is the time between last q start and this q start

public:

  /// MAIN THREAD:
  void begin_pushing();
  void push_action(const std::shared_ptr<action> action);
  void append_actions(std::queue<std::shared_ptr<action>> queue);
  void end_pushing(); //blocking as it needs to wait for q1 to be free before starting to copy
  void wait_for() const;
  inline void close() { m_should_close = true; }

  inline const history<unsigned, 120u>* get_q_time_us_his_ptr() const { return &m_q_time_his; }
  inline void clear_history() { m_q_time_his.clear(); }
  
  /// RENDER THREAD:
  void work();

private:
  void perform_actions();
};


