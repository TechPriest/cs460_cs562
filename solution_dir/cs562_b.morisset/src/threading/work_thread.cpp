/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    work_thread.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:56:16 2020

\brief  object used as a way to pass actions safelly to a thread. Uses an action queue.



***************************************************************************/


#include "work_thread.h"

#include <chrono>
#include <thread>

using namespace std::chrono_literals;

namespace
{
  constexpr auto SLEEP_DURATION = 200us; //should be tunned
}

void work_thread::begin_pushing()
{
  std::queue<std::shared_ptr<action>> temp;
  m_action_queue_0 = temp;
}

void work_thread::push_action(const std::shared_ptr<action> action)
{
  m_action_queue_0.push(action);
}

void work_thread::append_actions(std::queue<std::shared_ptr<action>> queue)
{
  while (!queue.empty())
  {
    m_action_queue_0.push(queue.front());
    queue.pop();
  }
}

// could be optimized with memcpy
void work_thread::end_pushing()
{
  wait_for();

  m_action_queue_1 = m_action_queue_0;
  m_q1_for_renderer = true;
}

void work_thread::wait_for() const
{
  while (m_q1_for_renderer)
  {
    std::this_thread::sleep_for(SLEEP_DURATION);
  }
}

void work_thread::work()
{
  while (!m_should_close) //one iteration per frame
  {
    //if no work to do, wait
    while (!m_should_close && !m_q1_for_renderer)
      std::this_thread::sleep_for(SLEEP_DURATION);

    //perform all the actions of the queue
    perform_actions();

    //notify the main thread that the renderer has finished with q1
    m_q1_for_renderer = false;
  }
}

void work_thread::perform_actions()
{
  chronometer chr;
  chr.start();

  while (!m_action_queue_1.empty())
  {
    m_action_queue_1.front()->do_action();
    m_action_queue_1.pop();
  }

  m_q_time_his.push(chr.read_us());
}

