/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file   final_project_settings.h

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu

\par    DigiPen login: b.morisset

\par    Course: CS562

\par    Final Project

\date    Sun Dec 6 2020

\brief  Simple class containing all the line rendering settings passed to gui

***************************************************************************/


#include "maths/utils.h" //eX_3, eY_3, eZ_3

#pragma once

class final_project_settings
{
public:
  glm::vec4 toon_dir = -eX_4 -eZ_4;

  float background_col = 1.0f;
  float base_col = 0.5f;

  int neighboorhood_dim = 4; //used in suggestive contour and suggestive highlight
  float max_z_diff = 15.0f;

  //principal countour
  float prim_cont_lim = 0.55f;
  float prim_cont_lim_margin = 0.2f;
  float prin_cont_col = 0.0f;

  //suggestive contour
  float sugg_cont_smaller_dot_allowed_proportion = 0.3f;
  float sugg_cont_min_dot_diff = 0.15f;
  float sugg_cont_col = 0.2f;

  //principal highlights
  float prin_high_lim_1 = 0.861f; //dot(n,v) > 
  float prin_high_lim_2 = 0.2f; //dot(w,e1) <
  //float prin_high_lim_margin = 0.05f;
  float prin_high_col = 1.0f;

  //suggestive highlights
  float sugg_high_bigger_dot_allowed_proportion = 0.3f;
  float sugg_high_min_dot_diff = 0.15f;
  float sugg_high_col = 0.8f;

  unsigned char opti = 0;
  /*
    0: no opti
    1: fewer options
    2: no options
  */

  unsigned char lines_flag = 0b10001111; //not used when opti
  /*
    0: principal contour
    1: suggestive contour
    2: principal highlight
    3: suggestive highlight
    4: 0 = flat base color, 1= toon shading for base color
    5: 0 = square neighborhood, 1 = circular neighborhood
    6: radius_scaling
    7: principal contour type: 0 = dot(n,v), 1 = sobel cam z
  */
};