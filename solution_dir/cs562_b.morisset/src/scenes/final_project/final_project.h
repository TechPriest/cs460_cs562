/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file   final_project.h

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu

\par    DigiPen login: b.morisset

\par    Course: CS562

\par    Final Project

\date   Tue Dec 15 2020

\brief  Manages all the assets for this scene.
Contains all the specifics for the Scene.

This scene contains a few models usefull to showcase the effect
Lines to Convey SHape

Possible improvements:
  -only recompute and resend to GPU perspective projection matrix
  upon resolution change.
  -load into cpu with multithreading

***************************************************************************/



#pragma once

#include <vector>
#include <memory> //std::shared_ptr, std::weak_ptr
#include <bitset>
#include "scenes/scene.h"
#include "graphics/effects_pipeline/effects_pipeline.h"
#include "graphics/AO/AO_pipeline.h"
#include "scenes/final_project/final_project_settings.h"

class point_light_node;

class final_project : public scene
{
public: 
  using active_effects = std::bitset<8u>;

private:

  gbuffer_final_project m_g_buff;
  rend_buffer m_work_fb_0; //to store the sobel edges

public:

  enum class e_diplay_mode
  {
    normals, depth, pos, lines, SIZE
  };
  
  final_project(window& window, work_thread& render_thread,
    scene_manager& scene_mngr)
      : scene::scene(window, render_thread, scene_mngr),
        m_g_buff(GL_LINEAR, GL_LINEAR),
        m_work_fb_0(GL_LINEAR, GL_LINEAR)
  {}

  void load() override;
  void init() override;
  void run() override;
  void unload() override;

  inline std::vector<std::shared_ptr<node>>* get_nodes_ptr()
    { return &m_nodes; };


protected:
  std::queue<std::shared_ptr<action>>
    get_set_additional_buffer_res_actions(
      int width, int height) override;


/// HELPERS
private:
  /* pushes into render thread, begin_pushing
  must have been called before, and end_pushing
  after must be called after */
  void display(e_diplay_mode display_mode, float contrast_min,
    float contrast_max, glm::mat4 worldToCam,
    glm::mat4 inv_projection, const final_project_settings& occl_setts);

  void display_on_screen(unsigned* texture_ptr);
  void add_on_screen(unsigned* texture_ptr);
  void tex_quad_draw(frame_buffer& out_fb, unsigned* in_tex, bool clear);

  void start_final_pass();
  void start_final_quad_pass();


  ///Lines For Conveying Shape functions

  void lines_effect(frame_buffer& out_fb,
    glm::mat4 world_to_cam, glm::mat4 inv_projection,
    const final_project_settings& highlight_setts);

  void lines_effect_opti_1(frame_buffer& out_fb,
    glm::mat4 world_to_cam, glm::mat4 inv_projection,
    const final_project_settings& highlight_setts);

  void lines_effect_opti_2(frame_buffer& out_fb,
    glm::mat4 world_to_cam, glm::mat4 inv_projection);
};


