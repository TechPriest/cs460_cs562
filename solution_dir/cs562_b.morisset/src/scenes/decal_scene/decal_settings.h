/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    decal_settings.h

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu

\par    DigiPen login: b.morisset

\par    Course: CS562

\par    Assignment #3

\date    Tue Nov 10 2020

\brief  A simple class that stores all the tweakable settings of the decals

***************************************************************************/

#pragma once

class decal_settings
{
public:
  float dot_lim = 0.01f;
  bool enable = true;
  bool draw_volumes = false;
};
