/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    decal_scene.h

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu

\par    DigiPen login: b.morisset

\par    Course: CS562

\par    Assignment #3

\date    Tue Nov 10 2020

\brief  Manages all the assets for this scene.
Contains all the specifics for the Scene.

This scene contains sponza and some decals projeted onto it

Possible improvements:
  -only recompute perspective matrix upon resolution change
  -load into cpu with multithreading
  -load decal's textures automaticly

***************************************************************************/



#pragma once

#include <vector>
#include <memory> //std::shared_ptr, std::weak_ptr
#include <bitset>
#include "scenes/scene.h"
#include "graphics/effects_pipeline/effects_pipeline.h"
#include "graphics/framebuffers/framebuffer.h"
#include "graphics/AO/AO_pipeline.h"
#include "graphics/AO/occlusion_settings.h"

class point_light_node;

class decal_scene : public lighted_scene
{
public: 
  using active_effects = std::bitset<8u>;

private:

  rend_buffer_with_unowned_depth_diff_norm m_decal_framebuffer;

  AO_pipeline m_AO;
  effects_pipeline m_eff_pipeline;

public:

  enum class e_diplay_mode
  {
    normals, diff, emi, depth, pos,
    specular, shininess, occlusion, occlusion_no_filter, lighted, SIZE
  };
  
  decal_scene(window& window, work_thread& render_thread, scene_manager& scene_mngr)
    : lighted_scene::lighted_scene(window, render_thread, scene_mngr),
      m_AO(m_g_buffer.get_texture_handle_ptr(e_gbuff_v4_textures::depth),
        2u, &m_shaders, &m_textures),
      m_decal_framebuffer(GL_NEAREST, GL_NEAREST),
      m_eff_pipeline(m_g_buffer.get_textures_arr_ptr(), m_light_result.get_texture_handle_ptr(), &m_shaders, &m_3Dtextures)
  {}

  void load() override;
  void init() override;
  void run() override;
  void unload() override;

  inline std::vector<std::shared_ptr<node>>* get_nodes_ptr()
    { return &m_nodes; };

protected:
  std::queue<std::shared_ptr<action>>
    get_set_additional_buffer_res_actions(
      int width, int height) override;

/// HELPERS
private:
  /* pushes into render thread, begin_pushing
  must have been called before, and end_pushing
  after must be called after */
  void display(e_diplay_mode display_mode, float contrast_min, float contrast_max,
    bool light_opti, glm::mat4 projection, glm::mat4 inv_projection, const occlusion_settings& occl_setts);

  void apply_decals(glm::mat4 projection, glm::mat4 inv_projection, glm::mat4 world_to_cam,
    glm::mat4 cam_to_world, float dot_lim);
  void draw_decal_volumes(glm::mat4 projection, glm::mat4 world_to_cam);

  void lighting_pass(glm::mat4 inv_projection);
  void opti_lighting_pass(glm::mat4 projection, glm::mat4 inv_projection);
  void apply_light(shader& shdr, std::shared_ptr<point_light_node> light);
  void apply_opti_light(shader& shdr, std::shared_ptr<point_light_node> light, glm::mat4 projection);
  void lighting_and_emissive(bool light_opti, glm::mat4 projection, glm::mat4 inv_projection);


  void display_on_screen(unsigned* texture_ptr);
  void add_on_screen(unsigned* texture_ptr);
  void tex_quad_draw(frame_buffer& out_fb, unsigned* in_tex, bool clear);

  void emissive_pass();

  void start_final_pass();
  void start_final_quad_pass();
  void start_lighting_pass();
};


