#include "scene.h"

#include "other/append_queue.h"

std::queue<std::shared_ptr<action>> scene::get_set_resolution_actions(
  int width, int height)
{
  std::queue<std::shared_ptr<action>> output;



  //append change additional buffers actions
  std::queue<std::shared_ptr<action>> q =
    get_set_additional_buffer_res_actions(width, height);
  append_queue(&output, &q);

  return output;
}

std::queue<std::shared_ptr<action>> lighted_scene::get_set_resolution_actions(
  int width, int height)
{
  std::queue<std::shared_ptr<action>> output =
    scene::get_set_resolution_actions(width, height);

  //append change g_buffer actions
  std::queue<std::shared_ptr<action>> q =
    m_g_buffer.get_set_resolution_actions(width, height);
  append_queue(&output, &q);

  //append change light_result actions
  q = m_light_result.get_set_resolution_actions(width, height);
  append_queue(&output, &q);

  return output;
}
