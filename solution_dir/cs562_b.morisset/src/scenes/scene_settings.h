/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file   scene_settings.h

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu

\par    DigiPen login: b.morisset

\par    Course: CS562

\par    Assignment #3

\date    Tue Nov 10 2020

\brief  Simple class containing all common scene settings passed to gui

***************************************************************************/

#pragma once

#include "other/history.h"

class window;
class scene_manager;
//class g_buffer;
class scene;

class scene_settings
{
public:
  scene* scn_ptr = nullptr;
  window* win_ptr = nullptr;
  scene_manager* scene_mngr_ptr = nullptr;
  //g_buffer* g_buff_ptr = nullptr;
  const history<unsigned, 120u>* main_thread_frame_us_his_ptr = nullptr;
  const history<unsigned, 120u>* rend_thread_frame_us_his_ptr = nullptr;

  bool loop = true;
  bool reload_shaders = false;
  int display_mode = 0;
  float depth_contrast_min = 0.0f;
  float depth_contrast_max = 1.0f;
  bool wireframe = false;
};