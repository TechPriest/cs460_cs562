/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    scene.h

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu

\par    DigiPen login: b.morisset

\par    Course: CS562

\par    Assignment #3

\date    Tue Nov 10 2020

\brief  Base class for a scene. A scene manages all its assets
and contains all the logic.

***************************************************************************/


#pragma once

#include <vector>
#include <memory> //std::shared_ptr, std::weak_ptr
#include <bitset>
#include "threading/work_thread.h"
#include "graphics/shader/shader.h"
#include "graphics/model/model.h"
#include "graphics/texture/texture.h"
#include "graphics/texture/texture3D.h"
#include "graphics/window/window.h"
#include "graphics/framebuffers/g_buffer.h"
#include "graphics/framebuffers/framebuffer.h"
#include "nodes/node.h"
#include "cam_controller/cam_controller.h"

class point_light_node;
class scene_manager;

class scene
{
protected:

  window& m_window;
  work_thread& m_rend_thr; //renderer thread
  scene_manager& m_scene_mngr;

  //assets
  std::vector<shader> m_shaders;
  std::vector<model> m_models;
  std::vector<texture> m_textures;
  std::vector<texture3D> m_3Dtextures;

  //nodes
  std::vector<std::shared_ptr<node>> m_nodes;

  //camera controller
  cam_controller m_cam_controller;

  float m_quad_positions[20u] =
  {
    -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
     1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
     1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
    -1.0f,  1.0f, 0.0f, 0.0f, 1.0f
  };

  unsigned m_quad_indices[6u] =
  {
    0,1,2,
    0,2,3
  };

public:
  scene(window& window, work_thread& render_thread, scene_manager& scene_mngr)
    : m_window(window),
    m_rend_thr(render_thread),
    m_scene_mngr(scene_mngr)
  {}
  virtual ~scene() {}

  virtual void load() = 0;
  virtual void init() = 0;
  virtual void run() = 0;
  virtual void unload() = 0;

  inline std::vector<std::shared_ptr<node>>* get_nodes_ptr()
    { return &m_nodes; };

  virtual std::queue<std::shared_ptr<action>> get_set_resolution_actions(
    int width, int height);

protected:
  virtual std::queue<std::shared_ptr<action>>
    get_set_additional_buffer_res_actions(int width, int height) = 0;
};

class lighted_scene : public scene
{
protected:

  unsigned m_first_decal_idx = 0u; //between this and m_first_light_idx are decals
  unsigned m_first_light_idx = 0u; //all the nodes after this one are light nodes

  g_buffer_v4 m_g_buffer;
  rend_buffer_with_unowned_depth m_light_result; //stores lighting result

public:
  lighted_scene(window& window, work_thread& render_thread, scene_manager& scene_mngr)
    : scene::scene(window, render_thread, scene_mngr),
    m_g_buffer(GL_LINEAR, GL_LINEAR),
    m_light_result(GL_LINEAR, GL_LINEAR)
  {}

  virtual ~lighted_scene() {}

  std::queue<std::shared_ptr<action>> get_set_resolution_actions(
    int width, int height) override final;

protected:
  virtual std::queue<std::shared_ptr<action>>
    get_set_additional_buffer_res_actions(int width, int height) = 0;
};


