/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    gui_windows_tess.h

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu

\par    DigiPen login: b.morisset

\par    Course: CS562

\par    Assignment #2

\date    We Oct 28 2020

\brief  the decinition of the enum e_shader_type.
It is the one used on all scenes to easily query shaders.

WARNING: Loading of the shaders must be done in exaclty this order on the scenes!

***************************************************************************/

#pragma once
  
enum e_shader_type
{
  g_buffer,
  g_buffer_tess,
  lighting,
  opti_lighting,
  emiss,
  texture_quad,
  texture_quad_contrast,
  luminance_filter,
  pos,
  edge_detection,
  edge_detection_depth,
  blur_x,
  blur_y,
  AA_blur_x,
  AA_blur_y,
  alpha_as_greyscale,
  decal,
  bilateral_filter_x,
  bilateral_filter_y,
  AO,
  my_AO,
  my_AO_opti,
  LUT,
  invert
};