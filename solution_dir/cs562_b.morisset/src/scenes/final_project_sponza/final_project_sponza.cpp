/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file   final_project_sponza.cpp

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu

\par    DigiPen login: b.morisset

\par    Course: CS562

\par    Final Project

\date   Tue Dec 15 2020

\brief  Manages all the assets for this scene.
Contains all the specifics for the Scene.

This scene contains sponza and the galeon. It has no lihghting and
only uses the Lines For Conveying Shape shader. Therefore it uses
a g-buffer that only contains a depth buffer and a normal buffer.

Possible improvements:
  -only recompute and resend to GPU perspective projection matrix
  upon resolution change.
  -load into cpu with multithreading

***************************************************************************/

#include "final_project_sponza.h"

#include <iostream> //std::cerr
#include <mutex>
#include <glm/gtc/matrix_transform.hpp>
#include "nodes/renderable_node.h"
#include "nodes/camera_node.h"
#include "nodes/point_light_node.h"
#include "cam_controller/cam_controller.h"
#include "maths/utils.h"
#include "maths/rng_utils.h"
#include "input/input.h"
#include "serialization/read_scene.h"
#include "actions/gui_actions.h"
#include "time/chronometer.h"
#include "time/fps.h"
#include "other/history.h"
#include "scenes/lvl_shader_types.h"
#include "scenes/gui_windows.h"
#include "scene_manager/scene_manager.h"
#include "scenes/scene_settings.h"
#include "other/append_queue.h"

void final_project_sponza::load()
{
  //create assets (not loaded yet)
  {
    //shaders
    {
      m_shaders.clear();
      m_shaders.emplace_back("./data/shaders/Gbuffer_4.shdr"); //g_buffer
      m_shaders.emplace_back("./data/shaders/Gbuffer_4.shdr"); //g_buffer_tess
      m_shaders.emplace_back("./data/shaders/differed_blinn_phong_4.shdr");
      m_shaders.emplace_back("./data/shaders/differed_blinn_phong_4_1.shdr");
      m_shaders.emplace_back("./data/shaders/emissive_3.shdr");
      m_shaders.emplace_back("./data/shaders/texture_quad.shdr");
      m_shaders.emplace_back("./data/shaders/texture_quad_contrast.shdr");
      m_shaders.emplace_back("./data/shaders/luminance_filter.shdr");
      m_shaders.emplace_back("./data/shaders/pos_4.shdr");
      m_shaders.emplace_back("./data/shaders/edge_detection.shdr");
      m_shaders.emplace_back("./data/shaders/edge_detection_depth.shdr");
      m_shaders.emplace_back("./data/shaders/blur_x.shdr");
      m_shaders.emplace_back("./data/shaders/blur_y.shdr");
      m_shaders.emplace_back("./data/shaders/AA_blur_x.shdr");
      m_shaders.emplace_back("./data/shaders/AA_blur_y.shdr");
      m_shaders.emplace_back("./data/shaders/alpha_as_greyscale.shdr");
      //m_shaders.emplace_back("./data/shaders/decal.shdr");
      //m_shaders.emplace_back("./data/shaders/bilateral_filter_x.shdr");
      //m_shaders.emplace_back("./data/shaders/bilateral_filter_y.shdr");
      //m_shaders.emplace_back("./data/shaders/occlusion_paper.shdr");
      //m_shaders.emplace_back("./data/shaders/occlusion_mine.shdr");
      //m_shaders.emplace_back("./data/shaders/occlusion_mine_opti.shdr");
      //m_shaders.emplace_back("./data/shaders/LUT.shdr");
      //m_shaders.emplace_back("./data/shaders/invert.shdr");
      m_shaders.emplace_back("./data/shaders/final_project_opti_2.shdr");
      m_shaders.emplace_back("./data/shaders/final_project_opti.shdr");
      m_shaders.emplace_back("./data/shaders/final_project.shdr");
    }
    //textures
    {
      m_textures.clear();
      m_textures.emplace_back("./data/textures/default.png"); //0
      m_textures.emplace_back("./data/textures/default_ddn.png"); //1
      m_textures.emplace_back("./data/textures/white_noise.png"); //2
    }
    //models
    {
      m_models.clear();
      m_models.emplace_back("./data/meshes/default_sphere.obj", &m_textures);
      m_models.emplace_back("./data/meshes/GalleonOGA.obj", &m_textures);
      m_models.emplace_back("./data/meshes/sponza.obj", &m_textures);
    }
  }

  //loading to CPU (put in loader threads)
  {
    for (shader& shdr : m_shaders)
      shdr.get_load_action()->do_action();
    for (model& modl : m_models)
      modl.get_load_action()->do_action();
    for (texture& tex : m_textures) //must go after the models as models may add textures to m_textures
      tex.get_load_action()->do_action();
    for (texture3D& tex : m_3Dtextures) //can go in any order
      tex.get_load_action()->do_action();
  }

  //create g_buffer and frame buffer
  {
    std::queue<std::shared_ptr<action>>q =
      m_g_buff.get_set_resolution_actions(m_window.m_width, m_window.m_height);
    assert(q.empty());

    //q = m_eff_pipeline.get_set_resolution_actions(m_window.m_width, m_window.m_height);
    //assert(q.empty());

    q = m_work_fb_0.get_set_resolution_actions(m_window.m_width, m_window.m_height);
    assert(q.empty());
  }

  //loading to GPU
  {
    m_rend_thr.begin_pushing();

    for (shader& shdr : m_shaders)
      m_rend_thr.push_action(shdr.get_create_action());
    for (model& modl : m_models)
      m_rend_thr.append_actions(modl.get_create_actions());
    for (texture& tex : m_textures)
      m_rend_thr.push_action(tex.get_create_action());
    for (texture3D& tex : m_3Dtextures)
      m_rend_thr.push_action(tex.get_create_action());
    
    //frame buffers
    {
      //g_buff
      m_rend_thr.append_actions(m_g_buff.get_create_actions(
        m_quad_positions, m_quad_indices));

      //post processing effects pipeline
      //m_rend_thr.append_actions(m_eff_pipeline.get_create_actions(
      //  m_quad_positions, m_quad_indices));

      //work buffer
      m_rend_thr.append_actions(m_work_fb_0.get_create_actions(
        m_quad_positions, m_quad_indices)); 
    }

    m_rend_thr.end_pushing();
  }
}

void final_project_sponza::init()
{
  const bool success = read_scene::read("./data/scenes/final_project_sponza.json",
    &m_nodes, m_models, nullptr);
  if (!success)
  {
    std::cerr << "ERROR: scene not loaded" << std::endl;
    return;
  }

  {
    m_window.m_cam_indices.push_back(0u); //TODO: should be done in json read
    std::shared_ptr<camera_node> cam = std::static_pointer_cast<camera_node>(m_nodes[0u]);
    cam->set_viewport(m_window.m_width, m_window.m_height);

    m_cam_controller.set_cam(cam);
    m_cam_controller.set_lin_speed(90.0f);
  }
}

void final_project_sponza::run()
{
  std::mutex nodes_mutex;
  chronometer cpu_chrono;
  cpu_chrono.start();
  history<unsigned, 120u> cpu_frame_us_history;
  std::mutex cpu_his_mutex;
  gui_windows gui_wins;
  {
    gui_wins.highlight_window = true;
  }

  
  //scene settings
  scene_settings scene_setts;
  {
    scene_setts.loop = true;
    scene_setts.reload_shaders = false;
    scene_setts.display_mode = static_cast<int>(e_diplay_mode::lines);
    scene_setts.depth_contrast_min = 0.995f;
    scene_setts.depth_contrast_max = 1.0f;
    scene_setts.wireframe = false;

    scene_setts.scn_ptr = this;
    scene_setts.win_ptr = &m_window;
    scene_setts.scene_mngr_ptr = &m_scene_mngr;
    scene_setts.rend_thread_frame_us_his_ptr = m_rend_thr.get_q_time_us_his_ptr();
    scene_setts.main_thread_frame_us_his_ptr = &cpu_frame_us_history;
  }

  final_project_settings highlight_setts;

  fps_system& fps_inst = fps_system::get_instance();
  fps_inst.Init(0u);

  //simulation loop
  while (!m_window.should_close() && scene_setts.loop)
  {
    fps_inst.Update();

    //logic
    {
      //shortcuts
      {
        input& input_instance = input::get_instance();

        if (input_instance.KeyPress(GLFW_KEY_SPACE))
          scene_setts.display_mode =
            (scene_setts.display_mode + 1) %
            static_cast<int>(e_diplay_mode::SIZE);

        //if (input_instance.KeyPress(GLFW_KEY_K))
        //  m_eff_pipeline.m_act_effects[effects_pipeline::e_effects::anti_aliasing] =
        //  !m_eff_pipeline.m_act_effects[effects_pipeline::e_effects::anti_aliasing];

        if (input_instance.KeyPress(GLFW_KEY_O))
          scene_setts.wireframe = !scene_setts.wireframe;

        if (input_instance.KeyMaintained(GLFW_KEY_F5))
          scene_setts.reload_shaders = true;

        const bool ctrl_maintained =
          input_instance.KeyMaintained(GLFW_KEY_LEFT_CONTROL) ||
          input_instance.KeyMaintained(GLFW_KEY_RIGHT_CONTROL);

        if ((ctrl_maintained && input_instance.KeyPress(GLFW_KEY_R)))
        {
          m_scene_mngr.restart_scene();
          scene_setts.loop = false;
        }

        //camera views
        {
          std::shared_ptr<camera_node> cam_ptr = m_cam_controller.get_cam().lock();

          //sofa view
          if (input_instance.KeyPress(GLFW_KEY_1))
          {
            cam_ptr->set_local_pos({-35.0f, 53.0f, 22.0f});

            glm::quat ori{ 0.153699f, -0.0324439f, -0.966299f, -0.203937f };
            ori = glm::normalize(ori);
            cam_ptr->set_local_ori(ori);
          }
          //serapis view
          else if (input_instance.KeyPress(GLFW_KEY_2))
          {
            cam_ptr->set_local_pos({ -56.16f, 60.37f, -50.98f });

            glm::quat ori{-0.745878f, 0.0778545f, 0.657943f, 0.0686711f };
            ori = glm::normalize(ori);
            cam_ptr->set_local_ori(ori);
          }
          //golf ball view
          else if (input_instance.KeyPress(GLFW_KEY_3))
          {
            cam_ptr->set_local_pos({ -105.302f, 42.6492f, 123.963f });

            glm::quat ori{ 0.321426f, -0.0154483f, -0.945719f, -0.0454259f };
            ori = glm::normalize(ori);
            cam_ptr->set_local_ori(ori);
          }

        }

        //styles
        {
          //Basic
          if (input_instance.KeyPress(GLFW_KEY_B))
          {
            highlight_setts.lines_flag = 0b10001111;
            highlight_setts.background_col = 1.0f;
            highlight_setts.base_col = 0.5f;
            highlight_setts.neighboorhood_dim = 4;
            highlight_setts.max_z_diff = 15.0f;
            highlight_setts.prim_cont_lim = 0.55f;
            highlight_setts.prim_cont_lim_margin = 0.2f;
            highlight_setts.prin_cont_col = 0.0f;
            highlight_setts.sugg_cont_smaller_dot_allowed_proportion = 0.3f;
            highlight_setts.sugg_cont_min_dot_diff = 0.15f;
            highlight_setts.sugg_cont_col = 0.2f;
            highlight_setts.prin_high_lim_1 = 0.861f;
            highlight_setts.prin_high_lim_2 = 0.2f;
            highlight_setts.prin_high_col = 1.0f;
            highlight_setts.sugg_high_bigger_dot_allowed_proportion = 0.3f;
            highlight_setts.sugg_high_min_dot_diff = 0.15f;
            highlight_setts.sugg_high_col = 0.8f;
          }
          //Frank Miller's
          else if (input_instance.KeyPress(GLFW_KEY_M))
          {
            highlight_setts.lines_flag = 0b10011111;
            highlight_setts.toon_dir = glm::vec4(-1.0,0.0,-1.0,0.0);
            highlight_setts.background_col = 0.0f;
            highlight_setts.base_col = 1.0f;
            highlight_setts.neighboorhood_dim = 4;
            highlight_setts.max_z_diff = 15.0f;
            highlight_setts.prim_cont_lim = 0.75f;
            highlight_setts.prim_cont_lim_margin = 0.0f;
            highlight_setts.prin_cont_col = 0.0f;
            highlight_setts.sugg_cont_smaller_dot_allowed_proportion = 0.3f;
            highlight_setts.sugg_cont_min_dot_diff = 0.15f;
            highlight_setts.sugg_cont_col = 0.0f;
            highlight_setts.prin_high_lim_1 = 0.861f;
            highlight_setts.prin_high_lim_2 = 0.2f;
            highlight_setts.prin_high_col = 1.0f;
            highlight_setts.sugg_high_bigger_dot_allowed_proportion = 0.3f;
            highlight_setts.sugg_high_min_dot_diff = 0.15f;
            highlight_setts.sugg_high_col = 1.0f;
          }
          //Roy Lichtenchtein's
          else if (input_instance.KeyPress(GLFW_KEY_L))
          {
            highlight_setts.lines_flag = 0b11111111;
            highlight_setts.toon_dir = glm::vec4(-1.0, 0.0, -1.0, 0.0);
            highlight_setts.background_col = 1.0f;
            highlight_setts.base_col = 1.0f;
            highlight_setts.neighboorhood_dim = 7;
            highlight_setts.max_z_diff = 15.0f;
            highlight_setts.prim_cont_lim = 0.75f;
            highlight_setts.prim_cont_lim_margin = 0.0f;
            highlight_setts.prin_cont_col = 0.0f;
            highlight_setts.sugg_cont_smaller_dot_allowed_proportion = 0.189f;
            highlight_setts.sugg_cont_min_dot_diff = 0.004f;
            highlight_setts.sugg_cont_col = 0.0f;
            highlight_setts.prin_high_lim_1 = 0.861f;
            highlight_setts.prin_high_lim_2 = 0.2f;
            highlight_setts.prin_high_col = 1.0f;
            highlight_setts.sugg_high_bigger_dot_allowed_proportion = 0.205f;
            highlight_setts.sugg_high_min_dot_diff = 0.008f;
            highlight_setts.sugg_high_col = 1.0f;
          }

        }
      }

      if (scene_setts.reload_shaders)
      {
        scene_setts.reload_shaders = false;

        m_rend_thr.begin_pushing();

        for (shader& shdr : m_shaders)
        {
          m_rend_thr.push_action(shdr.get_delete_action());
          m_rend_thr.push_action(shdr.get_clear_cache_action());
        }

        for (shader& shdr : m_shaders)
          m_rend_thr.push_action(shdr.get_load_action());

        for (shader& shdr : m_shaders)
          m_rend_thr.push_action(shdr.get_create_action());

        m_rend_thr.end_pushing();
      }

      m_cam_controller.update(fps_inst.m_dt);
    }

    m_rend_thr.begin_pushing();

    //gui
    {
      m_rend_thr.push_action(std::make_shared<gui_start_frame>());
      m_rend_thr.push_action(std::make_shared<gui_highlights_top_bar>(
        &scene_setts, &m_g_buff, &gui_wins, &cpu_his_mutex));
      //m_rend_thr.push_action(std::make_shared<gui_effects_window>(
      //  &(gui_wins.effects_window), &m_eff_pipeline));
      //m_rend_thr.push_action(std::make_shared<gui_demo>());
      m_rend_thr.push_action(std::make_shared<gui_highlights_window>(
        &(gui_wins.highlight_window), &highlight_setts));
    }

    //render
    {
      //compute camera matrices only once for this frame
      std::shared_ptr<camera_node> cam_ptr = m_cam_controller.get_cam().lock();
      const glm::mat4 projection = cam_ptr->get_projection();
      const glm::mat4 inv_projection = glm::inverse(projection);
      const glm::mat4 world_to_cam = cam_ptr->get_world_transform().get_inv_TR();

      //fill g_buffer
      m_rend_thr.append_actions( m_g_buff.fill(
        m_shaders[e_shader_type::g_buffer], &m_nodes,
        &m_models, &m_textures, projection, world_to_cam, scene_setts.wireframe) );

      display(static_cast<e_diplay_mode>(scene_setts.display_mode),
        scene_setts.depth_contrast_min, scene_setts.depth_contrast_max,
        world_to_cam, inv_projection, highlight_setts);

      m_rend_thr.push_action(std::make_shared<gui_end_render>()); //call becfore swap buffers
      m_rend_thr.push_action(std::make_shared<swap_buffers_and_poll_events>(m_window.get_glfw_win()));
    }


    cpu_his_mutex.lock();
    cpu_frame_us_history.push(cpu_chrono.read_ms());
    cpu_his_mutex.unlock();

    m_rend_thr.end_pushing();

    cpu_chrono.start();

    input::get_instance().Update();
  }

  /* alternativelly, I could store all the data
  that must be shared accross threads in the
  level class. This way the unload cpu process
  could be done while the last frame is being
  rendered. */
  m_rend_thr.wait_for();
}

void final_project_sponza::unload()
{
  m_window.m_cam_indices.clear();

  //unloading from GPU
  m_rend_thr.begin_pushing();

  for (shader& shdr : m_shaders)
  {
    m_rend_thr.push_action(shdr.get_delete_action());
  }
  for (texture& tex : m_textures)
  {
    m_rend_thr.push_action(tex.get_delete_action());
  }
  for (texture3D& tex : m_3Dtextures)
  {
    m_rend_thr.push_action(tex.get_delete_action());
  }
  for (model& modl : m_models)
  {
    m_rend_thr.append_actions(modl.get_delete_actions());
  }

  //delete buffers
  m_rend_thr.append_actions(m_work_fb_0.get_delete_actions());
  //m_rend_thr.append_actions(m_eff_pipeline.get_delete_actions());
  m_rend_thr.append_actions(m_g_buff.get_delete_actions());

  m_rend_thr.end_pushing();
  m_rend_thr.wait_for();
}

std::queue<std::shared_ptr<action>>
  final_project_sponza::get_set_additional_buffer_res_actions(
    int width, int height)
{
  std::queue<std::shared_ptr<action>> output = m_g_buff.get_set_resolution_actions(width, height);

  //std::queue<std::shared_ptr<action>> output =
  //  m_eff_pipeline.get_set_resolution_actions(width, height);

  std::queue<std::shared_ptr<action>> q =
    m_work_fb_0.get_set_resolution_actions(width, height);
  append_queue(&output, &q);

  return output;
}


/// HELPERS

void final_project_sponza::display(final_project_sponza::e_diplay_mode display_mode,
  float contrast_min, float contrast_max,
  glm::mat4 worldToCam, glm::mat4 inv_projection,
  const final_project_settings& highlight_setts)
{
  switch (display_mode)
  {
    //depth
  case e_diplay_mode::depth:
  {
    start_final_quad_pass();

    shader& shdr = m_shaders[e_shader_type::texture_quad_contrast];
    m_rend_thr.push_action(shdr.get_bind_action());

    m_rend_thr.push_action(texture::get_set_active_slot_action(0));
    m_rend_thr.push_action(std::make_shared<bind_texture>(
      m_g_buff.get_texture_handle_ptr(e_gbuff_final_project::depth),
      GL_TEXTURE_2D));
    m_rend_thr.push_action(shdr.get_set_uniform_texture_caching(
      "u_texture", 0));
    m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
      "u_min", contrast_min));
    m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
      "u_max", contrast_max));

    m_rend_thr.push_action(std::make_shared<draw_elements>(6u));

    break;
  }
  //position
  case e_diplay_mode::pos:
  {
    start_final_quad_pass();

    shader& shdr = m_shaders[e_shader_type::pos];

    m_rend_thr.push_action(shdr.get_bind_action());
    m_rend_thr.push_action(texture::get_set_active_slot_action(0));
    m_rend_thr.push_action(std::make_shared<bind_texture>(
      m_g_buff.get_texture_handle_ptr(e_gbuff_final_project::depth),
      GL_TEXTURE_2D));
    m_rend_thr.push_action(shdr.get_set_uniform_mat4_caching("u_invCamToVP", inv_projection));

    m_rend_thr.push_action(std::make_shared<draw_elements>(6u));

    break;
  }
  //suggestive_highlights
  case e_diplay_mode::lines:
  {
    switch (highlight_setts.opti)
    {
    case 0u:
      lines_effect(m_work_fb_0, worldToCam, inv_projection, highlight_setts);
      display_on_screen(m_work_fb_0.get_texture_handle_ptr());
      break;
    case 1u:
      lines_effect_opti_1(m_work_fb_0, worldToCam, inv_projection, highlight_setts);
      display_on_screen(m_work_fb_0.get_texture_handle_ptr());
      break;
    case 2u:
      lines_effect_opti_2(m_work_fb_0, worldToCam, inv_projection);
      display_on_screen(m_work_fb_0.get_texture_handle_ptr());
      break;
    default:
      assert(false);
    }

    break;
  }
  //normals
  default:
  {
    start_final_quad_pass();

    shader& shdr = m_shaders[e_shader_type::texture_quad];
    m_rend_thr.push_action(shdr.get_bind_action());

    m_rend_thr.push_action(texture::get_set_active_slot_action(0));
    m_rend_thr.push_action(std::make_shared<bind_texture>(
      m_g_buff.get_texture_handle_ptr(e_gbuff_final_project::normals), GL_TEXTURE_2D));
    m_rend_thr.push_action(shdr.get_set_uniform_texture_caching("u_texture", 0));

    m_rend_thr.push_action(std::make_shared<draw_elements>(6u));

    break;
  }

  }
}

void final_project_sponza::display_on_screen(unsigned* texture_ptr)
{
  start_final_pass();

  shader& shdr = m_shaders[e_shader_type::texture_quad];

  m_rend_thr.push_action(shdr.get_bind_action());
  m_rend_thr.push_action(texture::get_set_active_slot_action(0));
  m_rend_thr.push_action(std::make_shared<bind_texture>(
    texture_ptr, GL_TEXTURE_2D));
  m_rend_thr.push_action(shdr.get_set_uniform_texture_caching("u_texture", 0));

  m_rend_thr.push_action(std::make_shared<draw_elements>(6u));
}

void final_project_sponza::add_on_screen(unsigned* texture_ptr)
{
  shader& shdr = m_shaders[e_shader_type::texture_quad];

  m_rend_thr.push_action(shdr.get_bind_action());
  m_rend_thr.push_action(texture::get_set_active_slot_action(0));
  m_rend_thr.push_action(
    std::make_shared<bind_texture>(texture_ptr, GL_TEXTURE_2D));
  m_rend_thr.push_action(
    shdr.get_set_uniform_texture_caching("u_texture", 0));

  m_rend_thr.push_action(std::make_shared<draw_elements>(6u));
}

void final_project_sponza::tex_quad_draw(frame_buffer& out_fb, unsigned* in_tex, bool clear)
{
  shader& shdr = m_shaders[e_shader_type::texture_quad];

  m_rend_thr.push_action(out_fb.get_bind_action());
  m_rend_thr.push_action(out_fb.get_set_viewport_action());
  m_rend_thr.push_action(std::make_shared<disable>(GL_DEPTH_TEST));

  if (clear)
    m_rend_thr.push_action(std::make_shared<render_clear>(GL_COLOR_BUFFER_BIT));

  m_rend_thr.push_action(shdr.get_bind_action());
  m_rend_thr.push_action(texture::get_set_active_slot_action(0));
  m_rend_thr.push_action(
    std::make_shared<bind_texture>(in_tex, GL_TEXTURE_2D));
  m_rend_thr.push_action(
    shdr.get_set_uniform_texture_caching("u_texture", 0));

  m_rend_thr.push_action(std::make_shared<draw_elements>(6u));

  m_rend_thr.push_action(std::make_shared<enable>(GL_DEPTH_TEST));
}

void final_project_sponza::start_final_pass()
{
  m_rend_thr.push_action(frame_buffer::get_unbind_action());
  m_rend_thr.push_action(m_window.get_set_viewport_action());
  m_rend_thr.push_action(std::make_shared<gui_start_render>());
  m_rend_thr.push_action(std::make_shared<render_clear>(GL_COLOR_BUFFER_BIT/* | GL_DEPTH_BUFFER_BIT*/));
  m_rend_thr.push_action(std::make_shared<disable>(GL_DEPTH_TEST));
}

void final_project_sponza::start_final_quad_pass()
{
  start_final_pass();
  m_rend_thr.push_action(m_g_buff.get_bind_quad_action());
}




/// <summary>
/// Performs the "Lines for Conveying Shape" effect and
/// stores the result in the output frame buffer
/// 
/// This version uses the shader that has the most options
/// and tweakable values. It is bound to be less performant
/// than other versions but is usefull to explore all the
/// possibilities.
/// </summary>
/// <param name="out_fb"> output framebuffer </param>
/// <param name="world_to_cam"> view matrix </param>
/// <param name="inv_projection"> viewport to camera matrix </param>
/// <param name="highlight_setts"> options and tweakables to pass to the shader </param>
void final_project_sponza::lines_effect(frame_buffer& out_fb,
  glm::mat4 world_to_cam, glm::mat4 inv_projection,
  const final_project_settings& highlight_setts)
{
  m_rend_thr.push_action(out_fb.get_bind_action());
  m_rend_thr.push_action(out_fb.get_set_viewport_action());
  m_rend_thr.push_action(std::make_shared<render_clear>(GL_COLOR_BUFFER_BIT));
  m_rend_thr.push_action(std::make_shared<disable>(GL_DEPTH_TEST));
  m_rend_thr.push_action(m_g_buff.get_bind_quad_action());

  shader& shdr = m_shaders.back();
  m_rend_thr.push_action(shdr.get_bind_action());

  //pass uniforms
  {
    m_rend_thr.push_action(shdr.get_set_uniform_vec4_caching("u_toon_dir", highlight_setts.toon_dir));

    //matrices
    m_rend_thr.push_action(shdr.get_set_uniform_mat4_caching("u_invCamToVP", inv_projection));
    m_rend_thr.push_action(shdr.get_set_uniform_mat4_caching("u_worldToCam", world_to_cam));

    //textures
    m_rend_thr.push_action(texture::get_set_active_slot_action(0));
    m_rend_thr.push_action(std::make_shared<bind_texture>(
      m_g_buff.get_texture_handle_ptr(e_gbuff_final_project::normals), GL_TEXTURE_2D));
    m_rend_thr.push_action(shdr.get_set_uniform_texture_caching("u_norm_tex", 0));

    m_rend_thr.push_action(texture::get_set_active_slot_action(1));
    m_rend_thr.push_action(std::make_shared<bind_texture>(
      m_g_buff.get_texture_handle_ptr(e_gbuff_final_project::depth), GL_TEXTURE_2D));
    m_rend_thr.push_action(shdr.get_set_uniform_texture_caching("u_depth_tex", 1));

    //settings
    {
      m_rend_thr.push_action(shdr.get_set_uniform_int_caching(
        "u_lines_flag", static_cast<int>(highlight_setts.lines_flag)));
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_background_col", highlight_setts.background_col));
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_base_col", highlight_setts.base_col));
      m_rend_thr.push_action(shdr.get_set_uniform_int_caching(
        "u_neighboorhood_dim", highlight_setts.neighboorhood_dim));
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_max_z_diff", highlight_setts.max_z_diff));

      //principal countour
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_prin_cont_lim", highlight_setts.prim_cont_lim));
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_prin_cont_lim_margin", highlight_setts.prim_cont_lim_margin));
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_prin_cont_col", highlight_setts.prin_cont_col));

      //suggestive contour
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_sugg_cont_smaller_dot_allowed_proportion",
        highlight_setts.sugg_cont_smaller_dot_allowed_proportion));
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_sugg_cont_min_dot_diff", highlight_setts.sugg_cont_min_dot_diff));
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_sugg_cont_col", highlight_setts.sugg_cont_col));

      //principal highlights
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_prin_high_lim_1", highlight_setts.prin_high_lim_1));
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_prin_high_lim_2", highlight_setts.prin_high_lim_2));
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_prin_high_col", highlight_setts.prin_high_col));

      //suggestive highlights
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_sugg_high_bigger_dot_allowed_proportion",
        highlight_setts.sugg_high_bigger_dot_allowed_proportion));
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_sugg_high_min_dot_diff", highlight_setts.sugg_high_min_dot_diff));
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_sugg_high_col", highlight_setts.sugg_high_col));
    }
  }

  m_rend_thr.push_action(std::make_shared<draw_elements>(6u));

  m_rend_thr.push_action(std::make_shared<enable>(GL_DEPTH_TEST));
}

/// <summary>
/// Performs the "Lines for Conveying Shape" effect and
/// stores the result in the output frame buffer
/// 
/// This version uses the shader that has no options
/// but still has tweakable values.
/// It is bound to be more performant than the version with
/// everything.
/// </summary>
/// <param name="out_fb"> output framebuffer </param>
/// <param name="world_to_cam"> view matrix </param>
/// <param name="inv_projection"> viewport to camera matrix </param>
/// <param name="highlight_setts"> options and tweakables to pass to the shader </param>
void final_project_sponza::lines_effect_opti_1(frame_buffer& out_fb,
  glm::mat4 world_to_cam, glm::mat4 inv_projection,
  const final_project_settings& highlight_setts)
{
  m_rend_thr.push_action(out_fb.get_bind_action());
  m_rend_thr.push_action(out_fb.get_set_viewport_action());
  m_rend_thr.push_action(std::make_shared<render_clear>(GL_COLOR_BUFFER_BIT));
  m_rend_thr.push_action(std::make_shared<disable>(GL_DEPTH_TEST));
  m_rend_thr.push_action(m_g_buff.get_bind_quad_action());

  shader& shdr = m_shaders[m_shaders.size() - 2u];
  m_rend_thr.push_action(shdr.get_bind_action());

  //pass uniforms
  {
    m_rend_thr.push_action(shdr.get_set_uniform_vec4_caching("u_toon_dir", highlight_setts.toon_dir));

    //matrices
    m_rend_thr.push_action(shdr.get_set_uniform_mat4_caching("u_invCamToVP", inv_projection));
    m_rend_thr.push_action(shdr.get_set_uniform_mat4_caching("u_worldToCam", world_to_cam));

    //textures
    m_rend_thr.push_action(texture::get_set_active_slot_action(0));
    m_rend_thr.push_action(std::make_shared<bind_texture>(
      m_g_buff.get_texture_handle_ptr(e_gbuff_final_project::normals), GL_TEXTURE_2D));
    m_rend_thr.push_action(shdr.get_set_uniform_texture_caching("u_norm_tex", 0));

    m_rend_thr.push_action(texture::get_set_active_slot_action(1));
    m_rend_thr.push_action(std::make_shared<bind_texture>(
      m_g_buff.get_texture_handle_ptr(e_gbuff_final_project::depth), GL_TEXTURE_2D));
    m_rend_thr.push_action(shdr.get_set_uniform_texture_caching("u_depth_tex", 1));

    //settings
    {
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_background_col", highlight_setts.background_col));
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_base_col", highlight_setts.base_col));
      m_rend_thr.push_action(shdr.get_set_uniform_int_caching(
        "u_neighboorhood_dim", highlight_setts.neighboorhood_dim));
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_max_z_diff", highlight_setts.max_z_diff));

      //principal countour
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_prin_cont_lim", highlight_setts.prim_cont_lim));
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_prin_cont_lim_margin", highlight_setts.prim_cont_lim_margin));
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_prin_cont_col", highlight_setts.prin_cont_col));

      //suggestive contour
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_sugg_cont_smaller_dot_allowed_proportion",
        highlight_setts.sugg_cont_smaller_dot_allowed_proportion));
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_sugg_cont_min_dot_diff", highlight_setts.sugg_cont_min_dot_diff));
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_sugg_cont_col", highlight_setts.sugg_cont_col));

      //principal highlights
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_prin_high_lim_1", highlight_setts.prin_high_lim_1));
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_prin_high_lim_2", highlight_setts.prin_high_lim_2));
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_prin_high_col", highlight_setts.prin_high_col));

      //suggestive highlights
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_sugg_high_bigger_dot_allowed_proportion",
        highlight_setts.sugg_high_bigger_dot_allowed_proportion));
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_sugg_high_min_dot_diff", highlight_setts.sugg_high_min_dot_diff));
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_sugg_high_col", highlight_setts.sugg_high_col));
    }
  }

  m_rend_thr.push_action(std::make_shared<draw_elements>(6u));

  m_rend_thr.push_action(std::make_shared<enable>(GL_DEPTH_TEST));
}

/// <summary>
/// Performs the "Lines for Conveying Shape" effect and
/// stores the result in the output frame buffer
/// 
/// This version has no options nor tweakables. Therefore,
/// it is very rigid and the values have to be changed in
/// the shader to change the effect.
/// 
/// This should improve performance.
/// </summary>
/// <param name="out_fb"> output framebuffer </param>
/// <param name="world_to_cam"> view matrix </param>
/// <param name="inv_projection"> viewport to camera matrix </param>
void final_project_sponza::lines_effect_opti_2(frame_buffer& out_fb,
  glm::mat4 world_to_cam,
  glm::mat4 inv_projection)
{
  m_rend_thr.push_action(out_fb.get_bind_action());
  m_rend_thr.push_action(out_fb.get_set_viewport_action());
  m_rend_thr.push_action(std::make_shared<render_clear>(GL_COLOR_BUFFER_BIT));
  m_rend_thr.push_action(std::make_shared<disable>(GL_DEPTH_TEST));
  m_rend_thr.push_action(m_g_buff.get_bind_quad_action());

  shader& shdr = m_shaders[m_shaders.size() - 3u];
  m_rend_thr.push_action(shdr.get_bind_action());

  //pass uniforms
  {
    //matrices
    m_rend_thr.push_action(shdr.get_set_uniform_mat4_caching("u_invCamToVP", inv_projection));
    m_rend_thr.push_action(shdr.get_set_uniform_mat4_caching("u_worldToCam", world_to_cam));

    //textures
    m_rend_thr.push_action(texture::get_set_active_slot_action(0));
    m_rend_thr.push_action(std::make_shared<bind_texture>(
      m_g_buff.get_texture_handle_ptr(e_gbuff_final_project::normals), GL_TEXTURE_2D));
    m_rend_thr.push_action(shdr.get_set_uniform_texture_caching("u_norm_tex", 0));

    m_rend_thr.push_action(texture::get_set_active_slot_action(1));
    m_rend_thr.push_action(std::make_shared<bind_texture>(
      m_g_buff.get_texture_handle_ptr(e_gbuff_final_project::depth), GL_TEXTURE_2D));
    m_rend_thr.push_action(shdr.get_set_uniform_texture_caching("u_depth_tex", 1));
  }

  m_rend_thr.push_action(std::make_shared<draw_elements>(6u));

  m_rend_thr.push_action(std::make_shared<enable>(GL_DEPTH_TEST));
}



