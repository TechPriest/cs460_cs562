/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    decal_settings.h

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu

\par    DigiPen login: b.morisset

\par    Course: CS562

\par    Assignment #2

\date    We Oct 28 2020

\brief  A simple class that stores all the tweakable settings of
the phong tesselation

***************************************************************************/

#pragma once

class tesselation_settings
{
public:
  float max_tess_lvl = 5.0f;
  float dist_factor = 0.1f;
  float dist_cnt = 20.0f;
  bool adapt_to_dist = false;
  bool adapt_to_view = false;
  bool heatmap = false;
};
