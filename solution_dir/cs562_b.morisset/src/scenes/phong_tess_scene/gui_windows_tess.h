/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    gui_windows_tess.h

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu

\par    DigiPen login: b.morisset

\par    Course: CS562

\par    Assignment #2

\date    We Oct 28 2020

\brief  A simple class that stores all the booleans used to trak
the open/closed state of the gui windows

***************************************************************************/

#pragma once

class gui_windows_tess
{
public:
  bool lights_window = false;
  bool effects_window = false;
  bool tesselation_window = true;
};