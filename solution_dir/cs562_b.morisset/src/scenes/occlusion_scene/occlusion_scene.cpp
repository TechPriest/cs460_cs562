/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    occlusion_scene.cpp

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu

\par    DigiPen login: b.morisset

\par    Course: CS562

\par    Assignment #4

\date    Mon Nov 23 2020

\brief  Manages all the assets for this scene.
Contains all the specifics for the Scene.

This scene contains sponza and a serapis head. And only one ambient light. 

Possible improvements:
  -only recompute perspective matrix upon resolution change
  -load into cpu with multithreading
  -load decal's textures automaticly

***************************************************************************/

#include "occlusion_scene.h"
#include <iostream> //std::cerr
#include <mutex>
#include <glm/gtc/matrix_transform.hpp>
#include "nodes/renderable_node.h"
#include "nodes/camera_node.h"
#include "nodes/point_light_node.h"
#include "cam_controller/cam_controller.h"
#include "maths/utils.h"
#include "maths/rng_utils.h"
#include "input/input.h"
#include "serialization/read_scene.h"
#include "actions/gui_actions.h"
#include "time/chronometer.h"
#include "time/fps.h"
#include "other/history.h"
#include "scenes/lvl_shader_types.h"
#include "scenes/gui_windows.h"
#include "scene_manager/scene_manager.h"
#include "scenes/scene_settings.h"

#include "debug/glm_to_str.h"

void occlusion_scene::load()
{
  //create assets (not loaded yet)
  {
    //shaders
    {
      m_shaders.clear();
      m_shaders.emplace_back("./data/shaders/Gbuffer_4.shdr"); //g_buffer
      m_shaders.emplace_back("./data/shaders/Gbuffer_4.shdr"); //g_buffer_tess
      m_shaders.emplace_back("./data/shaders/differed_blinn_phong_4.shdr");
      m_shaders.emplace_back("./data/shaders/differed_blinn_phong_4_1.shdr");
      m_shaders.emplace_back("./data/shaders/emissive_3.shdr");
      m_shaders.emplace_back("./data/shaders/texture_quad.shdr");
      m_shaders.emplace_back("./data/shaders/texture_quad_contrast.shdr");
      m_shaders.emplace_back("./data/shaders/luminance_filter.shdr");
      m_shaders.emplace_back("./data/shaders/pos_4.shdr");
      m_shaders.emplace_back("./data/shaders/edge_detection.shdr");
      m_shaders.emplace_back("./data/shaders/edge_detection_depth.shdr");
      m_shaders.emplace_back("./data/shaders/blur_x.shdr");
      m_shaders.emplace_back("./data/shaders/blur_y.shdr");
      m_shaders.emplace_back("./data/shaders/AA_blur_x.shdr");
      m_shaders.emplace_back("./data/shaders/AA_blur_y.shdr");
      m_shaders.emplace_back("./data/shaders/alpha_as_greyscale.shdr");
      m_shaders.emplace_back("./data/shaders/decal.shdr");
      m_shaders.emplace_back("./data/shaders/bilateral_filter_x.shdr");
      m_shaders.emplace_back("./data/shaders/bilateral_filter_y.shdr");
      m_shaders.emplace_back("./data/shaders/occlusion_paper.shdr");
      m_shaders.emplace_back("./data/shaders/occlusion_mine.shdr");
      m_shaders.emplace_back("./data/shaders/occlusion_mine_opti.shdr");
      m_shaders.emplace_back("./data/shaders/LUT.shdr");
      m_shaders.emplace_back("./data/shaders/invert.shdr");
    }
    //textures
    {
      m_textures.clear();
      m_textures.emplace_back("./data/textures/default.png"); //0
      m_textures.emplace_back("./data/textures/default_ddn.png"); //1
      m_textures.emplace_back("./data/textures/white_noise.png"); //2
    }
    //models
    {
      m_models.clear();
      m_models.emplace_back("./data/meshes/default_sphere.obj", &m_textures);
      m_models.emplace_back("./data/meshes/serapis.obj", &m_textures);
      m_models.emplace_back("./data/meshes/sponza.obj", &m_textures);
    }
  }

  //loading to CPU (put in loader threads)
  {
    for (shader& shdr : m_shaders)
      shdr.get_load_action()->do_action();
    for (model& modl : m_models)
      modl.get_load_action()->do_action();
    for (texture& tex : m_textures) //must go after the models as models may add textures to m_textures
      tex.get_load_action()->do_action();
  }

  //create g_buffer and frame buffer
  {
    std::queue<std::shared_ptr<action>>q =
      m_g_buffer.get_set_resolution_actions(m_window.m_width, m_window.m_height);
    assert(q.empty());

    q = m_AO.get_set_resolution_actions(m_window.m_width, m_window.m_height);
    assert(q.empty());

    q = m_light_result.get_set_resolution_actions(m_window.m_width, m_window.m_height);
    assert(q.empty());

    q = m_eff_pipeline.get_set_resolution_actions(m_window.m_width, m_window.m_height);
    assert(q.empty());
  }

  //loading to GPU
  {
    m_rend_thr.begin_pushing();

    for (shader& shdr : m_shaders)
      m_rend_thr.push_action(shdr.get_create_action());
    for (model& modl : m_models)
      m_rend_thr.append_actions(modl.get_create_actions());
    for (texture& tex : m_textures)
      m_rend_thr.push_action(tex.get_create_action());

    //frame buffers
    {
      //g-buffer
      m_rend_thr.append_actions(m_g_buffer.get_create_actions(
        m_quad_positions, m_quad_indices));

      //occlusion
      m_rend_thr.append_actions(m_AO.get_create_actions(
        m_quad_positions, m_quad_indices));

      //light result
      m_light_result.set_shared_depth_texture(
        m_g_buffer.get_texture_handle_ptr(e_gbuff_v4_textures::depth));
      m_rend_thr.append_actions(m_light_result.get_create_actions(
        m_quad_positions, m_quad_indices));

      //effects
      m_rend_thr.append_actions(m_eff_pipeline.get_create_actions(
        m_quad_positions, m_quad_indices));
    }

    m_rend_thr.end_pushing();
  }
}

void occlusion_scene::init()
{
  const bool success = read_scene::read("./data/scenes/sceneAO.json",
    &m_nodes, m_models, &m_first_decal_idx);
  if (!success)
  {
    std::cerr << "ERROR: scene not loaded" << std::endl;
    return;
  }

  {
    m_window.m_cam_indices.push_back(0u); //TODO: should be done in json read
    std::shared_ptr<camera_node> cam = std::static_pointer_cast<camera_node>(m_nodes[0u]);
    cam->set_viewport(m_window.m_width, m_window.m_height);

    m_cam_controller.set_cam(cam);
    m_cam_controller.set_lin_speed(90.0f);
  }

  m_first_light_idx = static_cast<int>(m_nodes.size());
  m_nodes.push_back(std::make_shared<point_light_node>()); //light is last so I can access it with .back()

  std::shared_ptr<point_light_node> light =
    std::static_pointer_cast<point_light_node>(m_nodes[m_first_light_idx]);
  light->m_attenuation = glm::vec3(0.0f, 0.0002f, 0.0002f);
  light->m_id = glm::vec3(0.0f);
  light->m_ia = glm::vec3(1.0f);
  light->m_is = glm::vec3(0.0f);

}

void occlusion_scene::run()
{
  std::mutex nodes_mutex;
  chronometer cpu_chrono;
  cpu_chrono.start();
  history<unsigned, 120u> cpu_frame_us_history;
  std::mutex cpu_his_mutex;
  //m_rend_thr.clear_history(); SHOULD SYNC IF I WANT TO DO THIS
  gui_windows gui_wins;
  gui_wins.lights_window = true;

  //light variables
  float light_pos_incr_x = 3.0f;
  std::vector<glm::vec3> light_dirs;
  light_dirs.push_back(random_unit_vec3());
  bool light_paused = true;
  bool draw_bulbs = false;
  bool draw_infl = false;
  int light_count_diff = 0; //so we start with 1 lights
  glm::vec3 light_pos_max(200.0f);
  glm::vec3 light_pos_min(-200.0f, -20.0f, -200.0f);
  bool opti_light_pass = true;
  
  //scene settings
  scene_settings scene_setts;
  {
    scene_setts.loop = true;
    scene_setts.reload_shaders = false;
    scene_setts.display_mode = static_cast<int>(e_diplay_mode::occlusion);
    scene_setts.depth_contrast_min = 0.995f;
    scene_setts.depth_contrast_max = 1.0f;
    scene_setts.wireframe = false;

    scene_setts.scn_ptr = this;
    scene_setts.win_ptr = &m_window;
    scene_setts.scene_mngr_ptr = &m_scene_mngr;
    scene_setts.rend_thread_frame_us_his_ptr = m_rend_thr.get_q_time_us_his_ptr();
    scene_setts.main_thread_frame_us_his_ptr = &cpu_frame_us_history;
  }

  //occlusion_settings
  occlusion_settings occ_setts;
  {
    occ_setts.min_angle = glm::half_pi<float>();

    occ_setts.set_dirs(9u);
  }

  //initial post processing effects
  m_eff_pipeline.m_act_effects[effects_pipeline::e_effects::anti_aliasing] = true;

  fps_system& fps_inst = fps_system::get_instance();
  fps_inst.Init(0u);

  //simulation loop
  while (!m_window.should_close() && scene_setts.loop)
  {
    //apply main_thread gui effects
    {
      //add/remove lights
      {
        nodes_mutex.lock();

        std::shared_ptr<point_light_node> light =
          std::static_pointer_cast<point_light_node>(m_nodes[m_first_light_idx]);

        if (light_count_diff < 0)
        {
          for (int i = 0; i > light_count_diff; i--)
          {
            m_nodes.pop_back();
            light_dirs.pop_back();
          }
        }
        else if (light_count_diff > 0)
        {
          for (int i = 0; i < light_count_diff; i++)
          {
            std::shared_ptr<point_light_node> new_light = std::make_shared<point_light_node>();

            new_light->m_ia = random_vec3(glm::vec3(0), glm::vec3(1));
            new_light->m_id = new_light->m_ia;
            new_light->m_is = new_light->m_ia;
            new_light->m_attenuation = light->m_attenuation;
            new_light->set_local_pos(random_vec3(light_pos_min, light_pos_max));

            m_nodes.push_back(new_light);
            light_dirs.push_back(random_unit_vec3());
          }
        }

        nodes_mutex.unlock();

        light_count_diff = 0;
      }

      //update lights influence radius
      {
        auto update_infl = [&](std::shared_ptr<node> n)
        {
          std::shared_ptr<point_light_node> l =
            std::static_pointer_cast<point_light_node>(n);

          l->update_influence_volume();
        };

        std::for_each(m_nodes.begin() + m_first_light_idx, m_nodes.end(), update_infl);
      }
    }

    //logic
    {
      //shortcuts
      {
        input& input_instance = input::get_instance();

        if (input_instance.KeyPress(GLFW_KEY_SPACE))
          scene_setts.display_mode =
            (scene_setts.display_mode + 1) %
            static_cast<int>(e_diplay_mode::SIZE);

        if (input_instance.KeyPress(GLFW_KEY_K))
          m_eff_pipeline.m_act_effects[effects_pipeline::e_effects::anti_aliasing] =
          !m_eff_pipeline.m_act_effects[effects_pipeline::e_effects::anti_aliasing];

        if (input_instance.KeyPress(GLFW_KEY_L))
          m_eff_pipeline.m_act_effects[effects_pipeline::e_effects::emis_glow] =
          !m_eff_pipeline.m_act_effects[effects_pipeline::e_effects::emis_glow];

        if (input_instance.KeyPress(GLFW_KEY_O))
          scene_setts.wireframe = !scene_setts.wireframe;

        if (input_instance.KeyMaintained(GLFW_KEY_F5))
          scene_setts.reload_shaders = true;

        const bool ctrl_maintained =
          input_instance.KeyMaintained(GLFW_KEY_LEFT_CONTROL) ||
          input_instance.KeyMaintained(GLFW_KEY_RIGHT_CONTROL);

        if ((ctrl_maintained && input_instance.KeyPress(GLFW_KEY_R)))
        {
          m_scene_mngr.restart_scene();
          scene_setts.loop = false;
        }
      }

      if (scene_setts.reload_shaders)
      {
        scene_setts.reload_shaders = false;

        m_rend_thr.begin_pushing();

        for (shader& shdr : m_shaders)
        {
          m_rend_thr.push_action(shdr.get_delete_action());
          m_rend_thr.push_action(shdr.get_clear_cache_action());
        }

        for (shader& shdr : m_shaders)
          m_rend_thr.push_action(shdr.get_load_action());

        for (shader& shdr : m_shaders)
          m_rend_thr.push_action(shdr.get_create_action());

        m_rend_thr.end_pushing();
      }

      if (!light_paused)
      {
        int i = 0;
        auto move_light = [&](std::shared_ptr<node> n)
        {
          std::shared_ptr<point_light_node> l =
            std::static_pointer_cast<point_light_node>(n);

          glm::vec3& dir = light_dirs[i++];

          l->incr_world_pos(dir * 5.0f * fps_inst.m_dt);

          const glm::vec3 new_pos = l->get_world_pos();
          if (new_pos.x > light_pos_max.x)
            dir.x *= -1.0f;
          if (new_pos.y > light_pos_max.y)
            dir.y *= -1.0f;
          if (new_pos.z > light_pos_max.z)
            dir.z *= -1.0f;
          if (new_pos.x < light_pos_min.x)
            dir.x *= -1.0f;
          if (new_pos.y < light_pos_min.y)
            dir.y *= -1.0f;
          if (new_pos.z < light_pos_min.z)
            dir.z *= -1.0f;
        };
        std::for_each(m_nodes.begin() + m_first_light_idx, m_nodes.end(), move_light);
      }

      m_cam_controller.update(fps_inst.m_dt);
    }

    fps_inst.Update();

    m_rend_thr.begin_pushing();

    //gui
    {
      m_rend_thr.push_action(std::make_shared<gui_start_frame>());
      m_rend_thr.push_action(std::make_shared<gui_top_bar>(
        &scene_setts, &m_g_buffer, &gui_wins, &cpu_his_mutex));
      m_rend_thr.push_action(std::make_shared<gui_lights_window>(
        &(gui_wins.lights_window), &light_paused, &m_nodes, &nodes_mutex,
        m_first_light_idx, &light_count_diff, &draw_bulbs, &draw_infl,
        &opti_light_pass, &occ_setts));
      m_rend_thr.push_action(std::make_shared<gui_effects_window>(
        &(gui_wins.effects_window), &m_eff_pipeline));
      //m_rend_thr.push_action(std::make_shared<gui_demo>());
    }

    //render
    {
      //compute camera matrices only once for this frame
      std::shared_ptr<camera_node> cam_ptr = m_cam_controller.get_cam().lock();
      const glm::mat4 projection = cam_ptr->get_projection();
      const glm::mat4 inv_projection = glm::inverse(projection);
      const glm::mat4 world_to_cam = cam_ptr->get_world_transform().get_inv_TR();

      //fill g_buffer
      m_rend_thr.append_actions(m_g_buffer.fill_debug_lights(
        m_shaders[e_shader_type::g_buffer], &m_nodes,
        &m_models, &m_textures, projection, world_to_cam, scene_setts.wireframe,
        draw_bulbs, draw_infl, m_first_light_idx));

      display(static_cast<e_diplay_mode>(scene_setts.display_mode),
        scene_setts.depth_contrast_min, scene_setts.depth_contrast_max,
        opti_light_pass, projection, inv_projection, occ_setts);

      m_rend_thr.push_action(std::make_shared<gui_end_render>()); //call becfore swap buffers
      m_rend_thr.push_action(std::make_shared<swap_buffers_and_poll_events>(m_window.get_glfw_win()));
    }

    cpu_his_mutex.lock();
    cpu_frame_us_history.push(cpu_chrono.read_ms());
    cpu_his_mutex.unlock();

    m_rend_thr.end_pushing();

    cpu_chrono.start();

    input::get_instance().Update();
  }

  /* alternativelly, I could store all the data
  that must be shared accross threads in the
  level class. This way the unload cpu process
  could be done while the last frame is being
  rendered. */
  m_rend_thr.wait_for();
}

void occlusion_scene::unload()
{
  m_window.m_cam_indices.clear();

  //unloading from GPU
  m_rend_thr.begin_pushing();

  for (shader& shdr : m_shaders)
  {
    m_rend_thr.push_action(shdr.get_delete_action());
  }
  for (texture& tex : m_textures)
  {
    m_rend_thr.push_action(tex.get_delete_action());
  }
  for (model& modl : m_models)
  {
    m_rend_thr.append_actions(modl.get_delete_actions());
  }

  m_rend_thr.append_actions(m_eff_pipeline.get_delete_actions());
  m_rend_thr.append_actions(m_AO.get_delete_actions());

  m_rend_thr.append_actions(m_light_result.get_delete_actions());
  m_rend_thr.append_actions(m_g_buffer.get_delete_actions());

  m_rend_thr.end_pushing();
  m_rend_thr.wait_for();
}

std::queue<std::shared_ptr<action>>
  occlusion_scene::get_set_additional_buffer_res_actions(
    int width, int height)
{
  return m_eff_pipeline.get_set_resolution_actions(width, height);
}


/// HELPERS

void occlusion_scene::display(occlusion_scene::e_diplay_mode display_mode, float contrast_min,
  float contrast_max, bool light_opti, glm::mat4 projection, glm::mat4 inv_projection,
  const occlusion_settings& occl_setts)
{
  switch (display_mode)
  {
    //depth
    case e_diplay_mode::depth:
    {
      start_final_quad_pass();

      shader& shdr = m_shaders[e_shader_type::texture_quad_contrast];
      m_rend_thr.push_action(shdr.get_bind_action());

      m_rend_thr.push_action(texture::get_set_active_slot_action(0));
      m_rend_thr.push_action(std::make_shared<bind_texture>(
        m_g_buffer.get_texture_handle_ptr(e_gbuff_v4_textures::depth),
        GL_TEXTURE_2D));
      m_rend_thr.push_action(shdr.get_set_uniform_texture_caching(
        "u_texture", 0));
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_min", contrast_min));
      m_rend_thr.push_action(shdr.get_set_uniform_float_caching(
        "u_max", contrast_max));

      m_rend_thr.push_action(std::make_shared<draw_elements>(6u));

      break;
    }
    //position
    case e_diplay_mode::pos:
    {
      start_final_quad_pass();

      shader& shdr = m_shaders[e_shader_type::pos];

      m_rend_thr.push_action(shdr.get_bind_action());
      m_rend_thr.push_action(texture::get_set_active_slot_action(0));
      m_rend_thr.push_action(std::make_shared<bind_texture>(
        m_g_buffer.get_texture_handle_ptr(e_gbuff_v4_textures::depth),
        GL_TEXTURE_2D));
      m_rend_thr.push_action(shdr.get_set_uniform_mat4_caching("u_invCamToVP", inv_projection));

      m_rend_thr.push_action(std::make_shared<draw_elements>(6u));

      break;
    }
    //specular & shininess
    case e_diplay_mode::specular: //intended fallthrough
    case e_diplay_mode::shininess:
    {
      //look at both enum definitions to seee how this hack works
      e_gbuff_v4_textures tex_e =
        static_cast<e_gbuff_v4_textures>(static_cast<int>(display_mode) - 4);

      start_final_quad_pass();

      shader& shdr = m_shaders[e_shader_type::alpha_as_greyscale];
      m_rend_thr.push_action(shdr.get_bind_action());

      m_rend_thr.push_action(texture::get_set_active_slot_action(0));
      m_rend_thr.push_action(std::make_shared<bind_texture>(
        m_g_buffer.get_texture_handle_ptr(tex_e), GL_TEXTURE_2D));
      m_rend_thr.push_action(shdr.get_set_uniform_texture_caching(
        "u_texture", 0));

      m_rend_thr.push_action(std::make_shared<draw_elements>(6u));

      break;
    }
    case e_diplay_mode::occlusion:
    {
      m_rend_thr.append_actions(
        m_AO.get_AO_actions(projection, inv_projection, occl_setts));

      {
        start_final_quad_pass();

        shader& shdr = m_shaders[e_shader_type::texture_quad];
        m_rend_thr.push_action(shdr.get_bind_action());

        m_rend_thr.push_action(texture::get_set_active_slot_action(0));
        m_rend_thr.push_action(std::make_shared<bind_texture>(
          m_AO.get_final_texture(), GL_TEXTURE_2D));
        m_rend_thr.push_action(shdr.get_set_uniform_texture_caching(
          "u_texture", 0));

        m_rend_thr.push_action(std::make_shared<draw_elements>(6u));
      }

      break;
    }
    case e_diplay_mode::occlusion_no_filter:
    {
      m_rend_thr.append_actions(
        m_AO.get_unfiltered_AO_actions(projection, inv_projection, occl_setts));

      {
        start_final_quad_pass();

        shader& shdr = m_shaders[e_shader_type::texture_quad];
        m_rend_thr.push_action(shdr.get_bind_action());

        m_rend_thr.push_action(texture::get_set_active_slot_action(0));
        m_rend_thr.push_action(std::make_shared<bind_texture>(
          m_AO.get_unfiltered_texture(), GL_TEXTURE_2D));
        m_rend_thr.push_action(shdr.get_set_uniform_texture_caching(
          "u_texture", 0));

        m_rend_thr.push_action(std::make_shared<draw_elements>(6u));
      }

      break;
    }
    //lighting + emissive
    case e_diplay_mode::lighted:
    {
      m_rend_thr.append_actions(
        m_AO.get_AO_actions(projection, inv_projection, occl_setts));

      lighting_and_emissive(light_opti, projection, inv_projection);
      
      m_rend_thr.append_actions(m_eff_pipeline.get_pipeline_actions(inv_projection));

      display_on_screen(m_eff_pipeline.get_final_texture());

      break;
    }
    //normals, diffuse
    default:
    {
      start_final_quad_pass();

      shader& shdr = m_shaders[e_shader_type::texture_quad];

      m_rend_thr.push_action(shdr.get_bind_action());
      m_rend_thr.push_action(texture::get_set_active_slot_action(0));
      m_rend_thr.push_action(std::make_shared<bind_texture>(
        m_g_buffer.get_texture_handle_ptr(static_cast<e_gbuff_v4_textures>(display_mode)), GL_TEXTURE_2D));
      m_rend_thr.push_action(shdr.get_set_uniform_texture_caching("u_texture", 0));

      m_rend_thr.push_action(std::make_shared<draw_elements>(6u));

      break;
    }
    
  }
}

void occlusion_scene::lighting_pass(glm::mat4 inv_projection)
{
  m_rend_thr.push_action(m_g_buffer.get_bind_quad_action());

  m_rend_thr.push_action(std::make_shared<enable>(GL_BLEND));
  m_rend_thr.push_action(std::make_shared<set_blending>(GL_ONE, GL_ONE, GL_FUNC_ADD));

  shader& shdr = m_shaders[e_shader_type::lighting];

  m_rend_thr.push_action(m_light_result.get_set_viewport_action());
  m_rend_thr.push_action(shdr.get_bind_action());
  m_rend_thr.append_actions(m_g_buffer.get_bind_core_textures_actions(shdr));

  //ao texture
  m_rend_thr.push_action(texture::get_set_active_slot_action(4));
  m_rend_thr.push_action(
    std::make_shared<bind_texture>(m_AO.get_final_texture(), GL_TEXTURE_2D));
  m_rend_thr.push_action(
    shdr.get_set_uniform_texture_caching("u_AO", 4));

  m_rend_thr.push_action(shdr.get_set_uniform_mat4_caching("u_worldToCam",
    m_cam_controller.get_cam().lock()->get_world_transform().get_inv_TR()));
  m_rend_thr.push_action(shdr.get_set_uniform_mat4_caching("u_invCamToVP", inv_projection));

  auto apply_light_fn = [&](std::shared_ptr<node> n)
  {
    std::shared_ptr<point_light_node> l =
      std::static_pointer_cast<point_light_node>(n);
    apply_light(shdr, l);
  };
  std::for_each(m_nodes.begin() + m_first_light_idx, m_nodes.end(), apply_light_fn);

  m_rend_thr.push_action(std::make_shared<disable>(GL_BLEND));
}

//cull face should be already enabled
void occlusion_scene::opti_lighting_pass( glm::mat4 projection, glm::mat4 inv_projection)
{
  m_rend_thr.push_action(std::make_shared<enable>(GL_BLEND));
  m_rend_thr.push_action(std::make_shared<set_blending>(GL_ONE, GL_ONE, GL_FUNC_ADD));
  m_rend_thr.push_action(std::make_shared<enable>(GL_DEPTH_TEST));
  m_rend_thr.push_action(std::make_shared<set_depth_writing_permssion>(GL_FALSE));
  m_rend_thr.push_action(std::make_shared<set_depth_success>(GL_GREATER));

  m_rend_thr.push_action(std::make_shared<set_culling>(GL_FRONT));

  shader& shdr = m_shaders[e_shader_type::opti_lighting];

  m_rend_thr.push_action(m_light_result.get_set_viewport_action());
  m_rend_thr.push_action(shdr.get_bind_action());
  m_rend_thr.append_actions(m_g_buffer.get_bind_core_textures_actions(shdr));

  //ao texture
  m_rend_thr.push_action(texture::get_set_active_slot_action(4));
  m_rend_thr.push_action(
    std::make_shared<bind_texture>(m_AO.get_final_texture(), GL_TEXTURE_2D));
  m_rend_thr.push_action(
    shdr.get_set_uniform_texture_caching("u_AO", 4));

  m_rend_thr.push_action(shdr.get_set_uniform_mat4_caching("u_worldToCam",
    m_cam_controller.get_cam().lock()->get_world_transform().get_inv_TR()));
  m_rend_thr.push_action(shdr.get_set_uniform_mat4_caching("u_camToVP", projection));
  m_rend_thr.push_action(shdr.get_set_uniform_mat4_caching("u_invCamToVP", inv_projection));
  m_rend_thr.push_action(shdr.get_set_uniform_int_caching("u_width", m_g_buffer.get_width()));
  m_rend_thr.push_action(shdr.get_set_uniform_int_caching("u_height", m_g_buffer.get_height()));

  auto apply_light_fn = [&](std::shared_ptr<node> n)
  {
    std::shared_ptr<point_light_node> l =
      std::static_pointer_cast<point_light_node>(n);
    apply_opti_light(shdr, l, projection);
  };
  std::for_each(m_nodes.begin() + m_first_light_idx, m_nodes.end(), apply_light_fn);

  m_rend_thr.push_action(std::make_shared<set_culling>(GL_BACK));
  m_rend_thr.push_action(std::make_shared<set_depth_success>(GL_LESS));
  m_rend_thr.push_action(std::make_shared<set_depth_writing_permssion>(GL_TRUE));
  m_rend_thr.push_action(std::make_shared<disable>(GL_DEPTH_TEST));
  m_rend_thr.push_action(std::make_shared<disable>(GL_BLEND));
}

void occlusion_scene::apply_light(shader& shdr, std::shared_ptr<point_light_node> light)
{
  m_rend_thr.append_actions(shdr.get_set_light_uniforms_actions(light));
  m_rend_thr.push_action(std::make_shared<draw_elements>(6u));
}

void occlusion_scene::apply_opti_light(shader& shdr, std::shared_ptr<point_light_node> light, glm::mat4 projection)
{
  m_rend_thr.append_actions(shdr.get_set_light_uniforms_actions(light));
  m_rend_thr.append_actions(light->get_apply_light_actions(shdr, m_models[0u]));
}

void occlusion_scene::lighting_and_emissive(bool light_opti, glm::mat4 projection, glm::mat4 inv_projection)
{
  start_lighting_pass();

  if (light_opti)
    opti_lighting_pass(projection, inv_projection);
  else
    lighting_pass(inv_projection);

  emissive_pass();
}

void occlusion_scene::display_on_screen(unsigned* texture_ptr)
{
  start_final_pass();

  shader& shdr = m_shaders[e_shader_type::texture_quad];

  m_rend_thr.push_action(shdr.get_bind_action());
  m_rend_thr.push_action(texture::get_set_active_slot_action(0));
  m_rend_thr.push_action(std::make_shared<bind_texture>(
    texture_ptr, GL_TEXTURE_2D));
  m_rend_thr.push_action(shdr.get_set_uniform_texture_caching("u_texture", 0));

  m_rend_thr.push_action(std::make_shared<draw_elements>(6u));
}

void occlusion_scene::add_on_screen(unsigned* texture_ptr)
{
  shader& shdr = m_shaders[e_shader_type::texture_quad];

  m_rend_thr.push_action(shdr.get_bind_action());
  m_rend_thr.push_action(texture::get_set_active_slot_action(0));
  m_rend_thr.push_action(
    std::make_shared<bind_texture>(texture_ptr, GL_TEXTURE_2D));
  m_rend_thr.push_action(
    shdr.get_set_uniform_texture_caching("u_texture", 0));

  m_rend_thr.push_action(std::make_shared<draw_elements>(6u));
}

void occlusion_scene::tex_quad_draw(frame_buffer& out_fb, unsigned* in_tex, bool clear)
{
  shader& shdr = m_shaders[e_shader_type::texture_quad];

  m_rend_thr.push_action(out_fb.get_bind_action());
  m_rend_thr.push_action(out_fb.get_set_viewport_action());
  m_rend_thr.push_action(std::make_shared<disable>(GL_DEPTH_TEST));

  if(clear)
    m_rend_thr.push_action(std::make_shared<render_clear>(GL_COLOR_BUFFER_BIT));

  m_rend_thr.push_action(shdr.get_bind_action());
  m_rend_thr.push_action(texture::get_set_active_slot_action(0));
  m_rend_thr.push_action(
    std::make_shared<bind_texture>(in_tex, GL_TEXTURE_2D));
  m_rend_thr.push_action(
    shdr.get_set_uniform_texture_caching("u_texture", 0));

  m_rend_thr.push_action(std::make_shared<draw_elements>(6u));

  m_rend_thr.push_action(std::make_shared<enable>(GL_DEPTH_TEST));
}

void occlusion_scene::emissive_pass()
{
  shader& shdr = m_shaders[e_shader_type::emiss];

  m_rend_thr.push_action(std::make_shared<enable>(GL_BLEND));
  m_rend_thr.push_action(std::make_shared<set_blending>(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_FUNC_ADD));
  m_rend_thr.push_action(m_light_result.get_bind_quad_action());

  m_rend_thr.push_action(m_light_result.get_set_viewport_action());
  m_rend_thr.push_action(shdr.get_bind_action());
  m_rend_thr.append_actions(m_g_buffer.get_bind_emissive_texture_actions(shdr));

  m_rend_thr.push_action(std::make_shared<draw_elements>(6u));

  m_rend_thr.push_action(std::make_shared<disable>(GL_BLEND));
}

void occlusion_scene::start_final_pass()
{
  m_rend_thr.push_action(frame_buffer::get_unbind_action());
  m_rend_thr.push_action(m_window.get_set_viewport_action());
  m_rend_thr.push_action(std::make_shared<gui_start_render>());
  m_rend_thr.push_action(std::make_shared<render_clear>(GL_COLOR_BUFFER_BIT/* | GL_DEPTH_BUFFER_BIT*/));
  m_rend_thr.push_action(std::make_shared<disable>(GL_DEPTH_TEST));
}

void occlusion_scene::start_final_quad_pass()
{
  start_final_pass();
  m_rend_thr.push_action(m_g_buffer.get_bind_quad_action());
}

void occlusion_scene::start_lighting_pass()
{
  //bind frame_buffer
  m_rend_thr.push_action(m_light_result.get_bind_action());
  m_rend_thr.push_action(m_light_result.get_set_viewport_action());
  m_rend_thr.push_action(std::make_shared<render_clear>(GL_COLOR_BUFFER_BIT /*| GL_DEPTH_BUFFER_BIT*/));
}
