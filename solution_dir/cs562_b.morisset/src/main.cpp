/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    main.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 02:00:22 2020

\brief  Entry point. Calls application::run(). 

define HUNT_LEAKS to get allocation stats usefull for leak detection

define FLOAT_EXCEPTIONS to have the whole program throw exceptions when
dividing by 0 etc. Usefull to detect divides by zero, etc.

***************************************************************************/


#include "application/application.h"

#define HUNT_LEAKS
#define FLOAT_EXCEPTIONS

#ifdef HUNT_LEAKS
#include "debug/s_alloc_metrics.h"
//to print
#include <iostream>
//singletons 
#include "input/input.h"
#include "time/fps.h"
#endif
#ifdef FLOAT_EXCEPTIONS
#include "debug/float_exceptions.h"
#endif

int main()
{
#ifdef FLOAT_EXCEPTIONS
  float_exceptions::enable();
#endif

  application::run();

#ifdef HUNT_LEAKS

  const size_t bytes_in_usage = s_alloc_metrics.get_usage();

  std::cout << "\nALLOC_INFO: using " << bytes_in_usage << " bytes, allocations: " <<
    s_alloc_metrics.allocations << "(" << s_alloc_metrics.allocated <<
    " bytes), dealocations " << s_alloc_metrics.dealocations << "(" <<
    s_alloc_metrics.freed << " bytes)";

  std::cout << "\nALLOC_INFO: keep in mind the singletons (fps_system and input) weight "
    << sizeof(fps_system) + sizeof(input) << " bytes";

  std::cout << "\nALLOC_INFO: keep in mind the that input contains an unordered map. \
It is used to keep trak of the key states. A new value is inserted each time a key \
is pressed that wasn't pressed before.\n" << std::flush;
#endif
}
