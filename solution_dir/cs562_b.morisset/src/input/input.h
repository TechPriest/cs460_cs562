/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    input.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:59:58 2020

\brief  singleton class input declaration. Uses a ScanCode map to store the key/button states.

possible improvements:
  - use a static array
  - add levels of abstraction to be able to write something like IsPressed(eJump) 
  and it works for both gamepad, keyboard and custom bindings



***************************************************************************/

#pragma once
#include <unordered_map>
#include <glm/glm.hpp>

#include <GLFW/glfw3.h>

class input
{
  ///SINGLETON-START
public:
  static input &get_instance()
  {
    static input instance;
    return instance;
  }
private:
  input() = default;
  ~input() = default;
  input(const input &) = delete;
  input& operator=(const input &) = delete;
  ///SINGLETON-END

public:

  bool KeyPress(int scancode) const;
  bool KeyMaintained(int scancode) const;
  bool KeyRelease(int scancode) const;
  bool KeyIdle(int scancode) const;

  inline glm::vec2 get_cursor_pos() const { return m_cursor_pos; };
  inline glm::vec2 get_cursor_speed() const { return m_cursor_speed; };

  void Key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
  void Cursor_position_callback(GLFWwindow* window, double xpos, double ypos);
  void Mouse_button_callback(GLFWwindow* window, int button, int action, int mods);

  void Update();

private:

  /* could also use an std::map. The use of an std::unordered_map
  * gives O(1) access at the cost of using more memory.
  * Access is the main purpose of keymap so it seems like a good
  * idea to sacrifice some memory to get constant time access.
  * Profiling is needed to make sure this is a real gain. In anycase,
  * I highly doubt this is going to be of importance for this project.
  */
  using keymap = std::unordered_map<int, unsigned char>;

  keymap m_keys;
  keymap m_last_frame_keys;

  glm::vec2 m_cursor_pos{};
  glm::vec2 m_old_cursor_pos{};
  glm::vec2 m_cursor_speed{};
};

