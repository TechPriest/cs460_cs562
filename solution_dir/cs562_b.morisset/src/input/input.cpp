/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    input.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:59:52 2020

\brief  singleton class input declaration. Uses a ScanCode map to store the key/button states.

possible improvements:
  - use a static array
  - add levels of abstraction to be able to write something like IsPressed(eJump)
  and it works for both gamepad, keyboard and custom bindings


***************************************************************************/


/**
* @file input.cpp
* @author Benat Morisset, 540002817, b.morisset@digipen.edu
* @date 2020/01/08
* @brief singleton class input definition
*
* @copyright Copyright(C) 2020 DigiPen Institute of Technology .
*/

#include "input.h"

bool input::KeyPress(int scancode) const
{
  keymap::const_iterator found = m_keys.find(scancode);

  if (found != m_keys.cend())
    if (found->second == 1u)
      return true;

  return false;
}

bool input::KeyMaintained(int scancode) const
{
  keymap::const_iterator found = m_keys.find(scancode);

  if (found != m_keys.cend())
    if (found->second == 2u)
      return true;

  return false;
}

bool input::KeyRelease(int scancode) const
{
  keymap::const_iterator found = m_keys.find(scancode);

  if (found != m_keys.cend())
    if (found->second == 3u)
      return true;

  return false;
}

bool input::KeyIdle(int scancode) const
{
  keymap::const_iterator found = m_keys.find(scancode);

  if (found != m_keys.cend())
    if (found->second == 0u)
      return true;

  return false;
}

/**
* Callback function used by glfw when inputs are polled
* @param window
* @param key
* @param action
*/
void input::Key_callback(GLFWwindow* window, int key, int /*scancode*/,
  int action, int /*mods*/)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GLFW_TRUE);

  //GLFW_KEY_LEFT
  m_keys[key] = static_cast<unsigned char>(action); //0 released, 1 on press, 2 pressed
}

/**
* Callback function used by glfw to capture the mouse position
* @param xpos
* @param ypos
*/
void input::Cursor_position_callback(GLFWwindow*, double xpos, double ypos)
{
  m_cursor_pos = { static_cast<float>(xpos), static_cast<float>(ypos) };
}

/**
* Callback function used by glfw to capture the mouse clicks
* @param button
* @param action
*/
void input::Mouse_button_callback(GLFWwindow* /*window*/, int button, int action, int /*mods*/)
{
  //GLFW_KEY_LEFT
  m_keys[button] = static_cast<unsigned char>(action); //0 released, 1 on press, 2 pressed
}


/**
* Converts transitory states into their corresponding
* definitive states
*/
void input::Update()
{
  for (auto& key : m_keys)
  {
    if (m_last_frame_keys[key.first] == key.second)
    {
      if (key.second == 1u)
        key.second = 2u;
      else if (key.second == 0u)
        key.second = 3u;
    }
  }

  m_last_frame_keys = m_keys;

  m_cursor_speed = m_cursor_pos - m_old_cursor_pos;
  m_old_cursor_pos = m_cursor_pos;
}


