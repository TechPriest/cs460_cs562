/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    decal_node.cpp

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu

\par    DigiPen login: b.morisset

\par    Course: CS562

\par    Assignment #3

\date    Tue Nov 10 2020

\brief  Node derived from transform_node that constitutes the decal projector.

***************************************************************************/

#include "decal_node.h"

#include "graphics/shader/shader.h"
#include "graphics/model/model.h"
#include "other/append_queue.h"

/// <summary>
/// Return the sequence of actions needed to project the decal.
/// </summary>
/// <param name="shdr">
/// Shader to be used for projecting. Must have the correct uniform names.
/// </param>
/// <param name="models">
/// Model container
/// </param>
/// <param name="textures">
/// Textures container
/// </param>
/// <returns>
/// The sequence of actions needed to project the decal.
/// </returns>
std::queue<std::shared_ptr<action>> decal_node::get_project_decal_actions(
  shader& shdr, std::vector<model>& models, std::vector<texture>& textures) const
{
  std::queue<std::shared_ptr<action>> output;

  output.push(shdr.get_set_uniform_mat4_caching(
    "u_modelToWorld", get_world_transform().get_TRS()));
  output.push(shdr.get_set_uniform_mat4_caching(
    "u_worldToModel", get_world_transform().get_inv_TRS()));

  std::queue<std::shared_ptr<action>> model_queue =
    models[m_model_idx].get_draw_decal_actions(
    shdr, m_diff_tex, m_norm_tex, textures);

  append_queue(&output, &model_queue);

  return output;
}

//must be used with g_buffer.shdr

/// <summary>
/// Return the sequence of actions needed to draw the projector volume
/// in the emissive channel of the g_buffer.
/// </summary>
/// <param name="shdr">
/// Shader to be used for projecting. Must have the correct uniform names.
/// </param>
/// <param name="models">
/// Model container
/// </param>
/// <param name="textures">
/// Textures container
/// </param>
/// <returns>
/// The sequence of actions.
/// </returns>
std::queue<std::shared_ptr<action>> decal_node::get_draw_volume_actions(
  shader& shdr, std::vector<model>& models, std::vector<texture>& textures) const
{
  std::queue<std::shared_ptr<action>> output;

  output.push(shdr.get_set_uniform_mat4_caching(
    "u_modelToWorld", get_world_transform().get_TRS()));

  material m;
  {
    m.m_color_a = glm::vec3(0);
    m.m_color_d = glm::vec3(0);
    m.m_color_s = glm::vec3(0);
    m.m_color_e = glm::vec3(1.0f, 0.1f, 0.1f); //emissive color matches diffuse intensity (overrides model material)
    m.m_texture_ids[e_tex_types::ambient] = 0;
    m.m_texture_ids[e_tex_types::diffuse] = 0;
    m.m_texture_ids[e_tex_types::specular] = 0;
    m.m_texture_ids[e_tex_types::emissive] = 0;
    m.m_texture_ids[e_tex_types::normal] = 1;
  }

  std::queue<std::shared_ptr<action>> q =
    models[m_model_idx].m_meshes[0u].get_draw_with_custom_material_actions(
      shdr, m, textures);

  append_queue(&output, &q);

  return output;
}
