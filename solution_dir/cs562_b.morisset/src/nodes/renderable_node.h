/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    renderable_node.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:52:26 2020

\brief  object that can be rendered



***************************************************************************/


#pragma once

#include <vector>
#include <queue>
#include <memory> // std::shared_ptr
#include "transform_node.h"
#include "graphics/model/model.h"

//class shader;
//class texture;

class renderable_node : public transform_node
{
private:
  unsigned m_model_idx = 0;

public:

  inline void set_model_idx(unsigned model_idx) 
    { m_model_idx = model_idx; }

  std::queue<std::shared_ptr<action>> get_draw_actions(shader& shdr,
    std::vector<model>& models, std::vector<texture>& textures) const override; //shader must be bound before

  std::queue<std::shared_ptr<action>> get_draw_actions_patches(shader& shdr,
    std::vector<model>& models, std::vector<texture>& textures) const override; //shader must be bound before

};


