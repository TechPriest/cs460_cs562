/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    renderable_node.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:52:18 2020

\brief  object that can be rendered



***************************************************************************/


#include "renderable_node.h"

#include "graphics/shader/shader.h"
#include "graphics/texture/texture.h"
#include "actions/render_actions.h"
#include "other/append_queue.h"



std::queue<std::shared_ptr<action>> renderable_node::get_draw_actions(shader& shdr,
  std::vector<model>& models, std::vector<texture>& textures) const
{
  std::queue<std::shared_ptr<action>> output;

  output.push(shdr.get_set_uniform_mat4_caching(
    "u_modelToWorld", get_world_transform().get_TRS()));

  std::queue<std::shared_ptr<action>> model_queue =
    models[m_model_idx].get_draw_actions(shdr, textures);

  append_queue(&output, &model_queue);

  return output;
}

std::queue<std::shared_ptr<action>> renderable_node::get_draw_actions_patches(shader& shdr,
  std::vector<model>& models, std::vector<texture>& textures) const
{
  std::queue<std::shared_ptr<action>> output;

  output.push(shdr.get_set_uniform_mat4_caching(
    "u_modelToWorld", get_world_transform().get_TRS()));

  std::queue<std::shared_ptr<action>> model_queue =
    models[m_model_idx].get_draw_actions_patches(shdr, textures);

  append_queue(&output, &model_queue);

  return output;
}
