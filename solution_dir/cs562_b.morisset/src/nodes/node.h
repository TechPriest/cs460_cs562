/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    node.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:52:08 2020

\brief  Base class for all nodes. Resposible for parenting



***************************************************************************/



#pragma once

#include <vector>
#include <queue>
#include <memory>
#include <glm/glm.hpp>
#include "maths/transform.h"

class shader;
class model;
class action;
class texture;

class node
{
protected:
  node* m_parent = nullptr;
  std::vector<node*> m_children;

public:
  ~node() {}

  inline node* get_parent() const { return m_parent; }
  virtual void set_parent(node* parent);
  virtual transform get_world_transform() const;
  virtual std::queue<std::shared_ptr<action>> get_draw_actions(shader& shdr,
    std::vector<model>& models, std::vector<texture>& textures) const { return {}; }
  virtual std::queue<std::shared_ptr<action>> get_draw_actions_patches(shader& shdr,
    std::vector<model>& models, std::vector<texture>& textures) const {
    return {};
  }
};

