/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    camera_node.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:51:05 2020

\brief  Camera functionalities such as getting the projection matrix



***************************************************************************/

#pragma once

#include "transform_node.h"

#include <glm/glm.hpp>

class camera_node : public transform_node
{
protected:
  float m_near = 0.1f;
  float m_far = 500.0f;
  float m_FOVy = 60.0f;
  float m_max_pitch_yaw = 0.2f; //this value works well for 60fps (it should be scaled by fps to work for all)

  int m_vp_width = -1;
  int m_vp_height = -1;
  glm::mat4 m_projection;


public:
  void update_projection();

  void set_viewport(int width, int height);
  void set_frustum(float near, float far, float FOVy);

  inline glm::mat4 get_projection() const { return m_projection; }

  inline void move(glm::vec4 movement) { m_local_tr.m_pos += movement; }
  inline void move_forward(float amount) { move(amount*get_local_forward()); }
  inline void move_right(float amount) { move(amount * get_local_right()); }
  inline void move_up(float amount) { move(amount * get_local_up()); }

  void pitch(float amount);
  void yaw(float amount);

  void look_at(glm::vec3 target);
};


