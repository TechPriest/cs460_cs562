/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    transform_node.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:52:38 2020

\brief  node that has a position, scale and translation. Responsible for
correct world/local transformations with parenting.



***************************************************************************/


#include "transform_node.h"

///SETTERS

void transform_node::set_parent(node * parent)
{
  m_parent = parent;
}


///GETTERS

transform transform_node::get_world_transform() const
{
  const node* parent = get_parent();
  if (parent)
    return parent->get_world_transform() * m_local_tr;

  return m_local_tr;
}

glm::vec4 transform_node::get_local_forward() const
{
  return glm::normalize(m_local_tr.m_orientation * (-eZ_4));
}

glm::vec4 transform_node::get_local_right() const
{
  const glm::vec4 forward = get_local_forward();

  return glm::normalize(m_local_tr.m_orientation * eX_4);
}

glm::vec4 transform_node::get_local_up() const
{
  return glm::normalize(m_local_tr.m_orientation * eY_4);
}

glm::vec4 transform_node::get_world_forward() const
{
  return get_world_transform().operator*(eY_4);
}

glm::vec4 transform_node::get_world_right() const
{
  return get_world_transform().operator*(eX_4);
}

glm::vec4 transform_node::get_world_up() const
{
  return get_world_transform().operator*(eZ_4);
}

