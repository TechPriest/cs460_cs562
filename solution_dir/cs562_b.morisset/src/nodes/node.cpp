/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    node.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:52:03 2020

\brief  Base class for all nodes. Resposible for parenting



***************************************************************************/

#include "node.h"


void node::set_parent(node* parent)
{
  //if I have no children, as I have no transform, I can just set the parent

  //change children to world (NOT NEEDED IF I KEEP WORLD TRANSFORM)
  //if (!m_children.empty())
  //{
  //  for (node* c : m_children)
  //  {
  //    c->set_parent(nullptr); //or world
  //  }
  //}

  m_parent = parent;

  if (!m_children.empty())
  {
    for (node* c : m_children)
    {
      c->set_parent(this);
    }
  }
}

transform node::get_world_transform() const
{
  if (!m_parent)
    return {};

  return m_parent->get_world_transform();
}

