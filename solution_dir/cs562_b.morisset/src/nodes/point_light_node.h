/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    point_light_node.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:51:55 2020

\brief  point light object usefull for lighting



***************************************************************************/


#pragma once

#include <glm/glm.hpp>
#include "transform_node.h"

class shader;
class model;

class point_light_node : public transform_node
{
public:
  glm::vec3 m_ia = glm::vec3(0.1f); //ambient intensity
  glm::vec3 m_id = glm::vec3(1.0f, 191.0f/255.0f, 0.0f); //difuse intensity
  glm::vec3 m_is = glm::vec3(1.0f); //specular intensity
  glm::vec3 m_attenuation = glm::vec3(0.0f, 0.01f, 0.01f);
  float m_infl_radius = 0.0f;


  void update_influence_volume();


  //sphere_model should need no textures
  std::queue<std::shared_ptr<action>> get_draw_bulb_actions(shader& shdr,
    model& sphere_model, std::vector<texture>& textures) const;
  std::queue<std::shared_ptr<action>> get_draw_bulb_actions_patches(shader& shdr,
    model& sphere_model, std::vector<texture>& textures) const;

  //sphere_model should need no textures
  std::queue<std::shared_ptr<action>> get_draw_influence_actions(shader& shdr,
    model& sphere_model, std::vector<texture>& textures) const;
  std::queue<std::shared_ptr<action>> get_draw_influence_actions_patches(shader& shdr,
    model& sphere_model, std::vector<texture>& textures) const;

  //sphere_model should need no textures
  std::queue<std::shared_ptr<action>> get_apply_light_actions(shader& shdr,
    model& sphere_model) const;


  static float compute_influence_radius(glm::vec3 attenuation);
};


