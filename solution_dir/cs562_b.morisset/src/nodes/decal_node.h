/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    decal_node.h

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu

\par    DigiPen login: b.morisset

\par    Course: CS562

\par    Assignment #3

\date    Tue Nov 10 2020

\brief  Node derived from transform_node that constitutes the decal projector.

***************************************************************************/

#pragma once

#include "transform_node.h"

#include <queue>
#include <memory> // std::shared_ptr


class decal_node : public transform_node
{
private:
  //indices for the containers in scene (m_models and m_textures)
  unsigned m_model_idx = 0u;
  unsigned m_diff_tex = 0u;
  unsigned m_norm_tex = 0u;

public:
  inline void set_model_idx(unsigned model_idx)
  {
    m_model_idx = model_idx;
  }
  inline unsigned get_model_idx() const
  {
    return m_model_idx;
  }
  inline void set_diff_tex_idx(unsigned diff_tex_idx)
  {
    m_diff_tex = diff_tex_idx;
  }
  inline unsigned get_diff_tex_idx() const
  {
    return m_diff_tex;
  }
  inline void set_norm_tex_idx(unsigned norm_tex_idx)
  {
    m_norm_tex = norm_tex_idx;
  }
  inline unsigned get_norm_tex_idx() const
  {
    return m_norm_tex;
  }

  
  std::queue<std::shared_ptr<action>> get_project_decal_actions(shader& shdr,
    std::vector<model>& models, std::vector<texture>& textures) const;


  std::queue<std::shared_ptr<action>> get_draw_volume_actions(shader& shdr,
    std::vector<model>& models, std::vector<texture>& textures) const;
};

