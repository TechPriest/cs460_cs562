/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    transform_node.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:52:50 2020

\brief  node that has a position, scale and translation. Responsible for
correct world/local transformations with parenting.



***************************************************************************/


#pragma once


#include "node.h"
#include "maths/transform.h"
#include "maths/utils.h" //eX_3, eY_3, eZ_3



class transform_node : public node
{
protected:
  transform m_local_tr;

public:
  ~transform_node() {}

  void set_parent(node* parent) override;

  ///SETTERS
  inline void set_local_pos(glm::vec4 pos)
    { m_local_tr.m_pos = pos; }
  inline void set_local_pos(glm::vec3 pos)
    { set_local_pos({ pos.x, pos.y, pos.z, 1.0f }); }
  inline void set_local_scale(glm::vec4 scale)
    { m_local_tr.m_scale = scale; }
  inline void set_local_scale(glm::vec3 scale)
    { set_local_scale({ scale.x, scale.y, scale.z, .0f }); }
  inline void set_local_ori(glm::quat orientation)
    { m_local_tr.m_orientation = orientation; }
  inline void set_local_ori(glm::vec3 euler_angles) //[rad]
  {
    /* I could have used m_local_tr.m_orientation = glm::quat(euler_angles)
    if the order wasn't needed in this specific way */
    m_local_tr.m_orientation =
      glm::quat(euler_angles.x * eX_3) *
      glm::quat(euler_angles.y * eY_3) *
      glm::quat(euler_angles.z * eZ_3);
  }

  inline void set_world_pos(glm::vec4 pos)
  {
    assert(false && "does not work");  set_local_pos(get_world_transform().get_inv_TRS() * pos);
  }
  inline void set_world_pos(glm::vec3 pos)
     { set_world_pos({ pos.x, pos.y, pos.z, 1.0f }); }
  //inline void set_world_scale(glm::vec4 scale);
  //inline void set_world_scale(glm::vec3 scale);
  //inline void set_world_ori(glm::quat orientation);
  //inline void set_world_ori(glm::vec3 euler_angles); //[rad]

  /// INCREMENTERS
  inline void incr_local_pos(glm::vec4 pos_incr)
    { m_local_tr.m_pos += pos_incr; }
  inline void incr_local_pos(glm::vec3 pos_incr)
    { incr_local_pos({ pos_incr.x, pos_incr.y, pos_incr.z, .0f}); }
  inline void incr_local_scale(glm::vec4 scale_incr)
    { m_local_tr.m_scale += scale_incr; }
  inline void incr_local_scale(glm::vec3 scale_incr)
    { incr_local_scale({ scale_incr.x, scale_incr.y, scale_incr.z,.0f }); }
  inline void incr_world_pos(glm::vec4 pos_incr)
    { m_local_tr.m_pos += get_world_transform().get_inv_TRS() * pos_incr; }
  inline void incr_world_pos(glm::vec3 pos_incr)
    { incr_world_pos({ pos_incr.x, pos_incr.y, pos_incr.z, .0f}); }


  /// GETTERS
  inline transform get_local_tr() const { return m_local_tr; }
  inline void set_local_tr(const transform& tr) { m_local_tr = tr;}

  transform get_world_transform() const override;

  inline glm::vec4 get_local_pos() const { return m_local_tr.m_pos; }
  inline glm::vec4 get_local_scale() const { return m_local_tr.m_scale; }
  inline glm::quat get_local_ori() const { return m_local_tr.m_orientation; }
  inline glm::vec4 get_world_pos() const { return get_world_transform().m_pos; }
  inline glm::vec4 get_world_scale() const { return get_world_transform().m_scale; }
  inline glm::quat get_world_ori() const { return get_world_transform().m_orientation; }


  glm::vec4 get_local_forward() const; //world
  glm::vec4 get_local_right() const; //world
  glm::vec4 get_local_up() const; //world
  glm::vec4 get_world_forward() const; //world
  glm::vec4 get_world_right() const; //world
  glm::vec4 get_world_up() const; //world
};
