/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    camera_node.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:50:27 2020

\brief  Camera functionalities such as getting the projection matrix



***************************************************************************/

#include "camera_node.h"

#include <glm/gtx/quaternion.hpp>
#include "maths/utils.h"


void camera_node::update_projection()
{
  if (m_vp_width < 1 || m_vp_height < 1)
    return;


  m_projection = glm::perspective(m_FOVy,
    (float)m_vp_width / (float)m_vp_height, m_near, m_far);
}

void camera_node::set_viewport(int width, int height)
{
  /*if (width < 1 || height < 1)
    return;*/

  m_vp_width = width;
  m_vp_height = height;

  update_projection();
}

void camera_node::set_frustum(float near, float far, float FOVy)
{
  assert(near < far);
  assert(near > 0);
  assert(far > 0);
  assert(m_FOVy > 0);

  m_near = near;
  m_far = far;
  m_FOVy = FOVy;

  update_projection();
}

void camera_node::pitch(float amount)
{
  //same clamp must apply to both pitch and yaw
  amount = glm::clamp(amount, -m_max_pitch_yaw, m_max_pitch_yaw);

  const glm::vec3 forward = get_local_forward();
  const glm::vec3 right = glm::normalize(glm::cross(forward, eY_3));

  const glm::vec3 axis = right;
  const glm::quat old_ori = m_local_tr.m_orientation;
  m_local_tr.m_orientation =
    glm::normalize(glm::quat(axis*amount) * m_local_tr.m_orientation);

  /* avoid forward == eY bug
  (can still happen with huge pitches, that is why I clamp the amount) */
  {
    {
      const glm::vec3 prev_cross_n = glm::normalize(glm::cross(forward, eY_3));
      const glm::vec3 new_cross_n =
        glm::normalize(glm::cross(glm::vec3(get_local_forward()), eY_3));

      if (equal(prev_cross_n, -new_cross_n, 0.75f))
        m_local_tr.m_orientation = old_ori;
    }
  }
}

void camera_node::yaw(float amount)
{
  //same clamp must apply to both pitch and yaw
  amount = glm::clamp(amount, -m_max_pitch_yaw, m_max_pitch_yaw);

  const glm::vec3 axis = eY_4;
  m_local_tr.m_orientation =
    glm::normalize(glm::quat(axis*amount) * m_local_tr.m_orientation);
}



void camera_node::look_at(glm::vec3 target)
{
  assert(false);
}


