/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    point_light_node.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:51:43 2020

\brief  point light object usefull for lighting



***************************************************************************/


#include "point_light_node.h"

#include <array>
#include <cassert>
#include <limits>
#include "graphics/shader/shader.h"
#include "graphics/model/model.h"
#include "maths/utils.h"
#include "other/append_queue.h"

/*
namespace
{
  constexpr std::array<float, 122u * 3u> sphere_verts =
  { 0.0f, 1.0f, 0.0f,
    0.0f, -1.0f, 0.0f,
    0.0f, 0.959493f, 0.281733f,
    0.140866f, 0.959493f, 0.243988f,
    0.243988f, 0.959493f, 0.140866f,
    0.281733f, 0.959493f, -0.0000000123149f,
    0.243988f, 0.959493f, - 0.140866f,
    0.140866f, 0.959493f, - 0.243988f,
    -0.0000000246298f, 0.959493f, - 0.281733f,
    -0.140866f, 0.959493f, - 0.243988f,
    -0.243988f, 0.959493f, - 0.140866f,
    -0.281733f, 0.959493f, 0.00000000335963f,
    -0.243988f, 0.959493f, 0.140866f,
    -0.140866f, 0.959493f, 0.243988f,
    0.0f, 0.841254f, 0.540641f,
    0.27032f, 0.841254f, 0.468209f,
    0.468209f, 0.841254f, 0.27032f,
    0.540641f, 0.841254f, -0.0000000236322f,
    0.468209f, 0.841254f, -0.27032f,
    0.27032f, 0.841254f, -0.468209f,
    -0.0000000472643f, 0.841254f, -0.540641f,
    -0.27032f, 0.841254f, -0.468209f,
    -0.468209f, 0.841254f, -0.27032f,
    -0.540641f, 0.841254f, 0.00000000644708f,
    -0.468209f, 0.841254f, 0.27032f,
    -0.27032f, 0.841254f, 0.468209f,
    0.0f, 0.654861f, 0.75575f,
    0.377875f, 0.654861f, 0.654498f,
    0.654498f, 0.654861f, 0.377875f,
    0.75575f, 0.654861f, -0.00000000330349f,
    0.654498f, 0.654861f, -0.377875f,
    0.377875f, 0.654861f, -0.654498f,
    -0.0000000660697f, 0.654861f, -0.75575f,
    -0.377875f, 0.654861f, -0.654498f,
    -0.654498f, 0.654861f, -0.377875f,
    -0.75575f, 0.654861f, 0.00000000901222f,
    -0.654498f, 0.654861f, 0.377875f,
    -0.377875f, 0.654861f, 0.654498f,
    0.0f, 0.415415f, 0.909632f,
    0.454816f, 0.415415f, 0.787764f,
    0.787764f, 0.415415f, 0.454816f,
    0.909632f, 0.415415f, -0.0000000397613f,
    0.787764f, 0.415415f, -0.454816f,
    0.454816f, 0.415415f, -0.787764f,
    -0.0000000795226f, 0.415415f, -0.909632f,
    -0.454816f, 0.415415f, -0.787764f,
    -0.787764f, 0.415415f, -0.454816f,
    -0.909632f, 0.415415f, 0.0000000108473f,
    -0.787764f, 0.415415f, 0.454816f,
    -0.454816f, 0.415415f, 0.787765f,
    0.0f, 0.142315f, 0.989821f,
    0.494911f, 0.142315f, 0.857211f,
    0.857211f, 0.142315f, 0.494911f,
    0.989821f, 0.142315f, -0.0000000432665f,
    0.857211f, 0.142315f, -0.494911f,
    0.494911f, 0.142315f, -0.857211f,
    -0.0000000865329f, 0.142315f, -0.989821f,
    -0.494911f, 0.142315f, -0.857211f,
    -0.857211f, 0.142315f, -0.494911f,
    -0.989821f, 0.142315f, 0.0000000118035f,
    -0.857211f, 0.142315f, 0.494911f,
    -0.49491f, 0.142315f, 0.857211f,
    0.0f, -0.142315f, 0.989821f,
    0.494911f, -0.142315f, 0.857211f,
    0.857211f, -0.142315f, 0.494911f,
    0.989821f, -0.142315f, -0.0000000432665f,
    0.857211f, -0.142315f, -0.494911f,
    0.494911f, -0.142315f, -0.857211f,
    -0.0000000865329f, -0.142315f, -0.989821f,
    -0.494911f, -0.142315f, -0.857211f,
    -0.857211f, -0.142315f, -0.494911f,
    -0.989821f, -0.142315f, 0.0000000118035f,
    -0.857211f, -0.142315f, 0.494911f,
    -0.49491f, -0.142315f, 0.857211f,
    0.0f, -0.415415f, 0.909632f,
    0.454816f, -0.415415f, 0.787764f,
    0.787764f, -0.415415f, 0.454816f,
    0.909632f, -0.415415f, -0.0000000397613f,
    0.787764f, -0.415415f, -0.454816f,
    0.454816f, -0.415415f, -0.787764f,
    -0.0000000795226f, -0.415415f, -0.909632f,
    -0.454816f, -0.415415f, -0.787764f,
    -0.787764f, -0.415415f, -0.454816f,
    -0.909632f, -0.415415f, 0.0000000108473f,
    -0.787764f, -0.415415f, 0.454816f,
    -0.454816f, -0.415415f, 0.787765f,
    0.0f -0.654861f, 0.755749f,
    0.377875f, -0.654861f, 0.654498f,
    0.654498f, -0.654861f, 0.377875f,
    0.755749f, -0.654861f, -0.0000000330349f,
    0.654498f, -0.654861f, -0.377875f,
    0.377875f, -0.654861f, -0.654498f,
    -0.0000000660697f, -0.654861f, -0.755749f,
    -0.377875f, -0.654861f, -0.654498f,
    -0.654498f, -0.654861f, -0.377875f,
    -0.755749f, -0.654861f, 0.00000000901222f,
    -0.654498f, -0.654861f, 0.377875f,
    -0.377875f, -0.654861f, 0.654498f,
    0.0f, -0.841254f, 0.540641f,
    0.27032f, -0.841254f, 0.468209f,
    0.468209f, -0.841254f, 0.27032f,
    0.540641f, -0.841254f, -0.0000000236322f,
    0.468209f, -0.841254f, -0.27032f,
    0.27032f, -0.841254f, -0.468209f,
    -0.0000000472643f, -0.841254f, -0.540641f,
    -0.27032f, -0.841254f, -0.468209f,
    -0.468209f, -0.841254f, -0.27032f,
    -0.540641f, -0.841254f, 0.000000040644708f,
    -0.468209f, -0.841254f, 0.27032f,
    -0.27032f, -0.841254f, 0.468209f,
    0.0f, -0.959493f, 0.281732f,
    0.140866f, -0.959493f, 0.243987f,
    0.243987f, -0.959493f, 0.140866f,
    0.281732f, -0.959493f, -0.0000000123149f,
    0.243987f, -0.959493f, -0.140866f,
    0.140866f, -0.959493f, -0.243987f,
    -0.0000000246298f, -0.959493f, -0.281732f,
    -0.140866f, -0.959493f, -0.243987f,
    -0.243987f, -0.959493f, -0.140866f,
    -0.281732f, -0.959493f, 0.00000000335963f,
    -0.243987f, -0.959493f, 0.140866f,
    -0.140866f, -0.959493f, 0.243987f
  };
  constexpr std::array<unsigned, 240u * 3u> sphere_indices =
  {
    1, 3, 4,
    2, 112, 111,
    1, 4, 5,
    2, 113, 112,
    1, 5, 6,
    2, 114, 113,
    1, 6, 7,
    2, 115, 114,
    1, 7, 8,
    2, 116, 115,
    1, 8, 9,
    2, 117, 116,
    1, 9, 10,
    2, 118, 117,
    1, 10, 11,
    2, 119, 118,
    1, 11, 12,
    2, 120, 119,
    1, 12, 13,
    2, 121, 120,
    1, 13, 14,
    2, 122, 121,
    1, 14, 3,
    2, 111, 122,
    3, 15, 16,
    3, 16, 4,
    4, 16, 17,
    4, 17, 5,
    5, 17, 18,
    5, 18, 6,
    6, 18, 19,
    6, 19, 7,
    7, 19, 20,
    7, 20, 8,
    8, 20, 21,
    8, 21, 9,
    9, 21, 22,
    9, 22, 10,
    10, 22, 23,
    10, 23, 11,
    11, 23, 24,
    11, 24, 12,
    12, 24, 25,
    12, 25, 13,
    13, 25, 26,
    13, 26, 14,
    14, 26, 15,
    14, 15, 3,
    15, 27, 28,
    15, 28, 16,
    16, 28, 29,
    16, 29, 17,
    17, 29, 30,
    17, 30, 18,
    18, 30, 31,
    18, 31, 19,
    19, 31, 32,
    19, 32, 20,
    20, 32, 33,
    20, 33, 21,
    21, 33, 34,
    21, 34, 22,
    22, 34, 35,
    22, 35, 23,
    23, 35, 36,
    23, 36, 24,
    24, 36, 37,
    24, 37, 25,
    25, 37, 38,
    25, 38, 26,
    26, 38, 27,
    26, 27, 15,
    27, 39, 40,
    27, 40, 28,
    28, 40, 41,
    28, 41, 29,
    29, 41, 42,
    29, 42, 30,
    30, 42, 43,
    30, 43, 31,
    31, 43, 44,
    31, 44, 32,
    32, 44, 45,
    32, 45, 33,
    33, 45, 46,
    33, 46, 34,
    34, 46, 47,
    34, 47, 35,
    35, 47, 48,
    35, 48, 36,
    36, 48, 49,
    36, 49, 37,
    37, 49, 50,
    37, 50, 38,
    38, 50, 39,
    38, 39, 27,
    39, 51, 52,
    39, 52, 40,
    40, 52, 53,
    40, 53, 41,
    41, 53, 54,
    41, 54, 42,
    42, 54, 55,
    42, 55, 43,
    43, 55, 56,
    43, 56, 44,
    44, 56, 57,
    44, 57, 45,
    45, 57, 58,
    45, 58, 46,
    46, 58, 59,
    46, 59, 47,
    47, 59, 60,
    47, 60, 48,
    48, 60, 61,
    48, 61, 49,
    49, 61, 62,
    49, 62, 50,
    50, 62, 51,
    50, 51, 39,
    51, 63, 64,
    51, 64, 52,
    52, 64, 65,
    52, 65, 53,
    53, 65, 66,
    53, 66, 54,
    54, 66, 67,
    54, 67, 55,
    55, 67, 68,
    55, 68, 56,
    56, 68, 69,
    56, 69, 57,
    57, 69, 70,
    57, 70, 58,
    58, 70, 71,
    58, 71, 59,
    59, 71, 72,
    59, 72, 60,
    60, 72, 73,
    60, 73, 61,
    61, 73, 74,
    61, 74, 62,
    62, 74, 63,
    62, 63, 51,
    63, 75, 76,
    63, 76, 64,
    64, 76, 77,
    64, 77, 65,
    65, 77, 78,
    65, 78, 66,
    66, 78, 79,
    66, 79, 67,
    67, 79, 80,
    67, 80, 68,
    68, 80, 81,
    68, 81, 69,
    69, 81, 82,
    69, 82, 70,
    70, 82, 83,
    70, 83, 71,
    71, 83, 84,
    71, 84, 72,
    72, 84, 85,
    72, 85, 73,
    73, 85, 86,
    73, 86, 74,
    74, 86, 75,
    74, 75, 63,
    75, 87, 88,
    75, 88, 76,
    76, 88, 89,
    76, 89, 77,
    77, 89, 90,
    77, 90, 78,
    78, 90, 91,
    78, 91, 79,
    79, 91, 92,
    79, 92, 80,
    80, 92, 93,
    80, 93, 81,
    81, 93, 94,
    81, 94, 82,
    82, 94, 95,
    82, 95, 83,
    83, 95, 96,
    83, 96, 84,
    84, 96, 97,
    84, 97, 85,
    85, 97, 98,
    85, 98, 86,
    86, 98, 87,
    86, 87, 75,
    87, 99, 100,
    87, 100, 88,
    88, 100, 101,
    88, 101, 89,
    89, 101, 102,
    89, 102, 90,
    90, 102, 103,
    90, 103, 91,
    91, 103, 104,
    91, 104, 92,
    92, 104, 105,
    92, 105, 93,
    93, 105, 106,
    93, 106, 94,
    94, 106, 107,
    94, 107, 95,
    95, 107, 108,
    95, 108, 96,
    96, 108, 109,
    96, 109, 97,
    97, 109, 110,
    97, 110, 98,
    98, 110, 99,
    98, 99, 87,
    99, 111, 112,
    99, 112, 100,
    100, 112, 113,
    100, 113, 101,
    101, 113, 114,
    101, 114, 102,
    102, 114, 115,
    102, 115, 103,
    103, 115, 116,
    103, 116, 104,
    104, 116, 117,
    104, 117, 105,
    105, 117, 118,
    105, 118, 106,
    106, 118, 119,
    106, 119, 107,
    107, 119, 120,
    107, 120, 108,
    108, 120, 121,
    108, 121, 109,
    109, 121, 122,
    109, 122, 110,
    110, 122, 111,
    110, 111, 99
};
}*/

void point_light_node::update_influence_volume()
{
  m_infl_radius = compute_influence_radius(m_attenuation);

  /* only needed so that at leadt something is being drawn
  if using pointlights incorrectly. Should be half the far
  distance for example */
  constexpr float max_radius = 250.0f;
  m_infl_radius = glm::min(m_infl_radius, max_radius);
}

std::queue<std::shared_ptr<action>> point_light_node::get_draw_bulb_actions(
  shader& shdr, model& sphere_model, std::vector<texture>& textures) const
{
  std::queue<std::shared_ptr<action>> output;

  //make sure the model has 1 mesh
  assert(sphere_model.m_meshes.size() == 1u);

  output.push(shdr.get_set_uniform_mat4_caching("u_modelToWorld", get_world_transform().get_TRS()));

  material m;
  {
    m.m_color_a = glm::vec3(0);
    m.m_color_d = glm::vec3(0);
    m.m_color_s = glm::vec3(0);

    //emissive color matches diffuse intensity
    m.m_color_e = m_id; 
    if (equal(m.m_color_e, glm::vec3(0), 0.05f))
      m.m_color_e = glm::vec3(0.5f);

    m.m_texture_ids[e_tex_types::ambient] = 0;
    m.m_texture_ids[e_tex_types::diffuse] = 0;
    m.m_texture_ids[e_tex_types::specular] = 0;
    m.m_texture_ids[e_tex_types::emissive] = 0;
    m.m_texture_ids[e_tex_types::normal] = 1;
  }

  std::queue<std::shared_ptr<action>> q =
    sphere_model.m_meshes[0u].get_draw_with_custom_material_actions(shdr, m, textures);
  append_queue(&output, &q);

  return output;
}

std::queue<std::shared_ptr<action>> point_light_node::get_draw_bulb_actions_patches(
  shader& shdr, model& sphere_model, std::vector<texture>& textures) const
{
  std::queue<std::shared_ptr<action>> output;

  //make sure the model has 1 mesh
  assert(sphere_model.m_meshes.size() == 1u);

  output.push(shdr.get_set_uniform_mat4_caching("u_modelToWorld", get_world_transform().get_TRS()));

  material m;
  {
    m.m_color_a = glm::vec3(0);
    m.m_color_d = glm::vec3(0);
    m.m_color_s = glm::vec3(0);

    //emissive color matches diffuse intensity
    m.m_color_e = m_id; 
    if (equal(m.m_color_e, glm::vec3(0), 0.05f))
      m.m_color_e = glm::vec3(0.5f);

    m.m_texture_ids[e_tex_types::ambient] = 0;
    m.m_texture_ids[e_tex_types::diffuse] = 0;
    m.m_texture_ids[e_tex_types::specular] = 0;
    m.m_texture_ids[e_tex_types::emissive] = 0;
    m.m_texture_ids[e_tex_types::normal] = 1;
  }

  std::queue<std::shared_ptr<action>> q =
    sphere_model.m_meshes[0u].get_draw_with_custom_material_actions_patches(shdr, m, textures);
  append_queue(&output, &q);

  return output;
}

std::queue<std::shared_ptr<action>> point_light_node::get_draw_influence_actions(
  shader& shdr, model& sphere_model, std::vector<texture>& textures) const
{
  std::queue<std::shared_ptr<action>> output;

  //make sure the model has 1 mesh
  assert(sphere_model.m_meshes.size() == 1u);
  assert(sphere_model.m_materials.size() == 1u);

  transform tr = get_world_transform();
  tr.m_scale = glm::vec4(glm::vec3(m_infl_radius), 0.0f);

  output.push(shdr.get_set_uniform_mat4_caching("u_modelToWorld", tr.get_TRS()));

  material m;
  {
    m.m_color_a = glm::vec3(0);
    m.m_color_d = glm::vec3(0);
    m.m_color_s = glm::vec3(0);

    //emissive color matches diffuse intensity
    m.m_color_e = m_id;
    if (equal(m.m_color_e, glm::vec3(0), 0.05f))
      m.m_color_e = glm::vec3(0.5f);

    m.m_texture_ids[e_tex_types::ambient] = 0;
    m.m_texture_ids[e_tex_types::diffuse] = 0;
    m.m_texture_ids[e_tex_types::specular] = 0;
    m.m_texture_ids[e_tex_types::emissive] = 0;
    m.m_texture_ids[e_tex_types::normal] = 1;
  }


  std::queue<std::shared_ptr<action>> q =
    sphere_model.m_meshes[0u].get_draw_with_custom_material_actions(shdr, m, textures);
  append_queue(&output, &q);

  return output;
}

std::queue<std::shared_ptr<action>> point_light_node::get_draw_influence_actions_patches(
  shader& shdr, model& sphere_model, std::vector<texture>& textures) const
{
  std::queue<std::shared_ptr<action>> output;

  //make sure the model has 1 mesh
  assert(sphere_model.m_meshes.size() == 1u);
  assert(sphere_model.m_materials.size() == 1u);

  transform tr = get_world_transform();
  tr.m_scale = glm::vec4(glm::vec3(m_infl_radius), 0.0f);

  output.push(shdr.get_set_uniform_mat4_caching("u_modelToWorld", tr.get_TRS()));

  material m;
  {
    m.m_color_a = glm::vec3(0);
    m.m_color_d = glm::vec3(0);
    m.m_color_s = glm::vec3(0);

    //emissive color matches diffuse intensity
    m.m_color_e = m_id;
    if (equal(m.m_color_e, glm::vec3(0), 0.05f))
      m.m_color_e = glm::vec3(0.5f);

    m.m_texture_ids[e_tex_types::ambient] = 0;
    m.m_texture_ids[e_tex_types::diffuse] = 0;
    m.m_texture_ids[e_tex_types::specular] = 0;
    m.m_texture_ids[e_tex_types::emissive] = 0;
    m.m_texture_ids[e_tex_types::normal] = 1;
  }


  std::queue<std::shared_ptr<action>> q =
    sphere_model.m_meshes[0u].get_draw_with_custom_material_actions_patches(shdr, m, textures);
  append_queue(&output, &q);

  return output;
}

std::queue<std::shared_ptr<action>> point_light_node::get_apply_light_actions(shader& shdr, model& sphere_model) const
{
  std::queue<std::shared_ptr<action>> output;

  //make sure the model has 1 mesh
  assert(sphere_model.m_meshes.size() == 1u);
  assert(sphere_model.m_materials.size() == 1u);

  transform tr = get_world_transform();
  tr.m_scale = glm::vec4(glm::vec3(m_infl_radius), 0.0f);

  output.push(shdr.get_set_uniform_mat4_caching("u_modelToWorld", tr.get_TRS()));

  std::queue<std::shared_ptr<action>> q =
    sphere_model.m_meshes[0u].get_draw_no_material_actions(shdr);
  append_queue(&output, &q);

  return output;
}

float point_light_node::compute_influence_radius(glm::vec3 attenuation)
{
  constexpr float lim = 0.04f; // 1/255 = 0.0039f
  constexpr float max_lin_att = 0.9999f / lim;
  const float a = lim * attenuation.z;
  const float b = lim * attenuation.y;
  const float c = lim * glm::min(attenuation.x, max_lin_att) - 1.0f;

  const float bb_4ac = b * b - 4.0f * a * c;
  assert(bb_4ac >= 0.0f);
  const float sqrt_bb_4ac = glm::sqrt(bb_4ac);
  const float numerator = -b + sqrt_bb_4ac;

  if (is_zero(numerator, 0.001f))
    return std::numeric_limits<float>::max();

  return numerator / (2.0f * a);
}

