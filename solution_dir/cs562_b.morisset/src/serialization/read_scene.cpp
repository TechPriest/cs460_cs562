/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    read_scene.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 02:00:31 2020

\brief  Simple use of json.cpp to read the scene objects


***************************************************************************/


#include "read_scene.h"

#include <fstream>
#include <json.h>
#include <iostream>
#include "nodes/camera_node.h"
#include "nodes/renderable_node.h"
#include "nodes/decal_node.h"
#include "maths/utils.h" //eX, eY, eZ
#include "graphics/texture/texture.h"

namespace
{
  /**
  * Loads scene json value
  * @param path
  */
  Json::Value Loadfile(const std::string& path)
  {
    Json::Value output;

    std::ifstream file(path);

    //safety check
    if (!file.is_open())  
    {
      std::cerr <<
        "ERROR: error can't open file " << path
        << std::endl;
      return Json::nullValue;
    }

    //// read file's buffer contents into streams
    //std::stringstream sstream;
    //sstream << file.rdbuf();

    //store root json value
    file >> output;

    // close file handlers
    file.close();

    return output;
  }


  bool from_json(Json::Value& val, const char* var_name, float* value)
  {
    Json::Value& float_val = val[var_name];

    if (float_val == Json::nullValue)
      return false;

    *value = float_val.asFloat();

    return true;
  }

  bool from_json(Json::Value& val, const char* var_name, glm::vec3* value)
  {
    Json::Value& vec_val = val[var_name];

    if (vec_val == Json::nullValue)
      return false;

    Json::Value& val_x = vec_val["x"];
    Json::Value& val_y = vec_val["y"];
    Json::Value& val_z = vec_val["z"];

    if (val_x == Json::nullValue || val_y == Json::nullValue || val_z == Json::nullValue)
      return false;

    *value = { val_x.asFloat(), val_y.asFloat(), val_z.asFloat() };

    return true;
  }

  bool from_json(Json::Value& val, const char* var_name, glm::quat* value)
  {
    Json::Value& vec_val = val[var_name];

    if (vec_val == Json::nullValue)
      return false;

    Json::Value& val_w = vec_val["w"];
    Json::Value& val_x = vec_val["x"];
    Json::Value& val_y = vec_val["y"];
    Json::Value& val_z = vec_val["z"];

    if (val_x == Json::nullValue || val_y == Json::nullValue || val_z == Json::nullValue)
      return false;

    *value = { val_w.asFloat(), val_x.asFloat(), val_y.asFloat(), val_z.asFloat() };

    *value = normalize(*value);

    return true;
  }

  bool from_json(Json::Value& val, const char* var_name, std::string* value)
  {
    Json::Value& str_val = val[var_name];

    if (str_val == Json::nullValue)
      return false;

    *value = str_val.asString();

    return true;
  }


  unsigned find_model_idx(const std::string& model_path,
    const std::vector<model>& models)
  {
    for (unsigned i = 0u; i< models.size(); i++)
    {
      if (models[i].m_path == model_path)
        return i;
    }

    return static_cast<unsigned>(-1);
  }

  unsigned find_or_add_texture(const std::string& path, std::vector<texture>* textures,
    const std::string& default_tex_path)
  {
    std::string pth;

    if (path == "")
      pth = default_tex_path;
    else
      pth = path;

    unsigned output = -1;

    std::vector<texture>::const_iterator found =
      std::find(textures->cbegin(), textures->cend(), pth);

    if (found != textures->cend())
    {
      output = static_cast<unsigned>(found - textures->cbegin());
    }
    else
    {
      output = static_cast<unsigned>(textures->size());
      textures->emplace_back(pth);
    }

    return output;
  }
}

// fist node is always the cam
bool read_scene::read(const std::string& path,
  std::vector<std::shared_ptr<node>>* nodes,
  std::vector<model>& models, unsigned* first_decal_idx)
{
  nodes->clear();

  //first value
  Json::Value root = Loadfile(path);
  if (root == Json::nullValue)
    return false;


  //camera
  {
    std::shared_ptr<camera_node> cam_node_ptr = std::make_shared<camera_node>();
    nodes->push_back(cam_node_ptr);

    Json::Value& cam_value = root["camera"];
    if (cam_value == Json::nullValue)
      return false;

    glm::vec3 pos;
    float near, far, fov_y_deg;

    bool success = from_json(cam_value, "translate", &pos);

    if (cam_value.isMember("rotate"))
    {
      glm::vec3 euler_angles;
      success = success && from_json(cam_value, "rotate", &euler_angles);

      const glm::vec3 eul_angl_rads(
        glm::radians(euler_angles.x),
        glm::radians(euler_angles.y),
        glm::radians(euler_angles.z));

      cam_node_ptr->set_local_ori(eul_angl_rads);
    }
    else if (cam_value.isMember("rotate_q"))
    {
      glm::quat ori;

      success = success && from_json(cam_value, "rotate_q", &ori);

      cam_node_ptr->set_local_ori(ori);
    }
    else
      assert(false);

    success = success && from_json(cam_value, "near", &near);
    success = success && from_json(cam_value, "far", &far);
    success = success && from_json(cam_value, "FOVy", &fov_y_deg);

    if (!success)
      return false;

    cam_node_ptr->set_local_pos(pos);
    cam_node_ptr->set_frustum(near, far, glm::radians(fov_y_deg));
  }

  //objects
  {
    Json::Value& objects_value = root["objects"];
    if (objects_value == Json::nullValue)
      return false;

    nodes->reserve(nodes->size() + objects_value.size());

    for (Json::Value& obj_val : objects_value)
    {
      std::shared_ptr<renderable_node> node_ptr = std::make_shared<renderable_node>();
      nodes->push_back(node_ptr);

      //model
      {
        Json::Value& modl_val = obj_val["mesh"];
        if (modl_val == Json::nullValue)
          return false;

        const unsigned model_idx = find_model_idx(modl_val.asCString(), models);
        if (model_idx == static_cast<unsigned>(-1))
          return false;

        node_ptr->set_model_idx(model_idx);
      }
      //transform
      {
        glm::vec3 pos, euler_angles, scale;

        bool success = from_json(obj_val, "translate", &pos);
        success = success && from_json(obj_val, "rotate", &euler_angles);
        success = success && from_json(obj_val, "scale", &scale);

        if (!success)
          return false;

        const glm::vec3 eul_angl_rads(
          glm::radians(euler_angles.x),
          glm::radians(euler_angles.y),
          glm::radians(euler_angles.z));

        node_ptr->set_local_pos(pos);
        node_ptr->set_local_ori(eul_angl_rads);
        node_ptr->set_local_scale(scale);
      }
    }
  }

  //decals
  {
    Json::Value& decals_value = root["decals"];
    if (decals_value != Json::nullValue)
    {
      {
        const auto nodes_size = nodes->size();
        nodes->reserve(nodes_size + decals_value.size());
        if(first_decal_idx)
          *first_decal_idx = static_cast<unsigned>(nodes_size);
      }

      for (Json::Value& decal_val : decals_value)
      {
        std::shared_ptr<decal_node> node_ptr = std::make_shared<decal_node>();
        nodes->push_back(node_ptr);

        //model (HARDCODED TO CUBE.OBJ)
        {
          const unsigned model_idx = find_model_idx("./data/meshes/decal_cube.obj", models);
          if (model_idx == static_cast<unsigned>(-1))
            return false;

          node_ptr->set_model_idx(model_idx);
        }
        //textures
        {
          const unsigned model_idx = node_ptr->get_model_idx();
          model& modl = models[model_idx];
          assert(modl.m_materials.size() == 1u);

          std::string diff_tex_path, norm_tex_path;
          bool success = from_json(decal_val, "diffuse", &diff_tex_path);
          assert(success);
          success = from_json(decal_val, "normal", &norm_tex_path);
          assert(success);

          const unsigned diff_tex_id =
            find_or_add_texture(diff_tex_path, modl.m_textures, "./data/textures/default.png");
          const unsigned norm_tex_id =
            find_or_add_texture(norm_tex_path, modl.m_textures, "./data/textures/default_ddn.png");

          node_ptr->set_diff_tex_idx(diff_tex_id);
          node_ptr->set_norm_tex_idx(norm_tex_id);
        }
        //transform
        {
          glm::vec3 pos, euler_angles, scale;

          bool success = from_json(decal_val, "translate", &pos);
          success = success && from_json(decal_val, "rotate", &euler_angles);
          success = success && from_json(decal_val, "scale", &scale);

          if (!success)
            return false;

          const glm::vec3 eul_angl_rads(
            glm::radians(euler_angles.x),
            glm::radians(euler_angles.y),
            glm::radians(euler_angles.z));

          node_ptr->set_local_pos(pos);
          node_ptr->set_local_ori(eul_angl_rads);
          node_ptr->set_local_scale(scale);
        }
      }

    }
  }


  return true;
}

