/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    read_scene.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 02:00:39 2020

\brief  Simple use of json.cpp to read the scene objects



***************************************************************************/


#pragma once

#include <vector>
#include <string>
#include <memory>

class node;
class model;

class read_scene
{
public:

  static bool read(const std::string& path,
    std::vector<std::shared_ptr<node>>* nodes,
    std::vector<model>& models);

  static bool read(const std::string& path,
    std::vector<std::shared_ptr<node>>* nodes,
    std::vector<model>& models, unsigned* first_decal_idx);

};


