/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    cam_controller.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:58:42 2020

\brief  A controller that gives a freecam behavior to its attached camera_node.

 WASDQE: movement (+SHIFT for x3 speed)
 arrows: for pitch and yaw
 right-click + drag mouse: for pitch and yaw

 vertical movement does nt depend on angle, it always moves along eY
 to avoid problems the rotation has a full vertical limit

***************************************************************************/


#include "cam_controller.h"

#include "maths/utils.h"
#include "nodes/camera_node.h"
#include "input/input.h"


void cam_controller::set_cam(std::weak_ptr<camera_node> cam)
{
  m_cam_ptr = cam;

  m_init_tr = m_cam_ptr.lock()->get_local_tr();
}

void cam_controller::update(float dt)
{
  input& input_instance = input::get_instance();

  //WASDQE (and SHIFT to go faster)
  {
    float speed = m_lin_speed;

    if (input_instance.KeyMaintained(GLFW_KEY_LEFT_SHIFT) || input_instance.KeyMaintained(GLFW_KEY_RIGHT_SHIFT))
      speed *= 3.0f;


    //forward movement
    {
      char sense = 0;

      if (input_instance.KeyMaintained(GLFW_KEY_W))
        sense += 1;
      if (input_instance.KeyMaintained(GLFW_KEY_S))
        sense -= 1;

      if(sense)
        m_cam_ptr.lock()->move_forward(sense* speed *dt);
    }

    //lateral movement
    {
      char sense = 0;

      if (input_instance.KeyMaintained(GLFW_KEY_A))
        sense -= 1;
      if (input_instance.KeyMaintained(GLFW_KEY_D))
        sense += 1;

      if (sense)
        m_cam_ptr.lock()->move_right(sense* speed *dt);
    }

    //vertical movement
    {
      char sense = 0;

      if (input_instance.KeyMaintained(GLFW_KEY_Q))
        sense += 1;
      if (input_instance.KeyMaintained(GLFW_KEY_E))
        sense -= 1;

      if (sense)
        m_cam_ptr.lock()->move(sense* speed *dt * eY_4);
    }
  }

  //arrows
  {
    //pitch
    {
      char sense = 0;

      if (input_instance.KeyMaintained(GLFW_KEY_UP))
        sense += 1;
      if (input_instance.KeyMaintained(GLFW_KEY_DOWN))
        sense -= 1;

      if (sense)
        m_cam_ptr.lock()->pitch(sense*m_ang_speed*dt);
    }
    //yaw
    {
      char sense = 0;

      if (input_instance.KeyMaintained(GLFW_KEY_RIGHT))
        sense -= 1;
      if (input_instance.KeyMaintained(GLFW_KEY_LEFT))
        sense += 1;

      if (sense)
        m_cam_ptr.lock()->yaw(sense*m_ang_speed*dt);
    }
  }

  //mouse
  {
    if (input_instance.KeyMaintained(GLFW_MOUSE_BUTTON_2))
    {
      glm::vec2 cursor_speed = input_instance.get_cursor_speed();

      if (!is_zero(cursor_speed, 0.0001f))
      {
        m_cam_ptr.lock()->yaw(-cursor_speed.x*m_ang_speed*dt);
        m_cam_ptr.lock()->pitch(-cursor_speed.y*m_ang_speed*dt);
      }
    }
  }



  //reset cam
  {
    if (input_instance.KeyPress(GLFW_KEY_V))
      m_cam_ptr.lock()->set_local_tr(m_init_tr);
  }
}



