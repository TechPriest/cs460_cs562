/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    cam_controller.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:58:49 2020

\brief  A controller that gives a freecam behavior to its attached camera_node.

 WASDQE: movement (+SHIFT for x3 speed)
 arrows: for pitch and yaw
 right-click + drag mouse: for pitch and yaw

 vertical movement does nt depend on angle, it always moves along eY
 to avoid problems the rotation has a full vertical limit

***************************************************************************/

#pragma once

#include <memory> //std::shared_ptr, std::weak_ptr
#include <glm/glm.hpp>
#include "maths/transform.h"

class camera_node;

class cam_controller
{
protected:
  std::weak_ptr<camera_node> m_cam_ptr;
  float m_lin_speed = 1.0f;
  float m_ang_speed = 1.0f;
  transform m_init_tr{};

public:
  void set_cam(std::weak_ptr<camera_node> cam);

  inline std::weak_ptr<camera_node> get_cam() 
    { return m_cam_ptr; }

  void update( float dt);

  inline void set_lin_speed(float speed)
    { m_lin_speed = speed; }
  inline void set_ang_speed(float ang_speed)
    { m_ang_speed = ang_speed; }
};


