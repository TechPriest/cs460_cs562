/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    splash_screen.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 02:00:48 2020

\brief  cosmetic functions to draw a simple fullwindow texture



***************************************************************************/


#include "splash_screen.h"

#include "graphics/window/window.h"
#include "threading/work_thread.h"
#include "graphics/texture/texture.h"
#include "graphics/shader/shader.h"


void splash_screen::draw(work_thread* rend_thr, GLFWwindow** glfw_win_ptr)
{
  texture splash_screen("./data/textures/splashscreen.jpg");
  shader tex_quat_shdr("./data/shaders/texture_quad.shdr");

  splash_screen.get_load_action()->do_action();
  tex_quat_shdr.get_load_action()->do_action();

  {
    rend_thr->begin_pushing();
    rend_thr->push_action(splash_screen.get_create_action());
    rend_thr->push_action(tex_quat_shdr.get_create_action());

    unsigned vao, vbo, ibo;

    //mesh
    {
      float quad_verts[20u] =
      {
        -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
         1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
         1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
        -1.0f,  1.0f, 0.0f, 0.0f, 1.0f
      };

      unsigned quad_indices[6u] =
      {
        0,1,2,
        0,2,3
      };

      vertex_buffer_layout layout;
      layout.push_interleaved<float>(3u); //pos
      layout.push_interleaved<float>(2u); //uv


      rend_thr->push_action(std::make_shared<create_vertex_array>(&vao));//does not bind
      rend_thr->push_action(std::make_shared<bind_vertex_array>(&vao));

      rend_thr->push_action(std::make_shared<create_vertex_buffer>(&vbo,
        quad_verts, static_cast<unsigned>(4u * 5u * sizeof(float)), GL_STATIC_DRAW)); //binds
      rend_thr->push_action(std::make_shared<set_vertex_buffer_layout>(layout));
      rend_thr->push_action(std::make_shared<create_index_buffer>(&ibo, quad_indices,
        6u * static_cast<unsigned>(sizeof(unsigned)), GL_STATIC_DRAW)); //binds
    }

    //draw
    {
      unsigned loc = 0u;

      rend_thr->push_action(tex_quat_shdr.get_bind_action());

      rend_thr->push_action(texture::get_set_active_slot_action(0));
      rend_thr->push_action(splash_screen.get_bind_action());
      rend_thr->append_actions(tex_quat_shdr.get_set_uniform_texture_actions("u_texture", 0));

      rend_thr->push_action(std::make_shared<render_clear>(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

      rend_thr->push_action(std::make_shared<draw_elements>(6u));

      rend_thr->push_action(std::make_shared<swap_buffers_and_poll_events>(glfw_win_ptr));
    }

    //free from GPU
    {
      rend_thr->push_action(splash_screen.get_delete_action());
      rend_thr->push_action(tex_quat_shdr.get_delete_action());
      rend_thr->push_action(std::make_shared<delete_index_buffer>(&ibo));
      rend_thr->push_action(std::make_shared<delete_vertex_buffer>(&vbo));
      rend_thr->push_action(std::make_shared<delete_vertex_array>(&vao));
      rend_thr->end_pushing();
    }

    rend_thr->wait_for();
  }
}

