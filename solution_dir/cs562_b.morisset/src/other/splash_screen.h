/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    splash_screen.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 02:00:58 2020

\brief  cosmetic functions to draw a simple fullwindow texture



***************************************************************************/


#pragma once

class work_thread;
struct GLFWwindow;

class splash_screen
{
public:

  static void draw(work_thread* rend_thread, GLFWwindow** glfw_win_ptr);
};


