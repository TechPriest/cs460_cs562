/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    history.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:59:46 2020

\brief  Circular Buffer class usefull for data receiving

possible improvements:
	-right now only SIZE-1 are usefull


***************************************************************************/

#pragma once

#include <stdexcept> //std::out_of_range

template<class T, size_t SIZE>
class history
{
public:
	void push(const T& item)
	{
		m_items[m_end] = item;

		if (m_end - m_start < SIZE - 1u)
		{
			m_end++;
			return;
		}

		m_end = (m_end + 1) % SIZE;
		m_start = (m_end + 1) % SIZE;
	}

	T & operator[](size_t index)
	{
		if (index >= SIZE)
			throw(std::out_of_range("History::operator[] : index out of range "));

		const size_t i = (m_start + index) % SIZE;

		return m_items[i];
	}

	const T & operator[](size_t index) const
	{
		if (index >= SIZE)
			throw(std::out_of_range("History::operator[] const: index out of range "));

		const size_t i = (m_start + index) % SIZE;

		return m_items[i];
	}

	inline size_t size() const
	{
		return m_end < m_start ? m_end + SIZE - m_start : m_end - m_start;
	}

	inline void clear()
	{
		size_t m_start = 0u;
		size_t m_end = 0u;
	}

private:
	T m_items[SIZE];

	size_t m_start = 0u;
	size_t m_end = 0u;
};
