/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    append_queue.h

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu

\par    DigiPen login: b.morisset

\par    Course: CS562

\par    Assignment #1

\date    Wed Oct 14 13:38:08 2020

\brief  header only file containin function to append a queue onto another

***************************************************************************/

#pragma once

#include <queue>

/// <summary>
/// Appends orig into dest. Leaves orig empty
/// </summary>
/// <typeparam name="T"></typeparam>
/// <param name="dest"></param>
/// <param name="orig"></param>
template <class T>
inline void append_queue(std::queue<T>* dest, std::queue<T>* orig)
{
  while (!orig->empty())
  {
    dest->push(orig->front());
    orig->pop();
  }
}

/// <summary>
/// Appends a copy of orig into dest
/// </summary>
/// <typeparam name="T"></typeparam>
/// <param name="dest"></param>
/// <param name="orig"></param>
template <class T>
inline void append_queue(std::queue<T>* dest, std::queue<T> orig)
{
  append_queue(dest, &orig);
}


/// <summary>
/// clears a queue
/// </summary>
/// <typeparam name="T"></typeparam>
/// <param name="q"></param>
template <class T>
inline void clear_queue(std::queue<T>* q)
{
  while (!(q->empty()))
  {
    q->pop();
  }
}