/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    chronometer.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:56:54 2020

\brief  Wrapper around chrono that gives a simple interface for measuring time elapsed.



***************************************************************************/



#pragma once

#include <chrono>

class chronometer
{
private:
	using time_pt = std::chrono::time_point<std::chrono::high_resolution_clock>;
	time_pt m_start_time;

public:

	inline void start()
	{
		m_start_time = std::chrono::high_resolution_clock::now();
	}

	inline unsigned read_ms() const
	{
		const time_pt now = std::chrono::high_resolution_clock::now();
		const auto elapsed_time
			= std::chrono::duration_cast<std::chrono::milliseconds>(now - m_start_time);
		return static_cast<unsigned>(elapsed_time.count());
	}
	inline unsigned read_us() const
	{
		const time_pt now = std::chrono::high_resolution_clock::now();
		const auto elapsed_time
			= std::chrono::duration_cast<std::chrono::microseconds>(now - m_start_time);
		return static_cast<unsigned>(elapsed_time.count());
	}

	inline unsigned restart_ms()
	{
		const unsigned output = read_ms();
		start();
		return output;
	}
	inline unsigned restart_us()
	{
		const unsigned output = read_us();
		start();
		return output;
	}
};
