/**
* @file   fps.h
* @author Benat Morisset, 540002817, b.morisset@digipen.edu
* @date   2020/01/08
* @brief  Contains a function used to compute DeltaTime and cap fps if needed
*
* @copyright Copyright(C) 2020 DigiPen Institute of Technology .
*/
#pragma once

#include <chrono>

class fps_system
{
  ///SINGLETON-START
public:
  static fps_system &get_instance()
  {
    static fps_system instance;
    return instance;
  }
private:
  fps_system() = default;
  ~fps_system() = default;
  fps_system(const fps_system &) = delete;
  fps_system& operator=(const fps_system &) = delete;
  ///SINGLETON-END

public:
  void Init(unsigned char fps = 60u);
  void Update();

  float m_dt = 0.016f;
  float m_real_dt = 0.016f;
  float m_speed_factor = 1.0f;
  unsigned char m_fps = 60u;

private:
  std::chrono::time_point<std::chrono::system_clock>
    m_last_frame;
};
