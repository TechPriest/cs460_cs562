/**
* @file   fps.cpp
* @author Benat Morisset, 540002817, b.morisset@digipen.edu
* @date   2020/01/08
* @brief  Contains a function used to compute DeltaTime and cap fps if needed
*
* @copyright Copyright(C) 2020 DigiPen Institute of Technology .
*/

#include "fps.h"
#include <iostream>

#include <thread>
#include <stdlib.h> // sleep

/**
* Computes DeltaTime and updates m_last_frame
* @param fps_cap, fps cap fps if specified (0u == dont cap)
*/
void fps_system::Init(const unsigned char fps_cap)
{
  m_fps = fps_cap;

  if (fps_cap != 0u)
    m_dt = 1.0f / static_cast<float>(m_fps);
  else
    m_dt = 0.0f;

  m_last_frame = std::chrono::system_clock::now();
}

/**
* Computes DeltaTime and updates m_last_frame
* @param frameStart, timepoint of the start of the frame
* @param fpsCap, fps cap fps if specified (0u == dont cap)
*/
void fps_system::Update()
{
  //
  {
    // Fps cap and DeltaTime calculation
    if (m_fps != 0u)
    {
      const std::chrono::duration<float> targetFrameTime =
        std::chrono::duration<float>(1.0f / static_cast<float>(m_fps));

      // Actual time spent on calculations
      const std::chrono::duration<float> frameTime =
        std::chrono::system_clock::now() - m_last_frame;

      if (frameTime.count() > targetFrameTime.count())
      {
        std::cout << "WARNING: frame took " <<
          frameTime.count() - targetFrameTime.count()
          << "s more than expected" << std::endl;
        m_real_dt = frameTime.count(); // [s]
      }
      else
      {
        const unsigned sleepTime = static_cast<unsigned>(
          (targetFrameTime.count() - frameTime.count())*1000.0f); // [ms]

        std::this_thread::sleep_for(std::chrono::milliseconds(sleepTime));
        m_real_dt = targetFrameTime.count(); // [s]
      }
    }
    else
    {
      // Actual time spent on calculations
      const std::chrono::duration<float> frameTime =
        std::chrono::system_clock::now() - m_last_frame;
      m_real_dt = frameTime.count(); // [s]
    }
  }

  //for slowmo 
  m_dt = m_speed_factor * m_real_dt;

  //update 
  m_last_frame = std::chrono::system_clock::now();
}
