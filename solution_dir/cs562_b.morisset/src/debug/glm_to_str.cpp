/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    glm_to_str.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:59:14 2020

\brief  Contains the definition of the functions used to format
 vectors, matrices, etc into std::string



***************************************************************************/

#include <iomanip>
#include <sstream>

#include "glm_to_str.h"

/**
* prints a glm::vec2 as (x, y)
* @param v, the vector
* @return "(x, y)"
*/
std::string to_str(const glm::vec2 v)
{
	std::stringstream result;
	result<<"("<<v.x<<", "<<v.y<<")";
	return result.str();
}

/**
* prints a glm::vec3 as (x, y, z)
* @param v, the vector
* @return "(x, y, z)"
*/
std::string to_str(const glm::vec3 v)
{
	std::stringstream result;
	result << "(" << v.x << ", " << v.y << ", " << v.z << ")";
	return result.str();
}

/**
* prints a glm::vec3 as (x, y, z, w)
* @param v, the vector
* @return "(x, y, z, w)"
*/
std::string to_str(const glm::vec4 v)
{
	std::stringstream result;
	result << "(" << v.x << ", " << v.y << ", " << v.z << ", " << v.w << ")";
	return result.str();
}

/**
* prints a glm::quat as (x, y, z, w)
* @param v, the vector
* @return "(x, y, z, w)"
*/
std::string to_str(const glm::quat q)
{
	std::stringstream result;
	result << "(" << q.x << ", " << q.y << ", " << q.z << ", " << q.w << ")";
	return result.str();
}

/**
* prints a glm::mat3 as /00, 01, 02\
*                       |10, 11, 12|
*                       \32, 21, 22/
*                       
* @param v, the vector
* @return the string representing the matrix
*/
std::string to_str(const glm::mat3 m)
{
  std::stringstream result;
  result << "\n"
    << " /" << std::setw(10) << std::setprecision(3) << m[0][0] << " " << std::setw(10) << std::setprecision(3) << m[0][1] << " " << std::setw(10) << std::setprecision(3) << m[0][2]  << "\\" << "\n"
    << "| " << std::setw(10) << std::setprecision(3) << m[1][0] << " " << std::setw(10) << std::setprecision(3) << m[1][1] << " " << std::setw(10) << std::setprecision(3) << m[1][2]  << " |" << "\n"
    << " \\" << std::setw(10) << std::setprecision(3) << m[2][0] << " " << std::setw(10) << std::setprecision(3) << m[2][1] << " " << std::setw(10) << std::setprecision(3) << m[2][2]  << "/" << std::endl;

  return result.str();
}

/**
* prints a glm::mat4 as /00, 01, 02, 03\
*                       |10, 11, 12, 13|
*                       |20, 21, 22, 23|
*                       \30, 31, 32, 33/
* @param v, the vector
* @return the string representing the matrix
*/
std::string to_str(const glm::mat4 m)
{
	std::stringstream result;
	result << "\n"
	<< " /" << std::setw(10) << std::setprecision(3) << m[0][0] << " " << std::setw(10) << std::setprecision(3) << m[0][1] << " " << std::setw(10) << std::setprecision(3) << m[0][2] << ", " << std::setw(10) << std::setprecision(3) << m[0][3] << "\\" << "\n"
	<< "| " << std::setw(10) << std::setprecision(3) << m[1][0] << " " << std::setw(10) << std::setprecision(3) << m[1][1] << " " << std::setw(10) << std::setprecision(3) << m[1][2] << ", " << std::setw(10) << std::setprecision(3) << m[1][3] << " |" << "\n"
	<< "| " << std::setw(10) << std::setprecision(3) << m[2][0] << " " << std::setw(10) << std::setprecision(3) << m[2][1] << " " << std::setw(10) << std::setprecision(3) << m[2][2] << ", " << std::setw(10) << std::setprecision(3) << m[2][3] << " |" << "\n"
	<< " \\" << std::setw(10) << std::setprecision(3) << m[3][0] << " " << std::setw(10) << std::setprecision(3) << m[3][1] << " " << std::setw(10) << std::setprecision(3) << m[3][2] << ", " << std::setw(10) << std::setprecision(3) << m[3][3] << "/" << std::endl;
	return result.str();
}


