/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    glm_to_str.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:59:36 2020

\brief  Contains the definition of the functions used to format
 vectors, matrices, etc into std::string



***************************************************************************/

#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <string>

std::string to_str(const glm::vec2 v);
std::string to_str(const glm::vec3 v);
std::string to_str(const glm::vec4 v);
std::string to_str(const glm::quat v);
std::string to_str(const glm::mat3 m);
std::string to_str(const glm::mat4 m);


