/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    float_exceptions.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:32:13 2020

\brief  A function to enable float exceptions (divide by zero, etc)



***************************************************************************/


#include "float_exceptions.h"

#include <float.h> //EM_ZERODIVIDE, EM_INVALID, _MCW_EM

void float_exceptions::enable()
{
  
  /*unsigned old_control;
  unsigned control = 0u;

  control |= EM_ZERODIVIDE;
  control |= EM_INVALID;

  _controlfp_s(&old_control, control, _MCW_EM);*/

#ifdef _MSC_VER
  unsigned exceptions = 0;
  exceptions |= EM_ZERODIVIDE;
  exceptions |= EM_INVALID;
#pragma warning(suppress : 4996)
  _controlfp(~exceptions, _MCW_EM);
#else
  feenableexcept(FE_DIVBYZERO | FE_INVALID);
#endif
}


