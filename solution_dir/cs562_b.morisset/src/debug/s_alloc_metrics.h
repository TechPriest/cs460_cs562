/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    s_alloc_metrics.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:32:38 2020

\brief  A static struct with functions for getting allocation stats.
Usefull to hunt for leaks.
Including this file overloads the new and delete operators.



***************************************************************************/


#pragma once

#include <mutex>

static struct s_alloc_metrics
{
  size_t allocations = 0u;
  size_t dealocations = 0u;
  size_t allocated = 0u;
  size_t freed = 0u;

  std::mutex stats_mutex;

  inline size_t get_usage() const
  {
    return allocated - freed;
  }
  inline size_t get_call_difference() const
  {
    return allocations - dealocations;
  }

  inline bool all_freed() const
  {
    const size_t bytes_in_usage = get_usage();
  
    return bytes_in_usage == 0u;
  }

  //inline void print_info() const
  //{
  //  const size_t bytes_in_usage = get_usage();
  //
  //  std::cout << "ALLOC_INFO: using " << bytes_in_usage << " bytes, allocations: " << allocations << "(" << allocated << " bytes), dealocations " << dealocations << "(" << freed << " bytes)" << std::endl;
  //}
  //
  //inline void check_all_freed() const
  //{
  //  const size_t diff = get_call_difference();
  //  const size_t bytes_in_usage = get_usage();
  //
  //  if (diff > 0)
  //    std::cout << "ALLOC_INFO: missed " << bytes_in_usage << " bytes from " << diff << " allocations" << std::endl;
  //  else
  //    std::cout << "ALLOC_INFO: no leaks :) " << std::endl;
  //}

} s_alloc_metrics;

void* operator new(size_t size)
{
  s_alloc_metrics.stats_mutex.lock();
  s_alloc_metrics.allocated += size;
  s_alloc_metrics.allocations++;
  s_alloc_metrics.stats_mutex.unlock();

  return malloc(size);
}

void operator delete(void* memory, size_t size)
{
  s_alloc_metrics.stats_mutex.lock();
  s_alloc_metrics.freed += size;
  s_alloc_metrics.dealocations++;
  s_alloc_metrics.stats_mutex.unlock();

  free(memory);
}

void* operator new[](size_t size)
{
  return operator new(size);
}

void operator delete[](void* memory, size_t size)
{
  return operator delete(memory, size);
}