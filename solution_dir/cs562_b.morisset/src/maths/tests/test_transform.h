#pragma once
namespace test_transform
{
  bool test_pos_concat();
  bool test_sca_concat();
  bool test_ori_concat();
  bool test_all_concat();

} //namespace test_transform

