#include "test_transform.h"

#include "maths/transform.h"
#include "maths/utils.h"

namespace test_transform
{
  namespace
  {
    constexpr float test_epsilon = 0.01f;
  }

  bool test_pos_concat()
  {
    {
      transform parent;
      parent.m_pos = { 3,1,4,1 };

      transform child;
      child.m_pos = { 1,3,-4,1 };

      transform child_in_world = parent * child;

      constexpr glm::mat4 expected =
      {
        {1, 0, 0, 0},
        {0, 1, 0, 0},
        {0, 0, 1, 0},
        {4, 4, 0, 1}
      };

      if (!equal(child_in_world.m_pos, { 4, 4, 0, 1 }, test_epsilon))
        return false;

      if (!equal(child_in_world.get_TRS(), expected, test_epsilon))
        return false;
    }

    return true;
  }

  bool test_sca_concat()
  {
    transform parent;
    parent.m_scale = { 2.0f, 0.5f, 7.0f, 0.0f };

    transform child;
    child.m_scale = { 0.5f, 2.0f, 3.0f, 0.0f };

    transform child_in_world = parent * child;

    constexpr glm::mat4 expected =
    {
      {1, 0, 0, 0},
      {0, 1, 0, 0},
      {0, 0, 21, 0},
      {0, 0, 0, 1}
    };

    if (!equal(child_in_world.m_scale, { 1.0f, 1.0f, 21.0f, 0.0f }, test_epsilon))
      return false;

    if (!equal(child_in_world.get_TRS(), expected, test_epsilon))
      return false;

    return true;
  }

  bool test_ori_concat()
  {
    //simple test with parent and child both rotating around eX
    {
      constexpr float pi = glm::pi<float>();
      constexpr float pi_over_6 = pi / 6.0f;
      constexpr float pi_over_3 = pi / 3.0f;
      const float cos_pi_over_3 = glm::cos(pi_over_3);
      const float sin_pi_over_3 = glm::sin(pi_over_3);

      transform parent;
      parent.m_orientation = glm::quat({ pi_over_6, 0.0f, 0.0f });

      transform child;
      child.m_orientation = glm::quat({ pi_over_6, 0.0f, 0.0f });

      transform child_in_world = parent * child;

      const glm::mat4 expected =
      {
        {1, 0, 0, 0},
        {0, cos_pi_over_3, sin_pi_over_3, 0},
        {0, -sin_pi_over_3, cos_pi_over_3, 0},
        {0, 0, 0, 1}
      };

      if (!equal(child_in_world.m_orientation, { sin_pi_over_3, cos_pi_over_3, 0.0f, 0.0f }, test_epsilon))
        return false;

      if (!equal(child_in_world.get_TRS(), expected, test_epsilon))
        return false;
    }

    /* more complex test with grandparent and parent rotation around eZ
    and child around eY. It should be the same transformation as the one
    required to make eX become eY and then rotating around the new eY */
    {
      constexpr float pi = glm::pi<float>();
      constexpr float pi_over_6 = pi / 6.0f;
      constexpr float pi_over_3 = pi / 3.0f;
      const float cos_pi_over_6 = glm::cos(pi_over_6);
      const float sin_pi_over_6 = glm::sin(pi_over_6);

      transform grand_parent;
      grand_parent.m_orientation = glm::quat({ 0.0f, 0.0f, pi_over_6 });

      transform parent;
      parent.m_orientation = glm::quat({ 0.0f, 0.0f, pi_over_3});

      transform child;
      child.m_orientation = glm::quat({ 0.0f, -pi_over_6, 0.0f });

      transform child_in_world = grand_parent * parent * child;


      /* this is what the grandfather * the father rotation should be
      It is in row-major because that is how I am used to do maths,
      but glm uses column-major so I have to transpose before comparing */
      constexpr glm::mat4 rot_90_ez =
      {
        { 0, -1,  0,  0},
        { 1,  0,  0,  0},
        { 0,  0,  1,  0},
        { 0,  0,  0,  1}
      };
      if (!equal((grand_parent * parent).get_TRS(), glm::transpose(rot_90_ez), test_epsilon))
        return false;


      /* this is what the child rotation should be
      It is in row-major because that is how I am used to do maths,
      but glm uses column-major so I have to transpose before comparing */
      const glm::mat4 rot_30_ey =
      {
        { cos_pi_over_6,  0,  -sin_pi_over_6, 0},
        { 0,              1,  0,              0},
        { sin_pi_over_6, 0,  cos_pi_over_6,  0},
        { 0,              0,  0,              1}
      };
      if (!equal(child.get_TRS(), glm::transpose(rot_30_ey), test_epsilon))
        return false;


      const glm::mat4 expected = glm::transpose(rot_90_ez * rot_30_ey);

      if (!equal(child_in_world.get_TRS(), expected, test_epsilon))
        return false;
    }

    return true;
  }

  bool test_all_concat()
  {
    {
      constexpr float pi = glm::pi<float>();
      constexpr float pi_over_6 = pi / 6.0f; //30deg
      constexpr float pi_over_3 = pi / 3.0f; //60deg

      transform parent;
      parent.m_pos = { 4,1,0,1 };
      parent.m_scale = { 2.0f,1.0f,0.1f,0.0f };
      parent.m_orientation = glm::quat({ 0.0f, 0.0f, pi_over_6 });

      transform child;
      child.m_pos = { 4,2,0,1 };
      child.m_scale = { 2.0f,3.0f,10.0f,0.0f };
      child.m_orientation = glm::quat({ 0.0f, 0.0f, pi_over_3 });

      transform child_in_world = parent * child;

      const glm::vec4 expected_pos =
      {
        4.0f + 2.0f * 4.0f * glm::cos(pi_over_6) - 1.0f * 2.0f * glm::sin(pi_over_6),
        1.0f + 2.0f * 4.0f * glm::sin(pi_over_6) + 1.0f * 2.0f * glm::cos(pi_over_6),
        0.0f,
        1.0f
      };

      if (!equal(child_in_world.m_pos, expected_pos, test_epsilon))
        return false;

      if (!equal(child_in_world.m_scale, {4,3,1,0}, test_epsilon))
        return false;

      //orientation should be the same as rotating 90deg around eZ
      if (!equal(child_in_world.m_orientation, glm::quat({ 0.0f, 0.0f, glm::half_pi<float>() }), test_epsilon))
        return false;
    }

    return true;
  }
} //namespace test_transform