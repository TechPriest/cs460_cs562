/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    transform.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:48:18 2020

\brief  transform class that concatenates with the operator*, and has getters for its usefull matrix forms



***************************************************************************/


#pragma once


#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

class transform
{
public:
  glm::vec4 m_pos = glm::vec4(0);
  glm::quat m_orientation = glm::quat(1, 0, 0, 0);
  glm::vec4 m_scale = glm::vec4(1);

  glm::mat4 get_R() const;
  glm::mat4 get_TR() const;
  glm::mat4 get_TRS() const;

  glm::mat4 get_inv_R() const;
  glm::mat4 get_inv_TR() const;
  glm::mat4 get_inv_TRS() const;

  transform operator*(const transform& rhs);
  glm::vec4 operator*(const glm::vec4& rhs);
};


