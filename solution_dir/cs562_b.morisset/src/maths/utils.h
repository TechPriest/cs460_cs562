/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    utils.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:48:27 2020

\brief  simple matematical utilities



***************************************************************************/


#pragma once

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/quaternion.hpp>

constexpr glm::vec2 eX_2{ 1,0 };
constexpr glm::vec2 eY_2{ 0,1 };

constexpr glm::vec3 eX_3{ 1,0,0 };
constexpr glm::vec3 eY_3{ 0,1,0 };
constexpr glm::vec3 eZ_3{ 0,0,1 };

constexpr glm::vec4 eX_4{ 1,0,0,0 };
constexpr glm::vec4 eY_4{ 0,1,0,0 };
constexpr glm::vec4 eZ_4{ 0,0,1,0 };


inline bool is_zero(float val, float epsilon)
{
  return -epsilon < val && val < epsilon;
}

inline bool is_zero(glm::vec2 val, float epsilon)
{
  return glm::dot(val, val) < epsilon;
}

inline bool is_zero(glm::vec3 val, float epsilon)
{
  return glm::dot(val, val) < epsilon;
}

inline bool is_zero(glm::vec4 val, float epsilon)
{
  return glm::dot(val, val) < epsilon;
}

inline bool is_zero(glm::mat4 val, float epsilon)
{
  glm::vec4 row_0 = { val[0][0], val[0][1], val[0][2], val[0][3] };
  glm::vec4 row_1 = { val[1][0], val[1][1], val[1][2], val[1][3] };
  glm::vec4 row_2 = { val[2][0], val[2][1], val[2][2], val[2][3] };
  glm::vec4 row_3 = { val[3][0], val[3][1], val[3][2], val[3][3] };

  return is_zero(row_0, epsilon) &&
    is_zero(row_1, epsilon) &&
    is_zero(row_2, epsilon) &&
    is_zero(row_3, epsilon);
}

inline bool is_zero(glm::quat val, float epsilon)
{
  return is_zero(val.x, epsilon) &&
    is_zero(val.y, epsilon) &&
    is_zero(val.z, epsilon) &&
    is_zero(val.w, epsilon);
}


inline bool equal(float a, float b, float epsilon)
{
  return is_zero(a-b, epsilon);
}

inline bool equal(glm::vec2 a, glm::vec2 b, float epsilon)
{
  return is_zero(a - b, epsilon);
}

inline bool equal(glm::vec3 a, glm::vec3 b, float epsilon)
{
  return is_zero(a - b, epsilon);
}

inline bool equal(glm::vec4 a, glm::vec4 b, float epsilon)
{
  return is_zero(a - b, epsilon);
}

inline bool equal(glm::mat4 a, glm::mat4 b, float epsilon)
{
  return is_zero(a - b, epsilon);
}

inline bool equal(glm::quat a, glm::quat b, float epsilon)
{
  return is_zero(a - b, epsilon);
}
