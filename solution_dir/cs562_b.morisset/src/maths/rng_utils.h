/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    rng_utils.h

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:47:50 2020

\brief  random numeber generator utilities. uses rand from crand.



***************************************************************************/


#pragma once

#include <cstdlib>
#include <ctime>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp> //glm::toMat4
#include "maths/utils.h"

inline void random_seed()
{
  srand(static_cast<unsigned>(time(0)));
}

/**
* random float between two numbers both included
* @param min
* @param max
* @return random float between two numbers both included
*/
inline float random_float(const float min = 0.0f, const float max = 1.0f)
{
  assert(min < max);

  return min + (static_cast<float>(rand()) / RAND_MAX) * (max - min);
}

glm::vec3 random_unit_vec4();
glm::vec3 random_unit_vec3();

/**
* random vec3 inside a box defined by min and max
* @param min
* @param max
* @return random vec3 inside a box defined by min and max
*/
inline glm::vec3 random_vec3(const glm::vec3 min, const glm::vec3 max)
{
  const float x = random_float(min.x, max.x);
  const float y = random_float(min.y, max.y);
  const float z = random_float(min.z, max.z);

  return { x,y,z };
}

