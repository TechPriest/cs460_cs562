/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    transform.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:47:59 2020

\brief  transform class that concatenates with the operator*, and has getters for its usefull matrix forms



***************************************************************************/




#include "transform.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>

glm::mat4 transform::get_R() const
{
  return glm::toMat4(m_orientation);
}

glm::mat4 transform::get_TR() const
{
  const glm::mat4 R = get_R();

  glm::mat4 T(1);
  T = glm::translate(T, glm::vec3(m_pos));

  return T * R;
}

glm::mat4 transform::get_TRS() const
{
  return glm::scale(get_TR(), glm::vec3(m_scale));
}

glm::mat4 transform::get_inv_R() const
{
  return glm::mat4_cast(glm::conjugate(m_orientation));;
}

glm::mat4 transform::get_inv_TR() const
{
  //inv rot
  glm::mat4 output = get_inv_R();
  //inv translate
  output = glm::translate(output, -glm::vec3(m_pos));

  return output;
}

glm::mat4 transform::get_inv_TRS() const
{
  glm::mat4 output(1);

  //inv scale
  output = glm::scale(output,
    glm::vec3(1.0f/m_scale.x, 1.0f/m_scale.y, 1.0f/ m_scale.z));
  //inv rot
  const glm::mat4 rot = get_inv_R();
  output = output * rot;
  //inv translate
  output = glm::translate(output, -glm::vec3(m_pos));

  return output;
}

transform transform::operator*(const transform & rhs)
{
  transform output;

  output.m_pos = get_TRS() * rhs.m_pos;
  output.m_scale = m_scale * rhs.m_scale;
  output.m_orientation = rhs.m_orientation * m_orientation;

  return output;
}

glm::vec4 transform::operator*(const glm::vec4 & rhs)
{
  return get_TR() * rhs;
}

