/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    rng_utils.cpp

\author Benat Morisset de Perdigo 

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:47:39 2020

\brief  random numeber generator utilities. uses rand from crand.



***************************************************************************/



#include "rng_utils.h"

glm::vec3 random_unit_vec4()
{
  constexpr float pi = glm::pi<float>();
  constexpr float half_pi = glm::half_pi<float>();
  const float kx = random_float(-pi, pi);
  const float ky = random_float(-half_pi, half_pi);
  const glm::quat q_x(kx * eX_3);
  const glm::quat q_y(ky * eY_3);
  const glm::quat q_total = glm::normalize(q_x * q_y);
  const glm::mat4 matrix = glm::toMat4(q_total);

  glm::vec4 result = matrix * eX_4;
  result.w = 0.0f;

  return glm::normalize(result);
}

glm::vec3 random_unit_vec3()
{
  return glm::vec3(random_unit_vec4());
}

