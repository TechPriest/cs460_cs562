/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    gui_actions.cpp

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:29:42 2020

\brief  Actions that make calls on the GPU. Wrapper around OpenGL.



***************************************************************************/


#include "gui_actions.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <dear_imgui/imgui.h>
#include <dear_imgui/imgui_impl_glfw.h>
#include <dear_imgui/imgui_impl_opengl3.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "graphics/framebuffers/g_buffer.h"
#include "render_actions.h"
#include "graphics/window/window.h"
#include "graphics/effects_pipeline/effects_pipeline.h"
#include "scenes/phong_tess_scene/tesselation_settings.h"
#include "scenes/decal_scene/decal_settings.h"
#include "scenes/final_project/final_project_settings.h"
#include "scenes/gui_windows.h"
#include "scenes/phong_tess_scene/gui_windows_tess.h"
#include "scene_manager/scene_manager.h"
#include "scenes/scene.h"
#include "scenes/scene_settings.h"
#include "graphics/AO/occlusion_settings.h"

/// CORE ACTIONS

void gui_init::do_action() const
{
  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGuiIO& io = ImGui::GetIO(); (void)io;

  ImGui::StyleColorsDark();

  // Setup Platform/Renderer bindings
  ImGui_ImplGlfw_InitForOpenGL(*m_glfw_win, true);
  ImGui_ImplOpenGL3_Init("#version 150");
}

void gui_shutdown::do_action() const
{
  ImGui_ImplOpenGL3_Shutdown();
  ImGui_ImplGlfw_Shutdown();
  ImGui::DestroyContext();
}

void gui_start_frame::do_action() const
{
  ImGui_ImplOpenGL3_NewFrame();
  ImGui_ImplGlfw_NewFrame();
  ImGui::NewFrame();
}

//call after gl_poll_events
void gui_no_render_end::do_action() const
{
  ImGui::EndFrame();
}

//call before framebuffer clear
void gui_start_render::do_action() const
{
  ImGui::Render();
}

void gui_end_render::do_action() const
{
  ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}


/// DEMO SCENE

void gui_demo::do_action() const
{
  static bool show_demo_window = true;

  if (show_demo_window)
    ImGui::ShowDemoWindow(&show_demo_window);
}


/// TOOLS


/// <summary>
/// Helpers
/// </summary>
namespace
{
  //so that all menus have consistent colors
  const ImColor red(249, 1, 1);
  const ImColor yellow(230, 230, 1);
  const ImColor green(1, 249, 1);

  void help_menu()
  {
    if (ImGui::BeginMenu("Help"))
    {
      ImGui::Text(
        "Camera controls:\n\
 -WS: move forward/backward along its forward vector\n\
 -AD: move left/right along its side vector\n\
 -QE: move up/down along the global up vector (0,1,0)\n\
 -hold SHIFT: move faster (x3)\n\
 -RIGHT_CLICK + Drag: yaw and pitch\n\
 -UP/DOWN: pitch\n\
 -LEFT/RIGHT: yaw\n\
 -V: reset\n");

      ImGui::Text(
        "Other:\n\
 -SPACE: cycle through display modes\n\
 -K: toggle anti-aliasing\n\
 -L: toggle emissive_glow\n\
 -O: toggle wireframe\n\
 -F5: reload shaders\n\
 -CTRL+R: reload scene");

      ImGui::EndMenu();
    }
  }

  void help_menu_lines()
  {
    if (ImGui::BeginMenu("Help"))
    {
      ImGui::Text(
        "Camera controls:\n\
 -WS: move forward/backward along its forward vector\n\
 -AD: move left/right along its side vector\n\
 -QE: move up/down along the global up vector (0,1,0)\n\
 -hold SHIFT: move faster (x3)\n\
 -RIGHT_CLICK + Drag: yaw and pitch\n\
 -UP/DOWN: pitch\n\
 -LEFT/RIGHT: yaw\n\
 -1: move cam to sofa\n\
 -2: move cam to serapis\n\
 -3: move cam to torus\n\
 -V: reset\n");

      ImGui::Text(
        "Style presets:\n\
 -B: basic\n\
 -M: Frank Miller's\n\
 -L: Roy Lichtenchtein's\n");

      ImGui::Text(
        "Other:\n\
 -SPACE: cycle through display modes\n\
 -F5: reload shaders\n\
 -CTRL+R: reload scene");

      ImGui::Text(
        "Tip: You can get SSAA by unmatching the g-buffer\n\
resolution and setting it to something bigger\n\
than the screen's. x2 is suggested");

      ImGui::EndMenu();
    }
  }

  void scenes_menu(bool* loop, scene_manager* scene_mngr, bool* reload_shaders)
  {
    if (ImGui::BeginMenu("Scenes"))
    {
      //reload scene
      {
        if (ImGui::Button("reload scene"))
        {
          scene_mngr->restart_scene();
          *loop = false;
        }

        ImGui::SameLine();
        ImGui::Text("( CTRL+R )");
      }

      //reload shaders
      {
        if (ImGui::Button("reload shaders"))
          *reload_shaders = true;

        ImGui::SameLine();
        ImGui::Text("( F5 )");
      }

      int combo_int = static_cast<int>(scene_mngr->get_current_scene_id());
      if (ImGui::Combo("change scene", &combo_int, " simple\0 sponza\0 phong tesselation\0\
 decal\0 simple decal\0 simple AO\0 AO\0 final project\0 final project sponza\0 parenting demo\0"))
      {
        scene_mngr->change_scene(static_cast<scene_manager::scene_enum>(combo_int));
        *loop = false;
      }
        

      ImGui::EndMenu();
    }
  }

  void change_g_buff_res(scene* scn, int width, int height)
  {
    auto q = scn->get_set_resolution_actions(width, height);
    while (!q.empty())
    {
      q.front()->do_action();
      q.pop();
    }
  }

  void change_window_res(scene* scn, window* win, int width, int height, bool g_buff_match_window)
  {
    set_window_resolution set_win_res(win, width, height);
    set_win_res.do_action();

    //use in->m_width and win->m_height because OS can decide to clamp those values
    if (g_buff_match_window)
      change_g_buff_res(scn, win->m_width, win->m_height);
  }

  void window_resolution_menu(scene* scn, window* win, bool match_window_res)
  {
    if (ImGui::BeginMenu("Resolution##1"))
    {
      //usual resolutions
      {
        static int combo_int = 0;
        if (ImGui::Combo("usual resolutions", &combo_int,
          " 800 x 450\0 1200 x 675\0 1600 x 900\0 1920 x 1080\0"))
        {
        }

        if (ImGui::Button("   apply   "))
        {
          switch (combo_int)
          {
          case 0:
          {
            change_window_res(scn, win, 800, 450, match_window_res);
            break;
          }
          case 1:
          {
            change_window_res(scn, win, 1200, 675, match_window_res);
            break;
          }
          case 2:
          {
            change_window_res(scn, win, 1600, 900, match_window_res);
            break;
          }
          default: //intended fallthrough
          case 3:
          {
            change_window_res(scn, win, 1920, 1080, match_window_res);
            break;
          }
          }
        }
      }

      //custom resolution
      if (ImGui::BeginMenu("custom"))
      {
        static int width = -1;
        static int height = -1;

        if (width < 0)
        {
          width = win->m_width;
          height = win->m_height;
        }

        ImGui::InputInt("width", &width);
        ImGui::InputInt("height", &height);

        glm::clamp(width, 1, 3840);
        glm::clamp(height, 1, 3840);

        if (ImGui::Button("   apply   "))
        {
          change_window_res(scn, win, width, height, match_window_res);
        }

        ImGui::EndMenu();
      }

      ImGui::EndMenu();
    }
    ImGui::SameLine();
    ImGui::Text("%i x %i  ", win->m_width, win->m_height);
  }

  void g_buff_resolution_menu(scene* scn, window* win, bool* match_window_res, g_buffer* g_buff)
  {
    if (ImGui::BeginMenu("Resolution##2"))
    {
      if (ImGui::Checkbox("match window resolution", match_window_res))
      {
        if (*match_window_res)
          change_g_buff_res(scn, win->m_width, win->m_height);
      }

      if (!*match_window_res)
      {

        //usual resolutions
        {
          static int combo_int = 0;
          if (ImGui::Combo("usual resolutions", &combo_int, " 800 x 450\0 1200 x 675\0 1600 x 900\0 1920 x 1080\0"))
          {
          }

          if (ImGui::Button("   apply   "))
          {
            switch (combo_int)
            {
            case 0:
            {
              change_g_buff_res(scn, 800, 450);
              break;
            }
            case 1:
            {
              change_g_buff_res(scn, 1200, 675);
              break;
            }
            case 2:
            {
              change_g_buff_res(scn, 1600, 900);
              break;
            }
            default: //intended fallthrough
            case 3:
            {
              change_g_buff_res(scn, 1920, 1080);
              break;
            }
            }
          }
        }

        //custom resolution
        if (ImGui::BeginMenu("custom"))
        {
          static int width = -1;
          static int height = -1;

          if (width < 0)
          {
            width = g_buff->get_width();
            height = g_buff->get_height();
          }

          ImGui::InputInt("width", &width);
          ImGui::InputInt("height", &height);

          glm::clamp(width, 1, 3840);
          glm::clamp(height, 1, 3840);

          if (ImGui::Button("   apply   "))
          {
            change_g_buff_res(scn, width, height);
          }

          ImGui::EndMenu();
        }
      }

      ImGui::EndMenu();
    }
    ImGui::SameLine();
    ImGui::Text("%i x %i  ", g_buff->get_width(), g_buff->get_height());
  }

  void window_menu(scene* scn, window* win, bool match_window_res, gui_windows* gui_wins)
  {
    if (ImGui::BeginMenu("Window"))
    {
      window_resolution_menu(scn, win, match_window_res);

      //V-Syncs
      {
        const bool old_vsync = win->get_vsync();
        bool vsync = old_vsync;
        ImGui::Checkbox("Vsync", &vsync);
        if (vsync != old_vsync)
          (win->get_set_vsync_action(vsync))->do_action();
      }

      //open or close windows
      if(ImGui::BeginMenu("GUI windows"))
      {
        ImGui::Checkbox("Lights", &(gui_wins->lights_window));
        ImGui::Checkbox("Effects", &(gui_wins->effects_window));
        ImGui::Checkbox("Decals", &(gui_wins->decals_window));
        ImGui::Checkbox("Nodes", &(gui_wins->objs_window));

        ImGui::EndMenu();
      }

      ImGui::EndMenu();
    }
  }

  void window_menu_tess(scene* scn, window* win, bool match_window_res, gui_windows_tess* gui_wins)
  {
    if (ImGui::BeginMenu("Window"))
    {
      window_resolution_menu(scn, win, match_window_res);

      //V-Syncs
      {
        const bool old_vsync = win->get_vsync();
        bool vsync = old_vsync;
        ImGui::Checkbox("Vsync", &vsync);
        if (vsync != old_vsync)
          (win->get_set_vsync_action(vsync))->do_action();
      }

      //open or close windows
      if (ImGui::BeginMenu("GUI windows"))
      {
        ImGui::Checkbox("Lights", &(gui_wins->lights_window));
        ImGui::Checkbox("Effects", &(gui_wins->effects_window));
        ImGui::Checkbox("Phong Tesselation", &(gui_wins->tesselation_window));
        /**///TODO ImGui::Checkbox("Nodes", &(gui_wins->objs_window));

        ImGui::EndMenu();
      }

      ImGui::EndMenu();
    }
  }

  void window_menu_lines(scene* scn, window* win, bool match_window_res, gui_windows* gui_wins)
  {
    if (ImGui::BeginMenu("Window"))
    {
      window_resolution_menu(scn, win, match_window_res);

      //V-Syncs
      {
        const bool old_vsync = win->get_vsync();
        bool vsync = old_vsync;
        ImGui::Checkbox("Vsync", &vsync);
        if(vsync != old_vsync)
          (win->get_set_vsync_action(vsync))->do_action();
      }

      //open or close windows
      if (ImGui::BeginMenu("GUI windows"))
      {
        ImGui::Checkbox("Lines", &(gui_wins->highlight_window));

        ImGui::EndMenu();
      }

      ImGui::EndMenu();
    }
  }

  void g_buffer_menu(scene* scn, g_buffer* g_buff, window* win, bool* match_window_res)
  {
    if (ImGui::BeginMenu("G-Buffer"))
    {
      g_buff_resolution_menu(scn, win, match_window_res, g_buff);

      if(ImGui::BeginMenu("Info"))
      {
        ImGui::Text("size: %i [bytes/pixel]", g_buff->get_size_per_pixel());
        ImGui::Separator();
        ImGui::Text("\
1st buffer is for normals                        (6)\n\
2nd buffer is for diffuse and specular greyscale (4)\n\
3rd buffer is for emissive and shininess         (4)");
        ImGui::Separator();
        ImGui::Text("takes ambient same as diffuse");

        ImGui::EndMenu();
      }

      ImGui::EndMenu();
    }
  }

  void g_buffer_menu_lines(scene* scn, g_buffer* g_buff, window* win, bool* match_window_res)
  {
    if (ImGui::BeginMenu("G-Buffer"))
    {
      g_buff_resolution_menu(scn, win, match_window_res, g_buff);

      if (ImGui::BeginMenu("Info"))
      {
        ImGui::Text("size: %i [bytes/pixel]", g_buff->get_size_per_pixel());
        ImGui::Separator();
        ImGui::Text("only contains normals(6 bytes/pixel) and depth\n");

        ImGui::EndMenu();
      }

      ImGui::EndMenu();
    }
  }

  void display_menu(int* display_mode, float* contrast_min, float* contrast_max,
    bool* wireframe, bool tiny = false)
  {
    std::string dis_text_full("Displaying: ");
    if (tiny)
      dis_text_full = "disp: ";

    ImColor dis_text_col = yellow;

    switch (*display_mode)
    {
    case 0: //normals
      dis_text_full += "g_buffer:normals";
      break;
    case 1: //diffuse
      dis_text_full += "g_buffer:diffuse";
      break;
    case 2: //emissive
      dis_text_full += "g_buffer:emissive";
      break;
    case 3: //depth
      dis_text_full += "g_buffer:depth";
      break;
    case 4: //positions
      if (tiny)
        dis_text_full += "positions";
      else
        dis_text_full += "positions (comp. from depth)";
      break;
    case 5: //specular
      if (tiny)
        dis_text_full += "g_buffer:specular";
      else
        dis_text_full += "g_buffer:specular (greyscale)";
      break;
    case 6: //shininess
      if (tiny)
        dis_text_full += "g_buffer:shininess";
      else
        dis_text_full += "g_buffer:shininess (greyscale)";
      break;
    case 7: //occlusion
      dis_text_full += "occlusion";
      break;
    case 8: //occlusion w/o filter
      if (tiny)
        dis_text_full += "occl w/o filter";
      else
        dis_text_full += "occlusion w/o bilateral filter";
      dis_text_col = green;
      break;
    case 9: //lighted
      if (tiny)
        dis_text_full += "lighted";
      else
        dis_text_full += "full lighted pipeline";
      dis_text_col = green;
      break;

    }

    //add padding at end to avoid moving menus
    size_t length = 42u;
    if (tiny)
      length = 25u;
    while (dis_text_full.length() < length)
    {
      dis_text_full += " ";
    }

    ImGui::TextColored(dis_text_col, "*");

    if (ImGui::BeginMenu(dis_text_full.c_str()))
    {
      //display mode
      {
        //level::e_diplay_mode { normals, diff, emi, depth, pos, specular, shininess, lighted, SIZE };

        if (ImGui::Combo("display", display_mode,
          "normals\0diffuse\0emissive\0depth\0positions\
\0specular\0shininess\0occlusion\0occl w/o filter \0lighted"))
        {
        }

        if (*display_mode == static_cast<int>(e_gbuff_v4_textures::depth))
        {
          ImGui::Text("contrast");
          ImGui::InputFloat("min", contrast_min);
          ImGui::InputFloat("max", contrast_max);

          *contrast_min = glm::clamp(*contrast_min, 0.0f, 1.0f);
          *contrast_max = glm::clamp(*contrast_max, *contrast_min, 1.0f);
        }
      }

      //wireframe
      ImGui::Checkbox("wireframe", wireframe);

      ImGui::EndMenu();
    }
    
  }

  void display_menu_lines(int* display_mode, float* contrast_min,
    float* contrast_max, bool* wireframe, bool tiny = false)
  {
    std::string dis_text_full("Displaying: ");
    if (tiny)
      dis_text_full = "disp: ";

    ImColor dis_text_col = yellow;

    switch (*display_mode)
    {
    case 0: //normals
      dis_text_full += "g_buffer:normals";
      break;
    case 1: //depth
      dis_text_full += "g_buffer:depth";
      break;
    case 2: //positions
      if (tiny)
        dis_text_full += "positions";
      else
        dis_text_full += "positions (comp. from depth)";
      break;
    case 3: //lines
        dis_text_full += "lines";
        break;
    default:
      assert(false);

    }

    //add padding at end to avoid moving menus
    size_t length = 42u;
    if (tiny)
      length = 25u;
    while (dis_text_full.length() < length)
    {
      dis_text_full += " ";
    }

    ImGui::TextColored(dis_text_col, "*");

    if (ImGui::BeginMenu(dis_text_full.c_str()))
    {
      //display mode
      {
        //level::e_diplay_mode { normals, diff, emi, depth, pos, specular, shininess, lighted, SIZE };

        if (ImGui::Combo("display", display_mode,
          "normals\0depth\0positions\0lines"))
        {
        }

        if (*display_mode == static_cast<int>(e_gbuff_v4_textures::depth))
        {
          ImGui::Text("contrast");
          ImGui::InputFloat("min", contrast_min);
          ImGui::InputFloat("max", contrast_max);

          *contrast_min = glm::clamp(*contrast_min, 0.0f, 1.0f);
          *contrast_max = glm::clamp(*contrast_max, *contrast_min, 1.0f);
        }
      }

      //wireframe
      ImGui::Checkbox("wireframe", wireframe);

      ImGui::EndMenu();
    }

  }

  void performance_info(const history<unsigned, 120u>* cpu_frame_us_his,
    const history<unsigned, 120u>* gpu_frame_us_his,
    std::mutex* cpu_his_mutex, bool tiny = false)
  {
    cpu_his_mutex->lock();

    const size_t cpu_size = cpu_frame_us_his->size();
    const size_t gpu_size = gpu_frame_us_his->size();

    float cpu_average = 0.0;
    float gpu_average = 0.0;

    for (size_t i = 0u; i < cpu_size; i++)
      cpu_average += static_cast<float>((*cpu_frame_us_his)[i]);
    cpu_average /= static_cast<float>(cpu_size);

    cpu_his_mutex->unlock();

    for (size_t i = 0u; i < gpu_size; i++)
      gpu_average += static_cast<float>((*gpu_frame_us_his)[i]);
    gpu_average /= static_cast<float>(gpu_size);

    ImColor cpu_col = green;
    ImColor gpu_col = green;

    constexpr float fps60_lim = 1000000.0f / 60.0f;
    constexpr float warning_lim = 0.85f * fps60_lim;
    if (cpu_average > fps60_lim)
      cpu_col = red;
    else if (cpu_average > warning_lim)
      cpu_col = yellow;
    if (gpu_average > fps60_lim)
      gpu_col = red;
    else if (gpu_average > warning_lim)
      gpu_col = yellow;

    if (tiny)
    {
      ImGui::TextColored(cpu_col, "cpu: %5.1f [ms]", cpu_average / 1000.0f);
      ImGui::TextColored(gpu_col, "rend: %5.1f [ms]", gpu_average / 1000.0f);
    }
    else
    {
      ImGui::TextColored(cpu_col, "main thread frame average: %5.1f [ms]", cpu_average / 1000.0f);
      ImGui::TextColored(gpu_col, "render thread frame average: %5.1f [ms]", gpu_average / 1000.0f);
    }
  }

  void opti_lines_options(final_project_settings* settings)
  {
    
    ImGui::SliderFloat("background color", &(settings->background_col), 0.0f, 1.0f);
    ImGui::SliderFloat("base color", &(settings->base_col), 0.0f, 1.0f);

    ImGui::InputFloat3("toon light direction", glm::value_ptr(settings->toon_dir));

    //for both suggestive and highlight
    {
      ImGui::InputInt("neighboorhood dim", &(settings->neighboorhood_dim));
      settings->neighboorhood_dim = glm::max(settings->neighboorhood_dim, 0);

      ImGui::DragFloat("max z diff", &(settings->max_z_diff), 0.1f, 0.0f, 20.0f);
    }

    //PRINCIPAL CONTOUR
    {
      ImGui::Separator();
      ImGui::Text("PRINCIPAL CONTOUR");

      ImGui::SliderFloat("limit##1", &(settings->prim_cont_lim), 0.0f, 1.0f);
      ImGui::SliderFloat("margin##1", &(settings->prim_cont_lim_margin), 0.0f, 1.0f);
      ImGui::SliderFloat("color##1", &(settings->prin_cont_col), 0.0f, 1.0f);
    }
    //SUGGESTIVE CONTOUR
    {
      ImGui::Separator();
      ImGui::Text("SUGGESTIVE CONTOUR");
      ImGui::SliderFloat("smaller allowed proportion##2", &(settings->sugg_cont_smaller_dot_allowed_proportion), 0.0f, 1.0f);
      ImGui::SliderFloat("min difference##2", &(settings->sugg_cont_min_dot_diff), 0.0f, 1.0f);
      ImGui::SliderFloat("color##2", &(settings->sugg_cont_col), 0.0f, 1.0f);
    }
    //PRINCIPAL HIGHLIGHT
    {
      ImGui::Separator();
      ImGui::Text("PRINCIPAL HIGHLIGHT");
      ImGui::SliderFloat("limit dot(n,v)##3", &(settings->prin_high_lim_1), 0.0f, 1.0f);
      ImGui::SliderFloat("limit dot(e1,v)##3", &(settings->prin_high_lim_2), 0.0f, 1.0f);
      ImGui::SliderFloat("color##3", &(settings->prin_high_col), 0.0f, 1.0f);
    }
    //SUGGESTIVE CONTOUR
    {
      ImGui::Separator();
      ImGui::Text("SUGGESTIVE HIGHLIGHT");
      ImGui::SliderFloat("bigger allowed proportion##4", &(settings->sugg_high_bigger_dot_allowed_proportion), 0.0f, 1.0f);
      ImGui::SliderFloat("min difference##4", &(settings->sugg_high_min_dot_diff), 0.0f, 1.0f);
      ImGui::SliderFloat("color##4", &(settings->sugg_high_col), 0.0f, 1.0f);
    }
  }

  void non_opti_lines_options(final_project_settings* settings)
  {
    bool prin_cont = settings->lines_flag & (1 << 0);
    bool sugg_cont = settings->lines_flag & (1 << 1);
    bool prin_high = settings->lines_flag & (1 << 2);
    bool sugg_high = settings->lines_flag & (1 << 3);
    bool toon = settings->lines_flag & (1 << 4);
    ImGui::Checkbox("principal contour", &prin_cont);
    ImGui::Checkbox("suggestive contour", &sugg_cont);
    ImGui::Checkbox("principal highlight", &prin_high);
    ImGui::Checkbox("suggestive highlight", &sugg_high);
    ImGui::Checkbox("toon shading", &toon);
    settings->lines_flag = settings->lines_flag & ~(1 << 0) & ~(1 << 1) & ~(1 << 2) & ~(1 << 3) & ~(1 << 4);
    if (prin_cont)
      settings->lines_flag = settings->lines_flag | (1 << 0);
    if (sugg_cont)
      settings->lines_flag = settings->lines_flag | (1 << 1);
    if (prin_high)
      settings->lines_flag = settings->lines_flag | (1 << 2);
    if (sugg_high)
      settings->lines_flag = settings->lines_flag | (1 << 3);
    if (toon)
      settings->lines_flag = settings->lines_flag | (1 << 4);

    ImGui::Separator();
    ImGui::SliderFloat("background color", &(settings->background_col), 0.0f, 1.0f);
    ImGui::SliderFloat("base color", &(settings->base_col), 0.0f, 1.0f);

    if (toon)
    {
      ImGui::InputFloat3("toon light direction", glm::value_ptr(settings->toon_dir));
    }

    ImGui::Separator();



    if (sugg_cont || sugg_high || prin_high)
    {
      //chose neighborhood type
      {
        int type = (settings->lines_flag & (1 << 5)) ? 1 : 0;
        if (ImGui::Combo("neighborhood type", &type,
          " square\0 circle\0"))
        {
        }
        if (type == 0)
          settings->lines_flag = settings->lines_flag & ~(1 << 5);
        else
          settings->lines_flag = settings->lines_flag | (1 << 5);
      }

      ImGui::InputInt("neighboorhood dim", &(settings->neighboorhood_dim));
      settings->neighboorhood_dim = glm::max(settings->neighboorhood_dim, 0);

      bool scaling = settings->lines_flag & (1 << 6);
      ImGui::Checkbox("scaling by dim", &scaling);
      if (scaling)
        settings->lines_flag = settings->lines_flag | (1 << 6);
      else
        settings->lines_flag = settings->lines_flag & ~(1 << 6);

      //ImGui::SliderFloat("max z diff", &(settings->max_z_diff), 0.5f, 2.5f);
      ImGui::DragFloat("max z diff", &(settings->max_z_diff), 0.1f, 0.0f, 20.0f);
    }

    if (prin_cont)
    {
      ImGui::Separator();
      ImGui::Text("PRINCIPAL CONTOUR");

      bool sobel_cam_z = settings->lines_flag & (1 << 7);
      ImGui::Checkbox("use z-position sobel", &sobel_cam_z);
      if (sobel_cam_z)
        settings->lines_flag = settings->lines_flag | (1 << 7);
      else
        settings->lines_flag = settings->lines_flag & ~(1 << 7);

      ImGui::SliderFloat("limit##1", &(settings->prim_cont_lim), 0.0f, 1.0f);
      ImGui::SliderFloat("margin##1", &(settings->prim_cont_lim_margin), 0.0f, 1.0f);
      ImGui::SliderFloat("color##1", &(settings->prin_cont_col), 0.0f, 1.0f);
    }
    if (sugg_cont)
    {
      ImGui::Separator();
      ImGui::Text("SUGGESTIVE CONTOUR");
      ImGui::SliderFloat("smaller allowed proportion##2", &(settings->sugg_cont_smaller_dot_allowed_proportion), 0.0f, 1.0f);
      ImGui::SliderFloat("min difference##2", &(settings->sugg_cont_min_dot_diff), 0.0f, 1.0f);
      ImGui::SliderFloat("color##2", &(settings->sugg_cont_col), 0.0f, 1.0f);
    }
    if (prin_high)
    {
      ImGui::Separator();
      ImGui::Text("PRINCIPAL HIGHLIGHT");
      ImGui::SliderFloat("limit dot(n,v)##3", &(settings->prin_high_lim_1), 0.0f, 1.0f);
      ImGui::SliderFloat("limit dot(e1,v)##3", &(settings->prin_high_lim_2), 0.0f, 1.0f);
      ImGui::SliderFloat("color##3", &(settings->prin_high_col), 0.0f, 1.0f);
    }
    if (sugg_high)
    {
      ImGui::Separator();
      ImGui::Text("SUGGESTIVE HIGHLIGHT");
      ImGui::SliderFloat("bigger allowed proportion##4", &(settings->sugg_high_bigger_dot_allowed_proportion), 0.0f, 1.0f);
      ImGui::SliderFloat("min difference##4", &(settings->sugg_high_min_dot_diff), 0.0f, 1.0f);
      ImGui::SliderFloat("color##4", &(settings->sugg_high_col), 0.0f, 1.0f);
    }

  }
}


void gui_top_bar::do_action() const
{
  if (ImGui::BeginMainMenuBar())
  {
    static bool match_window_res = true;

    const bool use_short_version = m_scene_setts->win_ptr->m_width < 1200;

    help_menu();

    scenes_menu(&(m_scene_setts->loop), m_scene_setts->scene_mngr_ptr,
      &(m_scene_setts->reload_shaders));

    window_menu(m_scene_setts->scn_ptr, m_scene_setts->win_ptr, match_window_res,
      m_gui_wins);

    g_buffer_menu(m_scene_setts->scn_ptr, m_g_buff, m_scene_setts->win_ptr, &match_window_res);

    display_menu(&(m_scene_setts->display_mode), &(m_scene_setts->depth_contrast_min),
      &(m_scene_setts->depth_contrast_max), &(m_scene_setts->wireframe), use_short_version);

    performance_info(m_scene_setts->main_thread_frame_us_his_ptr,
      m_scene_setts->rend_thread_frame_us_his_ptr, m_cpu_his_mutex, use_short_version);

    ImGui::EndMainMenuBar();
  }

}

void gui_top_bar_tess::do_action() const
{
  if (ImGui::BeginMainMenuBar())
  {
    static bool match_window_res = true;

    const bool use_short_version = m_scene_setts->win_ptr->m_width < 1200;

    help_menu();

    scenes_menu(&(m_scene_setts->loop), m_scene_setts->scene_mngr_ptr,
      &(m_scene_setts->reload_shaders));

    window_menu_tess(m_scene_setts->scn_ptr, m_scene_setts->win_ptr, match_window_res,
      m_gui_wins);

    g_buffer_menu(m_scene_setts->scn_ptr, m_g_buff, m_scene_setts->win_ptr, &match_window_res);

    display_menu(&(m_scene_setts->display_mode), &(m_scene_setts->depth_contrast_min),
      &(m_scene_setts->depth_contrast_max), &(m_scene_setts->wireframe));

    performance_info(m_scene_setts->main_thread_frame_us_his_ptr,
      m_scene_setts->rend_thread_frame_us_his_ptr, m_cpu_his_mutex, use_short_version);

    ImGui::EndMainMenuBar();
  }
}

void gui_highlights_top_bar::do_action() const
{
  if (ImGui::BeginMainMenuBar())
  {
    static bool match_window_res = true;

    const bool use_short_version = m_scene_setts->win_ptr->m_width < 1200;

    help_menu_lines();

    scenes_menu(&(m_scene_setts->loop), m_scene_setts->scene_mngr_ptr,
      &(m_scene_setts->reload_shaders));

    window_menu_lines(m_scene_setts->scn_ptr, m_scene_setts->win_ptr,
      match_window_res, m_gui_wins);

    g_buffer_menu_lines(m_scene_setts->scn_ptr, m_g_buff, m_scene_setts->win_ptr, &match_window_res);

    display_menu_lines(&(m_scene_setts->display_mode), &(m_scene_setts->depth_contrast_min),
      &(m_scene_setts->depth_contrast_max), &(m_scene_setts->wireframe));

    performance_info(m_scene_setts->main_thread_frame_us_his_ptr,
      m_scene_setts->rend_thread_frame_us_his_ptr, m_cpu_his_mutex, use_short_version);

    ImGui::EndMainMenuBar();
  }
}


void gui_lights_window::do_action() const
{
  if (!*m_open)
    return;

  if (!ImGui::Begin("Lights", m_open))
  {
    // Early out if the window is collapsed, as an optimization.
    ImGui::End();
    return;
  }

  {
    ImGui::Checkbox("enable draw light volume optimization", m_light_opti);

    ImGui::Checkbox("paused", m_paused);

    std::shared_ptr<point_light_node> light =
      std::static_pointer_cast<point_light_node>((*m_nodes)[m_first_light_idx]);

    //change count
    {
      const int count = static_cast<int>(m_nodes->size()) - m_first_light_idx;
      int new_count = count;
      ImGui::InputInt("count", &new_count);
      new_count = glm::clamp(new_count, 1, 10000);
      *m_count_diff = new_count - count;
    }

    ImGui::Separator();

    //change properties
    {
      const glm::vec3 att0 = light->m_attenuation;
      float att[3] = { att0.x, att0.y, att0.z };

      ImGui::Text("Attenuation:");
      ImGui::InputFloat("constant", att + 0);
      ImGui::InputFloat("linear", att + 1);
      ImGui::InputFloat("quadratic", att + 2);

      att[0] = glm::max(0.0f, att[0]);
      att[1] = glm::max(0.0f, att[1]);
      att[2] = glm::max(0.0f, att[2]);

      auto apply_changes = [&](std::shared_ptr<node> n)
      {
        std::shared_ptr<point_light_node> l =
          std::static_pointer_cast<point_light_node>(n);
        l->m_attenuation = { att[0], att[1], att[2] };
      };

      m_nodes_mutex->lock();
      std::for_each(m_nodes->begin() + m_first_light_idx, m_nodes->end(),
        apply_changes);
      m_nodes_mutex->unlock();
    }

    ImGui::Separator();

    //debug drawing
    {
      ImGui::Checkbox("draw bulbs", m_draw_bulbs);
      ImGui::Checkbox("draw influence spheres", m_draw_infl);
    }

    ImGui::Separator();

    if (ImGui::BeginMenu("Ambient Occlusion"))
    {
      ImGui::Text("Horizon Based Screen Space Ambient Occulion");

      int combo_int = static_cast<int>(m_occl_setts->type);
      if (ImGui::Combo("type", &combo_int, " paper's\0 benat's\0 opti\0"))
      {}
      m_occl_setts->type = static_cast<occlusion_settings::occ_type>(combo_int);

      //directions
      {
        int dir_count = static_cast<int>(m_occl_setts->dirs.size());
        const int old_count = dir_count;

        ImGui::InputInt("directions", &dir_count);
        dir_count = glm::clamp(dir_count, 1, 16);
        if (dir_count != old_count)
          m_occl_setts->set_dirs(static_cast<size_t>(dir_count));
      }

      float angle_b = glm::half_pi<float>() - m_occl_setts->min_angle;
      ImGui::InputFloat("angle_bias", &angle_b);
      m_occl_setts->min_angle =
        glm::clamp(glm::half_pi<float>() - angle_b, 0.0f, glm::half_pi<float>());

      ImGui::InputFloat("attenuation", &(m_occl_setts->att));
      m_occl_setts->att = glm::max(m_occl_setts->att, 0.0f);

      ImGui::InputFloat("scale", &(m_occl_setts->scale));
      m_occl_setts->scale = glm::max(m_occl_setts->scale, 0.0f);

      ImGui::InputFloat("radius", &(m_occl_setts->radius));

      ImGui::InputInt("steps", &(m_occl_setts->step_count));
      m_occl_setts->step_count = glm::clamp(m_occl_setts->step_count, 1, 64);

      ImGui::Separator();

      ImGui::InputInt("dist kernel half_size", &(m_occl_setts->dist_kernel_size));
      m_occl_setts->dist_kernel_size = glm::clamp(m_occl_setts->dist_kernel_size, 1, 16);

      ImGui::InputFloat("dist sigma", &(m_occl_setts->dist_sigma));
      m_occl_setts->dist_sigma = glm::max(m_occl_setts->dist_sigma, 0.0f);

      ImGui::InputFloat("depth sigma", &(m_occl_setts->depth_sigma));
      m_occl_setts->depth_sigma = glm::max(m_occl_setts->depth_sigma, 0.0f);

      ImGui::EndMenu();
    }
  }

  ImGui::End();
}


void gui_effects_window::do_action() const
{
  if (!*m_open)
    return;

  if (!ImGui::Begin("Effects", m_open))
  {
    // Early out if the window is collapsed, as an optimization.
    ImGui::End();
    return;
  }

  {
    ImGui::Text("effects are only applied to the lighted scene");
    ImGui::Separator();

    //bloom
    {
      bool bloom = m_eff_pipeline->m_act_effects[effects_pipeline::e_effects::bloom];

      ImGui::Checkbox("enable bloom", &bloom);

      m_eff_pipeline->m_act_effects[effects_pipeline::e_effects::bloom] = bloom;

      if (bloom)
      {
        ImGui::InputFloat("luminance min", &(m_eff_pipeline->m_lumin_lim));
        ImGui::InputFloat("luminance lerp margin", &(m_eff_pipeline->m_lumi_lerp_margin));
      }
    }

    //anti aliasing
    {
      bool aa = m_eff_pipeline->m_act_effects[effects_pipeline::e_effects::anti_aliasing];

      ImGui::Checkbox("enable anti-aliasing", &aa);

      m_eff_pipeline->m_act_effects[effects_pipeline::e_effects::anti_aliasing] = aa;

      if (aa)
      {
        ImGui::InputFloat("edge detection factor##1", &(m_eff_pipeline->m_aa_factor));
        m_eff_pipeline->m_aa_factor = glm::max(0.1f, m_eff_pipeline->m_aa_factor);
      }
    }

    //emissive_glow
    {
      bool emissive_glow = m_eff_pipeline->m_act_effects[effects_pipeline::e_effects::emis_glow];

      ImGui::Checkbox("enable emissive_glow", &emissive_glow);

      m_eff_pipeline->m_act_effects[effects_pipeline::e_effects::emis_glow] = emissive_glow;

      if (emissive_glow)
      {
        ImGui::Text("Does not use luminance, uses emissive");
      }
    }

    //outlines
    {
      bool outlines = m_eff_pipeline->m_act_effects[effects_pipeline::e_effects::outlines];
      ImGui::Checkbox("enable camera z outlines", &outlines);
      m_eff_pipeline->m_act_effects[effects_pipeline::e_effects::outlines] = outlines;

      if (outlines)
      {
        ImGui::InputFloat("edge detection factor##2", &m_eff_pipeline->m_edge_detect_factor);

        ImGui::Text("edge detection is done using depth");
      }
    }

    //outlines depth
    {
      bool outlines = m_eff_pipeline->m_act_effects[effects_pipeline::e_effects::outlines_depth];
      ImGui::Checkbox("enable depth outlines", &outlines);
      m_eff_pipeline->m_act_effects[effects_pipeline::e_effects::outlines_depth] = outlines;

      if (outlines)
      {
        ImGui::InputFloat("edge detection factor##2", &m_eff_pipeline->m_edge_detect_depth_factor);

        ImGui::Text("edge detection is done using depth");
      }
    }

    //total blur
    {
      bool blur = m_eff_pipeline->m_act_effects[effects_pipeline::e_effects::blur];
      ImGui::Checkbox("enable blur", &blur);
      m_eff_pipeline->m_act_effects[effects_pipeline::e_effects::blur] = blur;

      if (blur)
      {
        int size = static_cast<int>(m_eff_pipeline->m_blur_weights_size);
        ImGui::InputInt("number of unique weights", &size);
        size = glm::clamp(size, 0, static_cast<int>(m_eff_pipeline->m_blur_weights.size()));
        m_eff_pipeline->m_blur_weights_size = static_cast<size_t>(size);

        for (size_t i = 0u; i < m_eff_pipeline->m_blur_weights_size; i++)
        {
          const std::string label = "##" + std::to_string(i);
          ImGui::InputFloat(label.c_str(), &(m_eff_pipeline->m_blur_weights[i]));
        }

        ImGui::Text("element 0 is the middle weight.\nThe rest are weights on either side (simmetry duplicates are ommited)");
      }
    }

    //LUT
#if 1
    /**///TODO
    {
      bool lut = m_eff_pipeline->m_act_effects[effects_pipeline::e_effects::LUT];
      ImGui::Checkbox("enable LUT", &lut);
      m_eff_pipeline->m_act_effects[effects_pipeline::e_effects::LUT] = lut;

      if (lut)
      {
        ImGui::Text("TODO: chose which LUT to use");
      }
    }
#endif

    //invert colors
    {
      bool invert = m_eff_pipeline->m_act_effects[effects_pipeline::e_effects::invert];
      ImGui::Checkbox("enable color inversion", &invert);
      m_eff_pipeline->m_act_effects[effects_pipeline::e_effects::invert] = invert;
    }

    //SSAA
    if (ImGui::BeginMenu("SSAA"))
    {
      ImGui::Text("Just change the g_buffer resolution\n\
to be higher than screen's resolution\n(x2 is recomended)");

      ImGui::EndMenu();
    }
  }

  ImGui::End();
}


void gui_tesselation_window::do_action() const
{
  if (!*m_open)
    return;

  if (!ImGui::Begin("Phong Tesselation", m_open))
  {
    // Early out if the window is collapsed, as an optimization.
    ImGui::End();
    return;
  }

  ImGui::InputFloat( "max tess. level", &(m_tess_settings->max_tess_lvl) );
  //clamp to (1,50) to avoid problems/lag
  m_tess_settings->max_tess_lvl = glm::clamp(m_tess_settings->max_tess_lvl, 1.0f, 50.0f);

  ImGui::Checkbox("adapth to dist", &(m_tess_settings->adapt_to_dist));

  if (m_tess_settings->adapt_to_dist)
  {
    ImGui::InputFloat("factor", &(m_tess_settings->dist_factor));
    ImGui::InputFloat("constant", &(m_tess_settings->dist_cnt));
  }

  ImGui::Checkbox("adapth to view", &(m_tess_settings->adapt_to_view));
  ImGui::Checkbox("heatmap", &(m_tess_settings->heatmap));

  ImGui::End();
}

void gui_decals_window::do_action() const
{
  if (!*m_open)
    return;

  if (!ImGui::Begin("Decals", m_open))
  {
    // Early out if the window is collapsed, as an optimization.
    ImGui::End();
    return;
  }

  ImGui::Checkbox("enable", &(m_decal_settings->enable));
  ImGui::Checkbox("draw volumes", &(m_decal_settings->draw_volumes));

  if (m_decal_settings->enable)
  {
    ImGui::InputFloat("dot limit", &(m_decal_settings->dot_lim));
    m_decal_settings->dot_lim = glm::clamp(m_decal_settings->dot_lim, -1.0f, 1.0f);
  }

  ImGui::End();
}

void gui_highlights_window::do_action() const
{
  if (!*m_open)
    return;

  if (!ImGui::Begin("Lines", m_open))
  {
    // Early out if the window is collapsed, as an optimization.
    ImGui::End();
    return;
  }

  {
    int opti_type = static_cast<int>(m_lines_settings->opti);
    if (ImGui::Combo("shader", &opti_type,
      " full options\0 less options\0 no options\0"))
    {
    }
    m_lines_settings->opti = static_cast<unsigned char>(opti_type);

    ImGui::Separator();

    switch (m_lines_settings->opti)
    {
    case 0u:
      non_opti_lines_options(m_lines_settings);
      break;
    case 1u:
      opti_lines_options(m_lines_settings);
      break;
    case 2u:
      //no options
      break;
    default:
      assert(false);
    }
  }

  ImGui::End();
}

void gui_objects_window::do_action() const
{
  if (!*m_open)
    return;

  if (!ImGui::Begin("Transformable Nodes", m_open))
  {
    // Early out if the window is collapsed, as an optimization.
    ImGui::End();
    return;
  }

  {
    //starting at 1 as 0 is the camera
    for (unsigned i = 1u; i < m_first_forbiden_node; ++i)
    {
      m_nodes_mutex->lock();

      std::shared_ptr<transform_node> n =
        std::static_pointer_cast<transform_node>((*m_nodes)[i]);

      const std::string name = "node_" + std::to_string(i);

      if (ImGui::BeginMenu(name.c_str()))
      {
        //position
        {
          const glm::vec3 pos = n->get_local_pos();
          glm::vec3 new_pos = pos;

          const std::string text = "local pos ##" + std::to_string(i);
          ImGui::InputFloat3(text.c_str(), glm::value_ptr(new_pos));

          if (!equal(pos, new_pos, 0.01f))
          {
            n->set_local_pos(new_pos);
          }
        }
        //scale
        {
          const glm::vec3 scale = n->get_local_scale();
          glm::vec3 new_sca = scale;

          const std::string text = "local scale ##" + std::to_string(i);
          ImGui::InputFloat3(text.c_str(), glm::value_ptr(new_sca));

          if (!equal(scale, new_sca, 0.01f))
          {
            n->set_local_scale(new_sca);
          }
        }
        //orientation
        {
          const glm::quat ori = n->get_local_ori();
          const glm::vec3 euler_ori = glm::eulerAngles(ori);
          glm::vec3 euler_ori_new = euler_ori;

          const std::string text = "local ori ##" + std::to_string(i);
          ImGui::DragFloat3(text.c_str(), glm::value_ptr(euler_ori_new), 0.015f);

          if (!equal(euler_ori, euler_ori_new, 0.01f))
          {
            n->set_local_ori(glm::quat(euler_ori_new));
          }
        }

        ImGui::EndMenu();
      }

      m_nodes_mutex->unlock();
    }
  }

  ImGui::End();
}
