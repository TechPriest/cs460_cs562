/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    render_actions.cpp

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:31:13 2020

\brief  Actions that make calls on the GPU. Wrapper around OpenGL.

define DEBUG_PRINTING to get a readable output on the standart error stream
each time an action is performed

define PRINT_WARNINGS to get a readable output on the standart error stream
each time something unintended but not fatal happened in do_action()
(for example, tells you when a uniform has not been found in the shader)

***************************************************************************/


#include "render_actions.h"

#include <iostream>
#include <array>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/type_ptr.hpp>
#include "graphics/debug/GL_CHECK.h"
#include "graphics/shader/build_shader.h"
#include "graphics/shader/read_shader.h"
#include "graphics/shader/shader.h"
#include "graphics/shader/uniform_location_caching.h"
#include "graphics/window/build_window.h"
#include "graphics/window/window.h"
#include "graphics/framebuffers/g_buffer.h"

//#define DEBUG_PRINTING
#define PRINT_WARNINGS

#ifdef DEBUG_PRINTING
#include "debug/glm_to_str.h"
#endif

/// INIT

void init::do_action() const
{
  assert(glfwInit());

#ifdef DEBUG_PRINTING
  std::cout << "> glfw initialized" << std::endl;
#endif
}


/// HINTS

hint_version::hint_version(int major, int minor)
  : m_major(major), m_minor(minor)
{}

void hint_version::do_action() const
{
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, m_major);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, m_minor);

#ifdef DEBUG_PRINTING
  std::cout << "> version "<< m_major<<"."<< m_minor<<" hinted" << std::endl;
#endif
}


hint_profile::hint_profile(int profile)
  : m_profile(profile)
{}

void hint_profile::do_action() const
{
  glfwWindowHint(GLFW_OPENGL_PROFILE, m_profile);

#ifdef DEBUG_PRINTING
  std::cout << "> profile set to " <<
    (m_profile == GLFW_OPENGL_CORE_PROFILE ? "\"core\"" : "?") <<std::endl;
#endif
}


/// WINDOW

create_window::create_window(GLFWwindow** glfw_win_ptr, window* win,
  const std::string& name, int width, int height)
  : m_glfw_win(glfw_win_ptr), m_win(win),
  m_name(name), m_width(width), m_height(height)
{}

void create_window::do_action() const
{
  const bool success = build_window::build(m_glfw_win, m_win,
    m_name, m_width, m_height);
  
  if (!success)
    std::cerr << "ERROR: window \"" << m_name << "\" could not be created" << std::endl;

#ifdef DEBUG_PRINTING
  else
    std::cout << "> window \""<< m_win->m_name <<
      "\" opened (w = "<< m_width<<", h = "<<m_height<<")"<< std::endl;
#endif
}


delete_window::delete_window(GLFWwindow** glfw_win)
  : m_glfw_win(glfw_win)
{}

void delete_window::do_action() const
{
  glfwDestroyWindow(*m_glfw_win);

#ifdef DEBUG_PRINTING
  std::cout << "> window \""<< m_glfw_win<<"\" closed" << std::endl;
#endif
}


bind_window::bind_window(GLFWwindow** glfw_win)
  : m_glfw_win(glfw_win)
{}

void bind_window::do_action() const
{
  glfwMakeContextCurrent(*m_glfw_win);
}


set_window_resolution::set_window_resolution(window* win, int width, int height)
  : m_win(win), m_width(width), m_height(height)
{}

void set_window_resolution::do_action() const
{
  m_win->m_width = m_width;
  m_win->m_height = m_height;
  glfwSetWindowSize(*(m_win->get_glfw_win()), m_win->m_width, m_win->m_height);

  //To make sure the variables match the real window size (OS may clamp if window doesn't fit)
  glfwGetWindowSize(*(m_win->get_glfw_win()), &(m_win->m_width), &(m_win->m_height));
}


/// PRINT OPENGL VERSION

void print_version::do_action() const
{
  GL_CHECK(std::cout << glGetString(GL_VERSION));

  int major, minor, revision;
  glfwGetVersion(&major, &minor, &revision);
  std::cout << " running against GLFW "<< major<<"."<<minor << "." << revision<<std::endl;
}


/// ENABLE/DISABLE

enable::enable(int what)
  : m_what(what)
{}

void enable::do_action() const
{
  GL_CHECK(glEnable(m_what));

#ifdef DEBUG_PRINTING
  std::cout << "> enabled ";
  
  switch (m_what)
  {
  case GL_BLEND:
    std::cout << "GL_BLEND";
    break;
  case GL_DEPTH_TEST:
    std::cout << "GL_DEPTH_TEST";
    break;
  case GL_CULL_FACE:
    std::cout << "GL_CULL_FACE";
    break;

  default:
    std::cout << m_what;

  }
  
  std::cout << std::endl;
#endif
}


disable::disable(int what)
  : m_what(what)
{}

void disable::do_action() const
{
  GL_CHECK(glDisable(m_what));

#ifdef DEBUG_PRINTING
  std::cout << "> disabled ";

  switch (m_what)
  {
  case GL_BLEND:
    std::cout << "GL_BLEND";
    break;
  case GL_DEPTH_TEST:
    std::cout << "GL_DEPTH_TEST";
    break;
  case GL_CULL_FACE:
    std::cout << "GL_CULL_FACE";
    break;

  default:
    std::cout << m_what;

  }

  std::cout << std::endl;
#endif
}



/// SWAP INTERVAL, BLENDING & CULLING

set_swap_interval::set_swap_interval(int interval)
  : m_interval(interval)
{}

void set_swap_interval::do_action() const
{
  glfwSwapInterval(m_interval);

#ifdef DEBUG_PRINTING
  std::cout << "> swap interval set to ";

  if (m_interval == 1)
    std::cout << "Vsync";
  else
    std::cout << m_interval;
  std::cout << std::endl;
#endif
}


set_blending::set_blending(int blend_param_0, int blend_param_1,
  int blend_equation)
  : m_blend_param_0(blend_param_0), m_blend_param_1(blend_param_1),
    m_blend_equation(blend_equation)
{}

void set_blending::do_action() const
{
  GL_CHECK(glBlendFunc(m_blend_param_0, m_blend_param_1));
  GL_CHECK(glBlendEquation(m_blend_equation));

#ifdef DEBUG_PRINTING
  std::cout << "> blending set to " << m_blend_param_0 <<", "<< m_blend_param_1 <<
    " with equation "<< m_blend_equation << std::endl;
#endif
}


set_culling::set_culling(int type)
  : m_type(type)
{}

void set_culling::do_action() const
{
  GL_CHECK(glCullFace(m_type));

#ifdef DEBUG_PRINTING
  std::cout << "> culling ";

  switch (m_type)
  {
  case GL_FRONT:
    std::cout << "GL_FRONT";
    break;
  case GL_BACK:
    std::cout << "GL_BACK";
    break;

  default:
    std::cout << m_type;
  }

  std::cout << std::endl;
#endif
}



/// DEPTH

set_depth_writing_permssion::set_depth_writing_permssion(int enable)
  :m_enable(enable)
{}

void set_depth_writing_permssion::do_action() const
{
  GL_CHECK(glDepthMask(m_enable));

#ifdef DEBUG_PRINTING
  std::cout << "> depth writing permission set to " << (m_enable == GL_TRUE ? "GL_TRUE" : "GL_FALSE") << std::endl;
#endif
}

set_depth_success::set_depth_success(int pass_cond)
  : m_pass_cond(pass_cond)
{}

void set_depth_success::do_action() const
{
  GL_CHECK(glDepthFunc(m_pass_cond));

#ifdef DEBUG_PRINTING
  std::cout << "> depth test pass condition set to ";
  switch (m_pass_cond)
  {
  case GL_NEVER:
    std::cout << "GL_NEVER" <<std::endl;
    break;
  case GL_LESS:
    std::cout << "GL_LESS" << std::endl;
    break;
  case GL_EQUAL:
    std::cout << "GL_EQUAL" << std::endl;
    break;
  case GL_LEQUAL:
    std::cout << "GL_LEQUAL" << std::endl;
    break;
  case GL_GREATER:
    std::cout << "GL_GREATER" << std::endl;
    break;
  case GL_NOTEQUAL:
    std::cout << "GL_NOTEQUAL" << std::endl;
    break;
  case GL_GEQUAL:
    std::cout << "GL_GEQUAL" << std::endl;
    break;
  case GL_ALWAYS:
    std::cout << "GL_ALWAYS" << std::endl;
    break;
  default:
    std::cout << "!!UNKNOWN!!" << std::endl;
    assert(false);
    break;
  }
#endif
}



/// VERTEX ARRAY OBJECT

create_vertex_array::create_vertex_array(unsigned* handle_ptr)
  :m_handle_ptr(handle_ptr)
{}

void create_vertex_array::do_action() const
{
  GL_CHECK(glGenVertexArrays(1, m_handle_ptr));

#ifdef DEBUG_PRINTING
  std::cout << "> vertex array " << *m_handle_ptr << " created" << std::endl;
#endif
}


delete_vertex_array::delete_vertex_array(const unsigned* handle)
  : m_handle(handle)
{}

void delete_vertex_array::do_action() const
{
  GL_CHECK(glDeleteVertexArrays(1, m_handle));

#ifdef DEBUG_PRINTING
  std::cout << "> vertex array "<<*m_handle <<" deleted" << std::endl;
#endif
}


bind_vertex_array::bind_vertex_array(const unsigned* handle)
  : m_handle(handle)
{}

void bind_vertex_array::do_action() const
{
  GL_CHECK(glBindVertexArray(*m_handle));

#ifdef DEBUG_PRINTING
  std::cout << "> vertex array "<< *m_handle <<" bound" << std::endl;
#endif
}


void unbind_vertex_array::do_action() const
{
  GL_CHECK(glBindVertexArray(0));

#ifdef DEBUG_PRINTING
  std::cout << "> vertex array unbound" << std::endl;
#endif
}


set_vertex_buffer_layout::set_vertex_buffer_layout(const vertex_buffer_layout & layout)
  : m_layout(layout)
{}

void set_vertex_buffer_layout::do_action() const
{
  const std::vector<layout_element>& elements = m_layout.get_elements();

  for (unsigned i = 0u; i < elements.size(); i++)
  {
    const layout_element& el = elements[i];
    GL_CHECK(glEnableVertexAttribArray(i));
    GL_CHECK(glVertexAttribPointer(i, el.count, el.type, el.normalize,
      m_layout.get_stride(), reinterpret_cast<const void*>(static_cast<size_t>(el.start))));

#ifdef DEBUG_PRINTING
    std::cout << "> vertex buffer layout " << i << " enabled and set with " << el.count << " ";
    switch (el.type)
    {
    case GL_FLOAT:
      std::cout << "GL_FLOAT";
      break;
    default:
      std::cout << "??";
      break;
    }
    std::cout << "s starting at " << el.start << " with stride " << m_layout.get_stride() << " (" <<
      (el.normalize ? "normalize" : "dont normalize") << ")" << std::endl;
#endif
  }
}



/// VERTEX BUFFER OBJECT

create_vertex_buffer::create_vertex_buffer(unsigned * handle_ptr, const void * buffer, unsigned size, unsigned type)
  : m_handle_ptr(handle_ptr), m_buffer(buffer), m_size(size), m_type(type)
{}

void create_vertex_buffer::do_action() const
{
  GL_CHECK(glGenBuffers(1, m_handle_ptr));

  //could be done in separate function: fill
  GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, *m_handle_ptr));
  GL_CHECK(glBufferData(GL_ARRAY_BUFFER, m_size, m_buffer, m_type/*GL_STATIC_DRAW*/)); //could make GL_STATIC_DRAW or GL_DYNAMIC_DRAW an argument

#ifdef DEBUG_PRINTING
  std::cout << "> vertex buffer "<< *m_handle_ptr << " (size "<< m_size<<") created and bound"<< std::endl;
#endif
}


delete_vertex_buffer::delete_vertex_buffer(const unsigned* handle)
  : m_handle(handle)
{}

void delete_vertex_buffer::do_action() const
{
  GL_CHECK(glDeleteBuffers(1, m_handle));

#ifdef DEBUG_PRINTING
  std::cout << "> vertex buffer " << *m_handle << " deleted" << std::endl;
#endif
}


bind_vertex_buffer::bind_vertex_buffer(const unsigned* handle)
  : m_handle(handle)
{}

void bind_vertex_buffer::do_action() const
{
  GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, *m_handle));

#ifdef DEBUG_PRINTING
  std::cout << "> vertex buffer " << *m_handle << " bound" << std::endl;
#endif
}


void unbind_vertex_buffer::do_action() const
{
  GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, 0));

#ifdef DEBUG_PRINTING
  std::cout << "> vertex buffer unbound" << std::endl;
#endif
}



/// INDEX BUFFER OBJECT

create_index_buffer::create_index_buffer(unsigned * handle_ptr, const void * buffer, unsigned size, unsigned type)
  : m_handle_ptr(handle_ptr), m_buffer(buffer), m_size(size), m_type(type)
{}

void create_index_buffer::do_action() const
{
  GL_CHECK(glGenBuffers(1, m_handle_ptr));
  GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *m_handle_ptr));
  GL_CHECK(glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_size, m_buffer, m_type/*GL_STATIC_DRAW*/));

#ifdef DEBUG_PRINTING
  std::cout << "> index buffer "<<*m_handle_ptr <<" (size "<< m_size <<") created and bound" << std::endl;
#endif
}


delete_index_buffer::delete_index_buffer(const unsigned* handle)
  : m_handle(handle)
{}

void delete_index_buffer::do_action() const
{
  GL_CHECK(glDeleteBuffers(1, m_handle));

#ifdef DEBUG_PRINTING
  std::cout << "> index buffer " << *m_handle << " deleted" << std::endl;
#endif
}


bind_index_buffer::bind_index_buffer(const unsigned* handle_ptr)
  : m_handle_ptr(handle_ptr)
{}

void bind_index_buffer::do_action() const
{
  GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *m_handle_ptr));

#ifdef DEBUG_PRINTING
  std::cout << "> index buffer " << *m_handle_ptr << " bound" << std::endl;
#endif
}


void unbind_index_buffer::do_action() const
{
  GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));

#ifdef DEBUG_PRINTING
  std::cout << "> index buffer unbound" << std::endl;
#endif
}



/// SHADER

create_shader::create_shader(unsigned* handle_ptr, const std::string& vert, const std::string& frag
  , const std::string& geom /*= ""*/, const std::string& tesc /*= ""*/, const std::string& tese /*= ""*/)
  : m_handle_ptr(handle_ptr), m_vert(vert), m_frag(frag), m_geom(geom), m_tesc(tesc), m_tese(tese)
{}

void create_shader::do_action() const
{
  *m_handle_ptr = build_shader::build(m_vert, m_frag, m_geom, m_tesc, m_tese);

#ifdef DEBUG_PRINTING
  std::cout << "> shader "<<*m_handle_ptr <<" created"<< std::endl;
#endif
}


delete_shader::delete_shader(const unsigned* handle)
  : m_handle(handle)
{}

void delete_shader::do_action() const
{
  GL_CHECK(glDeleteProgram(*m_handle));

#ifdef DEBUG_PRINTING
  std::cout << "> shader " << *m_handle << " deleted" << std::endl;
#endif
}


bind_shader::bind_shader(const unsigned* handle)
  : m_handle(handle)
{}

void bind_shader::do_action() const
{
  GL_CHECK(glUseProgram(*m_handle));

#ifdef DEBUG_PRINTING
  std::cout << "> shader "<< *m_handle << " bound" << std::endl;
#endif
}


clear_shader_loc_cache::clear_shader_loc_cache(
  std::unordered_map<std::string, int>* loc_cache)
  : m_loc_cache(loc_cache)
{}

void clear_shader_loc_cache::do_action() const
{
#ifdef DEBUG_PRINTING
  int cache_size = static_cast<int>(m_loc_cache->size());
#endif

  m_loc_cache->clear();

#ifdef DEBUG_PRINTING
  std::cout << "> cleared location cache (size: ";
  std::cout << cache_size <<")"<< std::endl;
#endif
}


get_uniform_location::get_uniform_location(int * loc, const unsigned* handle, const std::string & name)
  : m_loc(loc), m_uni_name(name), m_handle(handle)
{}

void get_uniform_location::do_action() const
{
  GL_CHECK(*m_loc = glGetUniformLocation(*m_handle, m_uni_name.c_str()));

#ifdef PRINT_WARNINGS
  if (*m_loc < 0)
    std::cout << "WARNING: uniform \"" << m_uni_name <<
    "\" was not found in shader " << *m_handle << std::endl;
#endif
#ifdef DEBUG_PRINTING
  std::cout << "> location of \"" << m_uni_name <<"\" in shader "<< *m_handle  <<
    " is "<< *m_loc << std::endl;
#endif
}


set_texture_slot::set_texture_slot(unsigned slot)
  : m_slot(slot)
{}

void set_texture_slot::do_action() const
{
  GL_CHECK(glActiveTexture(GL_TEXTURE0 + m_slot));

#ifdef DEBUG_PRINTING
  std::cout << "> texture slot "<<m_slot <<" activated"<< std::endl;
#endif
}


set_uniform_int::set_uniform_int(const int* loc, int value)
  : m_loc(loc), m_value(value)
{}

void set_uniform_int::do_action() const
{
  GL_CHECK(glUniform1i(*m_loc, m_value));

#ifdef DEBUG_PRINTING
  std::cout << "> int uniform in location " << *m_loc << " set to " << m_value << std::endl;
#endif
}


set_uniform_int_caching::set_uniform_int_caching(
  std::unordered_map<std::string, int>* loc_cache,
  const unsigned* shader_handle,
  const std::string& uniform_name, int value)
  : m_loc_cache(loc_cache), m_shdr_handle(shader_handle),
    m_uni_name(uniform_name), m_value(value)
{}

void set_uniform_int_caching::do_action() const
{
  const int loc = uniform_location_caching::get_uni_location(m_loc_cache, m_shdr_handle, m_uni_name);

#ifdef PRINT_WARNINGS
  if (loc < 0)
    std::cerr << "WARNING: uniform(int) \"" << m_uni_name <<
    "\" was not found in shader " << *m_shdr_handle << std::endl;
#endif
  GL_CHECK(glUniform1i(loc, m_value));

#ifdef DEBUG_PRINTING
  std::cout << "> int uniform \"" << m_uni_name << "\" in location " << loc << " set to " << m_value << std::endl;
#endif
}


set_uniform_float::set_uniform_float(const int * loc, float value)
  : m_loc(loc), m_value(value)
{}

void set_uniform_float::do_action() const
{
  GL_CHECK(glUniform1f(*m_loc, m_value));

#ifdef DEBUG_PRINTING
  std::cout << "> float uniform in location " << *m_loc << " set to " << m_value << std::endl;
#endif
}

set_uniform_float_caching::set_uniform_float_caching(std::unordered_map<std::string, int>* loc_cache,
  const unsigned * shader_handle, const std::string & uniform_name, float value)
  : m_loc_cache(loc_cache), m_shdr_handle(shader_handle), m_uni_name(uniform_name), m_value(value)
{}

void set_uniform_float_caching::do_action() const
{
  const int loc = uniform_location_caching::get_uni_location(
    m_loc_cache, m_shdr_handle, m_uni_name);

#ifdef PRINT_WARNINGS
  if (loc < 0)
    std::cerr << "WARNING: uniform(float) \"" << m_uni_name <<
    "\" was not found in shader " << *m_shdr_handle << std::endl;
#endif
  GL_CHECK(glUniform1f(loc, m_value));

#ifdef DEBUG_PRINTING
  std::cout << "> float uniform \"" << m_uni_name << "\" in location " << loc << " set to " << m_value << std::endl;
#endif
}


set_uniform_vec2::set_uniform_vec2(const int* loc, glm::vec2 value)
  : m_loc(loc), m_value(value)
{}

void set_uniform_vec2::do_action() const
{
  GL_CHECK(glUniform2f(*m_loc, m_value.x, m_value.y));

#ifdef DEBUG_PRINTING
  std::cout << "> vec2 uniform in location " << *m_loc << " set to " << to_str(m_value) << std::endl;
#endif
}

set_uniform_vec2_caching::set_uniform_vec2_caching(std::unordered_map<std::string, int>* loc_cache,
  const unsigned* shader_handle, const std::string& uniform_name, glm::vec2 value)
  : m_loc_cache(loc_cache), m_shdr_handle(shader_handle), m_uni_name(uniform_name), m_value(value)
{}

void set_uniform_vec2_caching::do_action() const
{
  const int loc = uniform_location_caching::get_uni_location(
    m_loc_cache, m_shdr_handle, m_uni_name);

#ifdef PRINT_WARNINGS
  if (loc < 0)
    std::cerr << "WARNING: uniform(vec2) \"" << m_uni_name <<
    "\" was not found in shader " << *m_shdr_handle << std::endl;
#endif
  GL_CHECK(glUniform2f(loc, m_value.x, m_value.y));

#ifdef DEBUG_PRINTING
  std::cout << "> vec2 uniform \"" << m_uni_name << "\" in location " << loc << " set to " << to_str(m_value) << std::endl;
#endif
}


set_uniform_vec3::set_uniform_vec3(const int * loc, glm::vec3 value)
  : m_loc(loc), m_value(value)
{}

void set_uniform_vec3::do_action() const
{
  GL_CHECK(glUniform3f(*m_loc, m_value.x, m_value.y, m_value.z));

#ifdef DEBUG_PRINTING
  std::cout << "> vec3 uniform in location " << *m_loc << " set to " << to_str(m_value) << std::endl;
#endif
}

set_uniform_vec3_caching::set_uniform_vec3_caching(std::unordered_map<std::string, int>* loc_cache,
  const unsigned * shader_handle, const std::string & uniform_name, glm::vec3 value)
  : m_loc_cache(loc_cache), m_shdr_handle(shader_handle), m_uni_name(uniform_name), m_value(value)
{}

void set_uniform_vec3_caching::do_action() const
{
  const int loc = uniform_location_caching::get_uni_location(
    m_loc_cache, m_shdr_handle, m_uni_name);

#ifdef PRINT_WARNINGS
  if (loc < 0)
    std::cerr << "WARNING: uniform(vec3) \"" << m_uni_name <<
    "\" was not found in shader " << *m_shdr_handle << std::endl;
#endif
  GL_CHECK(glUniform3f(loc, m_value.x, m_value.y, m_value.z));

#ifdef DEBUG_PRINTING
  std::cout << "> vec3 uniform \"" << m_uni_name << "\" in location " << loc << " set to " << to_str(m_value) << std::endl;
#endif
}


set_uniform_vec4::set_uniform_vec4(const int* loc, glm::vec4 value)
  : m_loc(loc), m_value(value)
{}

void set_uniform_vec4::do_action() const
{
  GL_CHECK(glUniform4f(*m_loc, m_value.x, m_value.y, m_value.z, m_value.w));

#ifdef DEBUG_PRINTING
  std::cout << "> vec4 uniform in location "<< *m_loc <<" set to "<<to_str(m_value)<< std::endl;
#endif
}

set_uniform_vec4_caching::set_uniform_vec4_caching(
  std::unordered_map<std::string, int>* loc_cache,
  const unsigned* shader_handle, const std::string & uniform_name,
  glm::vec4 value)
  : m_shdr_handle(shader_handle), m_loc_cache(loc_cache), m_uni_name(uniform_name),
  m_value(value)
{}

void set_uniform_vec4_caching::do_action() const
{
  const int loc = uniform_location_caching::get_uni_location(m_loc_cache, m_shdr_handle, m_uni_name);

#ifdef PRINT_WARNINGS  
  if(loc < 0)
    std::cerr << "WARNING: uniform(vec4) \"" << m_uni_name <<
      "\" was not found in shader " << *m_shdr_handle << std::endl;
#endif  
  GL_CHECK(glUniform4f(loc, m_value.x, m_value.y, m_value.z, m_value.w));

#ifdef DEBUG_PRINTING
  std::cout << "> vec4 uniform \"" << m_uni_name << "\" in location " << loc << " set to " << to_str(m_value) << std::endl;
#endif
}


set_uniform_mat4::set_uniform_mat4(const int * loc, glm::mat4 value)
  : m_loc(loc), m_value(value)
{}

void set_uniform_mat4::do_action() const
{
  GL_CHECK(glUniformMatrix4fv(*m_loc, 1, GL_FALSE, glm::value_ptr(m_value)));

#ifdef DEBUG_PRINTING
  std::cout << "> mat4 uniform in location " << *m_loc << " set to " << to_str(m_value) << std::endl;
#endif
}


set_uniform_mat4_caching::set_uniform_mat4_caching(std::unordered_map<std::string, int>* loc_cache,
  const unsigned * shader_handle, const std::string & uniform_name, glm::mat4 value)
  : m_loc_cache(loc_cache), m_shdr_handle(shader_handle), m_uni_name(uniform_name), m_value(value)
{}

void set_uniform_mat4_caching::do_action() const
{
  const int loc = uniform_location_caching::get_uni_location(m_loc_cache, m_shdr_handle, m_uni_name);

#ifdef PRINT_WARNINGS 
  if (loc < 0)
    std::cerr << "WARNING: uniform(mat4) \"" << m_uni_name <<
    "\" was not found in shader " << *m_shdr_handle << std::endl;
#endif
  GL_CHECK(glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(m_value)));

#ifdef DEBUG_PRINTING
  std::cout << "> mat4 uniform \""<< m_uni_name <<"\" in location " << loc << " set to " << to_str(m_value) << std::endl;
#endif
}


set_uniform_texture::set_uniform_texture(const int* loc, unsigned slot)
  : m_loc(loc), m_slot(slot)
{}

void set_uniform_texture::do_action() const
{
  GL_CHECK(glUniform1i(*m_loc, m_slot));

#ifdef DEBUG_PRINTING
  std::cout << "> texture uniform in location " << *m_loc << " set to texture slot " << m_slot << std::endl;
#endif
}


set_uniform_texture_caching::set_uniform_texture_caching(std::unordered_map<std::string, int>* loc_cache,
  const unsigned * shader_handle, const std::string & uniform_name, unsigned slot)
  : m_loc_cache(loc_cache), m_shdr_handle(shader_handle), m_uni_name(uniform_name), m_slot(slot)
{}

void set_uniform_texture_caching::do_action() const
{
  const int loc = uniform_location_caching::get_uni_location(m_loc_cache, m_shdr_handle, m_uni_name);

#ifdef PRINT_WARNINGS 
  if (loc < 0)
    std::cerr << "WARNING: uniform(texture) \"" << m_uni_name <<
    "\" was not found in shader " << *m_shdr_handle << std::endl;
#endif
  GL_CHECK(glUniform1i(loc, m_slot));

#ifdef DEBUG_PRINTING
  std::cout << "> texture uniform \""<< m_uni_name <<"\" in location " << loc << " set to texture slot " << m_slot << std::endl;
#endif
}


set_uniform_int_array_caching::set_uniform_int_array_caching(
  std::unordered_map<std::string, int>* loc_cache, const unsigned* shader_handle,
  const std::string& uniform_name, const int* array, unsigned count)
  : m_loc_cache(loc_cache), m_shdr_handle(shader_handle), m_uni_name(uniform_name),
    m_array(array), m_count(count)
{}

void set_uniform_int_array_caching::do_action() const
{
  const int loc = uniform_location_caching::get_uni_location(m_loc_cache, m_shdr_handle, m_uni_name);

#ifdef PRINT_WARNINGS 
  if (loc < 0)
    std::cerr << "WARNING: uniform(int[]) \"" << m_uni_name <<
    "\" was not found in shader " << *m_shdr_handle << std::endl;
#endif
  GL_CHECK(glUniform1iv(loc, m_count, m_array));

#ifdef DEBUG_PRINTING
  std::cout << "> int[] uniform \"" << m_uni_name << "\" in location " << loc << " set to ";
  
  for (size_t i = 0u; i < 4u && i < m_count; i++)
  {
    std::cout <<( i == 0u ? "" : ", ")<< m_array[i];
  }
  if (m_count > 4u)
    std::cout << "...";

  std::cout << ". Array size: "<< m_count;
  std::cout << std::endl;
#endif
}

set_uniform_float_array_caching::set_uniform_float_array_caching(
  std::unordered_map<std::string, int>* loc_cache, const unsigned* shader_handle,
  const std::string& uniform_name, const float* array, unsigned count)
  : m_loc_cache(loc_cache), m_shdr_handle(shader_handle),
    m_uni_name(uniform_name), m_count(count)
{
  m_array.resize(count);

  for (unsigned i = 0; i < m_count; i++)
  {
    m_array[i] = array[i];
  }
}

void set_uniform_float_array_caching::do_action() const
{
  const int loc = uniform_location_caching::get_uni_location(m_loc_cache, m_shdr_handle, m_uni_name);

#ifdef PRINT_WARNINGS 
  if (loc < 0)
    std::cerr << "WARNING: uniform(float[]) \"" << m_uni_name <<
    "\" was not found in shader " << *m_shdr_handle << std::endl;
#endif
  GL_CHECK(glUniform1fv(loc, m_count, m_array.data()));

#ifdef DEBUG_PRINTING
  std::cout << "> float[] uniform \"" << m_uni_name << "\" in location " << loc << " set to ";
  for (size_t i = 0u; i < 4u && i < m_count; i++)
  {
    std::cout << (i == 0u ? "" : ", ") << m_array[i];
  }
  if (m_count > 4u)
    std::cout << "...";

  std::cout << ". Array size: " << m_count;
  std::cout << std::endl;
#endif
}

set_uniform_vec2_array_caching::set_uniform_vec2_array_caching(std::unordered_map<std::string, int>* loc_cache,
  const unsigned* shader_handle, const std::string& uniform_name, const glm::vec2* array, unsigned count)
  : m_loc_cache(loc_cache), m_shdr_handle(shader_handle),
  m_uni_name(uniform_name), m_count(count)
{
  m_array.resize(count);

  for (unsigned i = 0; i < m_count; i++)
  {
    m_array[i] = array[i];
  }
}

void set_uniform_vec2_array_caching::do_action() const
{
  const int loc = uniform_location_caching::get_uni_location(m_loc_cache, m_shdr_handle, m_uni_name);

#ifdef PRINT_WARNINGS 
  if (loc < 0)
    std::cerr << "WARNING: uniform(vec2[]) \"" << m_uni_name <<
    "\" was not found in shader " << *m_shdr_handle << std::endl;
#endif

  GL_CHECK(glUniform2fv(loc, m_count, glm::value_ptr(*(m_array.data()))));

#ifdef DEBUG_PRINTING
  std::cout << "> vec2[] uniform \"" << m_uni_name << "\" in location " << loc << " set to ";
  for (size_t i = 0u; i < 4u && i < m_count; i++)
  {
    std::cout << (i == 0u ? "" : ", ") << to_str(m_array[i]);
  }
  if (m_count > 4u)
    std::cout << "...";

  std::cout << ". Array size: " << m_count;
  std::cout << std::endl;
#endif
}

/// TEXTURE

create_texture_2D::create_texture_2D(unsigned * handle_ptr, unsigned char * data,
  int width, int height, int wrap_mode, int sample_mode)
  : m_handle_ptr(handle_ptr), m_data(data),
    m_width(width), m_height(height),
    m_wrap_mode(wrap_mode), m_sample_mode(sample_mode)
{}

void create_texture_2D::do_action() const
{
  GL_CHECK(glGenTextures(1, m_handle_ptr));
  GL_CHECK(glBindTexture(GL_TEXTURE_2D, *m_handle_ptr));

  // set the texture wrapping mode (on the currently bound texture object)
  GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, m_wrap_mode));
  GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, m_wrap_mode));

  //set the texture filtering mode
  if (m_sample_mode == GL_LINEAR_MIPMAP_LINEAR)
  {
    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR));
    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
  }
  else
  {
    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, m_sample_mode));
    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, m_sample_mode));
  }

  //actual upload to GPU
  GL_CHECK(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, m_width, m_height, 0, GL_RGBA,
    GL_UNSIGNED_BYTE, m_data));

  if(m_sample_mode == GL_LINEAR_MIPMAP_LINEAR)
    GL_CHECK(glGenerateMipmap(GL_TEXTURE_2D));

  delete m_data; //free the texture from the CPU

#ifdef DEBUG_PRINTING
  std::cout << "> texture2D "<< *m_handle_ptr <<" ("<< m_width<<"x"<< m_height <<") created and bound" << std::endl;
#endif
}

create_texture_3D::create_texture_3D(unsigned* handle_ptr, unsigned char* data,
  int width, int height, int depth,
  int wrap_mode)
  : m_handle_ptr(handle_ptr), m_data(data),
    m_width(width), m_height(height), m_depth(depth),
    m_wrap_mode(wrap_mode)
{}

void create_texture_3D::do_action() const
{
  GL_CHECK(glGenTextures(1, m_handle_ptr));
  GL_CHECK(glBindTexture(GL_TEXTURE_3D, *m_handle_ptr));

  // set the texture wrapping mode (on the currently bound texture object)
  GL_CHECK(glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, m_wrap_mode));
  GL_CHECK(glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, m_wrap_mode));
  GL_CHECK(glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, m_wrap_mode));

  //set the texture filtering mode
  GL_CHECK(glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
  GL_CHECK(glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));

  //actual upload to GPU
  GL_CHECK(glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA8, m_width, m_height, m_depth, 0, GL_RGBA,
    GL_UNSIGNED_BYTE, m_data));

  delete m_data; //free the texture from the CPU

#ifdef DEBUG_PRINTING
  std::cout << "> texture3D " << *m_handle_ptr << " (" << m_width << "x" << m_height << "x" << m_depth << ") created and bound" << std::endl;
#endif
}

delete_texture::delete_texture(const unsigned* handle)
  : m_handle(handle)
{}

void delete_texture::do_action() const
{
  GL_CHECK(glDeleteTextures(1, m_handle));

#ifdef DEBUG_PRINTING
  std::cout << "> texture "<< m_handle<<" deleted" << std::endl;
#endif
}


bind_texture::bind_texture(const unsigned* handle, int type)
  : m_handle(handle), m_type(type)
{}

void bind_texture::do_action() const
{
  GL_CHECK(glBindTexture(m_type, *m_handle));

#ifdef DEBUG_PRINTING
  std::cout << "> texture " << *m_handle << " bound" << std::endl;
#endif
}



/// FRAME BUFFER

delete_frame_buffer::delete_frame_buffer(unsigned* handle)
  : m_handle(handle)
{}

void delete_frame_buffer::do_action() const
{
  //delete frame buffer
  GL_CHECK(glDeleteFramebuffers(1, m_handle));

#ifdef DEBUG_PRINTING
  std::cout << "> frame-buffer " << *m_handle << " deleted" << std::endl;
#endif
}


create_rend_buffer::create_rend_buffer(unsigned* handle, unsigned* tex_handle,
  int width, int height, int minification, int magnification)
  : m_handle(handle), m_tex_handle(tex_handle),
    m_width(width),   m_height(height),
    m_minif(minification), m_magnif(magnification)
{
}

void create_rend_buffer::do_action() const
{
  GL_CHECK(glGenFramebuffers(1, m_handle));
  GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, *m_handle));

  //generate texture
  unsigned tex_handle;
  {
    GL_CHECK(glGenTextures(1, &tex_handle));
    GL_CHECK(glBindTexture(GL_TEXTURE_2D, tex_handle));
    GL_CHECK(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_width, m_height, 0,
      GL_RGBA, GL_FLOAT, NULL));
    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, m_minif)); //GL_NEAREST
    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, m_magnif));
    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
    GL_CHECK(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
      GL_TEXTURE_2D, tex_handle, 0));
  }

  *m_tex_handle = tex_handle;

  // - tell OpenGL which color attachments we値l use (of this framebuffer)
  unsigned int attachments[1] = { GL_COLOR_ATTACHMENT0/*, GL_COLOR_ATTACHMENT1*/ };
  glDrawBuffers(1, attachments);

  // completeness.
  GL_CHECK(
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
      std::cerr << "ERROR: frame buffer  " << m_handle << " is not complete" << std::endl;
    }
  );

#ifdef DEBUG_PRINTING
  std::cout << "> rend buffer " << *m_handle << " created (texture:";
  std::cout << " " << *m_tex_handle;
  std::cout << ")" << std::endl;
#endif
}


delete_rend_buffer::delete_rend_buffer(unsigned* handle, unsigned* tex_handle)
  : m_handle(handle), m_tex_handle(tex_handle)
{
}

void delete_rend_buffer::do_action() const
{
  //delete texture
  GL_CHECK(glDeleteTextures(1, m_tex_handle));

  //delete frame buffer
  GL_CHECK(glDeleteFramebuffers(1, m_handle));

#ifdef DEBUG_PRINTING
  std::cout << "> rend-buffer " << *m_handle << " deleted (texture:";
  std::cout << " " << *m_tex_handle;
  std::cout << ")" << std::endl;
#endif
}



bind_frame_buffer::bind_frame_buffer(unsigned* handle)
  : m_handle(handle)
{}

void bind_frame_buffer::do_action() const
{
  GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, *m_handle));


  /*If you want to render your whole screen to a texture of a smaller or larger size you need to
call glViewport again (before rendering to your framebuffer) with the new dimensions
of your texture, otherwise render commands will only fill part of the texture.*/

#ifdef DEBUG_PRINTING
  std::cout << "> frame buffer " << *m_handle << " bound" << std::endl;
#endif
}


void unbind_frame_buffer::do_action() const
{
  GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, 0));

#ifdef DEBUG_PRINTING
  std::cout << "> frame buffer unbound" << std::endl;
#endif
}


create_g_buffer_v4::create_g_buffer_v4(unsigned* handle, unsigned* tex_handles,
  unsigned tex_handles_size, int width, int height,
  int minification, int magnification)
  : m_handle(handle), m_tex_handles(tex_handles),
    m_tex_handles_size(tex_handles_size), m_width(width), m_height(height),
    m_minif(minification), m_magnif(magnification)
{}

void create_g_buffer_v4::do_action() const
{
  GL_CHECK(glGenFramebuffers(1, m_handle));
  GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, *m_handle));

  //generate g-buffer textures
  {
    unsigned norms_tex_handle, diff_tex_handle, emis_tex_handle, depth_tex_handle;
    //normal (3*2 bytes)
    {
      GL_CHECK(glGenTextures(1, &norms_tex_handle));
      GL_CHECK(glBindTexture(GL_TEXTURE_2D, norms_tex_handle));
      GL_CHECK(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, m_width, m_height,
        0, GL_RGB, GL_FLOAT, NULL));
      GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, m_minif));
      GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, m_magnif));
      GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
      GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
      GL_CHECK(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
        GL_TEXTURE_2D, norms_tex_handle, 0));
    }
    //diffuse + greyscale specular (4*1 bytes)
    {
      GL_CHECK(glGenTextures(1, &diff_tex_handle));
      GL_CHECK(glBindTexture(GL_TEXTURE_2D, diff_tex_handle));
      GL_CHECK(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_width, m_height, 0,
        GL_RGBA, GL_FLOAT, NULL));
      GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, m_minif));
      GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, m_magnif));
      GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
      GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
      GL_CHECK(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1,
        GL_TEXTURE_2D, diff_tex_handle, 0));
    }
    //emissive + shininess (4*1 bytes)
    {
      GL_CHECK(glGenTextures(1, &emis_tex_handle));
      GL_CHECK(glBindTexture(GL_TEXTURE_2D, emis_tex_handle));
      GL_CHECK(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_width, m_height, 0,
        GL_RGBA, GL_UNSIGNED_BYTE, NULL));
      GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, m_minif));
      GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, m_magnif));
      GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
      GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
      GL_CHECK(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2,
        GL_TEXTURE_2D, emis_tex_handle, 0));
    }
    //depth buffer
    {
      GL_CHECK(glGenTextures(1, &depth_tex_handle));
      GL_CHECK(glBindTexture(GL_TEXTURE_2D, depth_tex_handle));
      GL_CHECK(glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, m_width, m_height, 0,
        GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL));
      GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, m_minif));
      GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, m_magnif));
      GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
      GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
      GL_CHECK(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
        GL_TEXTURE_2D, depth_tex_handle, 0));
    }

    m_tex_handles[static_cast<size_t>(e_gbuff_v4_textures::normals)] = norms_tex_handle;
    m_tex_handles[static_cast<size_t>(e_gbuff_v4_textures::diff)] = diff_tex_handle;
    m_tex_handles[static_cast<size_t>(e_gbuff_v4_textures::emi)] = emis_tex_handle;
    m_tex_handles[static_cast<size_t>(e_gbuff_v4_textures::depth)] = depth_tex_handle;
  }


  // - tell OpenGL which color attachments we値l use (of this framebuffer)
  unsigned int attachments[3] =
    { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
  glDrawBuffers(3, attachments);


  // completeness.
  GL_CHECK(
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
      std::cerr << "ERROR: frame buffer  " << m_handle << " is not complete" << std::endl;
    }
  );

#ifdef DEBUG_PRINTING
  std::cout << "> g-buffer " << *m_handle << " created (textures:"; 

  for (unsigned i = 0u; i < m_tex_handles_size; i++)
  {
    std::cout << " " << m_tex_handles[i];
  }

  std::cout <<")"<< std::endl;
#endif
}


create_final_g_buffer::create_final_g_buffer(unsigned* handle, unsigned* tex_handles,
  unsigned tex_handles_size, int width, int height, int minification, int magnification)
  : m_handle(handle), m_tex_handles(tex_handles),
  m_tex_handles_size(tex_handles_size), m_width(width), m_height(height),
  m_minif(minification), m_magnif(magnification)
{
}

void create_final_g_buffer::do_action() const
{
  GL_CHECK(glGenFramebuffers(1, m_handle));
  GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, *m_handle));

  //generate g-buffer textures
  {
    unsigned norms_tex_handle, depth_tex_handle;
    //normal (3*2 bytes)
    {
      GL_CHECK(glGenTextures(1, &norms_tex_handle));
      GL_CHECK(glBindTexture(GL_TEXTURE_2D, norms_tex_handle));
      GL_CHECK(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, m_width, m_height,
        0, GL_RGB, GL_FLOAT, NULL));
      GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, m_minif));
      GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, m_magnif));
      GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
      GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
      GL_CHECK(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
        GL_TEXTURE_2D, norms_tex_handle, 0));
    }
    //depth buffer
    {
      GL_CHECK(glGenTextures(1, &depth_tex_handle));
      GL_CHECK(glBindTexture(GL_TEXTURE_2D, depth_tex_handle));
      GL_CHECK(glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, m_width, m_height, 0,
        GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL));
      GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, m_minif));
      GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, m_magnif));
      GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
      GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
      GL_CHECK(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
        GL_TEXTURE_2D, depth_tex_handle, 0));
    }

    m_tex_handles[static_cast<size_t>(e_gbuff_final_project::normals)] = norms_tex_handle;
    m_tex_handles[static_cast<size_t>(e_gbuff_final_project::depth)] = depth_tex_handle;
  }


  // - tell OpenGL which color attachments we値l use (of this framebuffer)
  unsigned int attachments[1] =
  { GL_COLOR_ATTACHMENT0 };
  glDrawBuffers(1, attachments);


  // completeness.
  GL_CHECK(
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
      std::cerr << "ERROR: frame buffer  " << m_handle << " is not complete" << std::endl;
    }
  );

#ifdef DEBUG_PRINTING
  std::cout << "> g-buffer " << *m_handle << " created (textures:";

  for (unsigned i = 0u; i < m_tex_handles_size; i++)
  {
    std::cout << " " << m_tex_handles[i];
  }

  std::cout << ")" << std::endl;
#endif
}


delete_g_buffer::delete_g_buffer(unsigned* handle,
  unsigned* tex_handles, unsigned tex_handles_size)
  : m_handle(handle), m_tex_handles(tex_handles),
    m_tex_handles_size(tex_handles_size)
{}

void delete_g_buffer::do_action() const
{
  //delete textures
  GL_CHECK(glDeleteTextures(m_tex_handles_size, m_tex_handles));

  //delete frame buffer
  GL_CHECK(glDeleteFramebuffers(1, m_handle));

#ifdef DEBUG_PRINTING
  std::cout << "> g-buffer " << *m_handle << " deleted (textures:";

  for (unsigned i = 0u; i < m_tex_handles_size; i++)
  {
    std::cout << " " << m_tex_handles[i];
  }

  std::cout << ")" << std::endl;
#endif
}


create_rend_buffer_with_shared_depth::create_rend_buffer_with_shared_depth(
  unsigned* handle, unsigned* tex_handle,
  const unsigned* depth_tex_handle,
  int width, int height, int minification, int magnification)
  : m_handle(handle), m_tex_handle(tex_handle), m_depth_tex_handle(depth_tex_handle),
  m_width(width), m_height(height), m_minif(minification), m_magnif(magnification)
{}

void create_rend_buffer_with_shared_depth::do_action() const
{
  GL_CHECK(glGenFramebuffers(1, m_handle));
  GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, *m_handle));

  //generate texture
  unsigned tex_handle;
  {
    GL_CHECK(glGenTextures(1, &tex_handle));
    GL_CHECK(glBindTexture(GL_TEXTURE_2D, tex_handle));
    GL_CHECK(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_width, m_height, 0,
      GL_RGBA, GL_FLOAT, NULL));
    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, m_minif));
    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, m_magnif));
    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
    GL_CHECK(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
      GL_TEXTURE_2D, tex_handle, 0));
  }

  //depth
  {
    GL_CHECK(glBindTexture(GL_TEXTURE_2D, *m_depth_tex_handle));
    GL_CHECK(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
      GL_TEXTURE_2D, *m_depth_tex_handle, 0));
  }

  *m_tex_handle = tex_handle;

  // - tell OpenGL which color attachments we値l use (of this framebuffer)
  unsigned int attachments[1] = { GL_COLOR_ATTACHMENT0 };
  glDrawBuffers(1, attachments);

  // completeness.
  GL_CHECK(
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
      std::cerr << "ERROR: frame buffer  " << m_handle << " is not complete" << std::endl;
    }
  );

#ifdef DEBUG_PRINTING
  std::cout << "> frame buffer with shared depth " << *m_handle << " created (texture:";
  std::cout << " " << *m_tex_handle << " depth texture: ";
  std::cout << *m_depth_tex_handle << ")" << std::endl;
#endif
}


create_rend_buffer_with_shared_depth_diff_norm::create_rend_buffer_with_shared_depth_diff_norm(
  unsigned* handle, const unsigned* depth_tex_handle,
  const unsigned* diff_tex_handle, const unsigned* norm_tex_handle,
  int width, int height, int minification, int magnification)
  : m_handle(handle), m_depth_tex_handle(depth_tex_handle),
  m_diff_tex_handle(diff_tex_handle), m_norm_tex_handle(norm_tex_handle),
  m_width(width), m_height(height), m_minif(minification), m_magnif(magnification)
{}

void create_rend_buffer_with_shared_depth_diff_norm::do_action() const
{
  GL_CHECK(glGenFramebuffers(1, m_handle));
  GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, *m_handle));

  //depth
  {
    GL_CHECK(glBindTexture(GL_TEXTURE_2D, *m_depth_tex_handle));
    GL_CHECK(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
      GL_TEXTURE_2D, *m_depth_tex_handle, 0));
  }
  //diff
  {
    GL_CHECK(glBindTexture(GL_TEXTURE_2D, *m_depth_tex_handle));
    GL_CHECK(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
      GL_TEXTURE_2D, *m_diff_tex_handle, 0));
  }
  //norm
  {
    GL_CHECK(glBindTexture(GL_TEXTURE_2D, *m_depth_tex_handle));
    GL_CHECK(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1,
      GL_TEXTURE_2D, *m_norm_tex_handle, 0));
  }


  // - tell OpenGL which color attachments we値l use (of this framebuffer)
  unsigned int attachments[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
  glDrawBuffers(2, attachments);

  // completeness.
  GL_CHECK(
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
      std::cerr << "ERROR: frame buffer  " << m_handle << " is not complete" << std::endl;
    }
  );

#ifdef DEBUG_PRINTING
  std::cout << "> frame buffer with shared depth, diff and norm " << *m_handle << " created (";
  std::cout << "depth texture: " << *m_depth_tex_handle << ", ";
  std::cout << "diff texture: " << *m_diff_tex_handle << ", ";
  std::cout << "norm texture: " << *m_norm_tex_handle << ")" << std::endl;
#endif
}


/// DRAW

set_clear_color::set_clear_color(const glm::vec3 color)
  : m_color(color)
{}

void set_clear_color::do_action() const
{
  GL_CHECK(glClearColor(m_color.r, m_color.g, m_color.b, 1.0f));

#ifdef DEBUG_PRINTING
  std::cout << "> clear color set to " << to_str(m_color) << std::endl;
#endif
}


set_viewport::set_viewport(int width, int height)
  : m_width(width), m_height(height)
{}

void set_viewport::do_action() const
{
  GL_CHECK(GL_CHECK(glViewport(0, 0, m_width, m_height)););

#ifdef DEBUG_PRINTING
  std::cout << "> viewport set to "<<m_width<<" x "<<m_height << std::endl;
#endif
}


render_clear::render_clear(int param)
  : m_param(param)
{}

void render_clear::do_action() const
{
  GL_CHECK(glClear(m_param));

#ifdef DEBUG_PRINTING
  std::cout << "> cleared render buffer (";
  
  if(m_param & GL_COLOR_BUFFER_BIT)
    std::cout << " GL_COLOR_BUFFER_BIT";
  if (m_param & GL_DEPTH_BUFFER_BIT)
    std::cout << " GL_DEPTH_BUFFER_BIT";
  if (m_param & GL_STENCIL_BUFFER_BIT)
    std::cout << " GL_STENCIL_BUFFER_BIT";

  std::cout <<" )"<< std::endl;
#endif
}


set_polygon_mode::set_polygon_mode(int param_0, int param_1)
  : m_param_0(param_0), m_param_1(param_1)
{}

void set_polygon_mode::do_action() const
{
  GL_CHECK(glPolygonMode(m_param_0, m_param_1));

#ifdef DEBUG_PRINTING
  std::cout << "> polygon mode set to..." << std::endl;
#endif
}


draw_elements::draw_elements(unsigned count)
  : m_count(count)
{}

void draw_elements::do_action() const
{
  GL_CHECK(glDrawElements(GL_TRIANGLES, m_count, GL_UNSIGNED_INT, 0));

#ifdef DEBUG_PRINTING
  std::cout << "> drawed elements ("<< m_count <<" indices)" << std::endl;
#endif
}

draw_patches::draw_patches(unsigned count, unsigned patch_size)
  : m_count(count), m_patch_size(patch_size)
{}

void draw_patches::do_action() const
{
  GL_CHECK(glPatchParameteri(GL_PATCH_VERTICES, m_patch_size));

  GL_CHECK(glDrawElements(GL_PATCHES, m_count, GL_UNSIGNED_INT, 0));

#ifdef DEBUG_PRINTING
  std::cout << "> drawed elements (" << m_count <<
    " indices) (patch size: "<< m_patch_size <<")" << std::endl;
#endif
}


set_patch_size::set_patch_size(unsigned patch_size)
  : m_patch_size(patch_size)
{}

void set_patch_size::do_action() const
{
  GL_CHECK(glPatchParameteri(GL_PATCH_VERTICES, m_patch_size));

#ifdef DEBUG_PRINTING
  std::cout << "> patch size set to " << m_patch_size << std::endl;
#endif
}


/// SWAP BUFFERS AND POLL EVENTS

swap_buffers_and_poll_events::swap_buffers_and_poll_events(GLFWwindow** glfw_win)
  : m_glfw_win(glfw_win)
{}

void swap_buffers_and_poll_events::do_action() const
{
  glfwSwapBuffers(*m_glfw_win);
  glfwPollEvents();

#ifdef DEBUG_PRINTING
  std::cout << "> buffers swapped in window "<< m_glfw_win << std::endl;
#endif
}



/// FOR DEBUGGING

print::print(const std::string& str)
  : m_str(str)
{}

void print::do_action() const
{
  std::cout << m_str << std::endl;
}


display_rectangle::display_rectangle(GLFWwindow** glfw_win)
  : m_glfw_win(glfw_win)
{}

void display_rectangle::do_action() const
{
  float positions[12u] =
  {
    -0.5f, -0.5f, 0.0f,
      0.5f, -0.5f, 0.0f,
      0.5f, 0.5f, 0.0f,
      -0.5, 0.5f, 0.0f
  };

  std::array<unsigned, 6> indices =
  {
    0,1,2,
    0,2,3
  };

  std::string vert, frag, geo, tesc, tese;
  read_shader::read("res/shaders/hello_shader.shdr", &vert, &frag, &geo, &tesc, &tese);
  unsigned shdr0 = build_shader::build(vert, frag);

  unsigned vao = 0;
  unsigned vbo = 0;
  unsigned ibo = 0;

  GL_CHECK(glClear(GL_COLOR_BUFFER_BIT));

  GL_CHECK(glGenVertexArrays(1, &vao));
  GL_CHECK(glBindVertexArray(vao));

  GL_CHECK(glGenBuffers(1, &vbo));
  GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, vbo));
  GL_CHECK(glBufferData(GL_ARRAY_BUFFER, sizeof(positions), positions, GL_STATIC_DRAW));

  GL_CHECK(glEnableVertexAttribArray(0));
  GL_CHECK(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(float), reinterpret_cast<const void*>(0)));

  GL_CHECK(glGenBuffers(1, &ibo));
  GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo));
  GL_CHECK(glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned), indices.data(), GL_STATIC_DRAW));

  //unbind stuff
  GL_CHECK(glBindVertexArray(0));
  GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));
  GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, 0));

  //from the model, only the vao must be bound
  GL_CHECK(glBindVertexArray(vao));
  GL_CHECK(glUseProgram(shdr0));
  GL_CHECK(glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL)); //ME META AQUI!! error: INVALID_ENUM

  GL_CHECK(glDeleteProgram(shdr0));
  GL_CHECK(glDeleteBuffers(1, &ibo));
  GL_CHECK(glDeleteBuffers(1, &vbo));
  GL_CHECK(glDeleteVertexArrays(1, &vao));

  glfwSwapBuffers(*m_glfw_win);
  glfwPollEvents();
}


