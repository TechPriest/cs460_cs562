/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    render_actions.h

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:30:41 2020

\brief  Actions that make calls on the GPU. Wrapper around OpenGL.

define DEBUG_PRINTING to get a readable output on the standart error stream
each time an action is performed

define PRINT_WARNINGS to get a readable output on the standart error stream
each time something unintended but not fatal happened in do_action()
(for example, tells you when a uniform has not been found in the shader)



***************************************************************************/


/*

even though instructions like bind_vertex_array need the vao_handle,
we should pass the pointer to it istead. This is because the vao_handle
variable may not be initialized yet when being copied in the action.
By using the pointer we make sure that, if the order is correct, the
variable has been set by the GPU before binding.

*/


#pragma once

#include "action.h"

#include <vector>
#include <queue>
#include <unordered_map> //uniform caching
#include <string>
#include <glm/glm.hpp>
#include "graphics/model/vertex_buffer_layout.h"

class window;
struct GLFWwindow;


/// INIT

class init : public action
{
public:
  void do_action() const override;
};


/// HINTS

class hint_version : public action
{
public:

  const int m_major, m_minor;

  hint_version(int major, int minor);
  void do_action() const override;
};

class hint_profile : public action
{
public:

  const int m_profile; //GLFW_OPENGL_CORE_PROFILE

  hint_profile(int profile);
  void do_action() const override;
};



/// WINDOW

class create_window : public action
{
public:
  GLFWwindow** m_glfw_win = nullptr;
  window* m_win = nullptr;
  const std::string m_name;
  const int m_width = 0;
  const int m_height = 0;

  create_window(GLFWwindow** glfw_win_ptr, window* win,
    const std::string& name, int width, int height);
  void do_action() const override;
};

class delete_window :public action
{
public:
  GLFWwindow** m_glfw_win = nullptr;

  delete_window(GLFWwindow** m_glfw_win);
  void do_action() const override;
};

class bind_window : public action
{
public:
  GLFWwindow** m_glfw_win = nullptr;

  bind_window(GLFWwindow** glfw_win);
  void do_action() const override;
};

class set_window_resolution : public action
{
public:
  window* m_win = nullptr;
  const int m_width, m_height;

  set_window_resolution(window* win, int width, int height);
  void do_action() const override;
};


/// PRINT OPENGL VERSION

class print_version : public action
{
public:
  void do_action() const override;
};


/// ENABLE/DISABLE

class enable : public action
{
public:
  const int m_what; //for example GL_BLEND or GL_DEPTH_TEST

  enable(int what);
  void do_action() const override;
};

class disable : public action
{
public:
  const int m_what;  //for example GL_BLEND or GL_DEPTH_TEST

  disable(int what);
  void do_action() const override;
};


/// SWAP INTERVAL, BLENDING & CULLING

class set_swap_interval : public action
{
public:
  const int m_interval;

  set_swap_interval(int interval);
  void do_action() const override;
};

class set_blending : public action
{
public:
  const int m_blend_param_0;
  const int m_blend_param_1;
  const int m_blend_equation;

  set_blending(int blend_param_0, int blend_param_1, int blend_equation);
  void do_action() const override;
};

class set_culling : public action
{
public:
  const int m_type; //GL_FRONT or GL_BACK

  set_culling(int type);
  void do_action() const override;
};


/// DEPTH

class set_depth_writing_permssion : public action
{
public:
  const int m_enable = -1; //GL_FALSE or GL_TRUE
  set_depth_writing_permssion(int enable);
  void do_action() const override;
};

class set_depth_success : public action
{
public:
  const int m_pass_cond = -1; //GL_LESS, GL_GREATER, GL_GEQUAL, ...
  set_depth_success(int pass_cond);
  void do_action() const override;
};


/// VERTEX ARRAY OBJECT

class create_vertex_array : public action
{
public:
  unsigned* m_handle_ptr = nullptr;

  create_vertex_array(unsigned* handle_ptr);
  void do_action() const override; //does NOT bind
};

class delete_vertex_array : public action
{
public:
  const unsigned* m_handle;

  delete_vertex_array(const unsigned* handle);
  void do_action() const override;
};

class bind_vertex_array : public action
{
public:
  const unsigned* m_handle = 0u;

  bind_vertex_array(const unsigned* handle);
  void do_action() const override;
};

class unbind_vertex_array : public action
{
public:
  void do_action() const override;
};

class set_vertex_buffer_layout : public action
{
public:
  vertex_buffer_layout m_layout;

  set_vertex_buffer_layout(const vertex_buffer_layout& layout);
  void do_action() const override;
};


/// VERTEX BUFFER OBJECT

class create_vertex_buffer : public action
{
public:
  unsigned* m_handle_ptr = nullptr;
  const void * m_buffer = nullptr;
  const unsigned m_size = 0u;
  const unsigned m_type = 0u; //GL_STATIC_DRAW

  create_vertex_buffer(unsigned* handle_ptr, const void * buffer, unsigned size, unsigned type);
  void do_action() const override; //binds
};

class delete_vertex_buffer : public action
{
public:
  const unsigned* m_handle;

  delete_vertex_buffer(const unsigned*handle);
  void do_action() const override;
};

class bind_vertex_buffer : public action
{
public:
  const unsigned* m_handle = 0u;

  bind_vertex_buffer(const unsigned* handle);
  void do_action() const override;
};

class unbind_vertex_buffer : public action
{
public:
  void do_action() const override;
};


/// INDEX BUFFER OBJECT

class create_index_buffer : public action
{
public:
  unsigned* m_handle_ptr = nullptr;
  const void * m_buffer = nullptr;
  const unsigned m_size = 0u;
  const unsigned m_type = 0u; //GL_STATIC_DRAW

  create_index_buffer(unsigned * handle_ptr, const void * buffer, unsigned size, unsigned type);
  void do_action() const override; //binds
};

class delete_index_buffer : public action
{
public:
  const unsigned* m_handle;

  delete_index_buffer(const unsigned* handle);
  void do_action() const override;
};

class bind_index_buffer : public action
{
public:
  const unsigned* m_handle_ptr = 0u;

  bind_index_buffer(const unsigned* handle_ptr);
  void do_action() const override;
};

class unbind_index_buffer : public action
{
public:
  void do_action() const override;
};


/// SHADER

class create_shader : public action
{
public:
  unsigned* m_handle_ptr = nullptr;
  const std::string m_vert = "";
  const std::string m_frag = "";
  const std::string m_geom = "";
  const std::string m_tesc = "";
  const std::string m_tese = "";

  create_shader(unsigned* handle_ptr, const std::string& vert, const std::string& frag,
    const std::string& geom = "", const std::string& tesc = "", const std::string& tese = "");
  void do_action() const override;  //does NOT bind
};

class delete_shader : public action
{
public:
  const unsigned* m_handle;

  delete_shader(const unsigned* handle);
  void do_action() const override;
};

class bind_shader : public action
{
public:
  const unsigned* m_handle = 0u;

  bind_shader(const unsigned* handle);
  void do_action() const override;
};

class clear_shader_loc_cache : public action
{
public:
  std::unordered_map<std::string, int>* m_loc_cache;

  clear_shader_loc_cache(
    std::unordered_map<std::string, int>* loc_cache);
  void do_action() const override;
};

class get_uniform_location : public action
{
public:
  int* m_loc;
  const std::string m_uni_name;
  const unsigned* m_handle; //shader handle

  get_uniform_location(int* loc, const unsigned* handle, const std::string& name);
  void do_action() const override;
};

class set_texture_slot : public action
{
public:
  const unsigned m_slot;

  set_texture_slot(unsigned slot);
  void do_action() const override;
};

class set_uniform_int : public action
{
public:
  const int* m_loc;
  const int m_value;

  set_uniform_int(const int* loc, int value);
  void do_action() const override;
};

class set_uniform_int_caching : public action
{
public:
  std::unordered_map<std::string, int>* m_loc_cache;
  const unsigned* m_shdr_handle; //shader handle
  const std::string m_uni_name; //name of the uniform
  const int m_value;


  set_uniform_int_caching(std::unordered_map<std::string, int>* loc_cache,
    const unsigned* shader_handle, const std::string& uniform_name, int value);
  void do_action() const override;
};

class set_uniform_float : public action
{
public:
  const int* m_loc;
  const float m_value;

  set_uniform_float(const int* loc, float value);
  void do_action() const override;
};

class set_uniform_float_caching : public action
{
public:
  std::unordered_map<std::string, int>* m_loc_cache;
  const unsigned* m_shdr_handle; //shader handle
  const std::string m_uni_name; //name of the uniform
  const float m_value;


  set_uniform_float_caching(std::unordered_map<std::string, int>* loc_cache,
    const unsigned* shader_handle, const std::string& uniform_name, float value);
  void do_action() const override;
};

class set_uniform_vec2 : public action
{
public:
  const int* m_loc;
  const glm::vec2 m_value;

  set_uniform_vec2(const int* loc, glm::vec2 value);
  void do_action() const override;
};

class set_uniform_vec2_caching : public action
{
public:
  std::unordered_map<std::string, int>* m_loc_cache;
  const unsigned* m_shdr_handle; //shader handle
  const std::string m_uni_name; //name of the uniform
  const glm::vec2 m_value;


  set_uniform_vec2_caching(std::unordered_map<std::string, int>* loc_cache,
    const unsigned* shader_handle, const std::string& uniform_name, glm::vec2 value);
  void do_action() const override;
};

class set_uniform_vec3 : public action
{
public:
  const int* m_loc;
  const glm::vec3 m_value;

  set_uniform_vec3(const int* loc, glm::vec3 value);
  void do_action() const override;
};

class set_uniform_vec3_caching : public action
{
public:
  std::unordered_map<std::string, int>* m_loc_cache;
  const unsigned* m_shdr_handle; //shader handle
  const std::string m_uni_name; //name of the uniform
  const glm::vec3 m_value;


  set_uniform_vec3_caching(std::unordered_map<std::string, int>* loc_cache,
    const unsigned* shader_handle, const std::string& uniform_name, glm::vec3 value);
  void do_action() const override;
};

class set_uniform_vec4 : public action
{
public:
  const int* m_loc;
  const glm::vec4 m_value;

  set_uniform_vec4(const int* loc, glm::vec4 value);
  void do_action() const override;
};

class set_uniform_vec4_caching : public action
{
public:
  std::unordered_map<std::string, int>* m_loc_cache;
  const unsigned* m_shdr_handle; //shader handle
  const std::string m_uni_name; //name of the uniform
  const glm::vec4 m_value;
  

  set_uniform_vec4_caching(std::unordered_map<std::string, int>* loc_cache,
    const unsigned* shader_handle, const std::string& uniform_name, glm::vec4 value);
  void do_action() const override;
};

class set_uniform_mat4 : public action
{
public:
  const int* m_loc;
  const glm::mat4 m_value;

  set_uniform_mat4(const int* loc, glm::mat4 value);
  void do_action() const override;
};

class set_uniform_mat4_caching : public action
{
public:
  std::unordered_map<std::string, int>* m_loc_cache;
  const unsigned* m_shdr_handle; //shader handle
  const std::string m_uni_name; //name of the uniform
  const glm::mat4 m_value;


  set_uniform_mat4_caching(std::unordered_map<std::string, int>* loc_cache,
    const unsigned* shader_handle, const std::string& uniform_name, glm::mat4 value);
  void do_action() const override;
};

class set_uniform_texture : public action
{
public:
  const int* m_loc;
  const unsigned m_slot;

  set_uniform_texture(const int* loc, unsigned slot);
  void do_action() const override;
};

class set_uniform_texture_caching : public action
{
public:
  std::unordered_map<std::string, int>* m_loc_cache;
  const unsigned* m_shdr_handle; //shader handle
  const std::string m_uni_name; //name of the uniform
  const unsigned m_slot;


  set_uniform_texture_caching(std::unordered_map<std::string, int>* loc_cache,
    const unsigned* shader_handle, const std::string& uniform_name, unsigned slot);
  void do_action() const override;
};

class set_uniform_int_array_caching : public action
{
public:
  std::unordered_map<std::string, int>* m_loc_cache;
  const unsigned* m_shdr_handle; //shader handle
  const std::string m_uni_name; //name of the uniform
  const int* m_array;
  const unsigned m_count;


  set_uniform_int_array_caching(std::unordered_map<std::string, int>* loc_cache,
    const unsigned* shader_handle, const std::string& uniform_name,
    const int* array, unsigned count);
  void do_action() const override;
};

class set_uniform_float_array_caching : public action
{
public:
  std::unordered_map<std::string, int>* m_loc_cache;
  const unsigned* m_shdr_handle; //shader handle
  const std::string m_uni_name; //name of the uniform
  std::vector<float> m_array;
  const unsigned m_count;


  set_uniform_float_array_caching(std::unordered_map<std::string, int>* loc_cache,
    const unsigned* shader_handle, const std::string& uniform_name,
    const float* array, unsigned count);
  void do_action() const override;
};

class set_uniform_vec2_array_caching : public action
{
public:
  std::unordered_map<std::string, int>* m_loc_cache;
  const unsigned* m_shdr_handle; //shader handle
  const std::string m_uni_name; //name of the uniform
  std::vector<glm::vec2> m_array;
  const unsigned m_count;


  set_uniform_vec2_array_caching(std::unordered_map<std::string, int>* loc_cache,
    const unsigned* shader_handle, const std::string& uniform_name,
    const glm::vec2* array, unsigned count);
  void do_action() const override;
};


/// TEXTURE

class create_texture_2D : public action
{
public:
  unsigned* m_handle_ptr = nullptr;
  const unsigned char* m_data = nullptr;
  const int m_width = 0, m_height = 0;
  const int m_wrap_mode = 0; //GL_CLAMP_TO_EDGE, GL_REPEAT, ...
  const int m_sample_mode = 0; //GL_LINEAR, GL_LINEAR_MIPMAP_LINEAR, GL_NEAREST, ...

  create_texture_2D(unsigned* handle_ptr, unsigned char* data,
    int width, int height, int wrap_mode, int sample_mode);
  void do_action() const override;
};

class create_texture_3D : public action
{
public:
  unsigned* m_handle_ptr = nullptr;
  const unsigned char* m_data = nullptr;
  const int m_width = 0, m_height = 0, m_depth = 0;
  const int m_wrap_mode = 0; //GL_CLAMP_TO_EDGE, GL_REPEAT, ...
  const int m_type = 0; //GL_TEXTURE_2D, GL_TEXTURE_3D, ...

  create_texture_3D(unsigned* handle_ptr, unsigned char* data,
    int width, int height, int depth,
    int wrap_mode);
  void do_action() const override;
};

class delete_texture : public action
{
public:
  const unsigned* m_handle;

  delete_texture(const unsigned* handle);
  void do_action() const override;
};

class bind_texture : public action
{
public:
  const unsigned* m_handle;
  int m_type; //GL_TEXTURE_2D

  bind_texture(const unsigned* handle, int type);
  void do_action() const override;
};


/// FRAME BUFFER

class delete_frame_buffer : public action
{
public:
  unsigned* m_handle = 0u;

  delete_frame_buffer(unsigned* handle);
  void do_action() const override;
};

class create_rend_buffer : public action
{
public:
  unsigned* m_handle = 0u;
  unsigned* m_tex_handle = nullptr;;

  const int m_width = -1;
  const int m_height = -1;

  const int m_minif = -1; //GL_LINEAR, GL_NEAREST, etc
  const int m_magnif = -1; //GL_LINEAR, GL_NEAREST, etc

  create_rend_buffer(unsigned* handle, unsigned* tex_handle,
    int width, int height, int minification, int magnification);
  void do_action() const override;
};

class delete_rend_buffer : public action
{
public:
  unsigned* m_handle = 0u;
  unsigned* m_tex_handle = nullptr;

  delete_rend_buffer(unsigned* handle, unsigned* tex_handle);
  void do_action() const override;
};

class create_g_buffer_v4 : public action
{
public:
  unsigned* m_handle = 0u;
  unsigned* m_tex_handles = nullptr;
  const unsigned m_tex_handles_size = 0u;

  const int m_width = -1;
  const int m_height = -1;

  const int m_minif = -1; //GL_LINEAR, GL_NEAREST, etc
  const int m_magnif = -1; //GL_LINEAR, GL_NEAREST, etc

  create_g_buffer_v4(unsigned* handle, unsigned* tex_handles,
    unsigned tex_handles_size, int width, int height,
    int minification, int magnification);
  void do_action() const override;
};

class create_final_g_buffer : public action
{
public:
  unsigned* m_handle = 0u;
  unsigned* m_tex_handles = nullptr;
  const unsigned m_tex_handles_size = 0u;

  const int m_width = -1;
  const int m_height = -1;

  const int m_minif = -1; //GL_LINEAR, GL_NEAREST, etc
  const int m_magnif = -1; //GL_LINEAR, GL_NEAREST, etc

  create_final_g_buffer(unsigned* handle, unsigned* tex_handles,
    unsigned tex_handles_size, int width, int height,
    int minification, int magnification);
  void do_action() const override;
};

class delete_g_buffer : public action
{
public:
  unsigned* m_handle = 0u;
  unsigned* m_tex_handles = nullptr;
  const unsigned m_tex_handles_size = 0u;

  delete_g_buffer(unsigned* handle, unsigned* tex_handles,
    unsigned tex_handles_size);
  void do_action() const override;
};

class bind_frame_buffer : public action
{
public:
  const unsigned * m_handle = 0u;

  bind_frame_buffer(unsigned* handle);
  void do_action() const override;
};

class unbind_frame_buffer : public action
{
public:
  void do_action() const override;
};

class create_rend_buffer_with_shared_depth : public action
{
public:
  unsigned* m_handle = 0u;
  unsigned* m_tex_handle = nullptr;
  const unsigned* m_depth_tex_handle = nullptr;

  const int m_width = -1;
  const int m_height = -1;

  const int m_minif = -1; //GL_LINEAR, GL_NEAREST, etc
  const int m_magnif = -1; //GL_LINEAR, GL_NEAREST, etc

  create_rend_buffer_with_shared_depth(
    unsigned* handle, unsigned* tex_handle,
    const unsigned* depth_tex_handle,
    int width, int height,
    int minification, int magnification);
  void do_action() const override;
};

class create_rend_buffer_with_shared_depth_diff_norm : public action
{
public:
  unsigned* m_handle = 0u;
  const unsigned* m_depth_tex_handle = nullptr;
  const unsigned* m_diff_tex_handle = nullptr;
  const unsigned* m_norm_tex_handle = nullptr;

  const int m_width = -1;
  const int m_height = -1;

  const int m_minif = -1; //GL_LINEAR, GL_NEAREST, etc
  const int m_magnif = -1; //GL_LINEAR, GL_NEAREST, etc

  create_rend_buffer_with_shared_depth_diff_norm(
    unsigned* handle,
    const unsigned* depth_tex_handle,
    const unsigned* diff_tex_handle,
    const unsigned* norm_tex_handle,
    int width, int height,
    int minification, int magnification);
  void do_action() const override;
};


/// DRAW

class set_clear_color : public action
{
public:
  const glm::vec3 m_color;

  set_clear_color(const glm::vec3 color);
  void do_action() const override;
};

class set_viewport : public action
{
public:
  const int m_width = -1;
  const int m_height = -1;

  set_viewport(int width, int height);
  void do_action() const override;
};

class render_clear : public action
{
public:
  const int m_param; //GL_COLOR_BUFFER_BIT

  render_clear(int param);
  void do_action() const override;
};

class set_polygon_mode : public action
{
public:
  const int m_param_0;
  const int m_param_1;

  set_polygon_mode(int param_0, int param_1);
  void do_action() const override;
};

class draw_elements : public action
{
public:
  const unsigned m_count;

  draw_elements(unsigned count);
  void do_action() const override;
};

class draw_patches : public action
{
public:
  const unsigned m_count;
  const unsigned m_patch_size;

  draw_patches(unsigned count, unsigned patch_size);
  void do_action() const override;
};

class set_patch_size : public action
{
public:
  const unsigned m_patch_size; //number of vertices in the patch

  set_patch_size(unsigned patch_size);
  void do_action() const override;
};


/// SWAP BUFFERS AND POLL EVENTS

class swap_buffers_and_poll_events : public action
{
public:
  GLFWwindow** m_glfw_win = nullptr;

  swap_buffers_and_poll_events(GLFWwindow** glfw_win);
  void do_action() const override;
};


/// FOR DEBUGGING

/// <summary>
/// simple print action
/// </summary>
class print : public action
{
public:
  const std::string m_str;

  print(const std::string& str);
  void do_action() const override;
};

/// <summary>
/// push this action and this action only inside the game loop,
/// do nothing else in that loop.An orange rectangle should
/// appear on screen.It requires that the window is open
///
/// I dont know if it still works
/// </summary>
class display_rectangle : public action
{
public:
  GLFWwindow** m_glfw_win = nullptr;

  display_rectangle(GLFWwindow** glfw_win);
  void do_action() const override;
};

