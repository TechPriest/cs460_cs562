/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    load_actions.h

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:30:20 2020

\brief  Actions that load files to the CPU (meshes, textures, shaders, etc)

define DEBUG_PRINTING to get an readable output on the standart error stream
each time an action is performed

***************************************************************************/

#pragma once

#include <string>
#include <vector>

#include "action.h"

class vertex_buffer_layout;
class model;
class mesh;
class texture;
class material;

/// SHADER

class load_shader : public action
{
public:

  const std::string m_path = "";
  std::string* m_vert_code = nullptr;
  std::string* m_frag_code = nullptr;
  std::string* m_geom_code = nullptr;
  std::string* m_tesc_code = nullptr;
  std::string* m_tese_code = nullptr;

  load_shader(const std::string& path, std::string* vert_code, std::string* frag_code,
    std::string* geom_code, std::string* tesc_code, std::string* tese_code);
  void do_action() const override;
};


/// TEXTURE

class load_texture : public action
{
public:

  const std::string m_path = "";
  int* m_width = nullptr;
  int* m_height = nullptr;
  int* m_BPP = nullptr;
  unsigned char** m_data = nullptr;

  load_texture(const std::string& path, int* width, int* height, int* BPP, unsigned char** data);
  void do_action() const override;
};

class load_texture3D : public action
{
public:

  const std::string m_path = "";
  int* m_width = nullptr;
  int* m_height = nullptr;
  int* m_depth = nullptr;
  int* m_BPP = nullptr;
  unsigned char** m_data = nullptr;

  load_texture3D(const std::string& path, int* width, int* height, int* depth, int* BPP, unsigned char** data);
  void do_action() const override;
};

/// MODEL

class load_model_assimp : public action
{
public:

  model* m_model;

  load_model_assimp(model* modl);
  void do_action() const override;
};
