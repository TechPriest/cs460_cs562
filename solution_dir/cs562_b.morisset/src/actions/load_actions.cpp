/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    load_actions.cpp

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:30:32 2020

\brief  Actions that load files to the CPU (meshes, textures, shaders, etc)

define DEBUG_PRINTING to get an readable output on the standart error stream
each time an action is performed



***************************************************************************/


#include "load_actions.h"

#include <string_view>
#include <charconv>
#include "graphics/shader/read_shader.h"
#include "graphics/texture/read_texture.h"
#include "graphics/model/read_model.h"
#include "graphics/model/model.h"
#include "graphics/model/mesh.h"
#include "graphics/material/material.h"

//#define DEBUG_PRINTING

#ifdef DEBUG_PRINTING
#include <iostream>
#endif

/// SHADER

load_shader::load_shader(const std::string& path, std::string* vert_code, std::string* frag_code,
  std::string* geom_code, std::string* tesc_code, std::string* tese_code)
  : m_path(path), m_vert_code(vert_code), m_frag_code(frag_code), m_geom_code(geom_code),
    m_tesc_code(tesc_code), m_tese_code(tese_code)
{}

void load_shader::do_action() const
{
  read_shader::read(m_path, m_vert_code, m_frag_code, m_geom_code, m_tesc_code, m_tese_code);

#ifdef DEBUG_PRINTING
  std::cout << "> shader "<<m_path<<" loaded(CPU)" << std::endl;
#endif
}


/// TEXTURE

load_texture::load_texture(const std::string & path, int * width, int * height, int * BPP, unsigned char** data)
  : m_path(path), m_width(width), m_height(height), m_BPP(BPP), m_data(data)
{}

void load_texture::do_action() const
{
  *m_data = read_texture::read(m_path, m_width, m_height, m_BPP);

#ifdef DEBUG_PRINTING
    std::cout << "> texture " << m_path << " loaded(CPU)" << std::endl;
#endif
}

load_texture3D::load_texture3D(const std::string& path, int* width, int* height, int* depth, int* BPP, unsigned char** data)
  : m_path(path), m_width(width), m_height(height), m_depth(depth), m_BPP(BPP), m_data(data)
{}

void load_texture3D::do_action() const
{
  *m_data = read_texture::read(m_path, m_width, m_height, m_BPP);

  //find depth from path convention (LUT_name.32.png is depth 32)
  {
    const std::string_view path_view(m_path);
    const size_t last_dot = m_path.find_last_of('.');
    assert(last_dot != m_path.npos);

    const size_t prev_last_dot = m_path.find_first_of('.', last_dot - 4u);
    assert(prev_last_dot != m_path.npos);

    const std::string_view depth_view = path_view.substr(prev_last_dot + 1u, last_dot - prev_last_dot -1u);
    std::from_chars(depth_view.data(), depth_view.data() + depth_view.size(), *m_depth);
  }
  //divide width and height accordingly
  {
    const int div = static_cast<int>(std::sqrtf(static_cast<float>(*m_depth)));
    *m_width /= div;
    *m_height /= div;

    assert(*m_width == *m_height && *m_width == *m_depth);
  }
  
#if 1
  //rearange data to match opengl's glTexImage3D
  {
    const size_t N = static_cast<size_t>(*m_width);
    const size_t sqrtN = static_cast<size_t>(std::sqrtf(static_cast<float>(N)));
    const size_t byte_size = N * N * N * 4u;

    //save current m_data into a temporal variable
    std::vector<unsigned char> temp(byte_size, 0u); //init to all 0
    const errno_t err = memcpy_s(temp.data(), byte_size, *m_data, byte_size);
    if (err)
      assert(false);

    std::vector<size_t> subtextures(N * N, 0u); //init to all 0

    /**///used to be able to debug print (/**///TODO: remove it)
    /**/struct texel
    /**/{
    /**/  unsigned char r, g, b, a;
    /**/
    /**/  bool operator==(const texel& rhs) const = default;
    /**/};

    //rearange the data into m_data
    for (size_t byte_i = 0u; byte_i < byte_size; byte_i+=4u)
    {
      //struct texel
      //{
      //  unsigned char r, g, b, a;
      //};
      texel* tex = reinterpret_cast<texel*>(temp.data() + byte_i);

      const size_t texel_idx = byte_i / 4u;

      const size_t R = sqrtN -1u -texel_idx / (N * N * sqrtN);
      const size_t C = (texel_idx / N) % sqrtN;
      const size_t new_texel_idx = R * N*N*sqrtN + C * N*N + subtextures[R * N + C];
      //const size_t new_texel_idx = R * N * N + C * N + subtextures[R * N + C];
      const size_t new_byte_i = new_texel_idx * 4u;
      subtextures[R * N + C]++;

      texel* new_tex = reinterpret_cast<texel*>(*m_data + new_byte_i);

      *new_tex = *tex;

      //std::cout << "texel "<< texel_idx
      //  <<" (R"<<R<<", C"<<C<<")("
      //  << static_cast<int>(tex->r) << ", "
      //  << static_cast<int>(tex->g) << ", "
      //  << static_cast<int>(tex->b) << ", "
      //  << static_cast<int>(tex->a) << ")"
      //  << " to "<< new_texel_idx <<"\n" << std::flush;

    }
    /*
    std::cout << "\n old layout: \n";
    for (size_t byte_i = 0u; byte_i < byte_size; byte_i += 4u)
    {
      constexpr texel r{ 255u,0u,0u,255u };
      constexpr texel g{ 0u,255u,0u,255u };
      constexpr texel b{ 0u,0u,255u,255u };
      constexpr texel w{ 255u,255u,255u,255u };

      texel* tex = reinterpret_cast<texel*>(temp.data() + byte_i);

      if (*tex == r)
        std::cout << 'R';
      if (*tex == g)
        std::cout << 'G';
      if (*tex == b)
        std::cout << 'B';
      if (*tex == w)
        std::cout << 'W';
    }

    std::cout << "\n new layout: \n";
    for (size_t byte_i = 0u; byte_i < byte_size; byte_i += 4u)
    {
      constexpr texel r{ 255u,0u,0u,255u };
      constexpr texel g{ 0u,255u,0u,255u };
      constexpr texel b{ 0u,0u,255u,255u };
      constexpr texel w{ 255u,255u,255u,255u };

      texel* tex = reinterpret_cast<texel*>(*m_data + byte_i);

      if (*tex == r)
        std::cout << 'R';
      else if (*tex == g)
        std::cout << 'G';
      else if (*tex == b)
        std::cout << 'B';
      else if (*tex == w)
        std::cout << 'W';
      else
        std::cout << '?';
    }
    std::cout << std::flush;
    */
  }
#endif



#ifdef DEBUG_PRINTING
  std::cout << "> texture3D " << m_path << " loaded(CPU)" << std::endl;
#endif
}


/// MODEL

load_model_assimp::load_model_assimp(model * modl)
  : m_model(modl)
{}

void load_model_assimp::do_action() const
{
  read_model::read(m_model->m_path, m_model);
}


