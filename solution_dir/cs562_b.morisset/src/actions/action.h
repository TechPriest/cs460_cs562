/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    action.h

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:29:28 2020

\brief  Base class for actions. Actions are meant to be sharable across threads.
An action copies all needed data upon construction and then performs the task when
do_action is called.



***************************************************************************/

#pragma once

/// <summary>
/// Base class for actions. Actions are meant to be sharable across threads.
/// An action copies all needed data upon constructionand then performs the task when
/// do_action is called.
/// </summary>
class action
{
public:
  virtual ~action() {}

  virtual void do_action() const = 0;
};

