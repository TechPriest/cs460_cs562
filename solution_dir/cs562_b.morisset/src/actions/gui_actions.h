/*!**************************************************************************
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.

\file    gui_actions.h

\author Benat Morisset de Perdigo

\par    email: b.morisset@digipen.edu 

\par    DigiPen login: b.morisset 

\par    Course: CS562 

\par    Assignment #1 

\date    Fri Oct 09 01:29:57 2020

\brief  Actions that make calls on the GPU. Wrapper around OpenGL.



***************************************************************************/




/*
  all ImGui calls must be done on the active context thread

*/

#pragma once

#include "action.h"
#include <memory>
#include <mutex>
#include <bitset>
#include "nodes/point_light_node.h"

struct GLFWwindow;
class scene_settings;
class effects_pipeline;
class tesselation_settings;
class occlusion_settings;
class decal_settings;
class final_project_settings;
class gui_windows;
class gui_windows_tess;
class g_buffer;
class window;


/// CORE ACTIONS

/// <summary>
/// Initializes ImGui
/// Call after contex creation
/// </summary>
class gui_init : public action
{
  GLFWwindow** m_glfw_win = nullptr;

public:
  gui_init(GLFWwindow** glfw_wi) : m_glfw_win(glfw_wi){}
  void do_action() const override;

};

/// <summary>
/// Perform all necessary ImGui cleanup
/// Call before context deletion
/// </summary>
class gui_shutdown : public action
{
public:
  void do_action() const override;

};

/// <summary>
/// Sets ImGui for this frame
/// Call before any ImGui calls for this frame
/// </summary>
class gui_start_frame : public action
{
public:
  void do_action() const override;

};

/// <summary>
/// End ImGui for this frame
/// Call if frame was started but not going to render.
/// An example of this is if we want to stop drawing when
/// the window is out of focus
/// </summary>
class gui_no_render_end : public action
{
public:
  void do_action() const override;

};

/// <summary>
/// Starts rendering of ImGui
/// Call after all ImGui calls for this frame and after clearing framebuffer
/// </summary>
class gui_start_render : public action
{
public:
  void do_action() const override;

};

/// <summary>
/// Ends rendering of ImGui
/// Call before swaping buffers
/// </summary>
class gui_end_render : public action
{
public:
  void do_action() const override;

};


/// DEMO SCENE

/// <summary>
/// Shows ImGui demo. Usefull to learn how to use the library.
/// call this between gui_start_frame and gui_start_render
/// </summary>
class gui_demo : public action
{
public:
  void do_action() const override;

};


/// TOOLS

/// <summary>
/// Displays the top bar menu
/// </summary>
class gui_top_bar : public action
{
private:

  scene_settings* m_scene_setts = nullptr;
  gui_windows* m_gui_wins = nullptr;
  g_buffer* m_g_buff = nullptr;
  std::mutex* m_cpu_his_mutex = nullptr;

public:
  gui_top_bar(scene_settings* scene_setts, g_buffer* g_buff,
    gui_windows* gui_wins, std::mutex* cpu_his_mutex)
    : m_scene_setts(scene_setts), m_g_buff(g_buff),
      m_gui_wins(gui_wins), m_cpu_his_mutex(cpu_his_mutex)
  {}
  void do_action() const override;
};

/// <summary>
/// Displays the top bar menu
/// </summary>
class gui_top_bar_tess : public action
{
private:
  scene_settings* m_scene_setts = nullptr;
  gui_windows_tess* m_gui_wins = nullptr;
  g_buffer* m_g_buff = nullptr;
  std::mutex* m_cpu_his_mutex = nullptr;

public:
  gui_top_bar_tess(scene_settings* scene_setts, g_buffer* g_buff,
    gui_windows_tess* gui_wins, std::mutex* cpu_his_mutex)
    : m_scene_setts(scene_setts), m_g_buff(g_buff),
      m_gui_wins(gui_wins), m_cpu_his_mutex(cpu_his_mutex)
  {}
  void do_action() const override;
};

/// <summary>
/// Displays the top bar menu
/// </summary>
class gui_highlights_top_bar : public action
{
private:

  scene_settings* m_scene_setts = nullptr;
  gui_windows* m_gui_wins = nullptr;
  g_buffer* m_g_buff = nullptr;
  std::mutex* m_cpu_his_mutex = nullptr;

public:
  gui_highlights_top_bar(scene_settings* scene_setts, g_buffer* g_buff,
    gui_windows* gui_wins, std::mutex* cpu_his_mutex)
    : m_scene_setts(scene_setts), m_g_buff(g_buff),
    m_gui_wins(gui_wins), m_cpu_his_mutex(cpu_his_mutex)
  {}
  void do_action() const override;
};

/// <summary>
/// Displays the lights settings window
/// </summary>
class gui_lights_window : public action
{
private:
  bool* m_open = nullptr;

  bool* m_paused = nullptr;
  std::vector<std::shared_ptr<node>>* m_nodes;
  std::mutex* m_nodes_mutex;
  int* m_count_diff = nullptr;
  const unsigned m_first_light_idx;
  bool* m_draw_bulbs = nullptr;
  bool* m_draw_infl = nullptr;
  bool* m_light_opti = nullptr;
  occlusion_settings* m_occl_setts = nullptr;

public:
  gui_lights_window(bool* open, bool* paused, std::vector<std::shared_ptr<node>>* nodes,
    std::mutex* nodes_mutex, unsigned first_light_idx, int* count_diff,
    bool* draw_bulbs, bool* draw_infl, bool* light_opti, occlusion_settings* occl_setts)
    : m_open(open),
      m_paused(paused),
      m_nodes(nodes),
      m_nodes_mutex(nodes_mutex),
      m_count_diff(count_diff),
      m_first_light_idx(first_light_idx),
      m_draw_bulbs(draw_bulbs),
      m_draw_infl(draw_infl),
      m_light_opti(light_opti),
      m_occl_setts(occl_setts)
  {}
  void do_action() const override;
};

/// <summary>
/// Displays the effects settings window
/// </summary>
class gui_effects_window : public action
{
private:
  bool* m_open = nullptr;

  effects_pipeline* m_eff_pipeline = nullptr;

public:
  gui_effects_window(bool* open, effects_pipeline* eff_pipeline)
    : m_open(open), m_eff_pipeline(eff_pipeline)
  {}
  void do_action() const override;
};

/// <summary>
/// Displays the phong tesselation settings window
/// </summary>
class gui_tesselation_window : public action
{
private:
  bool* m_open = nullptr;

  tesselation_settings* m_tess_settings = nullptr;

public:
  gui_tesselation_window(bool* open, tesselation_settings* tess_settings)
    : m_open(open), m_tess_settings(tess_settings)
  {}
  void do_action() const override;
};

/// <summary>
/// Displays the decal settings window
/// </summary>
class gui_decals_window : public action
{
private:
  bool* m_open = nullptr;

  decal_settings* m_decal_settings = nullptr;

public:
  gui_decals_window(bool* open, decal_settings* decal_setts)
    : m_open(open), m_decal_settings(decal_setts)
  {}
  void do_action() const override;
};

/// <summary>
/// Displays the highlights settings window
/// </summary>
class gui_highlights_window : public action
{
private:
  bool* m_open = nullptr;

  final_project_settings* m_lines_settings = nullptr;

public:
  gui_highlights_window(bool* open, final_project_settings* lines_settings)
    : m_open(open), m_lines_settings(lines_settings)
  {}
  void do_action() const override;
};

/// <summary>
/// Controls Transforms of the objects in the scene
/// </summary>
class gui_objects_window : public action
{
private:
  bool* m_open = nullptr;

  std::vector<std::shared_ptr<node>>* m_nodes;
  std::mutex* m_nodes_mutex;

  //the tool will not show nor let you modify from this index onwards
  const unsigned m_first_forbiden_node; 

public:
  gui_objects_window(bool* open, std::vector<std::shared_ptr<node>>* nodes,
    std::mutex* nodes_mutex, unsigned first_forbiden_node)
    : m_open(open), m_nodes(nodes), m_nodes_mutex(nodes_mutex),
      m_first_forbiden_node(first_forbiden_node)
  {}
  void do_action() const override;
};






