/*
  This shader is used to project a decal onto the scene.
  It uses an OBB projector and writes into the G-buffer
*/

#version 400 core

layout(location = 0) in vec3 aPos;
layout(location = 1) in vec3 aNormal;
layout(location = 2) in vec3 aTangent;
layout(location = 3) in vec3 aBitangent;
layout(location = 4) in vec2 aTexCoord;

out vec2 TexCoord_vs;

uniform mat4 u_modelToWorld;
uniform mat4 u_worldToCam;
uniform mat4 u_camToVP;

void main()
{
  mat4 toCam = u_worldToCam*u_modelToWorld;
  vec4 PosCamSpace_vs = toCam*vec4(aPos, 1.0);
  gl_Position = u_camToVP*PosCamSpace_vs;
  TexCoord_vs = aTexCoord;
}