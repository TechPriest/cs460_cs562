#version 130

in vec3 aPos;
in vec3 aNormal;
in vec3 aTangent;
in vec3 aBitangent;
in vec2 aTexCoord;

///out vec2 v_texCoords;
out vec3 L; //lightsource position in Cam space

uniform mat4 u_modelToWorld;
uniform mat4 u_worldToCam;
uniform mat4 u_camToVP;
uniform vec3 u_LightPos; //lightsource position in world space

void main()
{
	gl_Position = u_camToVP*u_worldToCam*u_modelToWorld*vec4(aPos, 1.0);

  ///works but a bit wierd
  ///v_texCoords = (gl_Position.xy / gl_Position.w) * (0.5) + vec2(0.5);
  
  L = vec3(u_worldToCam*vec4(u_LightPos, 1.0));
}