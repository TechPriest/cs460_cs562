/*

Outputs the pixels that have a luminance above the limit

Lerps if pixel is above limit but below limit + margin
Else it just outputs the pixel
*/

#version 130

in vec3 attr_position;
in vec2 texCoords;

out vec2 v_texCoords;

void main()
{
	gl_Position = vec4(attr_position, 1.0f);

  v_texCoords = texCoords;
}