/*

Same as Gbuffer_4 but adds phong tesselation
to increase the amount of geometry of the models

Form the paper: Phong Tessellation by Tamy Boubekeur
and Marc Alexa (TU Berlin)

*/

#version 400 core


layout(triangles, fractional_odd_spacing) in;

in vec2 TexCoord_tcs[];
in vec3 T_tcs[];
in vec3 B_tcs[];
in vec3 N_tcs[];

in vec4 proj_sum[];

in float in_lvl[];

out vec2 TexCoord_tese;
out vec3 T_tese;
out vec3 B_tese;
out vec3 N_tese;

//out float outer_lvl_tese;
out float inner_lvl_tese;

uniform mat4 u_camToVP;

void main()
{
  //for brevity
  float u = gl_TessCoord[0];
  float v = gl_TessCoord[1];
  float w = gl_TessCoord[2];

  vec4 p0 = gl_in[0].gl_Position;
  vec4 p1 = gl_in[1].gl_Position;
  vec4 p2 = gl_in[2].gl_Position;

  //phong tesselation
  gl_Position = u_camToVP*(u*u*p0 + v*v*p1 + w*w*p2 + 
    u*v*proj_sum[0] + v*w*proj_sum[1] + w*u*proj_sum[2]);

  //usual stuff
  //gl_Position = u*p0 + v*p1 + w*p2;
  TexCoord_tese = u*TexCoord_tcs[0] + v*TexCoord_tcs[1] + w*TexCoord_tcs[2];
  T_tese = normalize(u*T_tcs[0] + v*T_tcs[1] + w*T_tcs[2]);
  B_tese = normalize(u*B_tcs[0] + v*B_tcs[1] + w*B_tcs[2]);
  N_tese = normalize(u*N_tcs[0] + v*N_tcs[1] + w*N_tcs[2]);

  inner_lvl_tese = u*in_lvl[0] + v*in_lvl[1] + w*in_lvl[2];
}