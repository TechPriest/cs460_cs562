/*

Changes the contrast so that the red channel becomes
0.0 when it was u_min and 1.0 when it was u_max.

Usefull to visualize the depth buffer for example.

*/

#version 130

in vec3 attr_position;
in vec2 texCoords;

out vec2 v_texCoords;

void main()
{
	gl_Position = vec4(attr_position, 1.0f);

  v_texCoords = texCoords;
}