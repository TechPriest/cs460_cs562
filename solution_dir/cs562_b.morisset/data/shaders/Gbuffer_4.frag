/*

This shader considers ka = kd.

gbuffer of 6+4+4 = 14 bytes (not considering depth)

*/


#version 400 core

layout (location = 0) out vec3 normsOutput;
layout (location = 1) out vec4 diffuseOutput; //last is specular grayscale
layout (location = 2) out vec4 emisiveOutput; //last is shininess

in vec2 TexCoord_vs; 
in vec3 T_vs;
in vec3 B_vs;
in vec3 N_vs;

uniform sampler2D u_texture_d;
uniform sampler2D u_texture_s;
uniform sampler2D u_texture_e;
uniform sampler2D u_texture_n;
uniform vec3 u_Kd;
uniform vec3 u_Ks;
uniform vec3 u_Ke;
uniform float u_ns;
 
void main() 
{
  //normalize columns again to avoid degeneration due to interpolation
  mat3 TanToCam = mat3(normalize(T_vs), normalize(B_vs), normalize(N_vs));

  vec3 normal_tanspace = 2.0 * texture(u_texture_n, TexCoord_vs).xyz - 1.0;
  vec3 normal = TanToCam * normal_tanspace;
  
  vec3 diffuse = u_Kd * texture(u_texture_d, TexCoord_vs).rgb;
  vec3 specular = u_Ks * texture(u_texture_s, TexCoord_vs).rgb;
  vec3 emmisive = u_Ke * texture(u_texture_e, TexCoord_vs).rgb;
  
  float ns = u_ns / (4.0 * 255.0);//so 1.0 corresponds to around 1k
  
  // Store data in the G-Buffer
  normsOutput = normal;
  diffuseOutput = vec4(diffuse, specular.r);
  emisiveOutput = vec4(emmisive, ns);
}