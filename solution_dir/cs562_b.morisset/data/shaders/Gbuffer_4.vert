/*

This shader considers ka = kd.

gbuffer of 6+4+4 = 14 bytes (not considering depth)

*/

#version 400 core

layout(location = 0) in vec3 aPos;
layout(location = 1) in vec3 aNormal;
layout(location = 2) in vec3 aTangent;
layout(location = 3) in vec3 aBitangent;
layout(location = 4) in vec2 aTexCoord;

out vec2 TexCoord_vs;
//out vec4 PosCamSpace_vs; //position in camera space
out vec3 T_vs;
out vec3 B_vs;
out vec3 N_vs;

uniform mat4 u_modelToWorld;
uniform mat4 u_worldToCam;
uniform mat4 u_camToVP;

void main()
{
  mat4 toCam = u_worldToCam*u_modelToWorld;
  vec4 PosCamSpace_vs = toCam*vec4(aPos, 1.0);
  gl_Position = u_camToVP*PosCamSpace_vs;
  TexCoord_vs = aTexCoord;
  
  T_vs = normalize(vec3(toCam * vec4(aTangent, 0.0)));
  B_vs = normalize(vec3(toCam * vec4(aBitangent, 0.0))); //or B = cross(N, T);
  N_vs = normalize(vec3(toCam * vec4(aNormal, 0.0)));
}