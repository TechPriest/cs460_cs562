/*
  Sobel filter using depth
*/

#version 130

out vec4 out_color;

uniform sampler2D u_depth_texture;
uniform vec2 u_tex_resolution;
uniform float u_factor;

void main()
{
  vec2 tex_coord = gl_FragCoord.xy / u_tex_resolution;
  vec2 onePix =  vec2(1.0,1.0) / u_tex_resolution;
  
  vec2 offset00 = onePix * vec2(-1.0,-1.0);
  vec2 offset01 = onePix * vec2(-1.0,0.0);
  vec2 offset02 = onePix * vec2(-1.0,1.0);
  
  vec2 offset10 = onePix * vec2(0.0,-1.0);
  vec2 offset11 = onePix * vec2(0.0,0.0);
  vec2 offset12 = onePix * vec2(0.0,1.0);
   
  vec2 offset20 = onePix * vec2(1.0,-1.0);
  vec2 offset21 = onePix * vec2(1.0,0.0);
  vec2 offset22 = onePix * vec2(1.0,1.0);
  

	float r00 = texture(u_depth_texture, tex_coord + offset00).r;
  float r01 = texture(u_depth_texture, tex_coord + offset01).r;
  float r02 = texture(u_depth_texture, tex_coord + offset02).r;
  
  float r10 = texture(u_depth_texture, tex_coord + offset10).r;
  float r11 = texture(u_depth_texture, tex_coord + offset11).r;
  float r12 = texture(u_depth_texture, tex_coord + offset12).r;
  
  float r20 = texture(u_depth_texture, tex_coord + offset20).r;
  float r21 = texture(u_depth_texture, tex_coord + offset21).r;
  float r22 = texture(u_depth_texture, tex_coord + offset22).r;
  
  float dx = 1.0*r00 + 0.0*r01 - 1.0*r02 +
              2.0*r10 + 0.0*r11 - 2.0*r12 +
              1.0*r20 + 0.0*r21 - 1.0*r22;
              
  float dy = 1.0*r00 + 2.0*r01 + 1.0*r02 +
              0.0*r10 + 0.0*r11 + 0.0*r12 +
             -1.0*r20 - 2.0*r21 - 1.0*r22;
  
  float d = u_factor * r11*r11 *(abs(dx) + abs(dy));
  //sqrt(dx*dx + dy*dy) may give slightly better results
  
  out_color = vec4(d);
}

