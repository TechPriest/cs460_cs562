/*
  This shader is used to project a decal onto the scene.
  It uses an OBB projector and writes into the G-buffer

  Details:
    - position of the screen fragment is deduced from its depth
    - the fragment position is then converted into projector space
    - the Tangent (T) and Bitangent (B) of the fragment are deduced from
  the depth derivatives on x and y
    - the normal fragment is computed by doing T x B
    - the decal normal is converted into camera space by doing
  TBN * decal_normal_from_texture

  Possible improvements:
    - instead of discarding for small alphas, perform blending (need to
  copy diffuse and normal and pass copies as uniform textures)
*/


#version 400 core

layout (location = 0) out vec3 normsOutput;
layout (location = 1) out vec4 diffuseOutput; //last is specular grayscale

in vec2 TexCoord_vs; 

uniform sampler2D u_depth;
uniform sampler2D u_texture_d;
uniform sampler2D u_texture_n;

uniform mat4 u_worldToModel;
uniform mat4 u_camToWorld;
uniform mat4 u_invCamToVP;
uniform mat4 u_modelToWorld;
uniform mat4 u_worldToCam;

uniform float u_dot_lim;

//compute the position in cam space from the depth and the screen uvs
vec4 cam_space_pos(vec2 screen_uvs, float depth)
{
  float z = depth * 2.0 - 1.0;
  vec4 clipSpacePos = vec4(screen_uvs * 2.0 - 1.0, z, 1.0);
  vec4 camSpacePos = u_invCamToVP * clipSpacePos;
  camSpacePos /= camSpacePos.w;
  
  return camSpacePos;
}
 
void main()
{
  //GET THE POSITION IN PROJECTOR SPACE
  vec2 resolution = textureSize(u_depth, 0);
  vec2 screen_uvs = gl_FragCoord.xy/resolution.xy;

  float depth = texture(u_depth, screen_uvs).r;
  vec4 camSpacePos = cam_space_pos(screen_uvs, depth);
  
  vec4 worldSpacePos =  u_camToWorld * camSpacePos;
  vec4 projectorSpacePos = u_worldToModel * worldSpacePos;
  
  
  //DISCARD OUT OF PROJECTOR BOX FRAGMENTS
  if( -0.5 > projectorSpacePos.x || projectorSpacePos.x > 0.5 || 
      -0.5 > projectorSpacePos.y || projectorSpacePos.y > 0.5 ||
      -0.5 > projectorSpacePos.z || projectorSpacePos.z > 0.5)
    discard;


  //GET DECAL DIFFUSE
  vec2 proj_uvs = vec2(0.5) + projectorSpacePos.xy;
  proj_uvs.y = 1.0-proj_uvs.y;
  vec4 diff_a = texture(u_texture_d, proj_uvs);
  vec3 diffuse = diff_a.rgb;
  float alpha = diff_a.a;


  //DISCARD IF ALPHA 0
  if(alpha < 0.95)
    discard;
  
  
  //GET DECAL NORMAL AND CONVERT IT TO CAM SPACE
  vec3 normal_tanspace = 2.0 * texture(u_texture_n, proj_uvs).xyz - 1.0;

  vec3 T_geo = normalize(dFdx(camSpacePos).xyz);
  vec3 B_geo = normalize(dFdy(camSpacePos).xyz);
  vec3 N_geo = -normalize(cross(T_geo,B_geo));
  mat3 ToCam = mat3(T_geo, B_geo, N_geo);
  vec3 normal = -ToCam * normal_tanspace;
  

  //DISCARD IF OUTSIDE ANGLE LIMIT
  vec4 cam_space_projector_dir =  normalize(u_worldToCam * u_modelToWorld * vec4(0,0,1,0));
  float current_dot = dot(N_geo, cam_space_projector_dir.xyz);
  if(current_dot < u_dot_lim)
    discard;
  

  //STORE DECAL DIFFUSE AND NORMAL IN THE G-BUFFER
  normsOutput = normal;
  diffuseOutput = vec4(diffuse, 1.0);
}