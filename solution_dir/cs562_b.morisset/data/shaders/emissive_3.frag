/*

just add emissive

*/

#version 130

in vec2 v_texCoords;

out vec4 out_color;


//g_buffer
uniform sampler2D u_emissive; //last is free

void main()
{
  vec3 emissive = texture(u_emissive, v_texCoords).rgb;
  

    
  //float use_emissive =
  //  emissive.r + emissive.g + emissive.b;
  //  
  //if(use_emissive < 0.1f)
  //  discard;
  //else
  //  out_color =  vec4(emissive, 1.0f);


  float alpha =
    clamp(256.0*(emissive.r + emissive.g + emissive.b), 0.0, 1.0);
    
  out_color =  vec4(emissive, alpha);
}

