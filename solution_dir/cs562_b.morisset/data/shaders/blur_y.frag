#version 330

const int max_blur = 32;

out vec4 out_color;

uniform sampler2D u_texture;
uniform vec2 u_tex_resolution;
uniform int u_dim; //up to max_blur
uniform float u_weights[max_blur + 1]; //with symetry must sum 1.0

void main()
{
  vec2 tex_coord = gl_FragCoord.xy / u_tex_resolution;
  vec2 onePix =  vec2(1.0,1.0) / u_tex_resolution;
  
  for (int i = -u_dim; i <= u_dim; i++)
  {
    vec2 offset = onePix * vec2(0.0,i);
    
    vec4 color_i = texture(u_texture, tex_coord + offset);
    
    out_color +=  u_weights[abs(i)] * color_i;
  }
}

