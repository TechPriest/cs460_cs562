/*

Changes the contrast so that the red channel becomes
0.0 when it was u_min and 1.0 when it was u_max.

Usefull to visualize the depth buffer for example.

*/

#version 130

in vec2 v_texCoords;

out vec4 out_color;

uniform sampler2D u_texture;
uniform float u_min;
uniform float u_max;

void main()
{
	float r = texture(u_texture, v_texCoords).r;
  
  r = (r-u_min) / (u_max-u_min);
  
  out_color = vec4(r,r,r,1.0);
}

