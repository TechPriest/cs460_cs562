/*
Computes the occlusion value of each pixel based
on the depth (Horizon-Based Ambient Occlusion)

sintead of linearizing the angle, works directly
with the dot product as the occlusion

operation gain: -1 acos, -1 division, -1 substraction

to further optimize cam_space_pos look at:
https://www.khronos.org/opengl/wiki/Compute_eye_space_from_window_space
*/

#version 130

in vec2 v_texCoords;

out vec4 out_color;

uniform sampler2D u_texture; //must be depth texture
uniform sampler2D u_noise_tex; //to rotate the directions randomely (white noise is recomended)
uniform mat4 u_invCamToVP;
uniform mat4 u_CamToVP;
uniform vec2 u_dirs[16];  //in screen space (assumes normalized)
uniform int u_dir_count;  //cannot be more than u_dirs size
uniform float u_att;      //attenuation, so that furthest samples contribute less to the occlusion
uniform float u_scale;    
uniform float u_radius;   //in cam space
uniform int u_step_count;
uniform float u_angle_lim;//angle between normal and direction
                          //to step pos from which to consider
                          //the occlusion

//compute the position in cam space from the depth and the screen uvs
vec4 cam_space_pos(vec2 screen_uvs, float depth)
{
  float z = depth * 2.0 - 1.0;
  vec4 clipSpacePos = vec4(screen_uvs * 2.0 - 1.0, z, 1.0);
  vec4 camSpacePos = u_invCamToVP * clipSpacePos;
  camSpacePos /= camSpacePos.w;
  
  return camSpacePos;
}

//returns screen uvs from cam_space_pos
vec2 cam_space_pos_to_uvs(vec4 cam_space_pos)
{
  vec4 clip_pos = u_CamToVP * cam_space_pos;
  clip_pos /= clip_pos.w;

  return vec2(0.5 + 0.5*clip_pos.x, 0.5 + 0.5*clip_pos.y);
}

void main()
{
  const float pi = 3.14159265359;
  float u_r2 = u_radius*u_radius;

  float step_size = u_radius / u_step_count;

  float depth = texture(u_texture, v_texCoords).r;

  /* compute a noise factor to avoid using interpolated
  values for the noise texture if it is smaller */
  vec2 depth_res = textureSize(u_texture, 0);
  vec2 noise_res = textureSize(u_noise_tex, 0);
  vec2 noise_tex_factor = depth_res / noise_res;
  float rand = texture(u_noise_tex, noise_tex_factor*v_texCoords).r;

  float rot = ((2.0 * rand) - 1.0) * pi;
  float cos_rot = cos(rot);
  float sin_rot = sin(rot);

  vec4 pos = cam_space_pos(v_texCoords, depth);
  float min_dot = cos(u_angle_lim);

  vec3 T_geo = normalize(dFdx(pos).xyz);
  vec3 B_geo = normalize(dFdy(pos).xyz);
  vec3 N_geo = normalize(cross(T_geo,B_geo));

  float occlusion = 0.0;

  //find the occlusion along each direction
  for(int dir_i = 0; dir_i < u_dir_count; dir_i++)
  {
    //covert dir to cam space
    vec2 dir = u_dirs[dir_i];
    vec2 rotated_dir = vec2(cos_rot*dir.x - sin_rot*dir.y, sin_rot*dir.x + cos_rot*dir.y);
    vec4 step_dir =
      cam_space_pos(v_texCoords + rotated_dir, depth) -
      cam_space_pos(v_texCoords, depth);
    step_dir.w = 0.0;
    step_dir = normalize(step_dir);

    vec4 step_pos_init = pos;
    float max_dot_i = min_dot;

    //find the maximum dot product along this direction
    for(int step_i = 0; step_i < u_step_count; step_i++)
    {
      step_pos_init += step_size*step_dir;

      //find the uvs and the depth at the step position
      vec2 step_uvs = cam_space_pos_to_uvs(step_pos_init);
      float step_depth = texture(u_texture, step_uvs).r;

      //find the stap position in camera space
      vec4 step_pos = cam_space_pos(step_uvs, step_depth);

      /* compute the dot product of the direction from the
      center to the step position with the normal */
      vec3 v = step_pos.xyz - pos.xyz;
      float r2 = v.x*v.x + v.y*v.y + v.z*v.z;
      vec3 v_dirf = normalize(v);
      float v_dot = (1.0 - u_att*r2/u_r2) * dot(N_geo, v_dirf);

      //update max_dot_i if this is a new maximum
      if(v_dot > max_dot_i && r2 < u_r2)
        max_dot_i = v_dot;
    }

    //compute occlusion base on max_dot_i
    if(max_dot_i <= min_dot)
      occlusion += 0.0;
    else
      occlusion += max_dot_i; //this works well enough
  }
  
  occlusion = clamp(u_scale*occlusion, 0.0, 1.0);
  //finish the occlusion average and display it
  occlusion *= (1.0/float(u_dir_count));
  out_color = vec4(vec3(1.0-occlusion), 1.0);
}

