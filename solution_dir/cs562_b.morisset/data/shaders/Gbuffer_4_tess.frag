/*

Same as Gbuffer_4 but adds phong tesselation
to increase the amount of geometry of the models

Form the paper: Phong Tessellation by Tamy Boubekeur
and Marc Alexa (TU Berlin)

*/


#version 400 core

layout (location = 0) out vec3 normsOutput;
layout (location = 1) out vec4 diffuseOutput; //last is specular grayscale
layout (location = 2) out vec4 emisiveOutput; //last is shininess

in vec2 TexCoord_tese; 
in vec3 T_tese;
in vec3 B_tese;
in vec3 N_tese;

in float inner_lvl_tese;

uniform sampler2D u_texture_d;
uniform sampler2D u_texture_s;
uniform sampler2D u_texture_e;
uniform sampler2D u_texture_n;
uniform vec3 u_Kd;
uniform vec3 u_Ks;
uniform vec3 u_Ke;
uniform float u_ns;

uniform bool u_heatmap;

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}
 
void main() 
{
  //normalize columns again to avoid degeneration due to interpolation
  mat3 TanToCam = mat3(normalize(T_tese), normalize(B_tese), normalize(N_tese));

  vec3 normal_tanspace = 2.0 * texture(u_texture_n, TexCoord_tese).xyz - 1.0;
  vec3 normal = TanToCam * normal_tanspace;
  
  vec3 diffuse = u_Kd * texture(u_texture_d, TexCoord_tese).rgb;
  vec3 specular = u_Ks * texture(u_texture_s, TexCoord_tese).rgb;
  vec3 emmisive = u_Ke * texture(u_texture_e, TexCoord_tese).rgb;
  
  float ns = u_ns / (4.0 * 255.0);//so 1.0 corresponds to around 1k
  
  // Store data in the G-Buffer
  normsOutput = normal;
  diffuseOutput = vec4(diffuse, specular.r);
  emisiveOutput = vec4(emmisive, ns);

  if(u_heatmap)
  {
    //to make blue be the lowest and red the highest
    float h = 0.5 - 0.5*inner_lvl_tese;

    vec3 rgb = hsv2rgb(vec3(h,1.0,1.0));
  
    emisiveOutput = vec4(rgb, ns);
  }
}