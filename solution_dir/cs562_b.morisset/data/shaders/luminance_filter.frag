/*

Outputs the pixels that have a luminance above the limit

Lerps if pixel is above limit but below limit + margin
Else it just outputs the pixel
*/

#version 130

in vec2 v_texCoords;

out vec4 out_color;

uniform sampler2D u_texture;
uniform float u_luminance_lim;
uniform float u_lumi_lerp_margin;

void main()
{
  vec4 in_color = texture(u_texture, v_texCoords);
  
  float luminance = dot(in_color.rgb, vec3(0.2126, 0.7152, 0.0722));
  
  
  if(luminance > u_luminance_lim + u_lumi_lerp_margin)
    out_color = in_color;
  else if(luminance > u_luminance_lim)
  {
    float lerp = (luminance - u_luminance_lim) / u_lumi_lerp_margin;
    
    out_color = lerp*in_color;
  }
  else
    out_color = vec4(0);
}

