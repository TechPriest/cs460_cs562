/*
Tesselation Control Shaders are responsible for outputing the control points
for the Tesselation Evaluation Shader and to specify the inner and outer
tesselation levels.

Same as Gbuffer_4 but adds phong tesselation
to increase the amount of geometry of the models

Form the paper: Phong Tessellation by Tamy Boubekeur
and Marc Alexa (TU Berlin)

*/

#version 400 core

layout(vertices=3) out;

in vec2 TexCoord_vs[];
in vec3 T_vs[];
in vec3 B_vs[];
in vec3 N_vs[];

out vec2 TexCoord_tcs[];
out vec3 T_tcs[];
out vec3 B_tcs[];
out vec3 N_tcs[];

out vec4 proj_sum[];
out float in_lvl[];

uniform float u_max_tess_lvl;
uniform bool u_view_adaptive;
uniform bool u_dist_adaptive;
uniform float u_dist_factor;
uniform float u_dist_cnt;

vec4 proj(vec4 pt, vec4 plane_norm, vec4 plane_pt)
{
  float dist = dot((pt-plane_pt), plane_norm);

  return pt - dist * plane_norm;
}

void main()
{
  //indices
  int i = gl_InvocationID;
  int j = (i + 1)%3;
  int k = (i + 2)%3;

  //tesselation levels
  {
    float inner = 0.0f;
    float outer = 0.0f;

    vec3 p_i = gl_in[i].gl_Position.xyz;
    vec3 p_j = gl_in[j].gl_Position.xyz;
    vec3 p_k = gl_in[k].gl_Position.xyz;

    if(u_view_adaptive)
    {
      vec3 n_i = normalize(N_vs[i]);
      vec3 n_j = normalize(N_vs[j]);
      vec3 n_k = normalize(N_vs[k]);
      float d_i = u_max_tess_lvl * (1.0 - dot(n_i, normalize(-p_i)));
      float d_j = u_max_tess_lvl * (1.0 - dot(n_j, normalize(-p_j)));
      float d_k = u_max_tess_lvl * (1.0 - dot(n_k, normalize(-p_k)));
      d_i = clamp(d_i, 1.0, u_max_tess_lvl);
      d_j = clamp(d_j, 1.0, u_max_tess_lvl);
      d_k = clamp(d_k, 1.0, u_max_tess_lvl); //clamping neededbacause edge triangles may give a negative dot product

      float par = (d_j + d_k);
      outer = par/2.0;
      inner = (d_i + par)/3.0;
    }
    else
    {
      outer = u_max_tess_lvl;
      inner = u_max_tess_lvl;
    }

    if(u_dist_adaptive)
    {
      float dist_i = u_dist_factor * max(length(p_i) - u_dist_cnt, 0.0);
      float dist_j = u_dist_factor * max(length(p_j) - u_dist_cnt, 0.0);
      float dist_k = u_dist_factor * max(length(p_k) - u_dist_cnt, 0.0);
      float mod_i = min(1.0/dist_i, 1.0);
      float mod_j = min(1.0/dist_j, 1.0);
      float mod_k = min(1.0/dist_k, 1.0);

      float par = (mod_j + mod_k);
      outer *= par/2.0;
      inner *= (mod_i + par)/3.0;
    }

    in_lvl[gl_InvocationID] = inner/ u_max_tess_lvl;

    gl_TessLevelOuter[gl_InvocationID] = outer;
    gl_TessLevelInner[0] = inner;
  }

  //projections for phong tesselation  
  proj_sum[gl_InvocationID] =
    proj(gl_in[j].gl_Position, vec4(N_vs[i], 0.0), gl_in[i].gl_Position) +
    proj(gl_in[i].gl_Position, vec4(N_vs[j], 0.0), gl_in[j].gl_Position);

  //regular stuff
  gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
  
  TexCoord_tcs[gl_InvocationID] = TexCoord_vs[gl_InvocationID];
  T_tcs[gl_InvocationID] = T_vs[gl_InvocationID];
  B_tcs[gl_InvocationID] = B_vs[gl_InvocationID];
  N_tcs[gl_InvocationID] = N_vs[gl_InvocationID];
}
