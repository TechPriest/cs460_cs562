/*

Performs Blin-Phong lighting for a light given:
  - g_buffer textures
  - ambient occlusion texture
  - light variables
  - light position

Call this for a screen quad for each light

Bugs: Attenuation is wrong. It shouldn't depend on cam position.

Possible improvements:
  - pass all uniforms into a UniformBufferObject
*/

#version 130

in vec3 attr_position;
in vec2 texCoords;

out vec2 v_texCoords;
out vec3 L; //lightsource position in Cam space

uniform mat4 u_worldToCam;
uniform vec3 u_LightPos; //lightsource position in world space

void main()
{
  gl_Position = vec4(attr_position, 1.0f);

  v_texCoords = texCoords;
  
  L = vec3(u_worldToCam*vec4(u_LightPos, 1.0));
}