/*
Computes the occlusion value of each pixel based
on the depth (Screen Space Horizon Based Ambient Occlusion)

Bugs:
  tangent_in_dir is wrong

*/

#version 130

in vec2 v_texCoords;

out vec4 out_color;

uniform sampler2D u_texture; //must be depth texture
uniform sampler2D u_noise_tex; //to rotate the directions randomely (white noise is recomended)
uniform mat4 u_invCamToVP;
uniform mat4 u_CamToVP;
uniform vec2 u_dirs[16];  //in screen space (assumes normalized)
uniform int u_dir_count;  //cannot be more than u_dirs size
uniform float u_att;      //attenuation, so that furthest samples contribute less to the occlusion
uniform float u_scale;    
uniform float u_radius;   //in cam space
uniform int u_step_count;
uniform float u_angle_lim;//angle between normal and direction
                          //to step pos from which to consider
                          //the occlusion

//compute the position in cam space from the depth and the screen uvs
vec4 cam_space_pos(vec2 screen_uvs, float depth)
{
  float z = depth * 2.0 - 1.0;
  vec4 clipSpacePos = vec4(screen_uvs * 2.0 - 1.0, z, 1.0);
  vec4 camSpacePos = u_invCamToVP * clipSpacePos;
  camSpacePos /= camSpacePos.w;
  
  return camSpacePos;
}

//returns screen uvs from cam_space_pos
vec2 cam_space_pos_to_uvs(vec4 cam_space_pos)
{
  vec4 clip_pos = u_CamToVP * cam_space_pos;
  clip_pos /= clip_pos.w;
  
  return vec2(0.5 + 0.5*clip_pos.x, 0.5 + 0.5*clip_pos.y);
}

//rot in radians
vec2 rotate(vec2 v, float rot)
{
  float cos_rot = cos(rot);
  float sin_rot = sin(rot);

  return vec2(
    cos_rot*v.x - sin_rot*v.y,
    sin_rot*v.x + cos_rot*v.y);
}

vec3 plane_proj(vec3 plane_N, vec3 v)
{
  return v - (dot(v, plane_N))*plane_N;
}

vec3 plan_proj_along_d(vec3 plane_N, vec3 d, vec3 v)
{
  float t = dot(plane_N,v)/dot(plane_N,d);
  return v + t*d;
}

void main()
{
  const float half_pi = 1.57079632679;
  const float pi = 3.14159265359;
  float u_r2 = u_radius*u_radius;

  float step_size = u_radius / u_step_count;

  float depth = texture(u_texture, v_texCoords).r;

  /* compute a noise factor to avoid using interpolated
  values for the noise texture if it is smaller */
  vec2 depth_res = textureSize(u_texture, 0);
  vec2 noise_res = textureSize(u_noise_tex, 0);
  vec2 noise_tex_factor = depth_res / noise_res;
  float rand = texture(u_noise_tex, noise_tex_factor*v_texCoords).r;

  float rot = ((2.0 * rand) - 1.0) * pi;
  float cos_rot = cos(rot);
  float sin_rot = sin(rot);

  vec4 pos = cam_space_pos(v_texCoords, depth);

  vec3 T_geo = normalize(dFdx(pos).xyz);
  vec3 B_geo = normalize(dFdy(pos).xyz);
  vec3 N_geo = normalize(cross(T_geo,B_geo));

  float occlusion;

  //find the occlusion along each direction
  for(int dir_i = 0; dir_i < u_dir_count; dir_i++)
  {
    //covert dir to cam space
    vec2 dir = u_dirs[dir_i];
    vec2 rotated_dir = vec2(cos_rot*dir.x - sin_rot*dir.y, sin_rot*dir.x + cos_rot*dir.y);
    vec4 step_dir =
      cam_space_pos(v_texCoords + rotated_dir, depth) -
      cam_space_pos(v_texCoords, depth);
    step_dir.w = 0.0;
    step_dir = normalize(step_dir);

    //compute t
    vec4 f_4 = 
      cam_space_pos(v_texCoords, depth + 0.2) -
      cam_space_pos(v_texCoords, depth);
    vec3 f = normalize(f_4.xyz);
    f.x = step_dir.z;
    f.y = 0.0;
    f.z = -step_dir.x;
    vec3 tangent_in_dir = plan_proj_along_d(N_geo, f, step_dir.xyz);
    tangent_in_dir = normalize(tangent_in_dir);
    tangent_in_dir.z = -tangent_in_dir.z;

    float t = atan(tangent_in_dir.z / length(tangent_in_dir.xy));
    t += half_pi - u_angle_lim;

    //find the maximum alpha product along this direction
    float max_alpha = 0.0;
    vec4 sample_pos = pos;
    for(int sample_i = 0; sample_i < u_step_count; sample_i++)
    {
      sample_pos += step_size*step_dir;

      //find the uvs and the depth at the step position
      vec2 sample_uvs = cam_space_pos_to_uvs(sample_pos);
      float sample_depth = texture(u_texture, sample_uvs).r;

      //find the step position in camera space
      vec4 sample_pos = cam_space_pos(sample_uvs, sample_depth);

      /* compute alpha for this step */
      vec3 D = sample_pos.xyz - pos.xyz;
      float r2 = dot(D,D);
      float alpha = atan((-D.z)/length(D.xy)); 
      alpha *= u_att * (1.0 - r2/u_r2);
      //update max_alpha if step alpha is greater
      if(alpha > max_alpha && r2 < u_r2)
        max_alpha = alpha;
    }

    //this ensures occlusion is zero when surface is convex
    float h = max(max_alpha, t);

    occlusion += sin(h) - sin(t);
  }
  
  occlusion = clamp(u_scale*occlusion, 0.0, 1.0);
  //finish the occlusion average and display it
  occlusion *= 1.0/float(u_dir_count);
  out_color = vec4(vec3(1.0-occlusion), 1.0);
}

