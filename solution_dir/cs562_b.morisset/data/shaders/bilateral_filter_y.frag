/*
Blurs the pixels doing a weighted average of a square 
of neighboring NxN pixels. Uses the pixel distance and
the intensity(depth) difference to get the weights.
*/

#version 130

in vec2 v_texCoords;

out vec4 out_color;

uniform sampler2D u_texture;
uniform sampler2D u_intensity_tex;
uniform int u_N; //cannot be more than MAX_KERNEL_SIZE
uniform float u_dist_sigma; //10.0 is a good value
uniform float u_intensity_sigma; // 0.1 is a good value

const int MAX_KERNEL_SIZE = 16;

float gauss_weigth(float d, float sigma)
{
  const float one_over_sqrt_2_pi = 0.39894; // 1/sqrt(2*pi)

	return one_over_sqrt_2_pi *
    exp(-0.5*d*d/(sigma*sigma))/sigma;
}

void main()
{
  out_color = vec4(0);

  vec2 onePix =  vec2(1.0,1.0) / textureSize(u_texture, 0);;

  float depth = texture(u_intensity_tex, v_texCoords).r;  

  float dist_kernel[MAX_KERNEL_SIZE];

  /* create the dist kernel
  (only half of it, as the other halfis simetric) */
	for (int i = 0; i <= u_N; i++)
		dist_kernel[i] = gauss_weigth(float(i), u_dist_sigma);

  float total_weight = 0.0;

  /* iterate through each pixel in the square
  neighpourhood (size = (2*u_N+1)*(2*u_N+1)) */
  for (int i=-u_N; i <= u_N; ++i)
	{
    vec2 offset = onePix * vec2(0,i);
    vec4 color_i = texture(u_texture, v_texCoords + offset);
    float depth_i = texture(u_intensity_tex, v_texCoords + offset).r;  
      
    float weight = dist_kernel[abs(i)] *
      gauss_weigth(depth_i - depth, u_intensity_sigma);
    
    out_color +=  weight * color_i;

    total_weight += weight;
  }

  out_color /= total_weight;
}

