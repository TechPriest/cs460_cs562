/*

Performs Blin-Phong lighting for a light given:
  - g_buffer textures
  - ambient occlusion texture
  - light variables
  - light position

Call this for a screen quad for each light

Bugs: Attenuation is wrong. It shouldn't depend on cam position.

*/

#version 130

/*layout (location = 0)*/ out vec4 out_color;

in vec2 v_texCoords;
in vec3 L; //lightsource position in cam space

//
uniform mat4 u_invCamToVP;

//g_buffer
uniform sampler2D u_norms;      
uniform sampler2D u_diffuse;      //last is specular greyscale
uniform sampler2D u_emissive;     //last is shininess
uniform sampler2D u_depth;

//ambient occlusion
uniform sampler2D u_AO;

//light
uniform vec3 u_Ia;
uniform vec3 u_Id;
uniform vec3 u_Is;
uniform vec3 u_attenuation;


//compute the position in cam space from the depth and the screen uvs
vec4 cam_space_pos(vec2 screen_uvs, float depth)
{
  float z = depth * 2.0 - 1.0;
  vec4 clipSpacePos = vec4(screen_uvs * 2.0 - 1.0, z, 1.0);
  vec4 camSpacePos = u_invCamToVP * clipSpacePos;
  camSpacePos /= camSpacePos.w;
  
  return camSpacePos;
}


void main()
{
  //extract the data from the textures
  vec3 norm = texture(u_norms, v_texCoords).xyz; //in cam space
  vec4 kd_ks = texture(u_diffuse, v_texCoords);
  vec4 ke_ns = texture(u_emissive, v_texCoords);
  float depth = texture(u_depth, v_texCoords).r;
  
  float ao = texture(u_AO, v_texCoords).r;
  
  //reconstruct position
  vec3 pos = cam_space_pos(v_texCoords, depth).xyz;
  
  ////vec3 pos = pos_sns.xyz;
  vec3 kd = kd_ks.rgb;
  vec3 ka = kd;
  vec3 ks = vec3(kd_ks.a);
  float ns = ke_ns.a * 4.0 * 255.0; //should correcpond to gbuffer
  vec3 n = norm;
  
  //make the lighting computations
  vec3 l = normalize(pos - L);
  vec3 V = normalize(pos);
  vec3 H = (-l-V)*0.5;
  
  vec3 Iambient = ao * ka * u_Ia;
  vec3 Idiffuse = kd * u_Id * max( dot(n, -l), 0 );
  vec3 Ispecular = ks * u_Is * pow( max( dot(n, H), 0), ns);

  float d = length(pos - L);
  float att = min(1.0/(u_attenuation.x + u_attenuation.y *d + u_attenuation.z *d *d ), 1.0);
  out_color =  att * vec4(Iambient + Idiffuse + Ispecular, 1.0);
}

