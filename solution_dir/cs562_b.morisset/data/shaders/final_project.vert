/*
  Implementation based on following papers:

  "Suggestive Contours for Conveying Shape"
    by DECARLO, D., FINKELSTEIN, A., RUSINKIEWICZ, S., AND SANTELLA, A. 
    2003 ACM Transactions on Graphics (Proc. SIGGRAPH) 22, 3, 848� 855. 

  "Highlight Lines for Conveyong Shape"
    In NPAR 2007 by Doung DeCarlo from Rutgers University and
    Szymon Rusinkiewicz from Princeston University


  ASK:
    -bitwise operations

  TODO:
    -bitwise operations
    -principal highlight fix artifacts
    -add fade to prin_high, sugg_high, sugg_cont
*/

#version 130

in vec3 attr_position;
in vec2 texCoords;

out vec2 v_texCoords;

void main()
{
	gl_Position = vec4(attr_position, 1.0f);

  v_texCoords = texCoords;
}