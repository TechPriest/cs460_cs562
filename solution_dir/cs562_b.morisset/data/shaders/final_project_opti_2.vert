/*
  Implementation based on following papers:

  "Suggestive Contours for Conveying Shape"
    by DECARLO, D., FINKELSTEIN, A., RUSINKIEWICZ, S., AND SANTELLA, A. 
    2003 ACM Transactions on Graphics (Proc. SIGGRAPH) 22, 3, 848� 855. 

  "Highlight Lines for Conveyong Shape"
    In NPAR 2007 by Doung DeCarlo from Rutgers University and
    Szymon Rusinkiewicz from Princeston University

  This version has all the values hardcoded in an attempt to optimize by
  passing less uniforms. The setting should match a Roy Lichtenstein,
  �Golf Ball�'s style
*/

#version 130

in vec3 attr_position;
in vec2 texCoords;

out vec2 v_texCoords;

void main()
{
	gl_Position = vec4(attr_position, 1.0f);

  v_texCoords = texCoords;
}