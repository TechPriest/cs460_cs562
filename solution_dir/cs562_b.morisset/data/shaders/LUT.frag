/*

*/

#version 130

in vec2 v_texCoords;

out vec4 out_color;

uniform sampler3D u_LUT;
uniform sampler2D u_texture;

void main()
{
  //vec3 old_col = texture(u_texture, v_texCoords).rgb;
  //old_col.g = 1.0-old_col.g;
  //old_col.b = 1.0-old_col.b;
  //out_color = texture(u_LUT, old_col);

  
  vec2 res = textureSize(u_texture, 0);

  //if(res.x > 2000)
  //  discard;

  out_color = texture(u_LUT, vec3(v_texCoords, 0.0));

  vec2 uv = v_texCoords * vec2(res.x/res.y, 1.0);
  uv.x *= 8.0;
  uv.y *= 8.0;

  if(uv.x > 8.0)
  {
    out_color = vec4(0);
    return;
  }
  if(uv.y > 8.0)
  {
    out_color = vec4(0);
    return;
  }
    

  float frac_x = fract(uv.x);
  float n = floor(uv.x) / 8;

  //if(frac_y < 0.55 && frac_y > 0.45)
  //{
  //  out_color = vec4(0.5);
  //  return;
  //}

  vec3 uv3 = vec3(frac_x, uv.y, n);
	out_color = texture(u_LUT, uv3);
  
  
  out_color.a = 1.0f;
}

