#version 130

in vec2 v_texCoords;

out vec4 out_color;

uniform sampler2D u_texture;

void main()
{
	float alpha = texture(u_texture, v_texCoords).a;
  
  out_color = vec4(vec3(alpha), 1.0f);
}

