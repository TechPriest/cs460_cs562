#version 130

in vec3 attr_position;
in vec2 texCoords;

void main()
{
	gl_Position = vec4(attr_position, 1.0f);
}