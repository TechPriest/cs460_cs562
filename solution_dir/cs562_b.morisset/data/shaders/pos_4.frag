/*

Outputs the position in camera space.
It computes it from the depth.

*/

#version 130

in vec2 v_texCoords;

out vec4 out_color;

uniform sampler2D u_depth;

uniform mat4 u_invCamToVP;


//compute the position in cam space from the depth and the screen uvs
vec4 cam_space_pos(vec2 screen_uvs, float depth)
{
  float z = depth * 2.0 - 1.0;
  vec4 clipSpacePos = vec4(screen_uvs * 2.0 - 1.0, z, 1.0);
  vec4 camSpacePos = u_invCamToVP * clipSpacePos;
  camSpacePos /= camSpacePos.w;
  
  return camSpacePos;
}


void main()
{
  float depth = texture(u_depth, v_texCoords).r;
  
  if(depth == 1.0)
    discard;
  
  vec3 pos = cam_space_pos(v_texCoords, depth).xyz;
  
  out_color = vec4(pos, 1.0f);
}

