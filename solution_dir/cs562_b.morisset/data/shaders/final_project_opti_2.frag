/*
  Implementation based on following papers:

  "Suggestive Contours for Conveying Shape"
    by DECARLO, D., FINKELSTEIN, A., RUSINKIEWICZ, S., AND SANTELLA, A. 
    2003 ACM Transactions on Graphics (Proc. SIGGRAPH) 22, 3, 848� 855. 

  "Highlight Lines for Conveyong Shape"
    In NPAR 2007 by Doung DeCarlo from Rutgers University and
    Szymon Rusinkiewicz from Princeston University

  This version has all the values hardcoded in an attempt to optimize by
  passing less uniforms. The setting should match a Frank Miller type
  of style.

  POSSIBLE OPTIMIZATIONS:
    -if only using face normals, derive the normal from the depth
    with DFDX(cam_pos) etc via the normal and binormal (see decal.frag).
    Then remove the normal-buffer texture sampler.

  ASK:
    -bitwise operations
    -how to further optimize

  TODO:
    -bitwise operations
    -principal highlight fix artifacts
    -add fade to prin_high, sugg_high, sugg_cont
*/

#version 130

in vec2 v_texCoords;

out vec4 out_color;

uniform sampler2D u_norm_tex; //must be normal texture
uniform sampler2D u_depth_tex; //must be depth texture

uniform mat4 u_invCamToVP; //inverse projection matrix
uniform mat4 u_worldToCam; //view matrix


///////SETTINGS///////

const float u_background_col = 0.0;
const float u_base_col = 1.0;
const vec4 u_toon_dir = vec4(-1.0, 0.0, -1.0, 0.0);

//for both suggestive linetypes
const int u_neighboorhood_dim = 4;
const float u_max_z_diff = 15.0;

//principal countour vars:
const float u_prin_cont_lim = 0.75;
const float u_prin_cont_lim_margin = 0.0;//not needed if using suggestive contour
const float u_prin_cont_col = 0.0;

//suggestive contour vars:
const float u_sugg_cont_smaller_dot_allowed_proportion = 0.3;
const float u_sugg_cont_min_dot_diff = 0.15;
const float u_sugg_cont_col = 0.0;

//principal highlight vars:
const float u_prin_high_lim_1 = 0.861; //dot(n,v) above this value
const float u_prin_high_lim_2 = 0.2; //dot(e1,w) below this value 
const float u_prin_high_col = 1.0;

//suggestive highlight vars:
const float u_sugg_high_bigger_dot_allowed_proportion = 0.3;
const float u_sugg_high_min_dot_diff = 0.15;
const float u_sugg_high_col = 1.0;

/////END OF SETTINGS/////


/*
compute the position in cam space from the depth and the screen uvs
*/
vec4 cam_space_pos(vec2 screen_uvs, float depth)
{
  float z = depth * 2.0 - 1.0;
  vec4 clipSpacePos = vec4(screen_uvs * 2.0 - 1.0, z, 1.0);
  vec4 camSpacePos = u_invCamToVP * clipSpacePos;
  camSpacePos /= camSpacePos.w;
  
  return camSpacePos;
}

/*
compute the z component of the position in camera space from the depth
*/
float cam_z(float depth)
{
  float numerator = u_invCamToVP[3][2];
  float denominator = u_invCamToVP[2][3]*(2.0*depth  -1.0) + u_invCamToVP[3][3];

  return numerator/denominator;
}

/*
computes the radius of curvature from two points and two normals, all in a normal plane
*/
float radius_of_curvature(vec3 posA, vec3 posB, vec3 normA, vec3 normB)
{
  float theta = 0.5*acos(dot(normB, normA));
  float d = abs(length(posB - posA));
  return (0.5*d)/sin(theta);
}

/*
computes the radius of curvature from two points and two normals, all in a normal plane
alternative implementation. Must yield the exact same result
*/
float radius_of_curvature_alt(vec3 posA, vec3 posB, vec3 normA, vec3 normB)
{
  const float HALF_PI = 1.57079632679;

  float theta = 0.5*acos(dot(normB, normA));
  float alpha = HALF_PI - theta;
  float d = abs(length(posB - posA));
  return (0.5*d)/cos(alpha);
}


/*
finds the contour in the places where the z component in camera space chages quickly.
*/
bool principal_contour_sobel_cam_z(in vec2 tex_res, inout vec4 color)
{
  vec2 tex_coord = gl_FragCoord.xy / tex_res;
  vec2 onePix =  vec2(1.0,1.0) / tex_res;
  
  vec2 offset00 = onePix * vec2(-1.0,-1.0);
  vec2 offset01 = onePix * vec2(-1.0,0.0);
  vec2 offset02 = onePix * vec2(-1.0,1.0);
  
  vec2 offset10 = onePix * vec2(0.0,-1.0);
  vec2 offset11 = onePix * vec2(0.0,0.0);
  vec2 offset12 = onePix * vec2(0.0,1.0);
   
  vec2 offset20 = onePix * vec2(1.0,-1.0);
  vec2 offset21 = onePix * vec2(1.0,0.0);
  vec2 offset22 = onePix * vec2(1.0,1.0);
  

	float depth00 = texture(u_depth_tex, v_texCoords + offset00).r;
  float depth01 = texture(u_depth_tex, v_texCoords + offset01).r;
  float depth02 = texture(u_depth_tex, v_texCoords + offset02).r; 
  float depth10 = texture(u_depth_tex, v_texCoords + offset10).r;
  float depth11 = texture(u_depth_tex, v_texCoords + offset10).r;
  float depth12 = texture(u_depth_tex, v_texCoords + offset12).r;
  float depth20 = texture(u_depth_tex, v_texCoords + offset20).r;
  float depth21 = texture(u_depth_tex, v_texCoords + offset21).r;
  float depth22 = texture(u_depth_tex, v_texCoords + offset22).r;

  float r00 = cam_z(depth00); //same as cam_space_pos(v_texCoords + offset00, depth00).z;
  float r01 = cam_z(depth01);
  float r02 = cam_z(depth02);
  float r10 = cam_z(depth10);
  float r11 = cam_z(depth11);
  float r12 = cam_z(depth12);
  float r20 = cam_z(depth20);
  float r21 = cam_z(depth21);
  float r22 = cam_z(depth22);
  
  float dx = 1.0*r00 + 0.0*r01 - 1.0*r02 +
              2.0*r10 + 0.0*r11 - 2.0*r12 +
              1.0*r20 + 0.0*r21 - 1.0*r22;
              
  float dy = 1.0*r00 + 2.0*r01 + 1.0*r02 +
              0.0*r10 + 0.0*r11 + 0.0*r12 +
             -1.0*r20 - 2.0*r21 - 1.0*r22;
  
  float d = abs(dx) + abs(dy);


  float lim = 16.0 * u_prin_cont_lim;
  
  if(d > lim)//so I can use the same uniform for both contour types
  {
    float mar = 16.0*u_prin_cont_lim_margin;
    float t = (d-lim)/mar;
    t = min(t, 1.0); //clamp to 1
    float c = mix(u_base_col, u_prin_cont_col, t);
    color = vec4(vec3(c), 1.0);

    return true;
  }

  return false;
}

/*
Finds the maximum and the minimum dot(n,v) in a square neighborhood.
Records what is the proportion of neighboors with a higher and a lower
value than the current pixel.

Discards pixels with z in cam-space too far away from the z in cam-space
of the center pixel.
*/
void do_square_neighboorhood(in float dot_n_v, in float z,
  in vec2 tex_res, out float max_dot, out float min_dot,
  out float smaller_dot_prop, out float bigger_dot_prop)
{
  max_dot = 0.0;
  min_dot = 1.0;
  int smaller_dot_count = 0;
  int bigger_dot_count = 0;
  int count = 0;
  
  vec2 onePix =  vec2(1.0,1.0) / tex_res;

  for(int i = -u_neighboorhood_dim; i <= u_neighboorhood_dim; i++)
  {
    for(int j = -u_neighboorhood_dim; j <= u_neighboorhood_dim; j++)
    {
      if(i == 0 && j == 0)
        continue;
      
      vec2 uv = v_texCoords + onePix * vec2(i,j);

      vec3 norm_ij = texture(u_norm_tex, uv).rgb;
      if(dot(norm_ij,norm_ij) < 0.5)
        continue;

      norm_ij = normalize(norm_ij);

      float depth_ij = texture(u_depth_tex, uv).r;

      vec4 pos_ij = cam_space_pos(uv, depth_ij); //in cam space

      //dont consider as a neighboor pixels from other objects
      if(abs(pos_ij.z - z) > u_max_z_diff)
        continue;

      vec3 view_ij = -normalize(pos_ij.xyz); //cam_pos is 0,0,0

      float dot_n_v_ij = dot(norm_ij, view_ij);

      if(dot_n_v_ij < dot_n_v)
        smaller_dot_count++;
      else if(dot_n_v_ij > dot_n_v)
        bigger_dot_count++;
      
      count++;
      
      if(dot_n_v_ij > max_dot)
        max_dot = dot_n_v_ij;
      else if(dot_n_v_ij < min_dot)
        min_dot = dot_n_v_ij;
    }
  }

  smaller_dot_prop = float(smaller_dot_count) / float(count);
  bigger_dot_prop = float(bigger_dot_count) / float(count);
}

/*
finds the contour in relaxed local minima of dot(n,v). The current pixel
is considered a relaxed local minimum if the proportion of neighboors smaller
than him is below a certain limit.
If the difference between the local minimum and the local maximum is below a 
certain limit, I do not consider it a suggestive contour
*/
bool suggestive_contour(in float dot_n_v, in float max_dot, in float smaller_dot_prop, inout vec4 color)
{
  if(smaller_dot_prop > u_sugg_cont_smaller_dot_allowed_proportion)
    return false;
  else if(max_dot - dot_n_v < u_sugg_cont_min_dot_diff)
    return false;

  color = vec4(vec3(u_sugg_cont_col), 1.0);
  return true;
}

/*
finds principal highlight in places where dot(n,v) is almost 1 and where
the axis of minimum curvature is almost parallel to the view direction 
projected onto the tangent plane.
*/
bool principal_highlight(in float dot_n_v, in vec3 view,
  in vec3 normal, in vec3 pos, in vec2 tex_res, inout vec4 color)
{
  //project view into tangent plane
  vec3 w = view - dot_n_v * normal;

  //find out if w is a singularity (comparing lenght squared)
  if(dot(w,w) < 0.0001)
  {
    color = vec4(vec3(u_prin_high_col), 1.0);
    return true;
  }

  w = normalize(w);

  //find principal axis of curvature
  vec2 onePix =  vec2(1.0,1.0) / tex_res;
  float max_r = 0.0;
  float min_r = 2.0;
  vec3 e1 = vec3(0); //axis of max curvature
  vec3 e2 = vec3(0); //axis of min curvature

  for(int i = -u_neighboorhood_dim; i <= u_neighboorhood_dim; i++)
  {
    for(int j = 0; j <= u_neighboorhood_dim; j++)
    {
      if(i == 0 && j == 0)
        continue;
      
      vec2 uv_1 = v_texCoords + onePix * vec2(i,j);
      vec2 uv_2 = v_texCoords + onePix * vec2(-i,-j);

      //discard when at least one sample gets the clear color
      vec3 norm_ij_1 = texture(u_norm_tex, uv_1).rgb;
      vec3 norm_ij_2 = texture(u_norm_tex, uv_2).rgb;
      if(dot(norm_ij_1,norm_ij_1) < 0.5)
        continue;
      if(dot(norm_ij_2,norm_ij_2) < 0.5)
        continue;

      //in cam space
      float depth_ij_1 = texture(u_depth_tex, uv_1).r;
      vec3 pos_ij_1 = cam_space_pos(uv_1, depth_ij_1).xyz;
      float depth_ij_2 = texture(u_depth_tex, uv_2).r;
      vec3 pos_ij_2 = cam_space_pos(uv_2, depth_ij_2).xyz;

      //discard if sampled normal goes towards center
      vec3 sample_dir_1 = normalize(vec3(pos_ij_1.xy - pos.xy, 0.0));
      vec3 sample_dir_2 = -sample_dir_1;
      if(dot(norm_ij_1, sample_dir_1) < 0.0)
        continue;
      if(dot(norm_ij_2, sample_dir_2) < 0.0)
        continue;

      //remove the part that is not in the plane (normal, sample_dir)
      vec3 cross_1 = cross(sample_dir_1, normal);
      vec3 cross_2 = cross(sample_dir_2, normal);
      norm_ij_1 = norm_ij_1 - dot(norm_ij_1, cross_1)*cross_1;
      norm_ij_2 = norm_ij_2 - dot(norm_ij_2, cross_1)*cross_1;
      norm_ij_1 = normalize(norm_ij_1);
      norm_ij_2 = normalize(norm_ij_2);

      //conpute radius of curvature
      float r_1 = radius_of_curvature(pos, pos_ij_1, normal, norm_ij_1);
      float r_2 = radius_of_curvature(pos, pos_ij_2, normal, norm_ij_2);
      float r = 0.5*(r_1+r_2);

      //max curv is when min radius and viceversa
      if(r > max_r)
      {
        max_r = r;

        e2 = pos_ij_1.xyz - pos;
      }
      if(r < min_r)
      {
        min_r = r;

        e1 = pos_ij_1.xyz - pos;
      }
    }
  }

  //project e1 and e2 into the tangent plane (THE EFFECT DOESNT CHANGE WITHOUT THIS)
  e1 = e1 - dot(e1, normal)*normal;
  e2 = e2 - dot(e2, normal)*normal;

  //normalize the principal axis of curvature
  e1 = normalize(e1);
  e2 = normalize(e2);

  if(abs(dot(e1, w)) < u_prin_high_lim_2 && dot_n_v > u_prin_high_lim_1)
  {
    color = vec4(vec3(u_prin_high_col),1.0);
    return true;
  }

  return false;
}

/*
finds the contour in relaxed local maxima of dot(n,v). The current pixel
is considered a relaxed local maximum if the proportion of neighboors higher
than him is below a certain limit.
If the difference between the local minimum and the local maximum is below a 
certain limit, I do not consider it a suggestive highlight
*/
bool suggestive_highlight(in float dot_n_v, in float min_dot, in float bigger_dot_prop, inout vec4 color)
{  
  if(bigger_dot_prop > u_sugg_high_bigger_dot_allowed_proportion)
    return false;
  else if(dot_n_v - min_dot < u_sugg_high_min_dot_diff)
    return false;

  color = vec4(vec3(u_sugg_high_col), 1.0);
  return true;
}

void main()
{
  vec3 normal = texture(u_norm_tex, v_texCoords).rgb; //in cam space

  //so that the background is not black
  if(dot(normal,normal) < 0.01)
  {
    out_color = vec4(vec3(u_background_col), 1.0);
    return;
  }

  normal = normalize(normal);

  float depth = texture(u_depth_tex, v_texCoords).r;

  vec4 pos = cam_space_pos(v_texCoords, depth); //in cam space
  vec3 view = -normalize(pos.xyz); //cam_pos is 0,0,0

  float dot_n_v = max(dot(normal, view), 0.0); //clamp to 0 as with normal maps some may give < 0.0

  vec2 tex_res = textureSize(u_depth_tex, 0);


  //do the neighboorhood computation only once for the whole shader
  float max_dot = 0.0;
  float min_dot = 1.0;
  float smaller_dot_prop = 0;
  float bigger_dot_prop = 0;
  
  //conpute neighborhood max/min if needed
  do_square_neighboorhood(dot_n_v, pos.z, tex_res, max_dot, min_dot, smaller_dot_prop, bigger_dot_prop);

  //apply base color
  {
    vec4 dir4 = u_worldToCam * normalize(u_toon_dir);
    vec3 dir = normalize(dir4.xyz);
    float dot_n_dir = dot(normal, dir);
    if(dot_n_dir > 0.5)
      out_color = vec4(vec3(u_base_col), 1.0);
    else
      out_color = vec4(vec3(0.0), 1.0);
  }

  //apply each requested line type
  suggestive_contour(dot_n_v, max_dot, smaller_dot_prop, out_color);
  suggestive_highlight(dot_n_v, min_dot, bigger_dot_prop, out_color);
  principal_contour_sobel_cam_z(tex_res, out_color);
  principal_highlight(dot_n_v, view, normal, pos.xyz, tex_res, out_color);
}

