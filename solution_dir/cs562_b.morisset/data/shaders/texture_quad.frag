#version 130

in vec2 v_texCoords;

out vec4 out_color;

uniform sampler2D u_texture;

void main()
{
	out_color = texture(u_texture, v_texCoords);
  
  //out_color.a = 1.0f;
}

