/*

WARNING: both textures should have the same resolution

gaussian kernels from http://dev.theomader.com/gaussian-kernel-calculator/

*/

#version 460 //needed for arrays of arrays (or GL_ARB_arrays_of_arrays)

out vec4 out_color;

uniform sampler2D u_texture;
uniform sampler2D u_edge_texture;
uniform vec2 u_tex_resolution;

const int dim = 8;

////all Gaussian Kernels computed with sigma = 3
//const float gauss_krnls[dim][dim]
//{
//  {1.0,       0.0,      0.0,      0.0,      0.0,      0.0,      0.0,      0.0},
//  {0.345675,  0.327162, 0.0,      0.0,      0.0,      0.0,      0.0,      0.0},
//  {0.222338,  0.210431, 0.178400, 0.0,      0.0,      0.0,      0.0,      0.0},
//  {0.174938,  0.165569, 0.140367, 0.106595, 0.0,      0.0,      0.0,      0.0},
//  {0.152781,  0.144599, 0.122589, 0.093095, 0.063327, 0.0,      0.0,      0.0},
//  {0.141836,  0.134240, 0.113806, 0.086425, 0.058790, 0.035822, 0.0,      0.0},
//  {0.141836,  0.134240, 0.113806, 0.086425, 0.058790, 0.035822, 0.0,      0.0},
//  {0.136498,  0.129188, 0.109523, 0.083173, 0.056577, 0.034474, 0.018816, 0.0},
//  {0.134032,  0.126854, 0.107545, 0.08167,  0.055555, 0.033851, 0.018476, 0.009033},
//};

//here sigma start at 1 for second row, then is incremented by 0.5 for each row
const float gauss_krnls[dim][dim] = 
float[][](
  float[](1.0,       0.0,      0.0,      0.0,      0.0,      0.0,      0.0,      0.0     ),
  float[](0.441980,  0.279010, 0.0,      0.0,      0.0,      0.0,      0.0,      0.0     ),//sigma = 1
  float[](0.288713,  0.233062, 0.122581, 0.0,      0.0,      0.0,      0.0,      0.0     ),//sigma = 1.5
  float[](0.214607,  0.189879, 0.131514, 0.071303, 0.0,      0.0,      0.0,      0.0     ),//sigma = 2
  float[](0.170793,  0.157829, 0.124548, 0.083930, 0.048297, 0.0,      0.0,      0.0     ),//sigma = 2.5
  float[](0.141836,  0.134240, 0.113806, 0.086425, 0.058790, 0.035822, 0.0,      0.0     ),//sigma = 3
  float[](0.121272,  0.116454, 0.103119, 0.084199, 0.063396, 0.044016, 0.02818,  0.0     ),//sigma = 3.5
  float[](0.105915,  0.102673, 0.093531, 0.080066, 0.064408, 0.048689, 0.034587, 0.023089) //sigma = 4
);

void main()
{
  vec2 tex_coord = gl_FragCoord.xy / u_tex_resolution;
  vec2 onePix =  vec2(1.0,1.0) / u_tex_resolution;
  
  //read egde value
  float mask_val = texture(u_edge_texture, tex_coord).a;
  int ker_idx = int(floor(mask_val * (dim-1)));
  
  for (int i = -ker_idx; i <= ker_idx; i++)
  {
    vec2 offset = onePix * vec2(0.0,i);
    vec4 color_i = texture(u_texture, tex_coord + offset);
    
    out_color +=  gauss_krnls[ker_idx][abs(i)] * color_i;
  }
}

