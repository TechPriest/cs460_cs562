/*

  inverts the colors of the texture. Preserves alpha.

*/
#version 130

in vec2 v_texCoords;

out vec4 out_color;

uniform sampler2D u_texture;

void main()
{
  vec4 in_color = texture(u_texture, v_texCoords);
  
  out_color = vec4(vec3(1.0) - in_color.rgb, in_color.a);
}

