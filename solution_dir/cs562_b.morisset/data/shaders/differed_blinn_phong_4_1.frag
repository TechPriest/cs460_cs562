/*

Performs Blin-Phong lighting for a light given:
  - g_buffer textures
  - ambient occlusion texture
  - light variables
  - light position

Call this for a screen quad for each light

Bugs: Attenuation is wrong. It shouldn't depend on cam position.

Possible improvements:
  -could be improved by using vec2 uv = gl_FragCoord.xy / gl_Texture_Size(u_depth);
  -pass all uniforms into a UniformBufferObject
*/


#version 330

/*layout (location = 0)*/ out vec4 out_color;

in vec3 L; //lightsource position in cam space

//
uniform mat4 u_invCamToVP;
uniform int u_width;
uniform int u_height;

//g_buffer
uniform sampler2D u_norms;      
uniform sampler2D u_diffuse;      //last is specular greyscale
uniform sampler2D u_emissive;     //last is shininess
uniform sampler2D u_depth;

//ambient occlusion
uniform sampler2D u_AO;

//light
uniform vec3 u_Ia;
uniform vec3 u_Id;
uniform vec3 u_Is;
uniform vec3 u_attenuation;


//compute the position in cam space from the depth and the screen uvs
vec4 cam_space_pos(vec2 screen_uvs, float depth)
{
  float z = depth * 2.0 - 1.0;
  vec4 clipSpacePos = vec4(screen_uvs * 2.0 - 1.0, z, 1.0);
  vec4 camSpacePos = u_invCamToVP * clipSpacePos;
  camSpacePos /= camSpacePos.w;
  
  return camSpacePos;
}


void main()
{
  vec2 uv = vec2(gl_FragCoord.x / u_width, gl_FragCoord.y / u_height);
  vec3 n = texture( u_norms, uv).xyz; //in cam space
  vec4 kd_ks = texture( u_diffuse, uv);
  vec4 ke_ns = texture( u_emissive, uv);
  float depth = texture( u_depth, uv).r;

  float ao = texture( u_AO, uv).r;
  
  //reconstruct position
  vec3 pos = cam_space_pos(uv, depth).xyz;
  
  vec3 kd = kd_ks.rgb;
  vec3 ka = kd;
  vec3 ks = vec3(kd_ks.a);
  float ns = ke_ns.a * 4.0 * 255.0; //should correcpond to gbuffer
  
  //make the lighting computations
  vec3 l = normalize(pos - L);
  vec3 V = normalize(pos);
  vec3 H = (-l-V)*0.5;
  
  vec3 Iambient = ao * ka * u_Ia;
  vec3 Idiffuse = kd * u_Id * max( dot(n, -l), 0 );
  vec3 Ispecular = ks * u_Is * pow( max( dot(n, H), 0), ns);

  float d = length(pos - L);
  float att = min(1.0/(u_attenuation.x + u_attenuation.y *d + u_attenuation.z *d *d ), 1.0);
  out_color =  att * vec4(Iambient + Idiffuse + Ispecular, 1.0);
  
  //to avoid artefacts at influence sphere limit
  const float lim = 0.04; //must be same value as lim in point_light_node::update_influence_volume()
  
  out_color -= vec4(vec3(lim), 0.0); 
  
  //to partially recover the intensity. (light that was 1.0 rebecomes 1.0)
  //so the dimming just becomes another attenuation!
  const float corrector = 1.0 / (1.0 - lim);
  out_color *= corrector;
}

