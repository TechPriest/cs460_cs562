
#TODO

## ASK

  + why cant I use layout = 0?
  + why light lim visible? (happens when many lights)
  + bitwise operations in shaders
  + how to optimize final project

  
## ALLWAYS

  + comment all files/functions
  + fill readme.txt
  + hunt for leaks
  + hunt for race conditions
  + update splash screen
  + test all scenes
 
 
## COMPULSORY TASKS

  + replace all raw pointers with smart pointers
  + 
  + add no-Ambient Occlusion option
  + make sure no race condition with style presets
  + margin fade for all sugg (maybe not needed in prims)
  + LUT
  + compute Pi on CPU
  + fix non-opti lighting attenuation bug
  + fix paper AO
  + check uncoupled resolutions (not working on laptop)
  +  Save all useful files, shaders and shader functions in code snippets
	  +  g_buffer
	  +  action
	  +  render_actions
	  +  fps
	  +  chronometer
	  +  ...
	  +  bilinear filtering
	  +  sobel filter
	  +  AA
	  +  ambient occlusion
	  +  ...
	  +  cam pos from depth
	  +  z coord from depth
	  +  radius of curvature from points and normals
	  +  ...
  
## NICE TO HAVE

  + function set_depth, set_blend etc, that remember if already set to skip the push (will optimize and prevent bugs)
  + Roundiness variable for Phong Tesselation
  + 
  + editable: FOV, near, far
  + effects in terms of distance and not pixels (bloom, blur, AA, etc)
  + 
  + Fullscreen option
  + Resizable window with mouse
  + parenting
  + 
  + Skybox
  + gamma correction
  + shadows (at least from 1 directional light)
  + HDR
  + stencil light optimization
  + 
  + 
  + use geometry normals-edge detection as well for AA (only depth makes it so that the edge between 2 close object doesn't get blurred)
  + 
  + 
  + threaded load
  + all vertex data and uniforms aligned with 1, 2 or 4 bytes (no vec3s!) 
  + use Uniform Buffer Objects instead of plain uniforms. This allows to pass one UBO for man shaders. For example I could regroup the view and the projection matrices in a UBO and set it once for all shaders of the frame. UBOs in addition to that, Passing a UBO is cheaper than passing many uniforms. So Regrouping all the non_shared uniforms of a shader into a UBO is also nice. Also, if some uniforms never change on normal runtime, I can regroup them in a UBO and set it only once outside the game loop (then update it if the setting changes). (https://learnopengl.com/Advanced-OpenGL/Advanced-GLSL)
  
## DONE

  + support for many lights
    + point_light optimization (drawing influence spheres)
	  + also use depth from gbuffer and pass when depth is greater
  + performance info
  + render thread
  + differed shading
    + LDR position reconstructing g_buffer (14bytes/pixel depth)
    + final project g_buffer of 6 bytes/pixel (normals) and depth
  + nice free-cam
  + uniform caching
  + blinn-phong light model
  + anti-aliasing: post processing effect using Sobel edge detection and Gaussian blur
  + Sobel edge detection
  + Gaussian blur
  + Bloom (lerped)
  + hot reload for shaders
  + Phong tesselation
  + multiple scenes
  + decals
  + SSAA: just make the g_buffer resolution bigger than the screen's
  + Screen Space Horizon Based Ambient Occlusion
  + Screen Space Lines For Conveying Shape (final project)


## WARNINGS

  + influence volume light optimization is intended to work with lights that have a finite, small radius. So having null attenuation coefficients will break the effect.

  

